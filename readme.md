# Laravel HYIP  Manager Script

#### Version 1.1

## Features

To be documented

## How to Install

1. Pull the Repo from the GitLab
2. Run "composer update"
3. Run "npm install"
4. Duplicate .env.example file as .env
5. Add your mysql db details there
6. Run Migration as -- "php artisan migrate"
7. Populate Data as -- "php artisan db:seed"
8. Setup the Matrix -- "php artisan larahyip:setmatrix"
9. Setup the Root User as -- "php artisan larahyip:rootuser"

## URL
* http://localhost/ -- Public URL
* http://locahost/myaccount/home -- MyAccount URL
* http://localhost/admin/dashboard -- Admin URL
* http://localhost/superadmin/dashboard -- Superadmin URL

Firewall from https://github.com/antonioribeiro/firewall
* access whitelisting, blacklisting at /firewall (it is even need to be protected.)
* as of now masteradmin pages need whitelisting!

## License Commercial - http://phpHyipManagarScript.com



