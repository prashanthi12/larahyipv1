<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;


class MemberLoginTestAsAdmin extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testMemberLoginAsAdmin()
    {
        $this->browse(function ($browser) {
            $browser->visit('/login')
                    ->assertSee('Login')
                    ->value('#email', 'admin@larahyip.com')
                    ->value('#password', 'admin')
                    ->press('Login')
                    ->assertPathIs('admin/dashboard');
        });
    }
}
