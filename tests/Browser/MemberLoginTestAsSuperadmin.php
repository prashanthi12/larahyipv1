<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;


class MemberLoginTestAsSuperadmin extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testMemberLoginAsSuperadmin()
    {
        $this->browse(function ($browser) {
            $browser->visit('/login')
                    ->assertSee('Login')
                    ->value('#email', 'superadmin@larahyip.com')
                    ->value('#password', 'superadmin')
                    ->press('Login')
                    ->assertSee('superadmin/dashboard');
        });
    }
}
