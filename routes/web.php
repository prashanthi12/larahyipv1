<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'fw-block-bl'], function () {
  
    Route::get('/', 'WelcomeController@index');   

    Route::get('/investplans', function () {
        return App\Plan::where('active', 1)->with('plantype')->orderBy('orderby')->get();
    });

    Route::get('/translations', function () {

        $array = array(
                'calculator' => trans('welcome.calculator'), 
                'select_plan' => trans('welcome.select_plan'),
                'min_desposit' => trans('welcome.min_deposit'), 
                'max_deposit' => trans('welcome.max_deposit'),
                'interest_rate' => trans('welcome.interest_rate'),
                'investment_amount' => trans('welcome.investment_amount'),
                'plan_type' => trans('welcome.plan_type'),
                'investment_duration' => trans('welcome.investment_duration'),
                'total_interest' => trans('welcome.total_interest'),
                'total_return' => trans('welcome.total_return'),
                'return_on_investment' => trans('welcome.return_on_investment'),
                'currency' => \Config::get('settings.currency'),
                'calculator_slider_value' => \Config::get('settings.calculator_slider_value'),
                );
        
        return json_encode($array);
    });

    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/superadmin/logout', 'Auth\LoginController@logout')->name('logout');
    Route::post('/superadmin/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/admin/logout', 'Auth\LoginController@logout')->name('logout');
    Route::post('/admin/logout', 'Auth\LoginController@logout')->name('logout');

    Route::group([ 'middleware' => 'guest' ], function () {

        Route::get('/ref/{referrer}', 'WelcomeController@refferal');
        Route::get('/login', 'Auth\LoginController@showLoginForm' )->name('login');
        Route::post('/login', 'Auth\LoginController@login' );
        Route::get('/password/reset','Auth\ForgotPasswordController@showLinkRequestForm');
        Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
        Route::post('/password/email','Auth\ForgotPasswordController@sendResetLinkEmail');
        Route::get('/register','Auth\RegisterController@showRegistrationForm');
        Route::post('/register','Auth\RegisterController@register');      
        
        //for admin
        Route::get('/admin', function () {
                return redirect('/admin/login');
        });
        Route::get('/admin/login', 'Admin\LoginController@showAdminLoginForm' );
        Route::post('admin/login', 'Admin\LoginController@login' );

        //for staff
        Route::get('/staff', function () {
                return redirect('/staff/login');
        });
        Route::get('/staff/login', 'Staff\LoginController@showStaffLoginForm' );
        Route::post('staff/login', 'Staff\LoginController@login' );

        //for superadmin
        Route::get('/superadmin', function () {
                return redirect('/superadmin/login');
        });
        Route::group(['middleware' => 'fw-block-bl'], function () {
            Route::get('/superadmin/login', 'Superadmin\LoginController@showSuperAdminLoginForm' );
            Route::post('superadmin/login', 'Superadmin\LoginController@doLogin' );
        });

    });

    // For v1.1
    Route::get('/redirect', 'SocialAuthTwitterController@redirect');
    Route::get('/callback', 'SocialAuthTwitterController@callback');
    Route::get('/fbredirect', 'SocialAuthFacebookController@redirect');
    Route::get('/fbcallback', 'SocialAuthFacebookController@callback');
    Route::get('/goredirect', 'SocialAuthGoogleController@redirect');
    Route::get('/gocallback', 'SocialAuthGoogleController@callback');
    Route::get('/socialemail/{id}', 'SocialAuthTwitterController@socialEmail');
    Route::post('/setemail', 'SocialAuthTwitterController@setEmail');

    Route::get('/users/{id}/impersonate', 'UserController@impersonate');
    Route::get('/users/stop', 'UserController@stopImpersonate');

    Route::get('/about', 'PageController@about');
    Route::get('/invest', 'PageController@invest');
    Route::get('/invest/plan/{id}', 'PageController@viewplan');
    Route::get('/earn', 'PageController@earn');
    Route::get('/exchange', 'PageController@exchange');
    Route::get('/news', 'PageController@news');
    Route::get('/reviews', 'PageController@reviews');
    Route::get('/faq','PageController@faq');
    Route::get('/contact', 'PageController@contact');
    Route::get('/privacy', 'PageController@privacy');
    Route::get('/terms', 'PageController@terms');
    Route::get('/security', 'PageController@security');
    Route::get('/contact', 'ContactController@create');
    Route::post('/contact', 'ContactController@store');
    Route::get('/page/{lang}/{slug}', 'PageController@show');
    Route::get('/quotes', 'PageController@quotes');
    Route::get('/stats', 'PageController@statistics');

    Route::get('setlocale/{locale}', 'LanguageController@index');

    Route::get('/deposit/coinpaymentsuccess', 'DepositController@coinpaymentsuccess');

    // Investors, Referrals and Payouts
    Route::get('/lastinvestors', 'PageController@lastinvestors');
    Route::get('/topinvestors', 'PageController@topinvestors');
    Route::get('/topreferrals', 'PageController@topreferrals');
    Route::get('/payouts', 'PageController@payouts');

    Route::get('/emailverification/{token}', 'Auth\EmailverificationController@emailverification');

    // For google analytics
    Route::get('/analytics', 'AnalyticsController@index');
    
    Route::group([ 'middleware' => ['auth'], 'prefix'=>'myaccount', 'namespace' =>'Myaccount' ], function () {
       

        Route::get('/home', 'HomeController@index');
        Route::get('/reportuser/{withdrawid}/{manualdepid}', 'HomeController@reportuser');
        // Add 2FA validation when edit the profile
        Route::get('/profile/validate2fa', 'UserprofileController@index')->name('profile');
        Route::post('/profile/validate2fa', 'UserprofileController@validate2fa')->name('profile');
        
        Route::get('/profile', 'UserprofileController@create')->name('profile');
        Route::get('/viewprofile', 'UserprofileController@view')->name('profile');
        Route::post('/profile', 'UserprofileController@update')->name('profile');
        Route::get('/change_password', 'UserprofileController@change_password')->name('profile');
        Route::post('/update_change_password', 'UserprofileController@update_change_password')->name('profile');
        Route::get('/transaction_password', 'UserprofileController@transaction_password')->name('profile');
        Route::post('/update_transaction_password', 'UserprofileController@update_transaction_password')->name('profile');

        Route::get('/rest_transaction_password', 'UserprofileController@change_transaction_password')->name('profile');

        Route::get('/changeavatar', 'UserprofileController@changeavatar')->name('profile');
        Route::post('/saveavatar', 'UserprofileController@saveavatar')->name('profile');

        Route::post('/getmobilecode', 'UserprofileController@getmobilecode')->name('profile');

        Route::get('/viewdeposits/{status}', 'DepositController@index')->name('mydeposits');
        Route::get('/viewpayaccounts', 'UserpayaccountsController@index')->name('mypayaccounts');
        Route::post('/payaccounts', 'UserpayaccountsController@store');
        Route::get('/activeaccounts/{id}/{paymentid}/{status}', 'UserpayaccountsController@activeaccounts');
        Route::get('/currentaccounts/{id}/{paymentid}/{status}', 'UserpayaccountsController@currentaccounts');
        Route::post('/removeaccount/{id}', 'UserpayaccountsController@removeaccount');

        Route::get('/deposit', 'DepositController@create')->name('deposit');
        Route::post('/deposit', 'DepositController@store')->name('deposit');
        Route::post('/deposit/bitcoin', 'DepositController@bitcoin');
        Route::post('/deposit/saveblockio', 'DepositController@saveblockio');
        Route::post('/deposit/advcash', 'DepositController@advcash');
        Route::post('/deposit/advcash_success', 'DepositController@advcash_success');
        Route::post('/deposit/process_bankwire', 'DepositController@process_bankwire');
        Route::post('/deposit/processpaypal', 'DepositController@processpaypal');
        Route::get('/deposit/paypalstatus', 'DepositController@getPaypalStatus');
        // Process and Success Paypal
        Route::post('/deposit/successpaypal', 'DepositController@store_paypal');
        // For Payeer
        Route::post('/deposit/payeer', 'DepositController@store_payeer');
        Route::get('/deposit/skrillprocess', 'DepositController@skrillprocess');
        Route::get('/deposit/perfectmoneyprocess', 'DepositController@perfectmoneyprocess');
        Route::get('/deposit/stpprocess', 'DepositController@stpprocess');
        Route::get('/deposit/okpayprocess', 'DepositController@okpayprocess');
        Route::get('/deposit/paymentcancelled', 'DepositController@paymentcancelled');
        Route::get('/deposit/bitpayprocess', 'DepositController@bitpayprocess');
        Route::get('/deposit/bitpaysuccess', 'DepositController@bitpaysuccess');
        Route::post('/deposit/bankwire', 'DepositController@bankwire');
        Route::get('/deposit/bitcoindirect', 'DepositController@bitcoindirect');
        Route::post('/deposit/bitcoindirect', 'DepositController@savebitcoindirect');
        Route::post('/deposit/reinvest', 'DepositController@reinvest');
        Route::post('/deposit/payment', 'DepositController@payment');
        Route::post('/deposit/view_plan_amount', 'DepositController@view_plan_amount'); 
        Route::get('/deposit/partialwithdraw/{depositid}', 'DepositController@partialwithdraw');   
        Route::post('/deposit/processpartialwithdraw', 'DepositController@processpartialwithdraw');  
        Route::get('/deposit/printinvoice', 'DepositController@printinvoice');   
        Route::get('/viewdeposits/printinvoice/{depositid}', 'DepositController@viewdepositsprintinvoice');   
        Route::post('/deposit/processneteller', 'DepositController@processneteller'); 

        // For v1.1 payment by cheque, ewallet
        Route::post('/deposit/cheque', 'DepositController@cheque');  
        Route::post('/deposit/ewallet', 'DepositController@ewallet');    

        Route::get('/invite_friend', 'InviteFriendController@invite_friend');
        Route::post('/invitefriend/send_request', 'InviteFriendController@send_request');
        Route::get('/withdraw/{status}', 'WithdrawController@index');
        Route::get('/withdraw', 'WithdrawController@create');
        Route::post('/withdraw', 'WithdrawController@store');
        Route::post('/withdraw/userpayaccount', 'WithdrawController@userpayaccount');
        Route::get('/withdraw/viewbitcoinwallet/{id}', 'WithdrawController@viewbitcoinwallet');
        Route::get('/withdraw/otp/check', 'WithdrawController@otpcheck');
        Route::post('/withdraw/otpsendagain', 'WithdrawController@otpsendagain');
        Route::post('/withdraw/otp/check', 'WithdrawController@updatewithdrawdetails');
        Route::get('/autowithdrawal', 'WithdrawController@autowithdrawal');
        Route::post('/autowithdrawal', 'WithdrawController@saveautowithdrawal');

        Route::get('/ticket', 'TicketsController@index');
        Route::get('/ticket/create', 'TicketsController@create');
        Route::post('/ticket/store', 'TicketsController@store');
        Route::get('/ticket/{id}', 'TicketsController@show');
        Route::post('/ticket/storecomment', 'TicketsController@storecomment');
        Route::get('/ticket/update/{statusid}/{id}', 'TicketsController@update');
        Route::get('/ticket/download/{id}/{ticketid}', 'TicketsController@download');

        Route::get('/referrals', 'ReferralsController@referrals');
        Route::get('/earnings/{status}', 'EarningsController@earnings');
        Route::get('/network', 'NetworkController@network');

        Route::get('/testimonial', 'TestimonialController@create');
        Route::post('/testimonial', 'TestimonialController@store');

        Route::post('/notification', 'NotificationController@markasread');

        Route::get('/mobileverification/{code}', 'UserprofileController@mobileverification');
        Route::post('/verificationprocess', 'UserprofileController@verificationprocess');

        Route::get('/fundtransfer/type/{type}', 'FundTransferController@index');
        Route::get('/fundtransfer/send', 'FundTransferController@create');
        Route::post('/fundtransfer/store', 'FundTransferController@store');
        Route::get('/fundtransfer/searchuser', 'FundTransferController@searchuser');
        Route::get('/penalties', 'EarningsController@penalties');
        Route::get('/bonus', 'EarningsController@bonus');

        Route::get('/message/send', 'MessageController@create');
        Route::post('/message/save', 'MessageController@store');
        Route::get('/message/list', 'MessageController@index');
        Route::get('/message/conversation/{conversationid}', 'MessageController@conversation');
        Route::post('/message/conversation/save/{conversationid}', 'MessageController@conversationsave');

        Route::get('/myinbox', 'MailMessageController@index');

        // For v1.1 fund transfer two factor authentication
        Route::get('/twofactor', 'Google2FAController@twofactor');
        Route::get('/2fa/enable', 'Google2FAController@enableTwoFactor');
        Route::get('/2fa/disable', 'Google2FAController@disableTwoFactor');
        Route::get('/2fa/validate', 'Google2FAController@getValidateToken');
        Route::post('/2fa/validate', 'Google2FAController@postValidateToken');

        // For payment by e-pin
        Route::get('/myepin', 'EpinController@index');
        Route::get('/myepin/create', 'EpinController@create');
        Route::post('/myepin/create', 'EpinController@store');
        Route::get('/epin/couponcode/check', 'DepositController@couponcodeform');
        Route::post('/epin/couponcode/check', 'DepositController@couponcodecheck');

        // For E-Wallet
        Route::get('/ewallet', 'EwalletController@create');
        Route::post('/ewallet/', 'EwalletController@store');
        Route::get('/ewallet/addfund', 'EwalletController@addFundCreate');
        Route::post('/ewallet/addfund', 'EwalletController@addFundStore');
        Route::get('/ewallet/{status}', 'EwalletController@show');
        // Route::get('/ewallet/transfer', 'EwalletController@transferform');
        // Route::post('/ewallet/transfer', 'EwalletController@transfer');

        // For E-Wallet BTC send receive
        Route::get('/type/btc/send', 'CoinController@send_create');
        Route::get('/type/btc/receive', 'CoinController@receive_create');
        Route::post('/type/btc/send', 'CoinController@send_store');
        // For create btc address
        Route::post('/createwallet/btc/', 'CoinController@create_btcwallet');

        // For E-Wallet payment gateways
        Route::post('/ewallet/bankwire', 'EwalletController@bankwire');
        Route::get('/ewallet/paymentcancelled', 'EwalletController@paymentcancelled');
        Route::post('/ewallet/successpaypal', 'EwalletController@store_paypal');

        // For Manual Deposit Proof Upload
        Route::get('/deposit/manual', 'DepositController@manual');
        Route::get('/viewdeposits/uploadproof/{depositid}', 'DepositController@uploadproof');
        Route::post('/viewdeposits/uploadproof/{depositid}', 'DepositController@storeuploadedproof');
        Route::get('/viewbankdetails/{id}', 'DepositController@viewbankdetails');
        // For user confirm deposit
        Route::get('/deposit/confirm/{id}/{withdrawid}', 'WithdrawController@approve');
        Route::get('/deposit/approvebyuser', 'WithdrawController@approveByUser');
        Route::get('/resend_email_verification', 'UserprofileController@resendverification');
    }); 
       
    Route::group(['prefix' => 'admin', 'middleware' => ['auth','admin2'], 'namespace' =>'Admin' ], function() {
        /*Route::get('/calculate-interest', function () {
            // dd('dfgdgkj');
             Artisan::call('larahyip:daily_interest');
        });

        Route::get('/mature-deposit', function () {
            // dd('dfgdgkj');
             Artisan::call('larahyip:mature_deposit_action');
        });*/

        Route::get('/dashboard', 'DashboardController@index')->name('home');
        Route::get('/users', 'UserController@index');
        Route::post('/users', 'UserController@index');
        Route::get('/users/{name}', 'UserController@show');
        Route::post('/users/attachdocdownload/{id}', 'UserController@attachdocdownload');
        Route::get('/users/create/new', 'UserController@create');
        Route::post('/users/create/new', 'UserController@store');
        Route::get('/users/verifykyc/{id}', 'UserController@verifykyc');
        Route::get('/users/rejectkyc/{id}', 'UserController@rejectkyc');
        Route::get('/users/resetpassword/{id}', 'UserController@resetpassword');
        //Route::get('/users/updatereferralgroup/{id}/{referralgroupid}', 'UserController@updatereferralgroup');
        Route::get('/users/updatereferralgroup/{id}', 'UserController@referralgroup');
        Route::post('/users/updatereferralgroup/{id}', 'UserController@updatereferralgroup');
        Route::post('/users/update/{id}', 'UserController@update');
        Route::get('/deposit/{status}', 'DepositController@index');
        Route::get('/deposit/confirm/{id}', 'DepositController@confirm');
        Route::get('/deposit/approve/{id}', 'DepositController@onlineapprove');
        Route::post('/deposit/approve/{id}', 'DepositController@approve');
        Route::post('/deposit/fundaddeduser/{id}', 'DepositController@fundaddeduser');        
        Route::get('/deposit/reject/{id}', 'DepositController@rejectform');
        Route::post('/deposit/reject/{id}', 'DepositController@reject');
        Route::get('/deposit/viewbitcoinwallet/{transactionid}', 'DepositController@viewbitcoinwallet');
        Route::get('/actions/viewbitcoinwallet/{transactionid}', 'DepositController@viewbitcoinwallet');
        Route::get('/viewbitcoinwallet/{transactionid}', 'DepositController@viewbitcoinwallet');
        Route::get('/users/viewbitcoinwallet/{transactionid}', 'DepositController@viewbitcoinwallet');
        Route::get('/ticket', 'TicketsController@index');
        Route::get('/ticket/{id}', 'TicketsController@show');
        Route::post('/ticket/storecomment', 'TicketsController@storecomment');
        Route::get('/ticket/update/{statusid}/{id}', 'TicketsController@update');
        Route::get('/ticket/download/{id}', 'TicketsController@download');
        Route::get('/contactus', 'ContactusController@index');
        Route::get('/contactus/moreinfo/{id}', 'ContactusController@moreinfo');
        Route::get('/analytics', 'AnalyticsController@index');
        Route::get('/log', 'LogController@index');
        Route::get('/testimonials', 'TestimonialController@index');
        Route::post('/testimonials/{id}', 'TestimonialController@update');
        Route::get('/testimonials/moreinfo/{id}', 'TestimonialController@moreinfo');
        Route::get('/withdraw/{status}', 'WithdrawController@index');
        Route::get('/withdraw/complete/{id}', 'WithdrawController@complete');
        Route::post('/withdraw/complete/{id}', 'WithdrawController@updatecomplete');
        Route::get('/withdraw/reject/{id}', 'WithdrawController@reject');
        Route::post('/withdraw/reject/{id}', 'WithdrawController@updatereject');
        Route::get('/earnings', 'EarningsController@index');
        Route::get('/actions/{name}', 'ActionController@index');
        Route::post('/releasedeposit', 'DepositController@releasedeposit');
        Route::get('/releasedeposit/{id}', 'DepositController@releasedepositwithid');
        Route::get('/fundtransfer', 'FundtransferController@index');
        Route::get('/staffs', 'UserController@stafflist');
        Route::get('/changepassword', 'ProfileController@changepassword');
        Route::post('/updatechangepassword', 'ProfileController@update_change_password');

        // Route::get('newsletter', 'NewsletterController@index');

        Route::get('penalty/list', 'PenaltyController@index');
        Route::get('penalty', 'PenaltyController@create');
        Route::post('penalty/send', 'PenaltyController@store');
        Route::get('sendpenalty/{userid}', 'PenaltyController@redirectpenalty');

        Route::get('bonus/list', 'BonusController@index');
        Route::get('bonus', 'BonusController@create');
        Route::post('bonus/send', 'BonusController@store');
        Route::get('sendbonus/{userid}', 'BonusController@redirectbonus');

        // For Reports
        Route::get('/reports/fundtransfers', 'ReportsController@FundsTransfer');
        Route::get('/reports/fundtransfers/export', 'ReportsController@FundsTransferExport');
        Route::get('/reports/withdraws', 'ReportsController@Withdraw');
        Route::get('/reports/withdraws/export/{status}', 'ReportsController@WithdrawExport');
        Route::get('/reports/deposits', 'ReportsController@DepositStatus');
        Route::get('/reports/deposits/export/{status}', 'ReportsController@DepositStatusExport');
        Route::get('/reports/deposits/plans', 'ReportsController@DepositPlan');
        Route::get('/reports/deposits/plan/export/{id}', 'ReportsController@DepositPlanExport');
        Route::get('/reports/deposits/payments', 'ReportsController@DepositPayment');
        Route::get('/reports/deposits/payments/export/{id}', 'ReportsController@DepositPaymentExport');
        Route::get('/reports/earnings', 'ReportsController@Earnings');
        Route::get('/reports/earnings/export/{status}', 'ReportsController@EarningsExport');
        Route::get('/reports/investors', 'ReportsController@Investors');
        Route::get('/reports/investors/export/{id}', 'ReportsController@InvestorsExport');
        Route::get('/reports/sponsors', 'ReportsController@Sponsors');
        Route::get('/reports/sponsors/export/{id}', 'ReportsController@SponsorsExport');
        // Settings by Value
        Route::get('/settings', 'SettingsController@index');

        Route::get('/message/send', 'MessageController@create');
        Route::post('/message/save', 'MessageController@store');
        Route::get('/message/list', 'MessageController@index');
        Route::get('/message/conversation/{conversationid}', 'MessageController@conversation');
        Route::post('/message/conversation/save/{conversationid}', 'MessageController@conversationsave');
        Route::get('/message/senduser/{id}', 'MessageController@show');
        Route::post('/quickmessage', 'MessageController@quickmessage');
        Route::post('/message/users/sendmail/{id}', 'MailMessageController@SendMail');

        Route::get('/loggedinusers', 'LogController@LoggedInUsers');

        // For v1.1
        Route::get('/importcontacts', 'ImportController@getImport');
        Route::post('/import_parse', 'ImportController@parseImport');
        Route::post('/import_process', 'ImportController@processImport');

        //Ewallet
        Route::get('/ewallet/fund/show','EwalletController@show');
        Route::get('/ewallet/fund/cancelled/{id}','EwalletController@cancelform');
        Route::post('/ewallet/fund/cancelled/{id}','EwalletController@cancelled');
        Route::get('/ewallet/fund/approved/{id}','EwalletController@approveform');
        Route::post('/ewallet/fund/approved/{id}','EwalletController@approved');

        Route::get('/searchuser', 'UserController@searchuser');

        // For Sending Massmail and sendmail list
        Route::get('/sendmail', 'SendMailController@index');
        Route::get('/massmail', 'SendMailController@create_massmail');
        Route::post('/massmail','SendMailController@store_massmail');
        Route::get('/viewmessage/{id}', 'SendMailController@viewmessage');
        Route::get('massmail/{batchid}', 'SendMailController@viewusers');

        // For Manual Paymentgateway
        Route::post('/getmobilecode', 'DepositController@getmobilecode');
        Route::get('/deposit/{id}/addbankdetails/{userid}', 'DepositController@addbankdetails');
        Route::post('/deposit/{id}/addbankdetails/{userid}', 'DepositController@storebankdetails');
        Route::get('/viewbankdetails/{id}', 'DepositController@viewbankdetails');
        Route::get('/withdraw/userpayaccount/{id}', 'DepositController@userpayaccount');
        Route::get('/reportuser', 'DepositController@reportuser');

    });

    Route::group(['prefix' => 'staff', 'middleware' => ['auth', 'staff'], 'namespace' =>'Staff' ], function() {
        Route::get('/dashboard', 'DashboardController@index')->name('home');
        Route::get('/ticket', 'TicketsController@index');
        Route::get('/ticket/{id}', 'TicketsController@show');
        Route::post('/ticket/storecomment', 'TicketsController@storecomment');
        Route::get('/ticket/update/{statusid}/{id}', 'TicketsController@update');
        Route::get('/ticket/download/{id}/{ticketid}', 'TicketsController@download');
        Route::get('/changepassword', 'ProfileController@changepassword');
        Route::post('/updatechangepassword', 'ProfileController@update_change_password');

      });

    Route::group(['prefix' => 'superadmin', 'middleware' => ['auth', 'admin1', 'fw-block-bl']], function() {
        // Backpack\CRUD: Define the resources for the entities you want to CRUD.
        Route::get('/deleteuser/{id}', 'Superadmin\UserCrudController@deleteUser');
        Route::get('/dashboard', 'Superadmin\DashboardController@index');
        CRUD::resource('plan', 'Superadmin\PlanCrudController');
        CRUD::resource('accountingcode', 'Superadmin\AccountingcodeCrudController');
        // Modifed CRUD paymentgateway to paymentgateway controller
        Route::get('paymentgateway/{status}', 'Superadmin\PaymentgatewayController@index');
        Route::get('/paymentgateway/{status}/edit/{id}', 'Superadmin\PaymentgatewayController@edit');
        Route::post('/paymentgateway/edit/{id}', 'Superadmin\PaymentgatewayController@update');
        // Below route is for edit bank transfer params modal window form 
        // Route::post('/offline/update/banktransfer', 'Superadmin\PaymentgatewayController@banktransferModal');
        // CRUD::resource('paymentgateway/online', 'Superadmin\PaymentgatewayCrudController');
        // CRUD::resource('paymentgateway/offline', 'Superadmin\PaymentgatewayCrudController@offline');
        // CRUD::resource('/paymentgateway/offline', 'Superadmin\PaymentgatewayCrudController');

        CRUD::resource('firewall', 'Superadmin\FirewallCrudController');
        CRUD::resource('news', 'Superadmin\NewsCrudController');
        CRUD::resource('banners', 'Superadmin\BannerCrudController');
        CRUD::resource('exchanges', 'Superadmin\ExchangeCrudController');
        CRUD::resource('faqs', 'Superadmin\FaqCrudController');
        CRUD::resource('ticketstatus', 'Superadmin\Ticket_statusCrudController');
        CRUD::resource('ticketpriority', 'Superadmin\Ticket_prioritiesCrudController');
        CRUD::resource('ticketcategory', 'Superadmin\Ticket_categoriesCrudController');
        CRUD::resource('ticketcategoryuser', 'Superadmin\Ticket_categories_usersCrudController');
        CRUD::resource('sliders', 'Superadmin\SliderCrudController');
        CRUD::resource('pages', 'Superadmin\PageCrudController');
        CRUD::resource('testimonials', 'Superadmin\TestimonialCrudController');
        CRUD::resource('quote', 'Superadmin\QuoteCrudController');

        CRUD::resource('members', 'Superadmin\UserCrudController');

        CRUD::resource('bonus', 'Superadmin\BonusCrudController');
        CRUD::resource('registrationbonus', 'Superadmin\RegistrationbonusCrudController');
        CRUD::resource('referralgroups', 'Superadmin\ReferralgroupCrudController');
        CRUD::resource('referralcommissions', 'Superadmin\ReferralcommissionCrudController');
        CRUD::resource('product', 'Superadmin\ProductCrudController');

        Route::get('helpdocument', 'Superadmin\HelpDocController@index');
        // [...] other routes
    });

});

Route::group(['prefix' => 'install', 'as' => 'LaravelInstaller::',  'namespace' => 'Install', 'middleware' => ['web','install']], function()
{
        Route::get('/', [
            'as' => 'welcome',
            'uses' => 'WelcomeController@welcome'
        ]);
        Route::get('environment', [
            'as' => 'environment',
            'uses' => 'EnvironmentController@environment'
        ]);
        Route::post('environment/save', [
            'as' => 'environmentSave',
            'uses' => 'EnvironmentController@save'
        ]);
        Route::get('requirements', [
            'as' => 'requirements',
            'uses' => 'RequirementsController@requirements'
        ]);
        Route::get('permissions', [
            'as' => 'permissions',
            'uses' => 'PermissionsController@permissions'
        ]);
        Route::get('database', [
            'as' => 'database',
            'uses' => 'DatabaseController@database'
        ]);
        Route::get('final', [
            'as' => 'final',
            'uses' => 'FinalController@finish'
        ]);
});
Route::group(['prefix' => 'update', 'as' => 'LaravelUpdater::',  'namespace' => 'Install', 'middleware' => 'web'], function()
{
    // Route::group(['middleware' => 'canUpdate'], function()
    // {
        Route::get('/', [
            'as' => 'welcome',
            'uses' => 'UpdateController@welcome'
        ]);
        Route::get('overview', [
            'as' => 'overview',
            'uses' => 'UpdateController@overview'
        ]);
        Route::get('database', [
            'as' => 'database',
            'uses' => 'UpdateController@database'
        ]);
    //});
    // This needs to be out of the middleware because right after the migration has been
    // run, the middleware sends a 404.
    Route::get('final', [
        'as' => 'final',
        'uses' => 'UpdateController@finish'
    ]);
});

require base_path('routes/firewall.php');