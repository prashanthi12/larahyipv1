<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use App\Events\TransactionPasswordSaved;
// use App\Events\TransactionPasswordUpdated;
// use Illuminate\Notifications\Notifiable;
// use Illuminate\Foundation\Auth\User as Authenticatable;

class Userprofileold extends Model
{
   // use Notifiable;

    protected $fillable = [
        'user_id', 'usergroup_id', 'sponsor_id', 'active', 'transaction_password', 'firstname', 'lastname', 'mobile', 'country', 'ssn', 'kyc_doc', 'kyc_verified','email_verified', 'email_verification_code','mobile_verified', 'mobile_verification_code','address1','address2','state','city', 'profile_avatar', 'referral_group_id'
    ];

    protected $dates = ['deleted_at'];

    // protected $events = [
    //     'saved' => TransactionPasswordSaved::class,
    //   //  'updated' => TransactionPasswordUpdated::class,
    // ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function useraccount() {
        return $this->hasOne('App\Useraccount', 'entity_reference_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country', 'id');
    }

}
