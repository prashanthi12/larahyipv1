<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{

    protected $fillable = [
        'authorimage', 'text', 'authorname', 'language', 'active'
    ];

    public function getTextAttribute($text)
    {
    	return \Purify::clean($text);
    }
}
