<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\ContactusAdded;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contactus;
use Config;

class NewContactusAdded
{
    protected $contact;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ContactusAdded $event)
    {
       // dd($event);
        Mail::to(Config::get('settings.adminemail'))->queue(new Contactus($event));   
    }
}
