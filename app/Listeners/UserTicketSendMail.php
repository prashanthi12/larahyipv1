<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserTicketSend;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserSendTicket;

class UserTicketSendMail
{
    protected $ticket, $admin;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserTicketSend $event)
    {
       // dd($event->user->email);
        Mail::to($event->ticket->user->email)->queue(new UserSendTicket($event)); 
    }

}
