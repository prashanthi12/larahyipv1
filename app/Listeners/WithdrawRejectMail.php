<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminWithdrawReject;
use Illuminate\Support\Facades\Mail;
use App\Mail\WithdrawReject;

class WithdrawRejectMail
{
    protected $result, $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AdminWithdrawReject $event)
    {
        //dd($event);
        Mail::to($event->user->email)->queue(new WithdrawReject($event)); 
    }
}
