<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminUpdateTicketStatus;
use Illuminate\Support\Facades\Mail;
use App\Mail\StaffNotifyTicketStatus;

class StaffNotifyTicketStatusMail
{
    protected $statusresult, $staff, $admin;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AdminUpdateTicketStatus $event)
    {
        Mail::to($event->statusresult->staff->email)->queue(new StaffNotifyTicketStatus($event));
    }
}
