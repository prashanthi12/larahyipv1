<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\InviteNewFriend;
use Illuminate\Support\Facades\Mail;
use App\Mail\InviteFriend;

class InviteNewFriendRequest
{
    protected $invite;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(InviteNewFriend $event)
    {
       // dd($event->invite->email);
        Mail::to($event->invite->email)->queue(new InviteFriend($event));
    }
}
