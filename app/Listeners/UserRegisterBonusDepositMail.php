<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserRegisterBonusDeposit;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterBonusDeposit;

class UserRegisterBonusDepositMail
{
    protected $deposit, $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserRegisterBonusDeposit $event)
    {
      //  dd($event);
        Mail::to($event->user->email)->queue(new RegisterBonusDeposit($event));
    }
}
