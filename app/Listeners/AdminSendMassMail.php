<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\MassMail;
use App\Traits\Common;

class AdminSendMassMail implements ShouldQueue
{
    use Common;

    /**
     * Create the event listener.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(MassMail $event)
    {
       // dd($event);
       $this->adminSendMassMail($event->data); 
    }
}
