<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserWithdrawSend;
use Illuminate\Support\Facades\Mail;
use App\Mail\WithdrawSend;

class WithdrawSendMail
{
    protected $result, $adminemail, $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserWithdrawSend $event)
    {
        //dd($event);
        Mail::to($event->adminemail->email)->queue(new WithdrawSend($event)); 
    }

}
