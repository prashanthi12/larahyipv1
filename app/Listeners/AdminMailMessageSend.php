<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminMailMessage;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminSendMail;
use App\User;

class AdminMailMessageSend
{
    protected $sendmail, $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AdminMailMessage $event)
    {
       // dd($event);
       Mail::to($event->user->email)->queue(new AdminSendMail($event));
    }
}
