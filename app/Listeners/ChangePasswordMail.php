<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserChangePassword;
use Illuminate\Support\Facades\Mail;
use App\Mail\ChangePassword;

class ChangePasswordMail
{
    protected $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserChangePassword $event)
    {
       // dd($event->email);
        Mail::to($event->user->email)->queue(new ChangePassword($event));
    }

}
