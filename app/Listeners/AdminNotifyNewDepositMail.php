<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserNewDeposit;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminNotifyNewDeposit;

class AdminNotifyNewDepositMail
{
    protected $deposit, $user, $admin;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserNewDeposit $event)
    {
        Mail::to($event->admin->email)->queue(new AdminNotifyNewDeposit($event));
    }
}
