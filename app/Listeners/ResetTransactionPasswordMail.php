<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\TransactionPasswordSaved;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetTransactionPassword;

class ResetTransactionPasswordMail
{
    protected $user, $newpassword;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TransactionPasswordSaved $event)
    {
        // dd($event);
        Mail::to($event->user->email)->queue(new ResetTransactionPassword($event));
    }
}
