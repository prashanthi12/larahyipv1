<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketPriorities extends Model
{
	protected $fillable = [
        'name', 'color'
    ];

    public function priority()
    {
        return $this->hasMany('App\Ticket', 'priority_id', 'id');
    }
}
