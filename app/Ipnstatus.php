<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ipnstatus extends Model
{
    protected $fillable = [
        'response'
    ]; 

}
