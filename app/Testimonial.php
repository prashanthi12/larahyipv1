<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
	protected $appends = array('description');

	protected $fillable = [
        'user_id', 'title', 'description', 'rating', 'language', 'active'
    ];

    public function testimonialuser() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function getDescriptionAttribute($description)
    {
    	return \Purify::clean($description);
    }

}
