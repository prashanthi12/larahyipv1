<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{

	protected $appends = array('story');

    protected $fillable = [
        'title', 'story', 'language', 'active'
    ]; 

    public function getStoryAttribute($story)
    {
    	return \Purify::clean($story);
    }
    
}
