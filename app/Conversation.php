<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
	protected $fillable = [
        'user_one', 'user_two', 'status'
    ];
	
    public function userone() {
        return $this->belongsTo('App\User', 'user_one', 'id');
    }

    public function usertwo() {
        return $this->belongsTo('App\User', 'user_two', 'id');
    }

    public function message() {
    	return $this->hasMany('App\Message', 'conversation_id', 'id');
    }

}