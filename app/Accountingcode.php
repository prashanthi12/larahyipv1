<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accountingcode extends Model
{
    protected $fillable = [
        'accounting_code', 'active',
    ];

    public function transaction() {
    	return $this->hasMany('App\Transaction');
    }
    
}
