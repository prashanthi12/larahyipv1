<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	protected $fillable = [
        'title', 'description', 'navlabel', 'slug', 'content', 'seotitle', 'seodescription', 'seokeyword', 'active', 'language'
    ]; 

    public function getContentAttribute($content)
    {
    	return \Purify::clean($content);
    }
     
}
