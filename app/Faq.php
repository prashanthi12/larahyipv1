<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = [
        'title', 'description', 'order', 'language', 'active'
    ]; 

    public function getDescriptionAttribute($description)
    {
    	return \Purify::clean($description);
    }
   
}
