<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    protected $fillable = [
        'credit_transaction_id', 'debit_transaction_id','amount','type', 'comments',
    ];

    public function credit() {
    	return $this->hasOne('App\Transaction', 'credit_transaction_id', 'id');
    }

    public function debit() {
    	return $this->hasOne('App\Transaction', 'debit_transaction_id', 'id');
    }
}
