<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    protected $fillable = [
        'name', 'value', 'type', 'triggertype', 'plan', 'bonus_credited_to', 'status'
    ];

    public function plan() {
        return $this->belongsTo('App\Plan');
    }


     
}
