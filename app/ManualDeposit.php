<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManualDeposit extends Model
{
    protected $table = "manual_deposit";

	protected $fillable = [
        'bank_name', 'country_id', 'swift_code', 'account_no', 'account_name', 'account_address', 'deposit_id', 'user_id', 'proof', 'status', 'details', 'details_type', 'mobile', 'deposit_type', 'withdraw_id'
    ]; 
    protected $dates=['report_at','approve_at','reject_at'];
    protected $with=['withdraw'];

    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function deposit() {
    	return $this->belongsTo('App\Deposit');
    }

    public function country() {
    	return $this->belongsTo('App\Country');
    }

    public function withdraw() {
        return $this->belongsTo('App\Withdraw');
    }

}
