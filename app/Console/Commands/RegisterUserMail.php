<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Userprofile;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerification;
use App\Mail\RegisterNewUser;

class RegisterUserMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:registerusermail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Register User Send Email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
        $users = Userprofile::where('usergroup_id', '4')->with('user')->get(); 
        foreach ($users as $user) 
        {                                
            Mail::to($user->user->email)->queue(new EmailVerification($user));         
            Mail::to($user->user->email)->queue(new RegisterNewUser($user->user));
            // Mail::to($data->admin->email)->queue(new AdminNotifyNewUser($data->user));
        }
    }

}
