<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use App\Userprofile;
use App\Useraccount;
use App\Transaction;
use App\Referralgroup;
use App\Settings;
use App\Traits\DepositProcess;
use Carbon\Carbon;
use App\Traits\LogActivity;

class createTestUser extends Command
{
        use DepositProcess, LogActivity;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:testuser {username} {--count=1} {--deposit=0} {--approve=0} {--chain=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Test User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        

        $count = $this->option('count');
        $count = intval($count);

        $deposit = $this->option('deposit');
        $deposit = (bool) $deposit;

        $approve = $this->option('approve');
        $approve = (bool) $approve;

        $chain = $this->option('chain');
        $chain = (bool) $chain;

        if($deposit) {
            $plan_id = $this->ask('Enter the plan id');
            $amount = $this->ask('Enter the amount');
       }


        for ($i=0; $i < $count; $i++) { 

        if($count > 1) {
             $username = $this->argument('username').$i;
        }else {
             $username = $this->argument('username');
        }
       

        if($chain) {
            $sponsor_id = User::orderBy('id', 'desc')->first()->id;
        } else {
            $sponsor_id = $this->ask('Enter the sponsor id');
        }

        $user = new User;
        $user->name = $username;
        $user->email = $username."@larahyip.com";
        $user->password = bcrypt($username);
        $user->save();

        $this->doActivityLog(
                $user,
                $user,
                ['ip' => request()->ip()],
                'register',
                'User Registered'
            );

        $referralgroup = Referralgroup::where([
                        ['active', '1'],
                        ['is_default', '1'],
                    ])->first();


        $userprofile = new Userprofile;
        $userprofile->user_id = $user->id;
        $userprofile->usergroup_id = "4";
        $userprofile->sponsor_id = $sponsor_id;
        $userprofile->referral_group_id = $referralgroup->id;
        $userprofile->save();

        $this->info("New user added {$user->name}! - User Id : {$user->id}");

        $account_no = "U-".(100000 + $user->id )."-"."1";



        $account = new Useraccount;
        $account->account_no = $account_no;
        $account->description = "User Operative Account";
        $account->user_id = $user->id;
        $account->accounttype_id = "3";
        $account->entity_reference_id = $userprofile->id;
        $account->entity_type = "Profile";
        $account->save();

        $transaction = new Transaction;
        $transaction->account_id = $account->id;
        $transaction->amount = "0.0";
        $transaction->type = "credit";
        $transaction->status ="1";
        $transaction->accounting_code_id = "1";
        $transaction->request = "{object:undefined}";
        $transaction->response = "{object:undefined}";
        $transaction->save();

        if ($deposit) {

            $request = [
                'amount' => $amount,
                'plan' => $plan_id,
                'paymentgateway' =>1,
                'transaction_id' => uniqid()
           ];
           $request = (object) $request;

            $deposit = $this->createNewDeposit($user, $request);
            $account = $this->createUserDepositAccount($user, $deposit);
            $interestAccount = $this->createUserDepositInterestAccount($user, $deposit);
            $transaction = $this->createNewDepositTransaction($account, $deposit, $request);
            $deposit = $this->updateTransactionIDinDeposit($deposit, $transaction);

            if($approve) {
                $this->approve($deposit->id);
            }
        }

        }
    }

}
