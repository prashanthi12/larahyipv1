<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Settings;
use Carbon\Carbon;

class SetMatrix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:setmatrix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set Matrix';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $width = $this->ask('Enter the matrix width value,If Unilevel set width as 0,Otherwise set width value');
        $update=[
                    'value'=>$width,
                    'active'=>0
                 ];
        Settings::where('key', 'matrix_width')->update($update);
        
    }
}
