<?php

namespace App\Console\Commands;
use App\Deposit;
use App\User;
use App\Transaction;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Notifications\User\BitcoinDepositActive;
use App\Traits\DepositProcess;
use App\Helpers\HyipHelper;
use App\Paymentgateway;


class CheckBlockioDeposit extends Command
{
     use DepositProcess;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:check_blockio_deposit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Blockio New Deposit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $deposits = Deposit::where([
                    'paymentgateway_id'   => '6',
                    'status'   => 'new'
            
            ])->get();
        //dd($deposits);

        $pgs = Paymentgateway::where([
                ['active', '=', '1'],
                ['id', '=', '6']
            ])->first();
        
        $params = json_decode($pgs->params, true);  
        //dd($params['mode']);
        if (!$deposits->isEmpty())
        {
            foreach ($deposits as $deposit)
            {
                $transaction = Transaction::where('id', $deposit['transaction_id'])->first();
                $request_json = json_decode($transaction->request, true);
                $response_json = json_decode($transaction->response, true);   

                //dd($response_json);
                //https://chain.so/api/v2/get_address_balance/BTCTEST/      
                $url = 'https://chain.so/api/v2/get_address_balance/'.$params['mode'].'/'.$response_json['transactionaddress'];
                $ch  = curl_init();
                curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2); 
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $curl_json = curl_exec($ch);
                curl_close($ch);

                $curl_json = json_decode($curl_json, true);

                //dd($curl_json);        

               if ($curl_json['data']['address'] == $response_json['transactionaddress'] && $curl_json['data']['confirmed_balance'] == $response_json['actual_btc_amount'] && $curl_json['status'] == 'success')
                { 
                             
                    $result = $this->approve($deposit['id']);
                    //dd($result);
                    
                    if ( $result->save())
                    {
                            $user = User::where('id', $deposit['user_id'])->first();
                            $user->notify(new BitcoinDepositActive);
                    }

                }
                // else
                // {

                //     if ($curl_json['data']['confirmed_balance'] > 0.00000000)
                //     {
                //         $deposit = Deposit::find($deposit['id']);
                //         $deposit->status = "problem";
                //         $deposit->comments_on_problem = "amount different than actual amount";
                //         $deposit->problem_on = Carbon::now();   
                //         $deposit->save();             

                //     }
                        
                // }
                                   
            }
        }
    }
}
