<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\CalculatesInterests;
use Carbon\Carbon;
use App\Deposit;
use App\User;
use App\Notifications\User\MatureDeposit;
use App\Traits\LogActivity;
class CalculateDailyInterest extends Command
{
    use CalculatesInterests;
 

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:daily_interest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate Daily Interest';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setPlanTypeID(PLANTYPE_DAILY);
        // dd($this);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $user = User::find('5');
        // dd($user);
        $this->doActivityLog(
                $user,
                $user,
                ['ip' => request()->ip()],
                'Cron',
                'Daily Interest Cron started'
            );


        // $activedeposits = $this->get_deposits();
        // dd($activedeposits);

        $plans = $this->get_plans();
        // dd($plans);

        foreach ($plans as $plan)
        {
           // $this->calcuateInterests($plan->activeDeposits, $plan);

                  if (!$plan->activeDeposits->isEmpty())
                    {
                        foreach ($plan->activeDeposits as $deposit)
                        {

                            if ($deposit->plan->duration != '-1')
                            {
                                if (!is_null($deposit->matured_on))
                                {
                                     if ($deposit->matured_on < Carbon::now())
                                    {

                                        $this->calcuateInterests($deposit, $plan);
                                        $maturedeposit = Deposit::where('id', $deposit->id)->with('user')->first();
                                        $user = User::where('id', $maturedeposit->user_id)->first();
                                        $maturedeposit->status = 'matured';
                                        $maturedeposit->comments_on_maturity = 'auto matured on '.Carbon::now();
                                        $maturedeposit->save();
                                        $user->notify(new MatureDeposit);


                                        //Activity log
                                      
                                       
                                        $this->doActivityLog(
                                                $user,
                                                $user,
                                                ['ip' => request()->ip()],
                                                'Cron',
                                                'Daily Interest Calculated and Matured deposit for user - '.$user->name.'. Deposit ID #'.$deposit->id
                                            );

                                    }
                                }               
                            }         

                        }
                    }
        }
    }

}
