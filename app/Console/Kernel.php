<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // \App\Console\Commands\calculateInterest::class,
        \App\Console\Commands\CalculateDailyInterest::class,
        \App\Console\Commands\CalculateDailyBusinessInterest::class,
        \App\Console\Commands\CalculateHourlyInterest::class,
        \App\Console\Commands\CalculateWeeklyInterest::class,
        \App\Console\Commands\CalculateMonthlyInterest::class,
        \App\Console\Commands\CalculateQuaterlyInterest::class,
        \App\Console\Commands\CalculateYearlyInterest::class,
        \App\Console\Commands\CreateRootUser::class,
        \App\Console\Commands\CreateTestUser::class,
        \App\Console\Commands\MatureDepositAction::class,
        \App\Console\Commands\CheckBitcoinDeposit::class,
        \App\Console\Commands\CheckCoinPaymentDeposit::class,
        \App\Console\Commands\CheckBlockioDeposit::class,
        \App\Console\Commands\AutoWithdrawalAction::class,
        \App\Console\Commands\CronTest::class,
        \App\Console\Commands\SetMatrix::class,
        \App\Console\Commands\RegisterUserMail::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // $schedule->command('backup:clean')->daily()->at('04:00');
        // $schedule->command('backup:run')->daily()->at('05:00');

        $schedule->command('larahyip:hourly_interest')
                    ->hourly();

        $schedule->command('larahyip:daily_interest')
                    ->daily();

        $schedule->command('larahyip:daily_business_interest')
                    ->weekdays()->at('00:05');

        $schedule->command('larahyip:weekly_interest')
                    ->daily();

        $schedule->command('larahyip:monthly_interest')
                    ->daily();

        $schedule->command('larahyip:quaterly_interest')
                    ->daily();

        $schedule->command('larahyip:yearly_interest')
                    ->daily();

      //  $schedule->command('larahyip:mature_deposit_action')->everyMinute();

        $schedule->command('larahyip:check_bitcoin_deposit')->everyMinute();

        $schedule->command('larahyip:check_coin_payment_deposit')->everyFiveMinutes();

        $schedule->command('larahyip:check_blockio_deposit')->everyFiveMinutes();

        $schedule->command('larahyip:AutoWithdrawalAction')->daily()->at('00:01');

        // $schedule->command('larahyip:cron_test')->everyMinute();

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
