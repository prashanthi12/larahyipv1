<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Userprofile extends Model
{
     use SoftDeletes;

    protected $fillable = [
        'user_id', 'usergroup_id', 'sponsor_id', 'active','firstname', 'lastname', 'mobile', 'country', 'ssn', 'kyc_doc', 'kyc_verified','email_verified', 'email_verification_code','mobile_verified', 'mobile_verification_code','address1','address2','state','city'
    ];

    protected $dates = ['deleted_at'];
    protected $with = ['sponsor','user','deposit'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function sponsor() {
        return $this->belongsTo('App\User', 'sponsor_id', 'id');
    }

    public function useraccount() {
        return $this->hasOne('App\Useraccount', 'entity_reference_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country', 'id');
    }
     
    public function deposit() {
        return $this->hasMany('App\Deposit', 'user_id', 'user_id');
    }

}
