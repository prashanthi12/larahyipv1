<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Ewallet;
use App\Helpers\HyipHelper;
use Lang;
use Config;

class EwalletFundCheckRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {    
        Validator::extend('checkamount', function ($attribute, $value, $parameters, $validator) 
        {
            if (Input::get('amount') < 0)
            {
                return FALSE;
            }
            return TRUE;             
        });

        Validator::extend('checkminamount', function ($attribute, $value, $parameters, $validator) 
        {                      
            if (config::get('settings.ewallet_fund_min_amount') > Input::get('amount'))
            {
                return FALSE;
            }
            return TRUE;            
        });

        Validator::extend('checkmaxamount', function ($attribute, $value, $parameters, $validator) 
        {
            if (config::get('settings.ewallet_fund_max_amount') < Input::get('amount'))
            {
                return FALSE;
            }
            return TRUE;                        
        }); 
      
        return [
            'amount' => 'required|checkamount|numeric|checkminamount|checkmaxamount',
        ];
    }

    public function messages()
    {
        return [   
            'amount.checkamount'=>trans('forms.fundamount_error'),
            'amount.checkminamount' => trans('forms.min_amount_error'),
            'amount.checkmaxamount' => trans('forms.max_amount_error'),       
        ];
    }

}