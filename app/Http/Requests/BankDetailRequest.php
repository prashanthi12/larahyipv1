<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;
use Validator;

class BankDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [           
            'deposit_details_type' => 'required',              
        ];
        
        if (Input::get('deposit_details_type') == 1)
        {
            $rules['users'] = 'required'; 
        }
        if(Input::get('deposit_details_type') == 2)
        {
            $rules['deposit_type'] = 'required';
        }

        if (Input::get('deposit_type') == 1)
        {
            $rules['bank_name'] = 'required'; 
            $rules['swift_code'] = 'required';
            $rules['account_no'] = 'required';
            $rules['account_name'] = 'required';
            $rules['account_address'] = 'required';
            $rules['bank_country'] = 'required';              
        }
        if (Input::get('deposit_type') == 2)
        {
            $rules['country'] = 'required'; 
            $rules['mobile'] = 'required|numeric|checkmobilenumber';                
        }
        Validator::extend('checkmobilenumber', function ($attribute, $value, $parameters, $validator) {  
            if ( strlen(Input::get('mobile')) < 10 || strlen(Input::get('mobile')) > 15)
            {
                return FALSE;
            }
            return TRUE;            
        });   

        return $rules;
    }

    public function messages()
    {
        return [
            'mobile.required' => trans('forms.mobile_no_req'),
            'mobile.numeric' => trans('forms.mobile_no_numeric'), 
            'mobile.checkmobilenumber' => trans('forms.mobile_check_isvalid'), 
            'country.required' => trans('forms.country_req'),
        ];
    }

}
