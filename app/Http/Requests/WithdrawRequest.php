<?php

namespace App\Http\Requests;

use App\Plan;
use App\Userpayaccounts;
use Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Traits\UserInfo;
use App\Withdraw;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Crypt;
use Google2FA;
use Cache;

class WithdrawRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    use UserInfo;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {          
        $min_max_validation = 'between:'.Config::get('settings.withdraw_min_amount').','.Config::get('settings.withdraw_max_amount'); 

        Validator::extend('checkpayaccount', function ($attribute, $value, $parameters, $validator) {         
              $result  = Userpayaccounts::where([                                           
                              ['user_id', '=', Auth::id()],
                              ['paymentgateways_id', '=', $value],  
                              ['active',1]
                              ])->exists();
              if($value == 7)  //Coinpayment
              {
                  $value = 9;   //Bitcoin Direct
              } 
              if (!$result)
              {
                  return FALSE;
              }
              return TRUE;            
          });  

        Validator::extend('checkbalance', function ($attribute, $value, $parameters, $validator) 
        {
            $user = User::where('id', Auth::id())->with(['userprofile','useraccounts','deposits'])->first();
            $userbalance = $this->getUserBalance($user);
                
              if ($value > $userbalance)
              {
                  return FALSE;
              }
              return TRUE;            
        });

        Validator::extend('checkdailywithdrawlimit', function ($attribute, $value, $parameters, $validator) {
            $withdrawcount = Withdraw::where('user_id', Auth::id())
                            ->whereRaw('Date(created_at) = CURDATE()')
                            ->where('autowithdraw', '0')->whereIn('status',['pending','completed'])
                            ->get()->count();

              if ($withdrawcount >= \Config::get('settings.daily_withdraw_limit'))
              {
                  return FALSE;
              }          
              return TRUE;            
        });    

        Validator::extend('checkmonthlywithdrawlimit', function ($attribute, $value, $parameters, $validator) {
          $currentMonth = date('m');            
          $withdrawcount = Withdraw::where('user_id', Auth::id())
                              ->whereRaw('MONTH(created_at) = ?',[$currentMonth])
                              ->where('autowithdraw', '0')->whereIn('status',['pending','completed'])
                              ->get()->count();

              if ($withdrawcount >= \Config::get('settings.monthly_withdraw_limit'))
              {
                  if(\Config::get('settings.monthly_withdraw_limit')< \Config::get('settings.daily_withdraw_limit'))
                  {
                      return FALSE;
                  }
                  return FALSE;
              }    
          return TRUE;            
        }); 

          Validator::extend('valid_token', function ($attribute, $value, $parameters, $validator) {
                $user = User::where('id', Auth::id())->first();      
                $secret = Crypt::decrypt($user->google2fa_secret);
               // dd($secret);
                return Google2FA::verifyKey($secret, $value);
            },
            'Not a valid token'
          ); 

          Validator::extend('used_token', function ($attribute, $value, $parameters, $validator) {
                $user = User::where('id', Auth::id())->first();
                $key = $user->id . ':' . $value;
                //dd($key);
                return !Cache::has($key);
            },
            'Cannot reuse token'      
          );  

            Validator::extend('checkwithdraw', function ($attribute, $value, $parameters, $validator) {
            $withdrawcount = Withdraw::where('user_id', Auth::id())                           
                            ->where('autowithdraw', '0')->whereIn('status',['pending','request'])
                            ->get()->count();

              if ($withdrawcount >= 2)
              {
                  return FALSE;
              }          
              return TRUE;            
        });  

        $rules = [
            'amount' => 'required|numeric|'.$min_max_validation.'|checkbalance|checkwithdraw|checkmonthlywithdrawlimit|checkdailywithdrawlimit',
            'paymentgateway' => 'required|checkpayaccount',                     
        ]; 

        if(\Config::get('settings.twofactor_auth_status') == '1')
        {
            $rules['totp'] = 'bail|required|digits:6|valid_token|used_token';                        
        } 

        return $rules;
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'amount.required' => trans('forms.amountrequired'),
            'amount.checkbalance' => trans('forms.errorbalance'),
            'amount.between' => trans('forms.withdraw_amount_between'),
            'amount.checkmonthlywithdrawlimit' => trans('forms.monthly_withdraw_limit'),
            'amount.checkdailywithdrawlimit' => trans('forms.daily_withdraw_limit'),
            'paymentgateway.required' => trans('forms.payment_req'),
            'paymentgateway.checkpayaccount' => trans('forms.checkpayaccount'),
            'amount.checkwithdraw' => trans('forms.checkwithdraw'),
        ];
    }

}
