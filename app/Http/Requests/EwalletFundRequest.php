<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Ewallet;
use App\Helpers\HyipHelper;
use Lang;

class EwalletFundRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      Validator::extend('checkbitcoinhashkey', function ($attribute, $value, $parameters, $validator)
      {
          $checktxnhashkey = Ewallet::where('bitcoin_hash_id', Input::get('hash_id'))->exists();
          if ($checktxnhashkey)
          {
              return FALSE;
          }
          return TRUE;            
      });

      Validator::extend('checkvalidhashkey', function ($attribute, $value, $parameters, $validator) 
      {            
          $response_json = HyipHelper::getBitcoinWalletDetails(Input::get('hash_id'));
          $response_json = json_decode($response_json, true);  
         // dd($response_json);       
          if (is_null($response_json))
          {
              return FALSE;
          }
          return TRUE;            
      }); 

        return [ 
            'hash_id' => 'required|checkbitcoinhashkey|checkvalidhashkey',
        ];
    }

    public function messages()
    {
        return [
            'hash_id.checkbitcoinhashkey' => trans('forms.hashkey_error'),
            'hash_id.checkvalidhashkey' => trans('forms.invalid_hashkey'),
        ];
    }

}