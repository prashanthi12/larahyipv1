<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Traits\EwalletProcess;
use App\User;
use App\Userpayaccounts;
use App\Classes\BlockIo;
use Exception;
use App\Paymentgateway;

class BitcoinSendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    use EwalletProcess;
 
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Validator::extend('checkamount', function ($attribute, $value, $parameters, $validator) { 
              if (Input::get('amount') <= 0)
              {
                  return FALSE;
              }                
              return TRUE;                         
        });

        Validator::extend('checkuserbalance', function ($attribute, $value, $parameters, $validator) {
     
            $pgId = Paymentgateway::find(6);    // for bitcoin blockio
            $pg = $this->getPgDetailsByGatewayName($pgId->gatewayname);

            $user_accounts = Userpayaccounts::where([['user_id', Auth::id()],['paymentgateways_id', $pg->id]])->first();
     
            $balance = 0;
            if(count($user_accounts) > 0)
            {
                if($user_accounts->btc_address != '')
                  {
                      $balance = $this->getWalletBalance($user_accounts->btc_address);
                  }
            }
            \Session::put('bitcoinerror','');

            $amount = sprintf("%.8f",Input::get('amount'));
            $total = 0;                         
            $params = json_decode($pg->params, true);
            $api_key = $params['api_key'];
            $pin = $params['pin'];   
            $to_btc_address = $params['btc_address'];   

            $version = $params['version']; // API version
            $block_io = new BlockIo( $api_key, $pin, $version);
            try
            {
                $network_fee = $block_io->get_network_fee_estimate(array('amounts' => $amount, 'to_addresses' => $to_btc_address));
        
                $total = $balance + ($network_fee->data->estimated_network_fee);
                     
                if ($amount > $total)
                {
                    return FALSE;
                }
                else
                {  
                    return TRUE; 
                }                     
            }
            catch (Exception $e)
            {
                $bitcoinerror = $e->getMessage();

                \Session::put('bitcoinerror', $bitcoinerror);
                return FALSE;
            }                       
        });

        $rules=[
            'amount' => 'sometimes|numeric|checkamount|checkuserbalance|required',
        ];
      return  $rules;
    }

    public function messages()
    {
        $messages = [
             'amount.checkamount' => trans('forms.amount_error'),           
        ];

        if(\Session::get('bitcoinerror') != '')
        {
            $messages['amount.checkuserbalance'] = \Session::get('bitcoinerror');        
        }
        else
        {
          $messages['amount.checkuserbalance'] = trans('forms.errorbalance');
        }
        return $messages;
    }

}