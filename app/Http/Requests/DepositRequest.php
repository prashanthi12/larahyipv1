<?php

namespace App\Http\Requests;

use App\Plan;
 
use Lang;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class DepositRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $plan = Plan::where('id', '=', Input::get('plan'))->first();
        //dd($plan); exit;
        $min_max_validation = FALSE;
        if ($plan !== null) 
        {
            $min_max_validation = 'between:'.$plan->min_amount.','.$plan->max_amount;  
           
        }  

        $rules = [
            'plan' => 'required',
            'amount' => 'required|numeric|'.$min_max_validation,
        ];     

        if (Input::get('reinvest') == 1)
        {
            Validator::extend('checkbalance', function ($attribute, $value, $parameters, $validator) {                  
                    if (Input::get('amount') > Input::get('reinvestamount'))
                    {
                        return FALSE;
                    }
                   return TRUE;            
            });    

            $rules['amount'] = 'required|numeric|checkbalance|'.$min_max_validation;            
        }             

        if (Input::get('reinvest') == '')
        {
            $rules['paymentgateway'] = 'required';     
        } 

        if (Input::get('paymentgateway') == 5)
        {
            $rules['account_email'] = 'required|email';                
        }
      
        return $rules;
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'plan.required' => trans('forms.plan_req'),  
            'amount.required' => trans('forms.amountrequired'), 
            'amount.between' => trans('forms.deposit_min_max'),
            'amount.checkbalance' => trans('forms.errorbalance'),
            'paymentgateway.required' => trans('forms.payment_req')
        ];
    }

}
