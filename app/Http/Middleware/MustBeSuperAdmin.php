<?php

namespace App\Http\Middleware;

use Closure;

class MustBeSuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(auth()->user()->userprofile->usergroup_id);
        if (auth()->check() && auth()->user()->userprofile->usergroup_id == USER_ROLE_SUPERADMIN)
        {
            return $next($request);
        }

        if (auth()->check() && auth()->user()->userprofile->usergroup_id == USER_ROLE_ADMIN)
        {
            return redirect(HOME_ADMIN);
        }

        return redirect('/');
    }
}
