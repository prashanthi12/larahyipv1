<?php

namespace App\Http\Controllers\Superadmin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\UserRequest as StoreRequest;
use App\Http\Requests\UserRequest as UpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Helpers\HyipHelper;
use App\Traits\NewUser;
use Alert;
use App\User;
use App\ActivityLog;
use App\Deposit;
use App\Placement;
use App\Testimonial;
use App\Ticket;
use App\Useraccount;
use App\Userpayaccounts;
use App\Userprofile;
use App\Withdraw;
use App\Transaction;
use App\Interest;
use App\Fundtransfer;

class UserCrudController extends CrudController
{
    use NewUser;
   
    public function setUp()
    {


        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\User');
        $this->crud->setRoute("superadmin/members");
        $this->crud->setEntityNameStrings('user', 'users');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        //$this->crud->setFromDb();

        $name = [
                'name' => 'name',
                'label' => "Name",
                'type' => 'text',
        ];

        $this->crud->addField($name, 'update/create/both');
        $this->crud->addColumn($name);
       
        $email = [
                'name' => 'email',
                'label' => "Email",
                'type' => 'text',
        ];

        $this->crud->addField($email, 'update/create/both');
        $this->crud->addColumn($email);

        $password = [
                'name' => 'password',
                'label' => "Password",
                'type' => 'password',
        ];

        $this->crud->addField($password, 'update/create/both');
        

        $confirmpassword = [
                'name' => 'password_confirmation',
                'label' => "Confirm Password",
                'type' => 'password',
        ];

        $this->crud->addField($confirmpassword, 'update/create/both');

        $created_at = [
                'name' => 'created_at',
                'label' => "Created On",
               // 'type' => 'password',
        ];
        $this->crud->addColumn($created_at);

         $deleted_at = [
                'name' => 'deleted_at',
                'label' => "Deleted at",
               // 'type' => 'password',
        ];
        $this->crud->addColumn($deleted_at);

        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS

        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
       
        //$this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function

        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        //$this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
         //$this->crud->denyAccess('create');

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function create()
    {
        $issuperadmin = HyipHelper::is_superadmin(Auth::id());
        //dd($issuperadmin);
        $usergroup = array('2' => 'Admin', '3' => 'Staff', '4' => 'Member');
         return view('superadmin.createuser',[
            'usergroup' => $usergroup,
            'issuperadmin' => $issuperadmin,
            'default_sponser' => \Config::get('settings.default_sponser'),
            ]);
    }

    public function store(StoreRequest $request)
    {
        //dd($request);
        // your additional operations before save here
        //$request->password = bcrypt($request->password);
        // $request->merge(['password' => bcrypt($request->password)]);
        //dd($request);
         //$redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
         //dd($request);
      
        $result = $this->createnewuser($request);

        if ($result)
        {
             \Alert::success('User Added Successfully')->flash();
        }
        return redirect(url('superadmin/members')); 
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $request->merge(['password' => bcrypt($request->password)]);
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function deleteUser ($id) 
    {
        //DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::statement('PRAGMA foreign_keys=on;');
        $user = User::find($id);
        $user->delete();
    }

    public function destroy_old($id)
    {   
        $userdetail = User::where('id', $id)->with('userprofile')->first(); 
         // dd($userdetail->userprofile->usergroup_id);
        if ($userdetail->userprofile->usergroup_id != 4)
        {             
            \Alert::error('Dont delete this User.')->flash(); 
            return redirect(url('superadmin/members')); 
        }
     
        if (!is_null($id))
        {            
          //  $user = User::where('id', $id)->delete();
           //\DB::statement('SET FOREIGN_KEY_CHECKS=ON;');

            $user = User::find($id);
            $user->delete();
         // \DB::table('users')->where('id',$id)->delete();
        }
    }

    public function destroy($id)
    {             
        $checkPlacement = Placement::where('spillover_id', $id)->exists();

        $userdetail = User::where('id', $id)->with('userprofile')->first(); 
         // dd($userdetail->userprofile->usergroup_id);
        if ($checkPlacement || $userdetail->userprofile->usergroup_id != 4)
        {              
            \Alert::error('Dont delete this User.')->flash(); 
            return redirect(url('superadmin/members')); 
        }
     
        if (!is_null($id))
        {            
            $deleteActivityLog = ActivityLog::where('causer_id', $id)->delete();
            
            $deleteDeposit = Deposit::where('user_id', $id)->delete();
            $deletePlacement = Placement::where('user_id', $id)->delete();
            $deleteTestimonial = Testimonial::where('user_id', $id)->delete();
            $deleteTicket = Ticket::where('user_id', $id)->delete();
            $useraccount = Useraccount::where('user_id', $id)->get();
            foreach ($useraccount as $deleteid)
            {     
                $gettransactioninFund = Fundtransfer::where('from_account_id', $deleteid['id'])->get();

                foreach ($gettransactioninFund as $fundtransfer)
                {
                    $fundtransfer = Transaction::where('id', $fundtransfer->credit_transaction_id)->delete();
                }

                $transactionInterest = Transaction::where('account_id', $deleteid['id'])->first();

                if (!is_null($transactionInterest))
                {
                    $deleteTransaction = Interest::where('transaction_id', $transactionInterest->id)->delete();
                }
            
               // $gettransactioninFund = Transaction::where([
               //              ['account_id', $deleteid['id']],
               //              ['account_id', 'credit'],
               //              ['accounting_code_id', '51'],
               //              ])->first();

                $deleteTransaction = Transaction::where('account_id', $deleteid['id'])->delete();
                $deleteUseraccount = Useraccount::where('id', $deleteid['id'])->delete();
            }
            $deleteUserpayaccounts = Userpayaccounts::where('user_id', $id)->delete();
            $deleteUserprofile = Userprofile::where('user_id', $id)->delete();
            $deleteWithdraw = Withdraw::where('user_id', $id)->delete();

            $user = User::where('id', $id)->delete();
        }
    }

}
