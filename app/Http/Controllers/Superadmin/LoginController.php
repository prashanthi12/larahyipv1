<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

// use App\Helpers\HyipHelper;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/superadmin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);

        // \Artisan::call('calculate:daily_interest');
        // echo HYIPHelper::plan_label(PLANTYPE_DAILY_BUSINESS);
    }

    public function doLogin(Request $request)
    {
        /* Get the redirection result */
        $out = $this->login($request);

        /* if the user is NOT authenticated.. */
        if (auth()->check() == false)
        {
            // dd($out);
            /* ..redirect to login page */
            return $out;
        }

        /* if the user is authenticated and belongs to proper role (i.e. superadmin role) */
        if (auth()->check() == true && auth()->user()->userprofile->usergroup_id == true)
        {
            /* redirect to destinated page. */
            return $out;
        }

        /* else logout the user .. */
        auth()->logout($request);

        /* .. and throw 401 */
        return abort(401, 'Unauthorized.');
    }

    public function showSuperAdminLoginForm()
    {
        return view('superadmin.login');
    }
}
