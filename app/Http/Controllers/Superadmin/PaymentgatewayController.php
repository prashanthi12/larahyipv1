<?php

namespace App\Http\Controllers\Superadmin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Paymentgateway;
use App\Http\Requests\PaymentgatewayRequest;

class PaymentgatewayController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin1']);
    }

    public function index($status)
    {
    	$paymentgatewaylist = Paymentgateway::where('status', $status)->get();
    	return view('superadmin.paymentgatewaylist', [
    		'paymentgatewaylist' => $paymentgatewaylist,
    		'status' => $status,
    	]);
    }

    public function edit($status, $id)
    {
    	$paymentgateway = Paymentgateway::find($id);
    	// dd($status);
    	return view('superadmin.offline_editform', [
    		'id' => $id,
    		'status' => $status,
    		'paymentgateway' => $paymentgateway,
    	]);
    }

    public function update(PaymentgatewayRequest $request, $id)
    {
    	// dd($request);
    	$paymentgateway = Paymentgateway::find($id);
    	$paymentgateway->gatewayname = $request->gatewayname;
    	$paymentgateway->displayname = $request->displayname;
    	$paymentgateway->active = $request->status;
    	$paymentgateway->deposit = $request->deposit;
    	if($id != '13' && $id != '18' && $id != '19')	// For reinvest, e-pin and e-wallet
    	{
    		$paymentgateway->withdraw = $request->withdraw;
    		$paymentgateway->e_wallet = $request->ewallet;
    		$paymentgateway->withdraw_commission = $request->withdrawcommission;
    		$paymentgateway->min_amount_for_ewallet = $request->ewalletminamount;
	    	$paymentgateway->max_amount_for_ewallet = $request->ewalletmaxamount;
	    	$paymentgateway->bonus_amount_for_ewallet = $request->ewalletbonusamount;
    	} 	
    	$paymentgateway->deposit_fee_status = $request->depositfee;
    	$paymentgateway->deposit_fee_value = $request->depositfeevalue;
    	$paymentgateway->deposit_fee_type = $request->depositfeetype;	
    	$paymentgateway->params = $request->params;
    	$paymentgateway->instructions = $request->instructions;
    	$paymentgateway->update();
    	return redirect(url('/superadmin/paymentgateway/online'));
    }

    // public function onlineEdit($id)
    // {
    // 	$paymentgateway = Paymentgateway::find($id);
    // 	return view('superadmin.online_editform', [
    // 		'id' => $id,
    // 		'paymentgateway' => $paymentgateway,
    // 	]);
    // }

    // public function banktransferModal(Request $request)
    // {
    // 	$gateway = Paymentgateway::find(1);
    // 	$params = array('bank_name' => $request->bank_name, 'bank_address' => $request->account_address, 'swift_code' => $request->swift_code, 'account_name' => $request->account_name,'account_no' => $request->account_no);
    // 	// dd($params);
    // 	$gateway->params = json_encode($params);
    // 	$gateway->update();
    // 	return "gjhdgdfhghdgfdfg";
    // }

}
