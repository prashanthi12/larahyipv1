<?php

namespace App\Http\Controllers\Superadmin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PlanRequest as StoreRequest;
use App\Http\Requests\PlanRequest as UpdateRequest;

class PlanCrudController extends CrudController
{   
   
    public function setUp()
    {

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\Plan");
        $this->crud->setRoute("superadmin/plan");
        $this->crud->setEntityNameStrings('plan', 'plans');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

         $image = [
                'name' => 'image',
                'label' => "Plan Image",
                'type' => 'browse',
                'upload' => true,
                'disk' => getenv('FILE_SYSTEM_DRIVER')
        ];

        $this->crud->addField($image, 'update/create/both');

        //$this->crud->setFromDb();
        $planname = [
                'name' => 'name',
                'label' => "Plan Name",
                'type' => 'text',
        ];

        $this->crud->addField($planname, 'update/create/both');
        $this->crud->addColumn($planname);

        $plantype = [  // Select
           'label' => "Plan Type",
           'type' => 'select',
           'name' => 'plantype_id', // the db column for the foreign key
           'entity' => 'plans', // the method that defines the relationship in your Model
           'attribute' => 'plantype', // foreign key attribute that is shown to user
           'model' => "App\Plantype" // foreign key model
        ];
        $this->crud->addField($plantype, 'update/create/both');
   
        $plantypeview = [
                'name' => 'plantype_id',
                'label' => "Plan Type",
                'type' => 'text',
                'attribute' => 'plans',
                'model'     => "App\Plantype",
        ];
        $this->crud->addColumn($plantypeview);

        $duration = [
                'name' => 'duration',
                'label' => "Duration (Set -1 for Unlimited)",
                'type' => 'text',
        ];

        $this->crud->addField($duration, 'update/create/both');
        $this->crud->addColumn($duration);

        $durationkey = [
                'name' => 'duration_key',
                'label' => "Duration Key",
                'type' => 'enum',
        ];

        $this->crud->addField($durationkey, 'update/create/both');
        $this->crud->addColumn($durationkey);

        $min_amount = [
                'name' => 'min_amount',
                'label' => "Min Amount". " (" .\Config::get('settings.currency'). ")",
                'type' => 'text',
        ];

        $this->crud->addField($min_amount, 'update/create/both');
        $this->crud->addColumn($min_amount);

        $max_amount = [
                'name' => 'max_amount',
                'label' => "Max Amount". " (" .\Config::get('settings.currency'). ")",
                'type' => 'text',
        ];

        $this->crud->addField($max_amount, 'update/create/both');
        $this->crud->addColumn($max_amount);

        $interest_rate = [
                'name' => 'interest_rate',
                'label' => "Interest Rate (%)",
                'type' => 'text',
        ];

        $this->crud->addField($interest_rate, 'update/create/both');
        $this->crud->addColumn($interest_rate);

        $publish = [
                'name' => 'publish',
                'label' => "Published",
                'type' => 'radio',
                'options'     => [ // the key will be stored in the db, the value will be shown as label; 
                        0 => "Draft",
                        1 => "Published"
                    ],

        ];

        $this->crud->addField($publish, 'update/create/both');
        $this->crud->addColumn('publish');

        $principle_return = [
                'name' => 'principle_return',
                'label' => "Principle Amount Return",
                'type' => 'radio',
                'options'     => [ // the key will be stored in the db, the value will be shown as label; 
                        0 => "No",
                        1 => "Yes"
                    ],
        ];
        $this->crud->addField($principle_return, 'update/create/both');
        $this->crud->addColumn('principle_return');

        $partial_withdraw_status = [
                'name' => 'partial_withdraw_status',
                'label' => "Partial Withdraw Status",
                'type' => 'radio',
                'options'     => [ // the key will be stored in the db, the value will be shown as label; 
                        0 => "No",
                        1 => "Yes"
                    ],
        ];
        $this->crud->addField($partial_withdraw_status, 'update/create/both');
        $this->crud->addColumn('partial_withdraw_status');

        $max_partial_withdraw_limit = [
                'name' => 'max_partial_withdraw_limit',
                'label' => "Maximum Partial Withdraw Limit (%)",
                'type' => 'text',               
        ];
        $this->crud->addField($max_partial_withdraw_limit, 'update/create/both');
        // $this->crud->addColumn($max_partial_withdraw_limit);

        $minimum_locking_period = [
                'name' => 'minimum_locking_period',
                'label' => "Minimum Locking Period (Days)",
                'type' => 'text',               
        ];
        $this->crud->addField($minimum_locking_period, 'update/create/both');
        // $this->crud->addColumn($minimum_locking_period);

        $paritial_withdraw_fee = [
                'name' => 'paritial_withdraw_fee',
                'label' => "Partial Withdraw Fee (%)",
                'type' => 'text',               
        ];
        $this->crud->addField($paritial_withdraw_fee, 'update/create/both');
        // $this->crud->addColumn($paritial_withdraw_fee);

        $active = [
                'name' => 'active',
                'label' => "Active",
                'type' => 'radio',
                'options'     => [ 
                        0 => "Inactive",
                        1 => "Active"
                    ],
        ];

        $this->crud->addField($active, 'update/create/both');
        $this->crud->addColumn('active');

         $orderby = [
                'name' => 'orderby',
                'label' => "OrderBy",
                'type' => 'text',
        ];

        $this->crud->addField($orderby, 'update/create/both');
        // $this->crud->addColumn($orderby);
        


        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

	public function store(StoreRequest $request)
	{
		// your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}

	public function update(UpdateRequest $request)
	{

        // dd($request);
		// your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}
}
