<?php

namespace App\Http\Controllers\Superadmin;
use App\Http\Controllers\Controller;
use App\User;
use App\Deposit;
use App\Withdraw;
use App\Transaction;
use App\Traits\GetData;
use Illuminate\Support\Facades\Auth;
use DB;
use Carbon\Carbon;
use App\Plan;

class DashboardController extends Controller
{
    use GetData;
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'admin1']);
    }

    public function index()
    {
        $members = DB::table('users')
            ->join('userprofiles', 'users.id', '=', 'userprofiles.user_id')
            ->where([
                    ['userprofiles.usergroup_id', '4'],
                    ['users.deleted_at', null]
                    ])
            ->get();

        $totalmembers = $members->count();
        $activemembers = $members->where('active', '1')->count();
        $unverifiedmembers = $members->where('kyc_verified', '0')->count();
        $newdeposits = Deposit::with(['plan', 'user', 'paymentgateway'])->where('status','new')->latest()->take(5)->get();
        $newdepositsCount = Deposit::with(['plan', 'user', 'paymentgateway'])->where('status','new')->count();

        $deposits = Deposit::get();
        $activedeposit = $deposits->where('status', 'active')->count();
        $unapprovedeposit = $newdeposits->count();
        $maturedeposit = $deposits->where('status', 'matured')->count();

        $withdraw = Withdraw::get();
        $pendingwithdraw = $withdraw->where('status', 'pending')->count();
        $completedwithdraw = $withdraw->where('status', 'completed')->count();
        $rejectedwithdraw = $withdraw->where('status', 'rejected')->count();

        $transaction = Transaction::get();
        $sumofinterest = $this->interestcalculation();
        $sumoflevelcommission = $this->levelcommissioncalculation($transaction);
        $sumofreferralcommission = $this->referralcommissioncalculation($transaction);

        // For Deposits chart
        $chart_deposits = Deposit::where('status', 'new')->where('created_at', '>=', Carbon::now()->subDays(7))->selectRaw('Date(created_at) as date, sum(amount) as amount')->groupBy('date')->pluck('amount','date');
        $deposits_array = $chart_deposits->toArray();
        
        $m = date("m");
        $de = date("d");
        $y = date("Y");
        // $today = date('Y-m-d', strtotime(Carbon::now()));
        $end_date = date('Y-m-d', mktime(0,0,0,$m,($de - 7), $y));
        $end = date("d", strtotime($end_date));
        for($i=0; $i<=7; $i++)
        {
            $date_array[] = date('Y-m-d', mktime(0,0,0,$m,($end + $i), $y)); 
        }
        // For Withdraws chart
        $chart_withdraws = Withdraw::where('status', 'pending')->where('created_at', '>=', Carbon::now()->subDays(7))->selectRaw('Date(created_at) as date, sum(amount) as amount')->groupBy('date')->pluck('amount','date');
        $withdraw_array = $chart_withdraws->toArray();

        $new_array = array();
        $dep_new_array = array();
        foreach($date_array as $key => $value)
        {
            $wd_date_exists = array_key_exists($value, $withdraw_array);
            $dep_date_exists = array_key_exists($value, $deposits_array);
            if(!$wd_date_exists)
            {
                $new_array[$value] = 0;
            }
            else
            {
                $new_array[$value] = $withdraw_array[$value];
            }
            // For Deposits
            if(!$dep_date_exists)
            {
                $dep_new_array[$value] = 0;
            }
            else
            {
                $dep_new_array[$value] = $deposits_array[$value];
            }
        }
        $chart_withdraws = collect($new_array);
        $chart_deposits = collect($dep_new_array);
        $date_array_coll = collect($date_array);

        // For Deposit by Plan chart
        $chart_plan_deposits = Deposit::where('status', 'active')->selectRaw('(plan_id) as plan,sum(amount) as amount')->with('plan')->groupBy('plan')->pluck('amount','plan');
        // dd($chart_plan_deposits);

    	return view('superadmin.dashboard', [
            'totalmembers' => $totalmembers,
            'activemembers' => $activemembers,
            'unverifiedmembers' => $unverifiedmembers,
            'activedeposit' => $activedeposit,
            'unapprovedeposit' => $unapprovedeposit,
            'maturedeposit' => $maturedeposit,
            'pendingwithdraw' => $pendingwithdraw,
            'completedwithdraw' => $completedwithdraw,
            'rejectedwithdraw' => $rejectedwithdraw,
            'sumofinterest' => $sumofinterest,
            'sumoflevelcommission' => $sumoflevelcommission,
            'sumofreferralcommission' => $sumofreferralcommission,
            'chart_deposits' => $chart_deposits,
            'chart_withdraws' => $chart_withdraws,
            'date_array_coll' => $date_array_coll,
            'chart_plan_deposits' => $chart_plan_deposits,
        ]);
    }

}