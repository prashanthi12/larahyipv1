<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

use App\User;

use Session;
use App\Helpers\HyipHelper;


class UserController extends Controller
{
    public function impersonate($id)
    {
        $user = User::find($id);
            //dd($user);
        // Guard against administrator impersonate
        //Auth::user()->setImpersonating($user->id);
        $is_superadmin = HyipHelper::is_superadmin($user->id);
        //if(! $user->isAdministrator())
        if(! $is_superadmin)
        {
            Auth::user()->setImpersonating($user->id);
        }
        else
        {
            flash()->error('Impersonate disabled for this user.');
        }

        return redirect('myaccount/home');
    }

    public function stopImpersonate()
    {  
        $user = User::where('id', Auth::user()->id)->with('userprofile')->first();
        Auth::user()->stopImpersonating();
        if ($user->userprofile->usergroup_id == 3)
        {
            return redirect(url('admin/staffs'));
        }
        elseif ($user->userprofile->usergroup_id == 4)
        {
            return redirect(url('admin/users'));
        } 
        else
        {
            return redirect(url('superadmin/dashboard'));
            
        }
    }
}
