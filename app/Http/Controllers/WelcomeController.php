<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Plan;
use App\Plantype;
use App\Userprofile;
use App\Slider;
use App\Deposit;
use App\Withdraw;
use App\News;
use App\Testimonial;
use App\Models\Quote;
use Config;

class WelcomeController extends Controller
{
    public function index()
    {
        $slider = Slider::where('active', 1)->get();

        $plans  = Plan::where([
                ['active', '=', '1'],
                ['publish', '=', '1']
            ])->with('plantype')->orderBy('orderby', 'asc')->take('4')->get();     

        $deposits = Deposit::with(['user', 'paymentgateway'])->where('status', 'active')->orderBy('id', 'desc')->take(10)->get();
        $withdraws = Withdraw::with(['user', 'userpayaccounts'])->where('status', 'completed')->orderBy('id', 'desc')->take(10)->get();

        $totalAccount = User::ByUserType(USER_ROLE_MEMBER)->count();
        $newAccount = User::ByUserType(USER_ROLE_MEMBER)->orderBy('user_id', 'desc')->first();

        $totalDeposits = Deposit::whereIn('status', array('active', 'matured', 'released'))->sum('amount');
        $totalWithdraws = Withdraw::where('status', 'completed')->sum('amount');
        $last_deposit = $deposits->first();
        $last_withdraw = $withdraws->first();
        if(!is_null(\Session::get('locale')))
        { 
            $lang = \Session::get('locale');
        }
        else
        {
            $lang = "en";
        }



        $news  = News::where([['active', 1],['language', $lang]])->latest('updated_at')->take(Config::get('settings.display_news_count'))->get();
        $testimonials  = Testimonial::where([['active', 1],['language', $lang]])->orderBy('id', 'DESC')->take(Config::get('settings.display_testimonials_count'))->get();
        $quotes  = Quote::where([['active', 1],['language', $lang]])->orderBy('id', 'DESC')->take(Config::get('settings.display_quotes_count'))->get();
        
        return view('welcome',
        [
                'slider' => $slider,
                'plans' => $plans,
                'deposits' => $deposits,
                'withdraws' => $withdraws,
                'totalDeposits' => $totalDeposits,
                'totalWithdraws' => $totalWithdraws,
                'last_deposit' => $last_deposit,
                'last_withdraw' => $last_withdraw,
                'totalAccount' => $totalAccount,
                'newAccount' => $newAccount,
                'news' => $news,
                'testimonials' => $testimonials,
                'quotes' => $quotes
        ]);
    }

    public function refferal($request)
    {   
        $check = User::where('name', $request)->exists();

        if($check) {
        	Cookie::queue('sponsor', $request, 48000);
        }
        else
        {
            \Session::put('failmessage','Invalid Sponsor.');
        }
        return Redirect::to('/');
    }
    
}
