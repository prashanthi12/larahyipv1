<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Language;
use Cookie;
use Session;

use App\Traits\LogActivity;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    use LogActivity;

     public function __construct()
    {
        $languages = Language::where('active', 1)->get();    
        if(!is_null(Cookie::get('locale')))
        {
            Session::put('languageslist', $languages);
        }
        Session::put('languageslist', $languages);        
    }
     
     
}
