<?php

namespace App\Http\Controllers\Myaccount;

use App\User;
use App\Userprofile;
use App\Invite;
//use App\Mail\InviteFriend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
//use Illuminate\Support\Facades\Mail;
use Validator;
use App\Traits\ProfileInfo;
use App\Traits\LogActivity;
use Illuminate\Bus\Queueable;
use App\Events\InviteNewFriend;
use Event;

class InviteFriendController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'member']);
    }
    
use profileinfo, LogActivity;

    public function invite_friend()
    {
        $user = User::where('id', Auth::id())->with('userprofile')->first();

        $inviteUrl = $this->getRefUrl($user);
        $hasProfile = $this->hasProfile($user);
        $completeProfile = $this->checkProfileForCompletion($user); 

        return view('invite.invite', [
            'inviteUrl' => $inviteUrl,
            'hasProfile' => $hasProfile,
            'completeProfile' => $completeProfile
            ]);
    }

    public function send_request(Request $request)
    {
        //dd($request);
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
                'email'      => 'required|email',
                'message'         => 'required'               
            ]);

        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }
        
        $invite = new Invite;
        $invite->user_id = $user->id;
        $invite->email = $request->email;
        $invite->link = $request->invite_url;
        $invite->message = rawurlencode($request->message);        
      
        if($invite->save()) 
        {
            //firing an event
            Event::fire(new InviteNewFriend($invite));

           // Mail::to($request->email)->queue(new InviteFriend($invite)); 

            $this->doActivityLog(
                        $invite,
                        $user,
                        ['email' => $request->email, 'ip' => request()->ip()],
                        'invite',
                        'Send a new invite.'                        
                    );          
           $request->session()->flash('successmessage', trans('forms.invite_friend_success_message'));
        } 
        else 
        {
            $request->session()->flash('errormessage', trans('forms.invite_friend_failure_message'));
        }
        
        return back();
    }
}
