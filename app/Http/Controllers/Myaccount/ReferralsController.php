<?php

namespace App\Http\Controllers\Myaccount;

use Illuminate\Http\Request;
use App\User;
use App\Deposit;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class ReferralsController extends Controller
{
    // public function referrals_old() 
    // {
    //     $myreferrals = User::where('id', Auth::id())->with('referrals')->get();
    //     $myreferrals =  $myreferrals[0]->referrals->pluck('user_id');

    //     $referrals = new Collection;
    //     $totalDepositByReferrals = 0;
    //     foreach ($myreferrals as $myreferral) {

    //         $user = User::where('id', $myreferral)->first();
    //         $deposits = Deposit::where([ 
    //                                     ['status', '!=', 'new'],['user_id', '=', $myreferral]
    //                                 ])->get();
    //         $name = $user->name;
    //         $created_at = $user->created_at;
    //         $userDepositCount = $deposits ->count();
    //         $userLifeTimeDeposit = $deposits ->sum('amount');
    //         $totalDepositByReferrals = $totalDepositByReferrals + $userLifeTimeDeposit;
    //         $referrals->push([$name, $created_at, $userDepositCount, $userLifeTimeDeposit]);
    //     }
    //     \Session::put('totalDepositByReferrals', $totalDepositByReferrals);

    //     return view ('home.viewreferrals', [
    //             'myreferrals'  => $referrals,
    //             'user' => $user,
    //     ]);
    // }

    public function referrals() 
    {
        $myreferrals = User::where('id', Auth::id())->with('referrals')->first();              
      //  $myreferrals =  $myreferrals[0]->referrals->pluck('user_id');

        $referrals = new Collection;
        $totalDepositByReferrals = 0;
        foreach ($myreferrals->referrals as $myreferral) 
        {
            $userLifeTimeDeposit = $myreferral->deposit->where('status', '!=', 'new')->sum('amount');
       
            $name = $myreferral->user->name;
            $created_at = $myreferral->user->created_at;    
            $userDepositCount =$myreferral->deposit->count();
            $totalDepositByReferrals = $totalDepositByReferrals + $userLifeTimeDeposit;
            $referrals->push([$name, $created_at, $userDepositCount, $userLifeTimeDeposit]);
        }
        \Session::put('totalDepositByReferrals', $totalDepositByReferrals);
       // dd($totalDepositByReferrals);

        return view ('home.viewreferrals', [
                'myreferrals'  => $referrals,
               // 'user' => $user,
                'count'=>$myreferrals->referrals->count(),
                'totalDepositByReferrals'=>$totalDepositByReferrals,
        ]);
    }

}
