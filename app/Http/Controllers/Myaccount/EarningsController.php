<?php

namespace App\Http\Controllers\Myaccount;

use Illuminate\Http\Request;
use App\User;
use App\Accountingcode;
use App\Transaction;
use Illuminate\Support\Facades\Auth;
use App\Traits\ProfileInfo;
use Config;
use App\Penalty;
use App\Sendbonus;

class EarningsController extends Controller
{
    use ProfileInfo;
    
    public function earnings($status) 
    {
        if (in_array( $status, array( 'interest', 'referral', 'level', 'bonus', 'partialwithdraw')) == FALSE)
        {
                abort(404);
        }

        $user = User::where('id', Auth::id())->first();

        $myOpertiveAccount = $user->useraccounts->where('accounttype_id', 3 )->first();
        $operativeAccountId = $myOpertiveAccount->id;

        //Interest
        $interests = $this->getInterest($user);

        // Referral Commmission
        $referralResult  = Accountingcode::where([
            ['active', '=', "1"],
            ['accounting_code', '=', "income-via-referral-commission"],
            ])->get(['id'])->toArray();
        $referral_code = $referralResult[0]['id'];

        $referralCommissions = Transaction::where([
                                                    ['account_id', '=', $operativeAccountId],
                                                    ['type','=','credit'],
                                                    ['accounting_code_id', '=', $referral_code]
                                                ])->latest('id')->paginate(Config::get('settings.pagecount'));

        // Level Commmission
        $levelcommResult  = Accountingcode::where([
            ['active', '=', "1"],
            ['accounting_code', '=', "income-via-level-commission"],
            ])->get(['id'])->toArray();
        $level_comm_code = $levelcommResult[0]['id'];

        $levelCommissions = Transaction::where([
                                                ['account_id', '=', $operativeAccountId],
                                                ['type','=','credit'],
                                                ['accounting_code_id', '=', $level_comm_code]
                                                ])->latest('id')->paginate(Config::get('settings.pagecount'));

        //Bonus
       $bonuses = $this->getBonus($operativeAccountId);

       // Partial Withdraw
        $partialWtithdrawResult  = Accountingcode::where([
            ['active', '=', "1"],
            ['accounting_code', '=', "income-via-deposit-partial-withdraw"],
            ])->get(['id'])->toArray();
        $partial_withdraw_code = $partialWtithdrawResult[0]['id'];

        $partialWithdraws = Transaction::where([
                                                    ['account_id', '=', $operativeAccountId],
                                                    ['type','=','credit'],
                                                    ['accounting_code_id', '=', $partial_withdraw_code]
                                                ])->latest('id')->paginate(Config::get('settings.pagecount'));      

        return view ('home.viewearnings', [
                'referralCommissions' => $referralCommissions,
                'levelCommissions' => $levelCommissions,
                'interests' => $interests,
                'bonuses' => $bonuses,
                'status' => $status,
                'partialWithdraws' =>$partialWithdraws,
                'totalLifeTimeEarnings' => $this->totalLifeTimeEarnings($user),
                'user' => $user,
            ]); 
    }

    public function getInterest($user) 
    {
        $myInterestAccount = $user->useraccounts->where('accounttype_id', 5 )->pluck('id')->toarray();
        //$interestAccountId = $myInterestAccount->id;

         $interestResult  = Accountingcode::where('active', "1")
            ->where('accounting_code', 'like', '%interest-via%')
            ->get(['id'])->toArray();
        //dd($interestResult);

        $interests = Transaction::where('type','=','credit')
                                ->whereIn('accounting_code_id', $interestResult)->
                                whereIn('account_id', $myInterestAccount)->latest('id')->paginate(Config::get('settings.pagecount'));
              //  dd($interests);

        return $interests;
    }

    public function getBonus($operativeAccountId) 
    {
         $bonusResult  = Accountingcode::where([
            ['active', '=', "1"],
            ['accounting_code', '=', "income-as-bonus"],
            ])->get(['id'])->toArray();
        $bonus_code = $bonusResult[0]['id'];

        $bonus = Transaction::where([
                                    ['account_id', '=', $operativeAccountId],
                                    ['type','=','credit'],
                                    ['accounting_code_id', '=', $bonus_code]
                                ])->latest('id')->paginate(Config::get('settings.pagecount'));
        //dd($bonus);
        return $bonus;

    }

    public function penalties() 
    {
        $penalties = Penalty::where('user_id', Auth::id())->latest('id')->paginate(Config::get('settings.pagecount'));
        // dd($penalties);
        return view ('home.penalties', [
                'penalties' => $penalties,
               
            ]); 
    }

    public function bonus() 
    {
        $bonus = Sendbonus::where('user_id', Auth::id())->latest('id')->paginate(Config::get('settings.pagecount'));
        // dd($penalties);
        return view ('home.bonus', [
                'bonuses' => $bonus,
               
            ]); 
    }
}
