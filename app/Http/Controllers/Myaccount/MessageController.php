<?php

namespace App\Http\Controllers\Myaccount;

use Illuminate\Http\Request;
use App\Message;
use App\Conversation;
use Illuminate\Support\Facades\Auth;
use DB;
use Config;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'member']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $message = Message::join('conversations', 'messages.conversation_id', '=', 'conversations.id')
        //     ->select('messages.*', 'conversations.*')
        //     ->where('conversations.user_one', Auth::id())
        //     ->orWhere('conversations.user_two', Auth::id())
        //     ->groupBy('conversation_id')
        //     ->paginate(\Config::get('settings.pagecount')); 
       
        $message = Conversation::where('user_two', Auth::id())->orWhere('user_one', Auth::id())->with(['userone', 'usertwo','message'])->latest()->paginate(Config::get('settings.pagecount')); 
       
        return view('message.show', [
                'messages' => $message,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('message.send');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $conversation = new Conversation;
        $conversation->user_one = Auth::id();
        $conversation->user_two = 2;
        if ($conversation->save())
        {
            $message = new Message;
            $message->message = $request->message;
            $message->user_id = Auth::id();
            $message->conversation_id = $conversation->id;
            if ($message->save())
            {
                $request->session()->flash('successmessage', trans('forms.message_success'));
            }
            else
            {
                $request->session()->flash('errormessage', trans('forms.message_failure'));
            }
        }
        else
        {
            $request->session()->flash('errormessage', trans('forms.message_failure'));
        }
        return \Redirect::to('myaccount/message/list');
    }

    public function conversation($conversationid)
    {
        // echo $conversationid;
        $messages = Message::where('conversation_id', $conversationid)->with('conversation')->get();
        $participants = Conversation::where('id', $conversationid)->with(['userone', 'usertwo'])->first();

        foreach ($messages as $message)
        {
            $message = Message::where('id', $message->id )->first();
            $message->is_seen = '1';
            $message->save();       
        }

        return view('message.view', [
                'message' => $messages,
                'conversationid' => $conversationid,
                'participants' => $participants,
        ]);
    }

    public function conversationsave($conversationid, Request $request)
    {

        $message = new Message;
        $message->message = $request->message;
        $message->user_id = Auth::id();
        $message->conversation_id = $conversationid;
        if ($message->save())
        {
            $request->session()->flash('successmessage', trans('forms.message_success'));
        }
        else
        {
            $request->session()->flash('failmessage', trans('forms.message_failure'));
        }
        // return \Redirect::to('myaccount/message/list');
        return \Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
