<?php

namespace App\Http\Controllers\Myaccount;

use App\User;
use App\Userpayaccounts;
use App\Paymentgateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Traits\PayaccountsProcess;
use App\Country;
class UserpayaccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'member']);
    }
    use PayaccountsProcess;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $allPayments = Paymentgateway::where([
                                                ['active', '=', "1"],
                                                ['withdraw', '=', "1"]
                                            ])->get();
            // dd($allPayments);
            $paypal  = $allPayments->where('id', '2')->count();
            $bank  = $allPayments->where('id', '1')->count();         
            $stp  = $allPayments->where('id', '3')->count();
            $payeer  = $allPayments->where('id', '4')->count();
            $advcash  = $allPayments->where('id', '5')->count();
            $bitcoin_direct = $allPayments->where('id', '9')->count();
            $bitcoin_coinpayemnt = $allPayments->where('id', '7')->count();
            if( $bitcoin_coinpayemnt == 1)
            {
                $bitcoin_direct = 1;
            }
            $skrill  = $allPayments->where('id', '11')->count();
            $okpay  = $allPayments->where('id', '12')->count();
            $perfectmoney  = $allPayments->where('id', '15')->count();
            $neteller  = $allPayments->where('id', '16')->count();
            $manual_deposit  = $allPayments->where('id', '20')->count();
            $manual_deposit_mobile  = $allPayments->where('id', '21')->count();

            $paymentsResult  = Userpayaccounts::where([                                        
                                            ['user_id', '=', Auth::id()],                   
                                            ['active', '=', "1"]
                                            ])->get(); 
            $bankwire_result  = $paymentsResult->where('paymentgateways_id', '1');
            $paypal_result  = $paymentsResult->where('paymentgateways_id', '2');
            $stpay_result  = $paymentsResult->where('paymentgateways_id', '3');
            $payeer_result  = $paymentsResult->where('paymentgateways_id', '4');
            $advcash_result  = $paymentsResult->where('paymentgateways_id', '5');
            $bitcoin_result  = $paymentsResult->where('paymentgateways_id', '9');
            $skrill_result  = $paymentsResult->where('paymentgateways_id', '11');
            $okpay_result  = $paymentsResult->where('paymentgateways_id', '12'); 
            $perfectmoney_result  = $paymentsResult->where('paymentgateways_id', '15'); 

            $neteller_result  = $paymentsResult->where('paymentgateways_id', '16'); 
            $manualdeposit_result = $paymentsResult->where('paymentgateways_id', '20');
            $manualdeposit_mobile_result = $paymentsResult->where('paymentgateways_id', '21');
            $country = Country::get(); 
            return view('home.mypayaccounts', [
                    'paypal' => $paypal,
                    'bank' => $bank,
                    'stp' => $stp,
                    'payeer' => $payeer,
                    'advcash' => $advcash,                   
                    'bitcoin_direct' => $bitcoin_direct,
                    'skrill' => $skrill,
                    'okpay' => $okpay,
                    'perfectmoney' => $perfectmoney,
                    'neteller' => $neteller,
                    'bankwire_result' => $bankwire_result,
                    'paypal_result' => $paypal_result,
                    'stpay_result' => $stpay_result,
                    'payeer_result' => $payeer_result,
                    'advcash_result' => $advcash_result,
                    'bitcoin_result' => $bitcoin_result,
                    'skrill_result' => $skrill_result,
                    'okpay_result' => $okpay_result,
                    'perfectmoney_result' => $perfectmoney_result,
                    'neteller_result' => $neteller_result,
                    'manual_deposit' => $manual_deposit,
                    'manualdeposit_result' => $manualdeposit_result,
                    'manualdeposit_mobile_result' => $manualdeposit_mobile_result,
                    'manual_deposit_mobile' => $manual_deposit_mobile,
                    'country' => $country,
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $result  = Userpayaccounts::where([                                           
                                            ['user_id', '=', Auth::id()],
                                            ['paymentgateways_id', '=', $request->paymentid],
                                            ['active',1]
                                            ])->exists();

        //$active = "0";
        $current = "0";
        if(!$result )
        {
            //$active = "1";
            $current = "1";
        } 

        //Bank wire
        if ($request->paymentid == 1)
        {
            $result = $this->bankwire($request,  $current);
        }

        //Paypal
        if ($request->paymentid == 2)
        {
            $result = $this->paypal($request, $current);            
        }

        //Stpay
        if ($request->paymentid == 3)
        {
            $result = $this->stpay($request, $current);           
        }

        //Payeer
        if ($request->paymentid == 4)
        {
            $result = $this->payeer($request, $current);           
        }

        //Advcash
        if ($request->paymentid == 5)
        {
            $result = $this->advcash($request, $current);         
        }

         //bitcoin -direct waalet
        if ($request->paymentid == 9)
        {
            $result = $this->bitcoin($request, $current);           
        }

        //Skrill
        if ($request->paymentid == 11)
        {
            $result = $this->skrill($request, $current);           
        }

        if ($request->paymentid == 12)
        {
            $result = $this->okpay($request, $current);           
        }

        if ($request->paymentid == 15)
        {
            $result = $this->perfectmoney($request, $current);           
        }

        if ($request->paymentid == 16)
        {
            $result = $this->neteller($request, $current);           
        }
        // For Manual Deposit
        if ($request->paymentid == 20)
        {
            $result = $this->bankwire($request,  $current);
        }
        if ($request->paymentid == 21)
        {
            $result = $this->mobile($request,Auth::id(),$current,1);
        }

        return Redirect::to('myaccount/viewpayaccounts')->with('status', trans('forms.payaccount_added_success'));
       
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Userpayaccounts  $userpayaccounts
     * @return \Illuminate\Http\Response
     */
    public function show(Userpayaccounts $userpayaccounts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Userpayaccounts  $userpayaccounts
     * @return \Illuminate\Http\Response
     */
    public function edit(Userpayaccounts $userpayaccounts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Userpayaccounts  $userpayaccounts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Userpayaccounts $userpayaccounts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Userpayaccounts  $userpayaccounts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Userpayaccounts $userpayaccounts)
    {
        //
    }

     public function removeaccount ($id)
    {

            $user_pay = Userpayaccounts::where('id', $id )->first();
            $user_pay->active = '0';
            $user_pay->save(); 

        return Redirect::to('myaccount/viewpayaccounts')->with('status', trans('forms.payaccount_remove_success'));
     }

    //  public function activeaccounts($id, $paymentid, $status)
    // {

    //     $result  = Userpayaccounts::where([                                        
    //                                         ['user_id', '=', Auth::id()],
    //                                         ['paymentgateways_id', '=', $paymentid]
    //                                         ])->get();         
    //     foreach($result as $payaccounts)
    //     {          
    //         $user_pay = Userpayaccounts::where('id', $payaccounts->id )->first();
    //         $user_pay->active = '0';
    //         $user_pay->save();                                 
    //     }

    //     $user_pay = Userpayaccounts::where('id', $id )->first();
    //         $user_pay->active = '1';
    //         $user_pay->save(); 

    //     return Redirect::to('myaccount/viewpayaccounts')->with('status', trans('forms.payaccount_active_success'));
    //  }

    public function currentaccounts($id, $paymentid, $current)
    {
         $result  = Userpayaccounts::where([                                        
                                            ['user_id', '=', Auth::id()],
                                            ['paymentgateways_id', '=', $paymentid]
                                            ])->get();        
        foreach($result as $payaccounts)
        {          
            $user_pay = Userpayaccounts::where('id', $payaccounts->id )->first();
            $user_pay->current = '0';
            $user_pay->save();                                  
        }

        $user_pay = Userpayaccounts::where('id', $id )->first();
            $user_pay->current = '1';
            $user_pay->save(); 

        return Redirect::to('myaccount/viewpayaccounts')->with('status', trans('forms.payaccount_default_success'));
    }
}
