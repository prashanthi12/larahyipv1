<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Analytics;
use Spatie\Analytics\Period;

class AnalyticsController extends Controller
{
    /*$analyticsData = Analytics::fetchVisitorsAndPageViews(Period::days(7));
        dd($analyticsData);*/

    public function index() 
    {
      	$analyticsData = Analytics::fetchMostVisitedPages(Period::days(7));
      	return view('analytics.index', compact('analyticsData'));
    }

}
