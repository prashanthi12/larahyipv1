<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection; 
use App\Plan;
use App\News;
use App\Banner;
use App\Exchange;
use App\Faq;
use App\Page;
use App\Testimonial;
use Config;
use DB;
use App\User;
use App\Deposit;
use App\Withdraw;
use App\Quote;
use App\Userprofile;
use App\Traits\UserInfo;

class PageController extends Controller
{  
    use UserInfo;

    public function about() 
    {
        if(!is_null(\Session::get('locale')))
        { 
            $lang = \Session::get('locale');
        }
        else
        {
            $lang = "en";
        }
        $pages  = Page::where([['active', 1],['language', $lang]])->get();

        return view('pages.about',[
                'pages' => $pages
        ]);
    }

    public function invest() 
    {
        Cookie::queue('tableViewPreferred', '0', 48000);

        $plans  = Plan::where([
                ['active', '=', '1'],
                ['publish', '=', '1']
            ])->with('plantype')->orderBy('orderby', 'asc')->get();

        return view('pages.invest', [
                'plans' => $plans,
        ]);
    }

    public function earn() 
    {
        $earn  = Banner::where('active', '=', '1')->latest('updated_at')->paginate(Config::get('settings.pagecount'));
        $referralusername = 'username';
        if (Auth::user())
        {
            $referralusername = Auth::user()->name;
        }
        $earn_url = url('/ref').'/'.$referralusername;

        return view('pages.earn', [
                'earn' => $earn,
                'earn_url' => $earn_url
        ]);
    }

    public function news() 
    {
        if(!is_null(\Session::get('locale')))
        { 
            $lang = \Session::get('locale');
        }
        else
        {
            $lang = "en";
        }
        $news  = News::where([['active', 1],['language', $lang]])->latest('updated_at')->paginate(Config::get('settings.pagecount'));

        return view('pages.news', [
                'news' => $news,
        ]);
    }

    public function exchange() 
    {
        $exchange  = Exchange::where('active', '=', '1')->orderBy('order', 'ASC')->paginate(Config::get('settings.pagecount'));
        //dd($exchange);
        return view('pages.exchange', [
                'exchange' => $exchange,
        ]);
    }

    public function reviews() 
    {
        if(!is_null(\Session::get('locale')))
        { 
            $lang = \Session::get('locale');
        }
        else
        {
            $lang = "en";
        }
        $testimonials  = Testimonial::where([['active', 1],['language', $lang]])->orderBy('id', 'DESC')->paginate(Config::get('settings.pagecount'));
        return view('pages.reviews', [
                'testimonials' => $testimonials
        ]);
    }

    public function faq() 
    {
        if(!is_null(\Session::get('locale')))
        { 
            $lang = \Session::get('locale');
        }
        else
        {
            $lang = "en";
        }
        $faq  = Faq::where([['active', 1],['language', $lang]])->orderBy('order', 'ASC')->paginate(Config::get('settings.pagecount'));
        
        return view('pages.faq', [
                'faq' => $faq,
        ]);
    }

    public function contact() 
    {
        return view('pages.contact');
    }

    public function privacy() 
    {
        $pages  = Page::get();

        return view('pages.privacy',[
                'pages' => $pages
        ]);
    }

    public function terms() 
    {
        $pages  = Page::get();

        return view('pages.terms',[
                'pages' => $pages
        ]);
    }

    public function security() 
    {
        $pages  = Page::get();
        
        return view('pages.security',[
                'pages' => $pages
        ]);
    }

    public function show($lang, $slug) 
    {    
        $pages  = Page::where([['active', 1],['language', '=', $lang]])->get();
        $pagedetails  = $pages->where('slug', '=', $slug)->first();

        if($pagedetails != '')
        {
            return view('pages.custompages',[
                'pages' => $pages,
                'pagedetails' => $pagedetails,
                'slug' => $slug
            ]);
        }
        else
        {
            abort(404);
        }
    }

    public function lastinvestors()
    {
        $lastinvestors = Deposit::where('status', '!=', 'new')->with(['user','paymentgateway','plan'])->latest('updated_at')->paginate(Config::get('settings.pagecount'));
        return view('pages.lastinvestors', [
                'lastinvestors' => $lastinvestors
        ]);
    }
    public function topinvestors()
    {
        $users = User::with(['userprofile', 'referrals', 'deposits', 'useraccounts'])->get();
    
        $topinvestors = new Collection;
        foreach($users as $user){
            if($user->userprofile->usergroup_id == "4")
            {
                $topinvestors->push([
                    'name' => $user->name,
                    'email' => $user->email,
                    'lifeTimeDeposit' => $this->getLifeTimeDeposit($user)
                ]);
            }
        }
       
        $topinvestors = $topinvestors->sortByDesc('lifeTimeDeposit');
        $top = $topinvestors->pluck('lifeTimeDeposit', 'name');
        $topinvestors = $top->filter(function ($value) {
            return $value > 0;
        });
        return view('pages.topinvestors', [
                'topinvestors' => $topinvestors
        ]);  
    }
    public function topreferrals()
    {
        $users = User::with(['userprofile', 'referrals'])->get();
            $members = new Collection;
            foreach ($users as $user) {
                if($user->userprofile->usergroup_id == "4") {
                    $members->push($user);
                }
            }
        $usersData = new Collection;
            foreach($members as $user){
                $usersData->push([
                    'name' => $user->name,
                    'email' => $user->email,
                    'refCount' => $this->getReferralCount($user),
                ]);
            }
        $usersData = $usersData->sortByDesc('refCount');
        $top = $usersData->pluck('refCount', 'name');
        $usersData = $top->filter(function ($value) {
            return $value > 0;
        });
        return view('pages.topreferrals', [
                'usersData' => $usersData
        ]);
    }
    public function payouts()
    {
        $payouts = Withdraw::where('status', 'completed')->with(['user', 'userpayaccounts','transaction'])->latest('updated_at')->paginate(Config::get('settings.pagecount'));
        return view('pages.payouts', [
                'payouts' => $payouts
        ]);
    }
    // Quotes
    public function quotes()
    {
        if(!is_null(\Session::get('locale')))
        { 
            $lang = \Session::get('locale');
        }
        else
        {
            $lang = "en";
        }
        $quotes  = Quote::where([['active', 1],['language', $lang]])->get();
        return view('pages.quotes', [
                'quotes' => $quotes,
        ]);
    }
    // Site Statistics
    public function statistics()
    {
        $deposits = Deposit::with(['user', 'paymentgateway'])->where('status', 'active')->orderBy('id', 'desc')->take(10)->get();
        $withdraws = Withdraw::with(['user', 'userpayaccounts'])->where('status', 'completed')->orderBy('id', 'desc')->take(10)->get();

        $totalAccount = User::ByUserType(USER_ROLE_MEMBER)->count();
        $newAccount = User::ByUserType(USER_ROLE_MEMBER)->orderBy('user_id', 'desc')->first();

        $totalDeposits = Deposit::whereIn('status', array('active', 'matured', 'released'))->sum('amount');
        $totalWithdraws = Withdraw::where('status', 'completed')->sum('amount');
        $last_deposit = $deposits->first();
        $last_withdraw = $withdraws->first();

        return view('pages.stats', [
                'totalDeposits' => $totalDeposits,
                'totalWithdraws' => $totalWithdraws,
                'last_deposit' => $last_deposit,
                'last_withdraw' => $last_withdraw,
                'totalAccount' => $totalAccount,
                'newAccount' => $newAccount,
        ]);
    }

    public function viewplan($id)
    {
        $plan  = Plan::where('id', $id)->with('plantype')->first();
        //dd($plan);

        if (is_null($plan))
        {
            abort('404');
        }

        return view('pages.viewplan', [
                'plan' => $plan               
        ]);
    }

}
