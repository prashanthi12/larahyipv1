<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use League\Csv\Reader;
use File;
use App\InviteContacts;
use Session;
use Excel;

class InviteContactController extends Controller
{
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('admin.invitecontacts.inviteform');
    }

    public function create(Request $request)
    {
    	$path = $request->file('invitecontacts_csv')->getRealPath();

        if ($request->has('header')) {
            $data = Excel::load($path, function($reader) {})->get()->toArray();
        } else {
            $data = array_map('str_getcsv', file($path));
        }

        if (count($data) > 0) {
            if ($request->has('header')) {
                $csv_header_fields = [];
                foreach ($data[0] as $key => $value) {
                    $csv_header_fields[] = $key;
                }
            }
            $csv_data = array_slice($data, 0, 2);

            $csv_data_file = CsvData::create([
                'csv_filename' => $request->file('invitecontacts_csv')->getClientOriginalName(),
                'csv_header' => $request->has('header'),
                'csv_data' => json_encode($data)
            ]);
        } else {
            return redirect()->back();
        }

        return view('import_fields', compact( 'csv_header_fields', 'csv_data', 'csv_data_file'));
    }

    public function store(Request $request)
    {
    	$data = Session()->get('csvfilename');
    	//dd($data);
    	// if($request->hasFile('invitecontacts_csv'))
     //    {
     //        $path = $request->file('invitecontacts_csv')->getRealPath();
     //    }
     //    $mime = File::mimeType($path);
       // dd($mime);
        $csv = Reader::createFromPath($data[0], 'r');
        $nbRows = $csv->each(function ($row) {
            return true;
        });
        $rows = $nbRows - 1;    // Count the number of rows in csv
       // dd($data);
        for($i=1;$i<=$rows;$i++)
        {
            $row = $csv->fetchOne($i);
          // dd($request['contacts']);
            // foreach($request['contacts'] as $key=>$value)
            // {
            // 	dd('ddfgdfgdgf');
            // 	if($value == '3')
            // 	{
            // 		$checkexists = InviteContacts::where('email', $value)->exists();
            // 	}           	
             
	            $checkexists = InviteContacts::where('email', $row[2])->exists();
	            $contacts = InviteContacts::where('email', $row[2])->first();
	            if($checkexists != 'true')
	            {
	                $contacts = InviteContacts::create(
	                    [
	                        'firstname' => $row[0],
	                        'lastname' => $row[1],
	                        'email' => $row[2],                
	                    ]
	                ); 
            	}  
        	//}
    	}
    	return redirect(url('/admin/invitecontacts'));	
    }
}