<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Userprofile;
use App\Country;
use App\Userpayaccounts;
use App\ActivityLog;
use App\Transaction;
use App\Useraccount;
use App\Fundtransfer;
use App\Deposit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\ResetPassword;
// use App\Mail\KYCReject;
// use App\Mail\KYCApprove;
use DB;
use File;
use Carbon\Carbon;
use Hash;
use Validator;
use App\Models\Referralgroup;
use App\Traits\UserInfo;
use App\Notifications\User\KycVerify;
use App\Notifications\User\KycRejected;
use  App\Notifications\User\UserRegistrationSuccesful;
use App\Traits\NewUser;
use Illuminate\Bus\Queueable;
use App\Paymentgateway;
use App\Penalty;
use App\Sendbonus;
use App\Conversation;
use App\MailMessage;
use App\Events\AdminUserResetPassword;
use App\Events\AdminKYCApprove;
use App\Events\AdminKYCReject;
use Event;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
   
    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }

    use UserInfo, NewUser;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function indexold()
    // {     
    //     $users = User::with(['userprofile', 'referrals', 'deposits', 'withdraws', 'useraccounts'])->get();
    
    //     $members = new Collection;
    //     foreach ($users as $user) {

    //         if($user->userprofile->usergroup_id == "4") {
    //             $members->push($user);
    //         }
    //     }

    //     $usersData = new Collection;
    //     foreach($members as $user){
    //         $usersData->push([
    //                 'name' => $user->name,
    //                 'email' => $user->email,
    //                 'doj' => $user->created_at,
    //                 'isUserProfileCompleted' => $this->isUserProfileCompleted($user),
    //                 'kycApproved' => $this->isKycApproved($user),
    //                 'active' => $this->isActive($user),
    //                 'emailVerified' => $this->isEmailVerified($user),
    //                 'sponsor' => $this->getSponsor($user),
    //                 'refCount' => $this->getReferralCount($user),
    //                 'balance' => $this->getUserBalance($user),
    //                 'activeDeposit' => $this->getActiveDeposit($user),
    //                 'lifeTimeDeposit' => $this->getLifeTimeDeposit($user),
    //                 'lifeTimeEarnings' => $this->totalLifeTimeEarnings($user),
    //                 ]);
    //     }
    //     return view ('admin.users', [
    //             'usersData' => $usersData
    //         ]);
    // }

    public function index()
    {    
        $totalusers = User::ByUserType('4')->count(); 
        $user = User::ByUserType('4')->with(['userprofile','referrals', 'deposits', 'withdraws', 'useraccounts','credittransactions','debittransactions', 'earnings']);

        $q = Input::get ( 'q' );
        if($q != "")
        {
            $users = $user->where ( 'name', 'LIKE', '%' . $q . '%' )->orWhere ( 'email', 'LIKE', '%' . $q . '%' )->paginate (20)->setPath ( '' );
            $pagination = $users->appends ( array ('q' => Input::get ( 'q' ) ));
            if (count ( $users ) > 0)
                return view ( 'admin.users',['users' => $users,'totalusers' => $totalusers] )->withQuery ( $q );
                // return view ( 'welcome' )->withMessage ( 'No Details found. Try to search again');
            else
            { 
                $users = $user->paginate('20');     
                return view ('admin.users', [
                        'users' => $users,
                        'totalusers' => $totalusers,
                ]);
            }
        }
        else
        {       
            $users = $user->paginate('20');
            return view ('admin.users', [
                'users' => $users,
                'totalusers' => $totalusers,
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {     
        $usergroup = array('3' => 'Staff', '4' => 'Member');
        return view('admin.createuser',[
            'usergroup' => $usergroup
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $validator = Validator::make($request->all(), [
                'usergroup' => 'required',
                'name' => 'required|min:6|max:12|unique:users',
                'email' => 'required|email|max:255|unique:users',           
                'password' => 'required|min:6', 
                //'cpassword' => 'required|min:6|same:password'                
            ]);

        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }   
       
        $this->createnewuser($request);
        return redirect(url('admin/users')); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        $user = User::where('name', $name)
                            ->with(['useraccounts', 'userprofile', 'deposits', 'referrals', 'withdraws', 'ticketuser', 'loguser'])
                            ->first();
        // dd(count($user));
        if(count($user) <= 0)
        {
            abort(403);
        }       
        else
        {
        $referralgroup = Referralgroup::where('active', '1')->get();
             // dd($user);

        $penalties = Penalty::where('user_id', $user->id)->get();
        $bonuses = Sendbonus::where('user_id', $user->id)->get();
            // dd($penalties);

        //Deposits
        $deposit = $user->deposits;
        $newdeposits = $deposit->where('status', 'new');
        $activedeposits = $deposit->where('status', 'active');
        $matureddeposits = $deposit->where('status', 'matured');
        $releaseddeposits = $deposit->where('status', 'released');
        $archiveddeposits = $deposit->where('status', 'archived');
        $rejecteddeposits = $deposit->where('status', 'rejected'); 

        $new_deposit_amount = $newdeposits->sum('amount');
        $active_deposit_amount = $activedeposits->sum('amount');
        $matured_deposit_amount = $matureddeposits->sum('amount');
        $released_deposit_amount = $releaseddeposits->sum('amount');
        $archived_deposit_amount = $archiveddeposits->sum('amount');
        //dd($archiveddeposits);
        $closeddeposits = $matured_deposit_amount + $released_deposit_amount + $archived_deposit_amount
            ;
        $lifetimedeposits = $deposit->where('status', '!=', 'new')->sum('amount');
            // dd($lifetimedeposits); 

        //Login History
        $loginhistory = ActivityLog::whereIn('log_name', array('login', 'logout'))->where('causer_id', $user->id)->orderBy('id', 'DESC')->get();
        $lastlogin = $loginhistory->where('log_name', 'login')->last();           
        $paymentsResult  = Userpayaccounts::where([
                                        ['user_id', '=', $user->id],                   
                                        ['active', '=', "1"]
                                        ])->get();

        //Withdraws
        $withdraw = $user->withdraws;
        $pendingwithdraws  = $withdraw->where('status', 'pending');
        $completedwithdraws  = $withdraw->where('status', 'completed');
        $rejectedwithdraws  = $withdraw->where('status', 'rejected');

        $pendingsum = $pendingwithdraws->sum('amount');
        $completedsum = $completedwithdraws->sum('amount');
        $lifetimesum = $withdraw->whereNotIn('status', array('rejected', 'request'))->sum('amount');   

        //Payaccounts
        $bankwire_result  = $paymentsResult->where('paymentgateways_id', '1');
        $paypal_result  = $paymentsResult->where('paymentgateways_id', '2');
        $stpay_result  = $paymentsResult->where('paymentgateways_id', '3');
        $payeer_result  = $paymentsResult->where('paymentgateways_id', '4');
        $advcash_result  = $paymentsResult->where('paymentgateways_id', '5');
        $bitcoin_result  = $paymentsResult->where('paymentgateways_id', '9');
        $skrill_result  = $paymentsResult->where('paymentgateways_id', '11');
        $okpay_result  = $paymentsResult->where('paymentgateways_id', '12');
        $pm_result  = $paymentsResult->where('paymentgateways_id', '15');

        $neteller_result  = $paymentsResult->where('paymentgateways_id', '16');

        // Fund Transfers
        $fundtransfer = Fundtransfer::with('fundtransfer_to_id', 'fundtransfer_from_id')->get();
        $fromaccount_id = Useraccount::where([
                ['user_id', '=', $user->id],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
        $account_id = $fromaccount_id[0]['id'];

        $sendtransferlists = $fundtransfer->where('from_account_id', $account_id);
        $receivetransferlists = $fundtransfer->where('to_account_id', $account_id);
           
        // Referrals
        $myreferrals = User::where('id', $user->id )->with('referrals')->get();
        $myreferrals =  $myreferrals[0]->referrals->pluck('user_id');

        $referrals = new Collection;
        foreach ($myreferrals as $myreferral) {
            $referraluser = User::where('id', $myreferral)->first();
            $deposits = Deposit::where([ 
                                            ['status', '!=', 'new'],
                                            ['user_id', '=', $myreferral]
                                        ])->get();
            $name = $referraluser->name;
            $created_at = $referraluser->created_at;
            $userDepositCount = $deposits ->count();
            $userLifeTimeDeposit = $deposits ->sum('amount');

            $referrals->push([$name, $created_at, $userDepositCount, $userLifeTimeDeposit]);
        }

        $allPayments = Paymentgateway::where([
                                                ['active', '=', "1"],
                                                ['withdraw', '=', "1"]
                                            ])->get();
        $paypal  = $allPayments->where('id', '2')->count();
        $bank  = $allPayments->where('id', '1')->count();    
        $stp  = $allPayments->where('id', '3')->count();
        $payeer  = $allPayments->where('id', '4')->count();
        $advcash  = $allPayments->where('id', '5')->count();
        $bitcoin_direct   = $allPayments->where('id', '9')->count();
        $skrill  = $allPayments->where('id', '11')->count();
        $okpay  = $allPayments->where('id', '12')->count();
        $perfectmoney  = $allPayments->where('id', '15')->count();
        $neteller  = $allPayments->where('id', '16')->count();

        $userID = $user->id;
        $messagelists = Conversation::where('user_one', $userID)->orWhere('user_two', $userID)->with(['userone', 'usertwo','message'])->latest()->get();
        $mailmessagelists = MailMessage::where('to_user_id', $userID)->with('touser')->latest()->get();
                                                 
        if(is_null($user)) {
                abort(404);
        }else{
            return view('admin.userdetail', [
                    'user' => $user,
                    'bankwire_result' => $bankwire_result,
                    'paypal_result' => $paypal_result,
                    'stpay_result' => $stpay_result,
                    'payeer_result' => $payeer_result,
                    'advcash_result' => $advcash_result,
                    'bitcoin_result' => $bitcoin_result,
                    'skrill_result' => $skrill_result,
                    'okpay_result' => $okpay_result,
                    'newdeposits' => $newdeposits,
                    'activedeposits' => $activedeposits,
                    'matureddeposits' => $matureddeposits,
                    'releaseddeposits' => $releaseddeposits,
                    'archiveddeposits' => $archiveddeposits,
                    'rejecteddeposits' => $rejecteddeposits,
                    'pendingwithdraws' => $pendingwithdraws,
                    'completedwithdraws' => $completedwithdraws,
                    'rejectedwithdraws' => $rejectedwithdraws,
                    'sendtransferlists' => $sendtransferlists,
                    'receivetransferlists' => $receivetransferlists,
                    'myreferrals'  => $referrals,
                    'balance' => $this->getUserBalance($user),
                    'activeDeposit' => $this->getActiveDeposit($user),
                    'lifeTimeDeposit' => $this->getLifeTimeDeposit($user),
                    'lifeTimeEarnings' => $this->totalLifeTimeEarnings($user),
                    'earnings' => $this->getEarnings($user),
                    'totalInterest' => $this->totalInterest($user),
                    'totalReferralCommission' => $this->totalReferralCommission($user),
                    'totalLevelCommission' => $this->totalLevelCommission($user),
                    'totalBonus' => $this->totalBonus($user),
                    'totalLifeTimeEarnings' => $this->totalLifeTimeEarnings($user),
                    'loginhistory' => $loginhistory,
                    'lastlogin' => $lastlogin,
                    'new_deposit_amount' => $new_deposit_amount,
                    'active_deposit_amount' => $active_deposit_amount,
                    'closeddeposits' => $closeddeposits,
                    'lifetimedeposits' => $lifetimedeposits,
                    'pendingsum' => $pendingsum,
                    'completedsum' => $completedsum,
                    'lifetimesum' => $lifetimesum,
                    'pm_result' => $pm_result,
                    'referral_group_id' => $user->userprofile->referral_group_id,
                    'referralgroups'  => $referralgroup,
                    'paypal' => $paypal,
                    'bank' => $bank,
                    'stp' => $stp,
                    'payeer' => $payeer,
                    'advcash' => $advcash,                   
                    'bitcoin_direct' => $bitcoin_direct,
                    'skrill' => $skrill,
                    'okpay' => $okpay,
                    'perfectmoney' => $perfectmoney,
                    'neteller_result' => $neteller_result,
                    'neteller' => $neteller,
                    'penalties' => $penalties,
                    'bonuses' => $bonuses,
                    'messagelists' =>$messagelists,
                    'mailmessagelists' => $mailmessagelists,
                ]);
            } 
        } 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $userprofile = Userprofile::where('id', '=', $id)->first();
        $userprofile->active = $request->userstatus;
        if( $userprofile->save() )
        {
            $request->session()->flash('successmessage', trans('forms.userprofile_success_message')); 
        }
        else
        {
            $request->session()->flash('errormessage', trans('forms.userprofile_failure_message')); 
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
    
    public function resetpassword($id, Request $request)
    {
       // dd($id);
        $user = User::where('id', $id)->with('userprofile')->first();
        //dd($user->email);

        $token = str_random(64);

        $password = DB::table(config('auth.passwords.users.table'))->insert([
                'email' => $user->email, 
                'token' => Hash::make($token),
                'created_at' => Carbon::now()
            ]);       

        if($password) 
        {
            //firing an event
            Event::fire(new AdminUserResetPassword($user, $token));

           // Mail::to($user->email)->queue(new ResetPassword($user, $token));
            $request->session()->flash('successmessage', trans('forms.password_reset_success_message')); 
        } 
        else 
        {
            $request->session()->flash('errormessage', trans('forms.password_reset_failure_message')); 
        }        
        return back();
    }

    public function attachdocdownload($id)
    {
        $attachment = Userprofile::where('id', '=', $id)->first();
        $path = storage_path('app/'.$attachment->kyc_doc);
       // dd($path);
        $headers = array('Content-Type' => File::mimeType($path));        
        return response()->download($path, $attachment->attachment_file, $headers);
    }

    public function verifykyc($id, Request $request)
    {
        $userprofile = Userprofile::where('id', '=', $id)->first();
        $userprofile->kyc_verified = 1;
        if( $userprofile->save() )
        {
            $user = User::where('id', $id)->with('userprofile')->first();
            $user->notify(new KycVerify);

            //firing an event
            Event::fire(new AdminKYCApprove($user));

           // Mail::to($userprofile->user->email)->queue(new KYCApprove($userprofile));
            $request->session()->flash('successmessage', trans('forms.kyc_verified_success_message')); 
        }
        else
        {
            $request->session()->flash('errormessage', trans('forms.kyc_verified_failure_message')); 
        }
        return back();
    }

    public function rejectkyc($id, Request $request)
    {
        $userprofile = Userprofile::where('id', '=', $id)->with('user')->first();

        $userprofile->kyc_verified = 2;
        if( $userprofile->save() )
        {
            $user = User::where('id', $id)->with('userprofile')->first();
            $user->notify(new KycRejected);

            //firing an event
            Event::fire(new AdminKYCReject($user));

           // Mail::to($userprofile->user->email)->queue(new KYCReject($userprofile));           
            $request->session()->flash('successmessage', trans('forms.kyc_rejected_success_message')); 
        }
        else
        {
            $request->session()->flash('errormessage', trans('forms.kyc_rejected_failure_message')); 
        }
        return back();
    }

    public function stafflist()
    {
        $users = User::with(['userprofile', 'agent'])->get();
        $staffs = new Collection;
        foreach ($users as $user) {
            if($user->userprofile->usergroup_id == "3") {
                $staffs->push($user);
            }
        }
       // dd($staffs);
        return view ('admin.staffs', [
                'staffs' => $staffs
            ]);
    }

    // public function updatereferralgroup($userid, $referralgroupid)
    // {
    //     $userprofile = Userprofile::where('id', '=', $userid)->first();
    //     $userprofile->referral_group_id = $referralgroupid;
    //     if ($userprofile->save())
    //     {
    //         session()->flash('successmessage', trans('forms.user_referralgroup_success_message')); 
    //     }
    //     else
    //     {
    //         session()->flash('errormessage', trans('forms.user_referralgroup_failure_message')); 
    //     }
    //     return back();
    // }

    public function referralgroup($id)
    {
        $user = Userprofile::where('id', '=', $id)->with('user')->first();
        $referralgroups = Referralgroup::where('active', '=', 1)->get();
        //dd($referralgroups);
        return view('admin.referralgroup', [
                'user' => $user,
                'referralgroups' => $referralgroups,
        ]);
    }

    public function updatereferralgroup(Request $request, $id)
    {
        $userprofile = Userprofile::with('user')->find($id);
        $userprofile->referral_group_id = $request->referralgroup;
        if ($userprofile->save())
        {
            session()->flash('successmessage', trans('forms.user_referralgroup_success_message')); 
        }
        else
        {
            session()->flash('errormessage', trans('forms.user_referralgroup_failure_message')); 
        }
        return redirect(url('admin/users/'.$userprofile->user->name));
    }

    public function searchuser(Request $request) 
    {
        $query = $request->get('query','');
        
        $users=User::where('name','LIKE',''.$query.'%')->get()->take(10);
        
        $data=array();

        foreach ($users as $users) 
        {
            $data[] = array('value' => $users->name,'id' => $users->id);
        }
        if(count($data))
        {
            return $data;
        }
        else
        {
            return ['value' => 'No Result Found','id' => ''];
        }
    }

}
