<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Transaction;
use App\Accountingcode;

class EarningsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }

    public function index()
    {
        $adminId = 2;
        $user = User::where('id', $adminId)
                            ->with('useraccounts')
                            ->first();
        // $usergroup = User::where('id', $adminId)->with('userprofile')->first();
        // $usergroupId = $usergroup->userprofile->usergroup_id;
        $useraccounts = $user->useraccounts();
        $myOpertiveAccount = $user->useraccounts->where('accounttype_id', 3 )->first();
       
        // if($usergroupId != '2')
        // {
            $operativeAccountId = $myOpertiveAccount->id;
        // }
        // else
        // {
        //     $operativeAccountId = 4;   // For other admin users, use the admin useraccount ID 4.
        // }
        
        $withdrawaccountcodeResult  = Accountingcode::where([
            ['active', '=', "1"],
            ['accounting_code', '=', "withdraw-commission"],
            ])->get(['id'])->toArray();
        $withdraw_accounting_code = $withdrawaccountcodeResult[0]['id'];
        
        $fundaccountcodeResult  = Accountingcode::where([
            ['active', '=', "1"],
            ['accounting_code', '=', "fund-transfer-commission"],
            ])->get(['id'])->toArray();
        $fund_accounting_code = $fundaccountcodeResult[0]['id'];
       
        $partialwithdrawaccountcodeResult  = Accountingcode::where([
            ['active', '=', "1"],
            ['accounting_code', '=', "deposit-partial-withdraw-commission"],
            ])->get(['id'])->toArray();
        $partial_withdraw_accounting_code = $partialwithdrawaccountcodeResult[0]['id'];

        $getcommissions = Transaction::where([
                                ['account_id', '=', $operativeAccountId],
                                ['type', 'credit'],
                                ['status', '1']                             
                        ])->whereIn('accounting_code_id', [$withdraw_accounting_code, $fund_accounting_code, $partial_withdraw_accounting_code])->with('accountingcode')->paginate('20');

        $totalearnings = $getcommissions->sum('amount');
       // dd($totalearnings);

        return view('admin.showearnings', [
        	'earings' => $getcommissions,
            'totalearnings' => $totalearnings	
        ]);
    }
    
}
