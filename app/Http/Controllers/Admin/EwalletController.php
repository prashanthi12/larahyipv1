<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\Ewallet;
use App\Traits\EwalletProcess;
use App\User;
use App\Http\Requests\EwalletFundApproveRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\Ewallet\CancelFund;

class EwalletController extends Controller
{ 
    use EwalletProcess;

    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }

    public function show()
    {
        $fundlist = Ewallet::orderBy('id','DESC')->get();
        return view('admin.ewallet.fund_show', [
                'fundlist' => $fundlist,
        ]);
    }

    public function cancelform(Request $request, $id) 
    {
        return view('admin.ewallet.fund_cancel', [
                'fund_id' => $id,
        ]);
    }

    public function cancelled(Request $request, $id) 
    {
        $validator = Validator::make($request->all(), [
                'comment' => 'required'               
        ]);

        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }

        $fund = Ewallet::find($id);
        $user_id = $fund['from_user_id'];
        $update = [
	                'status'=>'cancel',
	                'comments_cancel'=> $request->comment,
	                'cancel_at'=>Carbon::now(),
	                'process_via'=>'admin',       
             	];

        if (Ewallet::where('id',$id)->update($update))
        {
            $request->session()->flash('successmessage', 'Fund Cancelled Successfully');
            $user = User::find($user_id);
            $email = $user->email;
			Mail::to($email)->send(new CancelFund($fund));  
        }
        else
        {             
            $request->session()->flash('errormessage', 'Fund Cancelled Failed!!!. Please Try Again.');
        }   
        return Redirect::to('admin/ewallet/fund/show/');    
    }

    public function approveform($id) 
    {
        $fund = Ewallet::find($id);   
        return view('admin.ewallet.fund_approve', [           
                'fund'=>$fund,
        ]);
    }

    public function approved(EwalletFundApproveRequest $request, $id) 
    {
        $received_amount = $request->received_amount;
        $this->approveFund($id,'admin',$received_amount);
        return Redirect::to('admin/ewallet/fund/show/');
    }

}