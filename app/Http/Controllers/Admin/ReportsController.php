<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use League\Csv\Writer;
use App\Deposit;
use App\Paymentgateway;
use App\Plan;
use App\User;
use App\Fundtransfer;
use App\Withdraw;
use Config;
use App\Traits\UserInfo;

class ReportsController extends Controller
{
	use UserInfo;

    public function __construct()
    {   
        $this->middleware(['auth','admin2']);
    }

    public function DepositPayment()
    {
    	$payments = Paymentgateway::where([
        		['active', '=', '1'],
        		['deposit', '=', '1'],
                ['id', '!=', '10']
        	])->get();

        return view ('admin.reports.depositsbypaymentreports', [
        		'payments' => $payments              
        	]);
    }
    public function DepositPaymentExport($id)
    {
    	$details = Deposit::where('paymentgateway_id', $id)->with(['plan','user','paymentgateway'])->get();
        if(count($details) > 0)
        {
            $csv = Writer::createFromFileObject(new \SplTempFileObject());
              $csv->insertOne(['Mode of Payment','Plan Name','Min Amount'.' ( '.config::get('settings.currency').' )','Max Amount'.' ( '.config::get('settings.currency').' )','Interest','Duration','Duration Key','User Name','Deposit Status','Deposit Amount'.' ( '.config::get('settings.currency').' )','Deposited on','Approved on','Matured on','Released on','Rejected on','Archived on']);
            foreach ($details as $detail) {
              $csv->insertOne([$detail->paymentgateway->displayname,$detail->plan->name,$detail->plan->min_amount,$detail->plan->max_amount,$detail->plan->interest_rate.' %',$detail->plan->duration,$detail->plan->duration_key,$detail->user->name,$detail->status,$detail->amount,$detail->created_at,$detail->approved_on,$detail->matured_on,$detail->released_on,$detail->rejected_on,$detail->archived_on]);
            }
        }
        else
        {
            $csv = Writer::createFromFileObject(new \SplTempFileObject());
            $csv->insertOne(['No Records Found']);
        }
        $csv->output('Deposit_'.$detail->paymentgateway->displayname.date('_d/m/Y_H:i').'.csv');
    }

    public function DepositPlan()
    {
    	$plan = Plan::where('active', 1)->get();
    	return view('admin.reports.depositsbyplanreports', [
    		'plan' => $plan
    	]);
    }
    public function DepositPlanExport($id)
    {
    	$details = Deposit::where('plan_id', $id)->with(['plan','user','paymentgateway'])->get();
        if(count($details) > 0)
        {
            $csv = Writer::createFromFileObject(new \SplTempFileObject());
              $csv->insertOne(['Plan Name','Min Amount'.' ( '.config::get('settings.currency').' )','Max Amount'.' ( '.config::get('settings.currency').' )','Interest','Duration','User Name','Deposit Status','Deposit Amount'.' ( '.config::get('settings.currency').' )','Mode of Payment','Deposited on','Approved on','Matured on','Released on','Rejected on','Archived on']);
            foreach ($details as $detail) {
              $csv->insertOne([$detail->plan->name,$detail->plan->min_amount,$detail->plan->max_amount,$detail->plan->interest_rate.' %',$detail->plan->duration,$detail->user->name,$detail->status,$detail->amount,$detail->paymentgateway->displayname,$detail->created_at,$detail->approved_on,$detail->matured_on,$detail->released_on,$detail->rejected_on,$detail->archived_on]);
            }
        }
        else
        {
            $csv = Writer::createFromFileObject(new \SplTempFileObject());
            $csv->insertOne(['No Records Found']);
        }
        $csv->output('Deposit_'.$detail->plan->name.date('_d/m/Y_H:i').'.csv');
    }

    public function DepositStatus()
    {
        return view('admin.reports.depositsreports');
    }
    public function DepositStatusExport($status)
    {
        if($status == 'new')
        {
            $details = Deposit::where('status', 'new')->with(['user','paymentgateway','plan'])->get();
            if(count($details) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                  $csv->insertOne(['Name','Amount'.' ( '.config::get('settings.currency').' )','Email','Paymentgateway Name','Plan Name','Deposit Date']);
                foreach ($details as $detail) {
                  $csv->insertOne([$detail->user->name,$detail->amount,$detail->user->email,$detail->paymentgateway->displayname,$detail->plan->name,$detail->created_at]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Deposit_New'.date('_d/m/Y_H:i').'.csv');
        }
        else if($status == 'active')
        {
            $details = Deposit::where('status', 'active')->with(['user','paymentgateway','plan'])->get();
            if(count($details) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                  $csv->insertOne(['Name','Amount'.' ( '.config::get('settings.currency').' )','Email','Paymentgateway Name','Plan Name','Deposit Date','Approved Date']);
                foreach ($details as $detail) {
                  $csv->insertOne([$detail->user->name,$detail->amount,$detail->user->email,$detail->paymentgateway->displayname,$detail->plan->name,$detail->created_at,$detail->approved_on]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Deposit_Active'.date('_d/m/Y_H:i').'.csv');
        }
        else if($status == 'matured')
        {
            $details = Deposit::where('status', 'matured')->with(['user','paymentgateway','plan'])->get();
            if(count($details) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                  $csv->insertOne(['Name','Amount'.' ( '.config::get('settings.currency').' )','Email','Paymentgateway Name','Plan Name','Deposit Date','Matured Date','Comments']);
                foreach ($details as $detail) {
                  $csv->insertOne([$detail->user->name,$detail->amount,$detail->user->email,$detail->paymentgateway->displayname,$detail->plan->name,$detail->created_at,$detail->matured_on,$detail->comments_on_maturity]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Deposit_Matured'.date('_d/m/Y_H:i').'.csv');
        }
        else if($status == 'released')
        {
            $details = Deposit::where('status', 'released')->with(['user','paymentgateway','plan'])->get();
            if(count($details) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                  $csv->insertOne(['Name','Amount'.' ( '.config::get('settings.currency').' )','Email','Paymentgateway Name','Plan Name','Deposit Date','Released Date','Comments']);
                foreach ($details as $detail) {
                  $csv->insertOne([$detail->user->name,$detail->amount,$detail->user->email,$detail->paymentgateway->displayname,$detail->plan->name,$detail->created_at,$detail->released_on,$detail->comments_on_release]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Deposit_Released'.date('_d/m/Y_H:i').'.csv');
        }
        else if($status == 'rejected')
        {
            $details = Deposit::where('status', 'rejected')->with(['user','paymentgateway','plan'])->get();
            if(count($details) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                  $csv->insertOne(['Name','Amount'.' ( '.config::get('settings.currency').' )','Email','Paymentgateway Name','Plan Name','Deposit Date','Rejected Date','Comments']);
                foreach ($details as $detail) {
                  $csv->insertOne([$detail->user->name,$detail->amount,$detail->user->email,$detail->paymentgateway->displayname,$detail->plan->name,$detail->created_at,$detail->rejected_on,$detail->comments_on_reject]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Deposit_Rejected'.date('_d/m/Y_H:i').'.csv');
        }
        else if($status == 'problem')
        {
            $details = Deposit::where('status', 'problem')->with(['user','paymentgateway','plan'])->get();
            if(count($details) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                  $csv->insertOne(['Name','Amount'.' ( '.config::get('settings.currency').' )','Email','Paymentgateway Name','Plan Name','Deposit Date','Comments','Problem On']);
                foreach ($details as $detail) {
                  $csv->insertOne([$detail->user->name,$detail->amount,$detail->user->email,$detail->paymentgateway->displayname,$detail->plan->name,$detail->created_at,$detail->comments_on_problem,$detail->problem_on]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Deposit_Problem'.date('_d/m/Y_H:i').'.csv');
        }
    }

    public function Earnings()
    {
    	return view('admin.reports.earningsreports');
    }
    public function EarningsExport($status)
    {
    	if($status == 'referral')
        {
            $users = User::with(['userprofile', 'referrals', 'deposits', 'withdraws', 'useraccounts'])->get();
            $members = new Collection;
            foreach ($users as $user) {
                if($user->userprofile->usergroup_id == "4") {
                    $members->push($user);
                }
            }
            $usersData = new Collection;
            foreach($members as $user){
                $usersData->push([
                    'name' => $user->name,
                    'doj' => $user->created_at,
                    'refCount' => $this->getReferralCount($user),
                    'referralCommission' => $this->getUserTotalReferralCommission($user),
                    ]);
            }
            if(count($usersData) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                  $csv->insertOne(['Name','Date','Referral Count','Referral Commission'.' ( '.config::get('settings.currency').' )']);
                foreach ($usersData as $detail) {
                  $csv->insertOne([$detail['name'],$detail['doj'],$detail['refCount'],$detail['referralCommission']]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Earnings_Referral'.date('_d/m/Y_H:i').'.csv');
        }
        else if($status == 'interest')
        {
            $users = User::with(['userprofile', 'referrals', 'deposits', 'withdraws', 'useraccounts'])->get();
            $members = new Collection;
            foreach ($users as $user) {
            if($user->userprofile->usergroup_id == "4") {
                $members->push($user);
                }
            }
            $usersData = new Collection;
            foreach($members as $user){
            $usersData->push([
                'name' => $user->name,
                'doj' => $user->created_at,
                'totalinterest' => $this->getUserTotalInterest($user),
                ]);
            }
            if(count($usersData) > 0)
            {
    			$csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['Name','Date','Interest Amount'.' ( '.config::get('settings.currency').' )']);
            	foreach ($usersData as $detail) {
                  	$csv->insertOne([$detail['name'],$detail['doj'],$detail['totalinterest']]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Earnings_Interest'.date('_d/m/Y_H:i').'.csv');               
        }
    }

    public function FundsTransfer()
    {
        return view('admin.reports.fundstransferreports');
    }
    public function FundsTransferExport()
    {
        $details = Fundtransfer::with(['fundtransfer_from_id','fundtransfer_to_id'])->get();
        if(count($details) > 0)
        {
            $csv = Writer::createFromFileObject(new \SplTempFileObject());
              $csv->insertOne(['Sender Name','Amount'.' ( '.config::get('settings.currency').' )','Comments','Receiver Name','Date']);
            foreach ($details as $detail) {
              $csv->insertOne([$detail->present()->getUsername($detail->fundtransfer_from_id->user_id),$detail->amount,$detail->comments,$detail->present()->getUsername($detail->fundtransfer_to_id->user_id),$detail->created_at]);
            }
        }
        else
        {
            $csv = Writer::createFromFileObject(new \SplTempFileObject());
            $csv->insertOne(['No Records Found']);
        }
        $csv->output('Fundtransfer'.date('_d/m/Y_H:i').'.csv');
    }

    public function Investors()
    {
    	return view('admin.reports.investorsreports');
    }
    public function InvestorsExport($id)
    {
    	$users = User::with(['userprofile', 'referrals', 'deposits', 'useraccounts'])->get();
        $members = new Collection;
        foreach ($users as $user) {
            if($user->userprofile->usergroup_id == "4") {
                $members->push($user);
            }
        }
        $usersData = new Collection;
        foreach($members as $user){
            $usersData->push([
                'name' => $user->name,
                'email' => $user->email,
                'doj' => $user->created_at,
                'lifeTimeDeposit' => $this->getLifeTimeDeposit($user)
            ]);
        }
        $usersData = $usersData->sortByDesc('lifeTimeDeposit');
            if($id == 1)
            {
                $usersData = $usersData->forPage(1, 10);
            }
            else
            {
                $usersData = $usersData->forPage(1, 100);
            }
        if(count($usersData) > 0)
        {
            $csv = Writer::createFromFileObject(new \SplTempFileObject());
            $csv->insertOne(['Name','Email','Date of Join','Total Deposits'.' ( '.config::get('settings.currency').' )']);
            foreach ($usersData as $detail) {
            $csv->insertOne([$detail['name'],$detail['email'],$detail['doj'],$detail['lifeTimeDeposit']]);
            }
        }
        else
        {
            $csv = Writer::createFromFileObject(new \SplTempFileObject());
            $csv->insertOne(['No Records Found']);
        }
        $csv->output('Investors'.date('_d/m/Y_H:i').'.csv');
    }

    public function Sponsors()
    {
    	return view('admin.reports.sponsorsreports');
    }
    public function SponsorsExport($id)
    {
        if($id <= '2')
        {
        	$users = User::with(['userprofile', 'referrals'])->get();
            $members = new Collection;
            foreach ($users as $user) {
                if($user->userprofile->usergroup_id == "4") {
                    $members->push($user);
                }
            }
            $usersData = new Collection;
            foreach($members as $user){
                $usersData->push([
                    'name' => $user->name,
                    'email' => $user->email,
                    'doj' => $user->created_at,
                    'sponsor' => $this->getSponsor($user),
                    'refCount' => $this->getReferralCount($user),
                ]);
            }
           $usersData = $usersData->sortByDesc('refCount');
                if($id == 1)
                {
                    $usersData = $usersData->forPage(1, 10);
                }
                else
                {
                    $usersData = $usersData->forPage(1, 100);
                }
            if(count($usersData) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['Name','Email','Date of Join','Sponsors','Referrals Count']);
                foreach ($usersData as $detail) {
                $csv->insertOne([$detail['name'],$detail['email'],$detail['doj'],$detail['sponsor'],$detail['refCount']]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Sponsors_Numbers'.date('_d/m/Y_H:i').'.csv');
        }
        else
        {
            $users = User::with(['userprofile', 'referrals'])->get();
            $members = new Collection;
            foreach ($users as $user) {
                if($user->userprofile->usergroup_id == "4") {
                    $members->push($user);
                }
            }
            $usersData = new Collection;
            foreach($members as $user){
                $usersData->push([
                    'name' => $user->name,
                    'email' => $user->email,
                    'doj' => $user->created_at,
                    'refCount' => $this->getReferralCount($user),
                    'refDeposit' => $this->getReferralDeposit($user),
                ]);
            }
            $usersData = $usersData->sortByDesc('refDeposit');
                if($id == 3)
                {
                    $usersData = $usersData->forPage(1, 10);
                }
                else
                {
                    $usersData = $usersData->forPage(1, 100);
                }
            if(count($usersData) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['Name','Email','Date of Join','Referrals Count','Referrals Deposit'.' ( '.config::get('settings.currency').' )']);
                foreach ($usersData as $detail) {
                $csv->insertOne([$detail['name'],$detail['email'],$detail['doj'],$detail['refCount'],$detail['refDeposit']]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Sponsors_Deposits'.date('_d/m/Y_H:i').'.csv');
        }
    }

	public function Withdraw()
    {
        return view('admin.reports.withdrawreports');
    }
    public function WithdrawExport($status)
    {
    	if($status == 'request')
        {
            $details = Withdraw::where('status', 'request')->with('user')->with(['transaction', 'userpayaccounts'])->latest('updated_at')->paginate(Config::get('settings.pagecount'));
            if(count($details) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['Name','Amount'.' ( '.config::get('settings.currency').' )','Email','Paymentgateway Name','Param1','Param2','Param3','Param4','Param5','Request Comment','Request Date']);
                foreach ($details as $detail) {
                  $csv->insertOne([$detail->user->name,$detail->amount,$detail->user->email,$detail->userpayaccounts->payment->displayname,$detail->userpayaccounts->param1,$detail->userpayaccounts->param2,$detail->userpayaccounts->param3,$detail->userpayaccounts->param4,$detail->userpayaccounts->param5,trans('myaccount.withdrawrequestcomments'),$detail->created_at]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Withdraw_Request'.date('_d/m/Y_H:i').'.csv');
        }
        else if($status == 'pending')
        {
            $details = Withdraw::where('status', 'pending')->with(['user', 'userpayaccounts'])->get();
            if(count($details) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['Name','Amount'.' ( '.config::get('settings.currency').' )','Email','Paymentgateway Name','Param1','Param2','Param3','Param4','Param5','Request Date']);
                foreach ($details as $detail) {
                  $csv->insertOne([$detail->user->name,$detail->amount,$detail->user->email,$detail->userpayaccounts->payment->displayname,$detail->userpayaccounts->param1,$detail->userpayaccounts->param2,$detail->userpayaccounts->param3,$detail->userpayaccounts->param4,$detail->userpayaccounts->param5,$detail->created_at]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Withdraw_Pending'.date('_d/m/Y_H:i').'.csv');
        }
        else if($status == 'completed')
        {
            $details = Withdraw::where('status', 'completed')->with(['user', 'userpayaccounts','transaction'])->get();
            if(count($details) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['Name','Amount'.' ( '.config::get('settings.currency').' )','Email','Paymentgateway Name','Param1','Param2','Param3','Param4','Param5','Transaction Number','Request Date','Comments','Completed Date']);
                foreach ($details as $detail) {
                  $response = json_decode($detail->transaction->response, true);
                  $csv->insertOne([$detail->user->name,$detail->amount,$detail->user->email,$detail->userpayaccounts->payment->displayname,$detail->userpayaccounts->param1,$detail->userpayaccounts->param2,$detail->userpayaccounts->param3,$detail->userpayaccounts->param4,$detail->userpayaccounts->param5,$response['transaction_number'],$detail->created_at,$detail->comments_on_complete,$detail->completed_on]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Withdraw_Completed'.date('_d/m/Y_H:i').'.csv');
        }
        else if($status == 'rejected')
        {
            $details = Withdraw::where('status', 'rejected')->with(['user', 'userpayaccounts'])->get();
            if(count($details) > 0)
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['Name','Amount'.' ( '.config::get('settings.currency').' )','Email','Paymentgateway Name','Param1','Param2','Param3','Param4','Param5','Request Date','Comments','Rejected Date']);
                foreach ($details as $detail) {
                  $csv->insertOne([$detail->user->name,$detail->amount,$detail->user->email,$detail->userpayaccounts->payment->displayname,$detail->userpayaccounts->param1,$detail->userpayaccounts->param2,$detail->userpayaccounts->param3,$detail->userpayaccounts->param4,$detail->userpayaccounts->param5,$detail->created_at,$detail->comments_on_rejected,$detail->rejected_on]);
                }
            }
            else
            {
                $csv = Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne(['No Records Found']);
            }
            $csv->output('Withdraw_Rejected'.date('_d/m/Y_H:i').'.csv');
        }
    }    
}
