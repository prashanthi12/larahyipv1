<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Userprofile;
use App\Deposit;
use App\Withdraw;

class ActionController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($name)
    {
        $deposits = Deposit::with(['plan', 'user', 'paymentgateway','manualdeposit']);
        
        $depositlists = '';
        if ($name == 'newinvestment')
        {
            $depositlists = $deposits->where('status', 'new')->paginate(20);
        }
        elseif ($name =='maturedinvestment') 
        {
            $depositlists = $deposits->where('status', 'matured');
        }        

        $withdrawlists = Withdraw::where('status', 'pending')->with(['user', 'transaction', 'userpayaccounts'])->orderBy('id', 'DESC')->get();

        $userlists = Userprofile::whereNotIn('usergroup_id', array('1', '2', '3'))->where('kyc_verified', '=', 0)->where('kyc_doc', '!=', '')->with('user')->get();

        return view('admin.actionlist', [
                        'depositlists' => $depositlists,
                        'userlists' => $userlists,
                        'withdrawlists' => $withdrawlists,
                        'actionname' => $name
            ]);
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {      
                
        //
    }    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.withdraw.update');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       dd($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
   
}
