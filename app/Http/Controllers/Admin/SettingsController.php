<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Settings;

class SettingsController extends Controller
{
    public function index()
    {
    	$settings = Settings::whereIn('key', array('level_commission', 'withdraw_min_amount', 'withdraw_max_amount', 'fundtransfer_min_amount', 'fundtransfer_max_amount', 'fundtransfer_commission', 'monthly_withdraw_limit', 'force_email_verification_for_deposit', 'force_email_verification_for_fund_transfer', 'force_email_verification_for_withdraw', 'force_kyc_verification_for_deposit', 'force_kyc_verification_for_fund_transfer', 'force_kyc_verification_for_withdraw', 'force_deposit_down', 'force_withdraw_down', 'referral_commission_status', 'auto_withdrawal_status', 'auto_withdrawal_value', 'auto_withdraw_limit'))->get();
    	return view('admin.settingsbyvalue', [
			'settings' => $settings
		]);
    }
}
