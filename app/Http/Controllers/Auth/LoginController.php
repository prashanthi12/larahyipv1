<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Language;
use Session;
use Illuminate\Http\Request;
use Validator;
use App\User;
use Illuminate\Support\Facades\Input;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/myaccount/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $languages = Language::where('active', 1)->get();    
        Session::put('languageslist', $languages);      
        $this->middleware('guest', ['except' => 'logout']);
    }

     protected function validateLogin(Request $request)
    {

       Validator::extend('checklogin', function ($attribute, $value, $parameters, $validator) {
                 $login = User::where('email', Input::get('email'))->first();  
                
                   if (count($login)>0)
                   {

                    $user=User::where('id',$login->id)->with('userprofile')->first();
               
                    if(($user->userprofile->usergroup_id!=1)||($user->userprofile->usergroup_id!=2))
                    {
                            if($user->userprofile->active==1)
                            {
                                return TRUE;
                            }
                          return FALSE;    
                    }
                      return TRUE;    
                   }
                  return TRUE;          
        }, trans('auth.checklogin'));


        $this->validate($request, [
            $this->username() => 'required|string|checklogin',
            'password' => 'required|string',
        ]);
    }
}
