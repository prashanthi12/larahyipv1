<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Epin extends Model
{
    protected $fillable = [
        'coupon_value', 'coupon_code', 'status', 'owner', 'order_by', 'used_by'
    ];
}
