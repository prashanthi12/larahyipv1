<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Penalty extends Model
{
    use  SoftDeletes;    

    protected $fillable = [
        'transaction_id', 'amount','user_id','comments'
    ];

    protected $dates = ['deleted_at'];

    public function transaction () {
    	return $this->hasOne('App\Transaction');
    }  

    public function user() {
    	return $this->belongsTo('App\User')->withTrashed();
    }


}
