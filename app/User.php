<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use  App\Notifications\ResetPasswordNotification;
use Backpack\CRUD\CrudTrait;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\HyipNotifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;
use App\Traits\UserInfo;
use App\Traits\Common;
use Illuminate\Database\Eloquent\Collection;

class User extends Authenticatable
{
    use SoftDeletes;
    use HyipNotifiable;
    use CrudTrait;
    use HasRoles;
    use UserInfo;
    use Common;
    use \Nckg\Impersonate\Traits\CanImpersonate;
    use PresentableTrait;

    protected $presenter = "App\Presenters\UserPresenter";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'ip_address','google2fa_secret'
    ];

    protected $appends = [
        'referralgroup', 'userprofile', 'isPlaced', 'isActive', 'useralphakey', 'sponsorname', 'spillovername', 'uplines', 'openposition', 'SponsorOperatingAccount', 'isUserProfileCompleted', 'isKycApproved', 'referralCount', 'balance', 'activeDepositTotal', 'totalLifeTimeEarnings', 'lifeTimeDeposit', 'totalBonus', 'totalInterest', 'newDepositTotal', 'maturedDepositTotal', 'releasedDepositTotal', 'archivedDepositTotal', 'closedDepositTotal', 'depositByReferrals', 'pendingWithdraw', 'completedWithdraw', 'lifeTimeWithdraw'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'userprofile'
    ];

    protected $dates = ['deleted_at'];
    protected $referral_accountingcode = 2;
    protected $level_accountingcode = 3;
    protected $earnings_accountingcode = 4;
  

    public function sendPasswordResetNotification($token)
    {     
        $this->notify(new ResetPasswordNotification($token));
    }   

    public function usergroup() {
        return $this->belongsTo('App\Usergroup');
    }

    public function userprofile() {
        return $this->hasOne('App\Userprofile', 'user_id', 'id');
    }

    public function referrals() {
        return $this->hasMany('App\Userprofile', 'sponsor_id', 'id');
    }

    public function deposits() {
        return $this->hasMany('App\Deposit', 'user_id', 'id');
    }

    public function withdraws() {
        return $this->hasMany('App\Withdraw', 'user_id', 'id');
    }

    public function useraccounts() {
        return $this->hasMany('App\Useraccount', 'user_id', 'id');
    }

    public function transactions()
    {
        return $this->hasManyThrough('App\Transaction', 'App\Useraccount', 'user_id', 'account_id', 'id', 'id')->where('useraccounts.entity_type', 'Profile');
    }

    public function credittransactions()
    {
        return $this->hasManyThrough('App\Transaction', 'App\Useraccount', 'user_id', 'account_id', 'id', 'id')->whereIn('useraccounts.entity_type', ['Profile', 'Interest'])->where([['type','credit'], ['status', 1]])->whereNotIn('accounting_code_id', ['68','70','71','73']);
    } 

    public function debittransactions()
    {
        return $this->hasManyThrough('App\Transaction', 'App\Useraccount', 'user_id', 'account_id', 'id', 'id')->whereIn('useraccounts.entity_type', ['Profile', 'Interest'])->where([['type','debit'], ['status', 1]])->whereNotIn('accounting_code_id', ['69','72']);
    }

    public function referralCommissionTransactions()
    {
         //$referral_accountingcode = $this->getAccountingCode('matrix-deposit-via-referral-commission');
      
        return $this->hasManyThrough('App\Transaction', 'App\Useraccount', 'user_id', 'account_id', 'id', 'id')->where('useraccounts.entity_type', 'Profile')->where([['type','credit'], ['status', 1], ['accounting_code_id', $this->referral_accountingcode]]);
    }

    public function levelCommissionTransactions()
    {
         //$accountingcode = $this->getAccountingCode('matrix-deposit-via-level-commission');
        
        return $this->hasManyThrough('App\Transaction', 'App\Useraccount', 'user_id', 'account_id', 'id', 'id')->where('useraccounts.entity_type', 'Profile')->where([['type','credit'], ['status', 1], ['accounting_code_id', $this->level_accountingcode]]);
    }

    public function cycleOutEarnings()
    {
        // $accountingcode = $this->getAccountingCode('matrix-deposit-add-cycleout');
     
        return $this->hasManyThrough('App\Transaction', 'App\Useraccount', 'user_id', 'account_id', 'id', 'id')->where('useraccounts.entity_type', 'Profile')->where([['type','credit'], ['status', 1], ['accounting_code_id',$this->earnings_accountingcode]]);
    }

    public function placement() {
        return $this->hasOne('App\Placement', 'user_id');
    }  

    public function agent() {
        return $this->hasMany('App\Ticket', 'agent_id', 'id');
    }

     public function commentuser() {
        return $this->hasMany('App\TicketComments', 'user_id', 'id');
    }

    public function loguser()
    {
        return $this->hasMany('App\ActivityLog', 'causer_id', 'id');
    }

    public function ticketuser() {
        return $this->hasMany('App\Ticket', 'user_id', 'id');
    }

    public function testimonialuser() {
        return $this->hasMany('App\Testimonial', 'user_id', 'id');
    }

    public function penalties() {
        return $this->hasMany('App\Penalty', 'user_id', 'id');
    }
    
    public function mailmessage()
    {
        return $this->belongsTo('App\MailMessage');
    }

    public function session()
    {
        return $this->belongsTo('App\Session');
    }
     
    public function scopeByUserType($query, $group_id)
    {
        $query
            ->join('userprofiles', 'userprofiles.user_id', '=', 'users.id')
            ->where('userprofiles.usergroup_id', '=', $group_id);

        return $query;
        // return $query->where('usergroup_id', $id);
    }

    public function getIsplacedAttribute() {
        return $this->placement()->exists();
    }

    public function getSponsorAttribute() {
        $sponsorId = $this->userprofile->sponsor_id;
        if(is_null($sponsorId))
        {
            return null;
        }
        else
        {
            return User::where('id', $sponsorId)->first();
        }
    }

    public function getSponsornameAttribute() {

        if(isset($this->userprofile->sponsor->name))
        {
         return $this->userprofile->sponsor->name;
       }
       else
        return null;
    }

    public function getSponsorOperatingAccountAttribute() {
        $sponsor_account = Useraccount::where([
                                ['user_id', '=', $this->userprofile->sponsor_id ],
                                ['entity_type', '=', 'Profile']
                            ])->first();      
        return $sponsor_account->id; 
    }

    public function getSpillovernameAttribute() {
        if($this->placement()->exists()){
        $spilloverId = $this->placement->spillover_id;
        $spilloverUser = User::where('id', $spilloverId)->first();
            return $spilloverUser->name;
        }else {
            return null;
        }
    }

    public function getUseralphakeyAttribute() {
        if($this->placement()->exists()) {
            $network = $this->placement()->first();
            $networkKey = $network->alphakey;
            return $networkKey;
        }
            return null;
    }

    // public function getUplinesAttribute() {

    //     if($this->placement()->exists()){
    //         $network = $this->placement()->first();
    //         $alphakey = $network->alphakey;
    //         $start ="";
    //         $uplines = [];
    //         $alphakeyArray = preg_split( "/-/", $alphakey);
    //         foreach($alphakeyArray as $item) {
    //             $start = $start.'-'.$item;
    //             $uplineAlphakey = substr($start, 1);
    //             $uplineUser = Placement::where('alphakey', $uplineAlphakey)
    //                             ->first();
    //             $uplineUserId = $uplineUser->user_id;
    //             array_push($uplines, $uplineUserId);
    //         }
    //         return $uplines;
    //     }
    //         return null;
    // }
    public function getUplinesAttribute () 
    {       
        $totalLevelsForCommission = 12;
            
        if($this->placement()->exists()) 
        {
            $placement = $this->placement()->first();
            $alphaKey = $placement->alphakey;
            $uplines = [];
            if($alphaKey!='A')
            {
                $uplineKey=explode('-',$alphaKey);
                $uplineKeyLength=count($uplineKey);     
                if ($uplineKeyLength >= $totalLevelsForCommission) 
                {
                    $uplineCount = $totalLevelsForCommission;
                }
                else 
                {
                    $uplineCount = $uplineKeyLength;
                }
           // $uplines = [];

                $root_user_id=Placement::where([['alphakey', 'A'],['root_id',0],['spillover_id',0]])->pluck('user_id'); 

                $userid=explode('-B',$alphaKey);
                $userid = array_diff($userid,[$this->id]);//For Deposit User-Remove
                $userid = array_diff($userid, ["A"]);//For Root User Key-Remove
                array_unshift($userid ,$root_user_id[0]);//For Root User -Add

                $uplines = Placement::whereIn('user_id', $userid )->orderby('id','DESC')->pluck('user_id')->take($uplineCount)->toarray(); 
             
            //$uplines = User::whereIn('id', $userid )->orderby('id','DESC')->take($uplineCount)->get();
            }     
            return $uplines;
        } else {
            return null;
        }  
    }

    public function getOpenpositionAttribute() {
        if($this->placement()->exists()) {
            $network = $this->placement()->first();
            $alphakey = $network->alphakey;

            $openNode = Placement::where('alphakey', $alphakey)->where('user_id', null)->orderby('id','ASC')->first();
            $openNodeId = $openNode->id;
            return $openNodeId;
        }else {
            return null;
        }
    }

    public function getReferralgroupAttribute() 
    {
        if($this->userprofile->referral_group_id !== null) {
            return Referralgroup::where('id', $this->userprofile->referral_group_id)->first();
        }else {
            return null;
        }
    }

    public function getisUserProfileCompletedAttribute() {
        return $this->isUserProfileCompleted($this);
    }

    public function getisKycApprovedAttribute() {
        return $this->isKycApproved($this);
    }

    public function getisEmailVerifiedAttribute() {
        return $this->isEmailVerified($this);
    }

    public function getreferralCountAttribute() {
       return $this->referrals->count();
    }

    public function getBalanceAttribute() {
        return $this->credittransactions->sum('amount') - $this->debittransactions->sum('amount');
    }
    
    public function getActiveDepositTotalAttribute() {
        return $this->deposits->where('status', '=', 'active')->sum('amount');
    }

    public function earnings() {
        return $this->hasManyThrough('App\Transaction', 'App\Useraccount', 'user_id', 'account_id', 'id', 'id')->where('useraccounts.entity_type', 'Profile')->where([['type','credit'], ['status', 1] ])->whereIn('accounting_code_id', [$this->referral_accountingcode, $this->level_accountingcode,$this->earnings_accountingcode]);
        // return $this->referralCommissionTransactions->sum('amount') 
        //         + $this->levelCommissionTransactions->sum('amount') 
        //         + $this->cycleOutEarnings->sum('amount');
    }

    public function getLifeTimeDepositAttribute() {
        return $this->activeDepositTotal + $this->maturedDepositTotal + $this->releasedDepositTotal + $this->archivedDepositTotal;
    }

    public function getIsActiveAttribute() {
        return 1;
    }
    
    public function gettotalBonusAttribute() {
       // return $this->totalBonus($this);
        return $this->hasManyThrough('App\Transaction', 'App\Useraccount', 'user_id', 'account_id', 'id', 'id')->where('useraccounts.entity_type', 'Profile')->where([['type','credit'], ['status', 1], ['accounting_code_id', $this->earnings_accountingcode]]);
    }

    public function gettotalLifeTimeEarningsAttribute() {
        return $this->totalInterest + $this->totalBonus->sum('amount') + $this->referralCommissionTransactions->sum('amount') + $this->levelCommissionTransactions->sum('amount');
    }

    public function interest()
    {
        return $this->hasManyThrough('App\Interest', 'App\Deposit', 'user_id', 'deposit_id', 'id', 'id');
    }

    public function gettotalInterestAttribute() {
        return $this->interest->sum('amount');
       // return $this->totalInterest($this);
    }

    public function getNewDepositTotalAttribute() {
        return $this->deposits->where('status', '=', 'new')->sum('amount');
    }

    public function getMaturedDepositTotalAttribute() {
        return $this->deposits->where('status', '=', 'matured')->sum('amount');
    }

    public function getReleasedDepositTotalAttribute() {
        return $this->deposits->where('status', '=', 'released')->sum('amount');
    }

    public function getArchivedDepositTotalAttribute() {
        return $this->deposits->where('status', '=', 'archived')->sum('amount');
    }

    public function getClosedDepositTotalAttribute() {
        return $this->maturedDepositTotal + $this->releasedDepositTotal + $this->archivedDepositTotal;
    }

    public function getDepositByReferralsAttribute() {
       // return $this->getReferralDeposit($this);
        $myreferrals = $this->referrals->pluck('user_id');
        // $referrals = new Collection;
        // foreach ($myreferrals as $myreferral) {
        //     $deposits = Deposit::where([ 
        //                         ['status', '!=', 'new'],
        //                         ['user_id', '=', $myreferral]
        //                         ])->get();
        //     $userLifeTimeDeposit = $deposits ->sum('amount');
        //     $referrals->push($userLifeTimeDeposit);
        // }
        // return $referrals;

        // return $this->hasManyThrough('App\Userprofile', 'App\Deposit', 'user_id', 'amount', 'id', 'id')->where('active', '1')->where([['type','credit'], ['status', '!=', 'new'], ['user_id', $myreferrals]]);
    }

    public function getPendingWithdrawAttribute()
    {
        return $this->withdraws->where('status', '=', 'pending')->sum('amount');
    }

    public function getCompletedWithdrawAttribute()
    {
        return $this->withdraws->where('status', '=', 'completed')->sum('amount');
    }

    public function getLifeTimeWithdrawAttribute()
    {
        return $this->withdraws->whereNotIn('status', array('rejected', 'request'))->sum('amount');
    }

    public function manualdeposit() 
    {
        return $this->hasMany('App\ManualDeposit');
    }

}
