<?php

namespace App\Mail;
use App\Withdraw;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminWithdrawReject;

class WithdrawReject extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

       /**
     * The contact instance.
     *
     * @var Withdraw
     */
    protected $withdraw;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminWithdrawReject $withdraw)
    {
         $this->withdraw = $withdraw;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->withdraw);
      //  $user = User::where('id', $this->withdraw->user_id)->with('userprofile')->first();
        //dd($user);
        return $this->markdown('emails.withdraw.withdrawreject')
                    ->with([
                        'comments' => $this->withdraw->result->comments_on_rejected,
                        'amount' => $this->withdraw->result->amount,
                        'name' => $this->withdraw->user->name,
                        'signature' => trans('mail.signature'),
                    ]);
    }
}
