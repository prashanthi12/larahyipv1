<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Deposit;

class UserAssignedBankDetails extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $user,$deposit;
  
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $deposit)
    {
        $this->user = $user;
        $this->deposit = $deposit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
        $details = Deposit::where('id', $this->deposit->id)->with('plan')->first();       
        return $this->markdown('emails.deposit.userassignedbankdetails')
                    ->with([
                        'username' => $this->user->name,
                        'plan_name' => $details->plan->name,
                        'deposited_amount' => $details->amount,
                        'signature' => trans('mail.signature'),
        ]);
    }
}
