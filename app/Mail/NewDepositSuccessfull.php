<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Deposit;
// use App\Plan;
// use App\Transaction;
// use App\Paymentgateway;
use App\Events\UserNewDeposit;

class NewDepositSuccessfull extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $deposit;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserNewDeposit $deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $plan = Plan::where('id', $this->deposit->plan_id)->first();
        // //dd($plan);
        // $transaction = Transaction::where('id', $this->deposit->transaction_id)->first();
        // $response_json = json_decode($transaction->response, true);

        // $user = User::where('id', $this->deposit->user_id)->with('userprofile')->first();
        // $name = $user->name;

        // $paymentgatewayId = $this->deposit->paymentgateway_id;
        // if($paymentgatewayId == '17')
        // {
        //     $paymentgateway = Paymentgateway::where('id', $this->deposit->paymentgateway_id)->first();
        //     $response = json_decode($paymentgateway->params, true);
        // }

        // if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
        // {
        //     $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
        // }

        return $this->markdown('emails.deposit.newdepositsuccessfull')
                ->with([
                    'amount' => $this->deposit->deposit->amount,
                    'name' => $this->deposit->user->name,
                    'plan_name' => $this->deposit->deposit->plan_id,
                   // 'transaction_number' => $response_json['transaction_number'],
                    // 'bank_name' => $response['bank_name'],
                    // 'account_no' => $response['account_no'],
                    // 'payee_name' => $response['payee_name'],
                    'signature' => trans('mail.signature'),
        ]);
    }
}
