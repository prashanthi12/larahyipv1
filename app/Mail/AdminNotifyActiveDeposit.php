<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Deposit;
use App\Plan;

class AdminNotifyActiveDeposit extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $deposit;

    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Deposit $deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  

        $plan = Plan::where('id', $this->deposit->plan_id)->first(); 

         $user = User::where('id', Auth::id())->with('userprofile')->first();
         $name = $user->name;

         if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
         {
            $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
         } 

        return $this->markdown('emails.deposit.adminnotifyactivedeposit')
                    ->with([
                        'content' => trans('mail.admin_notify_active_deposit_content',             ['username' => $name]),
                        'plan_name' => $plan->name,
                        'deposited_amount' => $this->deposit->amount,
                        'active_deposit_link' => url('/admin/deposit/active'),
                        'active_deposit_link_text' => trans('mail.deposit_link_text'),
                        'signature' => trans('mail.signature'),
                    ]);
    }
}
