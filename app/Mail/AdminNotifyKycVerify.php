<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Userprofile;
use App\Events\AdminNotifyKycDocVerify;

class AdminNotifyKycVerify extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The transaction instance.
     *
     * @var transaction
     */
    protected $userprofile;

    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminNotifyKycDocVerify $userprofile)
    {
        $this->userprofile = $userprofile;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
       // $admin = User::find(2);
        return $this->markdown('emails.admin.adminnotifykycverify')
                    ->with([
                        'admin' => $this->userprofile->admin->name,
                        'message' => trans('mail.admin_notify_kyc_verify_content', ['name' => $this->userprofile->userprofile->user->name] ),
                        // 'username' => $this->userprofile->user->name,
                        'signature' => trans('mail.signature'),
                        'actionText' => trans('mail.click_to_login'),                   
                        'actionUrl' => url('/admin/users')
        ]);
    }
}
