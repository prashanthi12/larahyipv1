<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\ManualDeposit;

class AdminDepositBankDetails extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $add_details, $username, $transaction_number;
  
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ManualDeposit $add_details, $username, $transaction_number)
    {
        $this->add_details = $add_details;
        $this->username = $username;
        $this->transaction_number = $transaction_number;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {         
        if(count($this->add_details->country) > 0)
        {
            $country = $this->add_details->country->name;
        }   
        else
        {
            $country = '';
        }
        return $this->markdown('emails.deposit.adminbankdetails')
                    ->with([
                        'username' => $this->username,
                        'transaction_number' => $this->transaction_number,
                        'deposited_amount' => $this->add_details->deposit->amount,
                        'bankname' => $this->add_details->bank_name,
                        'swiftcode' => $this->add_details->swift_code,
                        'accountno' => $this->add_details->account_no,
                        'accountname' => $this->add_details->account_name,
                        'accountaddress' => $this->add_details->account_address,
                        'country' => $country,
                        'signature' => trans('mail.signature'),
        ]);
    }
}
