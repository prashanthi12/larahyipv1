<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Deposit;
// use App\Plan;
use App\Events\UserNewDeposit;

class AdminNotifyNewDeposit extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $deposit;

    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserNewDeposit $deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {        
        // $plan = Plan::where('id', $this->deposit->plan_id)->first();
        // //dd($plan);

        // $user = User::where('id', Auth::id())->with('userprofile')->first();
        // $name = $user->name;

        // if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
        // {
        //     $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
        // } 

        return $this->markdown('emails.deposit.adminnotifynewdeposit')
                    ->with([
                        'content' => trans('mail.admin_notify_new_deposit_content', ['username' => $this->deposit->deposit->name] ),
                        'new_deposit_link' => url('/admin/actions/newdeposit'),
                        'plan_name' => $this->deposit->deposit->plan_id,
                        'deposited_amount' => $this->deposit->deposit->amount,
                        'new_deposit_link_text' => trans('mail.deposit_link_text'),
                        'signature' => trans('mail.signature'),
        ]);
    }
}
