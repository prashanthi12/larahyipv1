<?php

namespace App\Mail;

use App\Invite;
use App\User;
use App\Userprofile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\InviteNewFriend;

class InviteFriend extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The invite instance.
     *
     * @var Invite
     */
    protected $invite;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(InviteNewFriend $invite)
    {
        $this->invite = $invite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = User::where('id', Auth::id())->with('userprofile')->first();
        //dd($user);
        return $this->markdown('emails.invite_friend')
                    ->with([
                        'url' => $this->invite->invite->link,
                        'message' => rawurldecode($this->invite->invite->message),
                        'sender' => $user->userprofile->firstname.' '. $user->userprofile->lastname,
                        'actionText' => trans('mail.invite_friend_action_button_text'),
                        'user_signature' => trans('mail.user_signature')
                    ]);
    }
}
