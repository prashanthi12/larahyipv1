<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Epin;

class CouponCodeSendMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The transaction instance.
     *
     * @var transaction
     */
    protected $user, $couponcode;
  
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Epin $couponcode)
    {
        $this->user = $user;
        $this->couponcode = $couponcode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        return $this->markdown('emails.deposit.couponcodesendmail')
                    ->with([
                        'name' => $this->user->name,
                        'amount' => $this->couponcode->coupon_value,
                        'couponcode' => $this->couponcode->coupon_code,
                        'signature' => trans('mail.signature')
        ]);
    }
}
