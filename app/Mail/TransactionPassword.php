<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\TransactionPasswordSaved;

class TransactionPassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    protected $userprofile;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TransactionPasswordSaved $userprofile)
    {
        $this->userprofile = $userprofile;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
     
        //dd($user);
        return $this->markdown('emails.profile.transactionpassword')
                    ->with([                        
                        'message' => trans('mail.transaction_password_changed'),
                        'name' => $this->userprofile->user->name,  
                        'signature' => trans('mail.signature'),
        ]);
    }
}