<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserTicketSend;

class StaffNotifyTicket extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The User instance.
     *
     * @var User
     */
    protected $ticket;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserTicketSend $ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        return $this->markdown('emails.ticket.staffnotifyticket')
                    ->with([                        
                        'user' => $this->ticket->ticket->user->name,
                        'name' => $this->ticket->ticket->staff->name,
                        'subject' => $this->ticket->ticket->subject,
                        'content' => rawurldecode($this->ticket->ticket->content),
                        'status' => $this->ticket->ticket->status->name,
                        'priority' => $this->ticket->ticket->priority->name,
                        'category' => $this->ticket->ticket->category->name,                               
                        'signature' => trans('mail.signature'),
                        'actionText' => trans('mail.click_to_login'),                   
                        'actionUrl' => url('/login')
        ]);
    }
}
