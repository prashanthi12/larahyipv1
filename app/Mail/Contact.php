<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The contact instance.
     *
     * @var Contact
     */
    protected $contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->contact);
        return $this->markdown('emails.contact')
                    ->with([
                        'queries' => $this->contact->qu,
                        'message' => $this->invite->message,
                        'sender' => $user->name,
                        'actionText' => 'Join Now',
                        'actionUrl' => url(),
                        'thanks_regards_text' => trans('mail.thanks_regards_text')
                    ]);
    }
}
