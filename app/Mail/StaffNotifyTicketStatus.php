<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Ticket;
// use App\TicketCategories;
// use App\TicketStatus;
// use App\TicketPriorities;
use App\Events\AdminUpdateTicketStatus;

class StaffNotifyTicketStatus extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The User instance.
     *
     * @var User
     */
    protected $statusresult;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminUpdateTicketStatus $statusresult)
    {
        $this->statusresult = $statusresult;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {     
        // $admin = User::find(2);     
        // $user = User::where('id', $this->statusresult->user_id)->first();
        // $staff = User::where('id', $this->statusresult->agent_id)->first();
        // $status = TicketStatus::where('id', $this->statusresult->status_id)->first();
        // $priority = TicketPriorities::where('id', $this->statusresult->priority_id)->first();
        // $category = TicketCategories::where('id', $this->statusresult->category_id)->first();

        return $this->markdown('emails.ticket.staffnotifyticketstatus')
                    ->with([  
                        'admin' => $this->statusresult->admin->name,                     
                        'user' => $this->statusresult->statusresult->user->name,
                        'name' => $this->statusresult->statusresult->agent->name,
                        'subject' => $this->statusresult->statusresult->subject,
                        'content' => rawurldecode($this->statusresult->statusresult->content),  
                        'status' => $this->statusresult->statusresult->status->name,
                        'priority' => $this->statusresult->statusresult->priority->name,
                        'category' => $this->statusresult->statusresult->category->name,                                 
                        'signature' => trans('mail.signature'),
                        'actionText' => trans('mail.click_to_login'),                   
                        'actionUrl' => url('/login')
                    ]);
    }
}
