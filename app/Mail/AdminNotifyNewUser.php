<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Events\NewUserRegister;

class AdminNotifyNewUser extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The User instance.
     *
     * @var User
     */
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.admin.adminnotifynewuser')
                    ->with([                  
                        'registered_user_name' => $this->user->name,
                        'ip_address' => request()->ip(),
                        'message' => trans('mail.admin_notification_new_user_content'),                                  
                       'signature' => trans('mail.signature'),
                       'actionText' => trans('mail.click'),                   
                       'actionUrl' => url('/superadmin/members')
        ]);
    }
}
