<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Deposit;
// use App\Plan;
// use App\Transaction;
use App\Events\UserActiveDeposit;

class ActiveDepositSuccessfull extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $deposit;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserActiveDeposit $deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $plan = Plan::where('id', $this->deposit->plan_id)->first();
        // //dd($this->deposit->paymentgateway_id);

        // $transaction = Transaction::where('id', $this->deposit->transaction_id)->first();
        // $response_json['transaction_number'] = "";
        
        // if($this->deposit->paymentgateway_id != 18)
        // {
        //     $response_json = json_decode($transaction->response, true);
        // }
        
        //  $user = User::where('id', $this->deposit->user_id)->with('userprofile')->first();
        //  $name = $user->name;

        //  if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
        //  {
        //     $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
        //  }
          
        return $this->markdown('emails.deposit.activedepositsuccessfull')
                ->with([
                    'amount' => $this->deposit->deposit->amount,
                    'name' => $this->deposit->user->name,
                    'plan_name' => $this->deposit->deposit->plan_id,
                   // 'transaction_number' => $response_json['transaction_number'],
                    'signature' => trans('mail.signature')
                ]);
    }
}
