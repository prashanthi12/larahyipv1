<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Deposit;
use App\Events\UserRegisterBonusDeposit;

class RegisterBonusDeposit extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $deposit;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserRegisterBonusDeposit $deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $user = User::where('id', $this->deposit->user_id)->with('userprofile')->first();
        // $name = $user->name;

        // if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
        // {
        //     $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
        // }
         
        return $this->markdown('emails.deposit.registerbonusdeposit')
                ->with([
                    'amount' => $this->deposit->deposit->amount,
                    'name' => $this->deposit->user->name,
                    'signature' => trans('mail.signature'),
        ]);
    }
}
