<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\StaffUpdateTicketStatus;

class AdminNotifyTicketStatus extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The User instance.
     *
     * @var User
     */
    protected $statusresult;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(StaffUpdateTicketStatus $statusresult)
    {
        $this->statusresult = $statusresult;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
        return $this->markdown('emails.ticket.adminnotifyticketstatus')
                    ->with([  
                        'name' => $this->statusresult->admin->name,                      
                        'user' => $this->statusresult->statusresult->user->name,
                        'staff' => $this->statusresult->statusresult->agent->name,
                        'subject' => $this->statusresult->statusresult->subject,
                        'content' => rawurldecode($this->statusresult->statusresult->content),  
                        'status' => $this->statusresult->statusresult->status->name,
                        'priority' => $this->statusresult->statusresult->priority->name,
                        'category' => $this->statusresult->statusresult->category->name,                                
                        'signature' => trans('mail.signature'),
        ]);
    }
}
