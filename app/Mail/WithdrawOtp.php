<?php

namespace App\Mail;
use App\Withdraw;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use App\Events\UserWithdrawOTP;

class WithdrawOtp extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

       /**
     * The contact instance.
     *
     * @var Withdraw
     */
    protected $withdraw;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserWithdrawOTP $withdraw)
    {
        $this->withdraw = $withdraw;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->withdraw);
        //$user = User::where('id', Auth::id())->with('userprofile')->first();
      //  $user = User::where('id', $this->withdraw->user_id)->with('userprofile')->first();
        //dd($user);
        return $this->markdown('emails.withdraw.withdrawotp')
                    ->with([
                        'otpcode' => $this->withdraw->result->param1,
                        'name' => $this->withdraw->userprofile->firstname.' '. $this->withdraw->userprofile->lastname,
                        'signature' => trans('mail.signature'),
                    ]);
    }
}
