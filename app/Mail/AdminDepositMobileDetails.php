<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\ManualDeposit;

class AdminDepositMobileDetails extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $add_details, $username, $transaction_number;

    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ManualDeposit $add_details, $username, $transaction_number)
    {
        $this->add_details = $add_details;
        $this->username = $username;
        $this->transaction_number = $transaction_number;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {     
        // dd($this->username);   
        return $this->markdown('emails.deposit.adminmobiledetails')
                    ->with([
                        'username' => $this->username,
                        'transaction_number' => $this->transaction_number,
                        'mobile' => $this->add_details->mobile,
                        'deposited_amount' => $this->add_details->deposit->amount,
                        'country' => $this->add_details->country->name,
                        'signature' => trans('mail.signature'),
        ]);
    }
}
