<?php

namespace App\Mail;
// use App\Contact;
use App\Events\ContactusAdded;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserContactus extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The contact instance.
     *
     * @var Contact
     */
    protected $contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContactusAdded $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->contact->contact->contactno);
        return $this->markdown('emails.usercontactmail')
                    ->with([
                        'contactno' => $this->contact->contact->contactno,
                        'skypeid' => $this->contact->contact->skype_gtalk,
                        'queries' => $this->contact->contact->queries,
                        'fromname' => $this->contact->contact->fullname,
                        'signature' => trans('mail.user_signature', array('name' => $this->contact->contact->fullname))
        ]);
    }
}
