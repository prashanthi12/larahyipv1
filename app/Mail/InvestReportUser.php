<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class InvestReportUser extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The details instance.
     *
     * @var details
     */
    protected $details;
    protected $withdrawuser;
    protected $admin;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details,$withdrawuser,$admin)
    {
        $this->details = $details; 
        $this->withdrawuser = $withdrawuser; 
        $this->admin = $admin; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       

         return $this->markdown('emails.admin.reportuser')
                    ->with([
                        'name' => $this->admin->name,
                        'withdrawuser' => $this->withdrawuser->user->name,
                        'investuser' => $this->details->user->name,
                        'signature' => trans('mail.signature'),
                    ]);
    }
}
