<?php

namespace App\Mail;
// use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminUserResetPassword;

class ResetPassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The token instance.
     *
     * @var Token
     */
    protected $token;

    /**
     * The userdetails instance.
     *
     * @var userdetails
     */
    protected $userdetails;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminUserResetPassword $userdetails)
    {
        $this->userdetails = $userdetails; 
       // $this->token = $token; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->token);
        //$user = User::where('id', Auth::id())->with('userprofile')->first();
        
        return $this->markdown('emails.admin.resetpassword')
                    ->with([
                        'resetlink' => url('/password/reset/'.$this->userdetails->token),
                        'resetlinktext' => trans('mail.resetlinktext'),
                        'message' => trans('mail.reset_password_message'),
                        'username' => $this->userdetails->user->name,                        
                        'signature' => trans('mail.signature'),
                    ]);
    }
}
