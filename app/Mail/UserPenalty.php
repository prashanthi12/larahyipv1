<?php

namespace App\Mail;
// use App\Penalty;
// use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminSendPenalty;

class UserPenalty extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

       /**
     * The contact instance.
     *
     * @var Penalty
     */
    protected $penalty;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminSendPenalty $penalty)
    {
        $this->penalty = $penalty;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $user = User::where('id', $this->penalty->user_id)->with('userprofile')->first();
        // $name = $user->name;

        //  if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
        //  {
        //     $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
        //  }

        return $this->markdown('emails.senduserpenalty')
                    ->with([
                        'comment' => $this->penalty->penalty->comments,
                        'amount' => $this->penalty->penalty->amount,
                        'name' => $this->penalty->user->name,
                        'signature' => trans('mail.signature'),
                    ]);
    }
}
