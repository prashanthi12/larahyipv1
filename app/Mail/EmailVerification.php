<?php

namespace App\Mail;
// use App\Userprofile;
// use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\Events\NewUserRegister;

class EmailVerification extends Mailable implements ShouldQueue
{
     /**
     * The contact instance.
     *
     * @var Contact
     */
    protected $userprofile;


    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userprofile)
    {
         $this->userprofile = $userprofile;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->userprofile);
      //  $user = User::find($this->userprofile->user_id);
        return $this->markdown('emails.emailverification')
                    ->with([
                        'link' => $this->userprofile->email_verification_code,
                        'name' => $this->userprofile->user->name,
                        'signature' => trans('mail.signature'),
                    ]);
    }
}
