<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserChangePassword;

class ChangePassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserChangePassword $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.profile.changepassword')
                    ->with([                        
                        'message' => trans('mail.password_changed'),
                        'name' => $this->user->user->name,  
                        'signature' => trans('mail.signature'),
                    ]);
    }
}
