<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Epin;

class EpinCouponCodeUsed extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The transaction instance.
     *
     * @var transaction
     */
    protected $deposit_result;
  
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Epin $deposit_result)
    {
       // $this->user = $user;
        $this->deposit_result = $deposit_result;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
      //  $ownerId = $this->deposit_result->owner;
        $userId = $this->deposit_result->used_by;
       // $owner = User::where('id', $ownerId)->pluck('name');
        $user = User::where('id', $userId)->first();

        return $this->markdown('emails.deposit.epincouponcodeused')
                    ->with([
                        'name' => $user->name,
                      //  'user' => $user,
                        'amount' => $this->deposit_result->coupon_value,
                      //  'couponcode' => $this->deposit_result->coupon_code,
                        'signature' => trans('mail.signature')
        ]);
    }
}
