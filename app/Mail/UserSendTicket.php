<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserTicketSend;

class UserSendTicket extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The User instance.
     *
     * @var User
     */
    protected $ticket;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserTicketSend $ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.ticket.usersendticket')
                    ->with([                        
                        'name' => $this->ticket->ticket->user->name,
                        'staff' => $this->ticket->ticket->agent->name,
                        'subject' => $this->ticket->ticket->subject,
                        'content' => rawurldecode($this->ticket->ticket->content),
                        'status' => $this->ticket->ticket->status->name,
                        'priority' => $this->ticket->ticket->priority->name,
                        'category' => $this->ticket->ticket->category->name,                                
                        'signature' => trans('mail.signature'),
        ]);
    }
}
