<?php

namespace App\Mail\Ewallet;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;


class ApproveFund extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $ewallet;

    public function __construct($ewallet)
    {
        //
         $this->ewallet = $ewallet;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
     


         $user = User::where('id', $this->ewallet['from_user_id'])->with('userprofile')->first();
         $name = $user->name;
         

         if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
         {
            $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
         } 



         $amount=$this->ewallet['amount'];
         $btc_amount=$this->ewallet['total_btc_amount'];
         $received_amount=$this->ewallet['received_amount'];
        
        return $this->subject('Approved E-Wallet Fund')
                    ->markdown('emails.ewallet.approvefund') ->with([
                    'name' => $name,                    
                    'amount'=>$amount,
                    'btc_amount'=>$btc_amount,
                    'approve_amount'=>$received_amount,
                ]);
    }
}
