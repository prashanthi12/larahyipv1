<?php

namespace App\Mail\Ewallet;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;


class AddFund extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $ewallet;

    public function __construct($ewallet)
    {
        //
         $this->ewallet = $ewallet;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
     


         $user = User::where('id', $this->ewallet['to_user_id'])->with('userprofile')->first();
         $name = $user->name;
         

         if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
         {
            $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
         } 


         $user_from = User::where('id', $this->ewallet['from_user_id'])->with('userprofile')->first();
         $fromname = $user_from->name;

         $amount=$this->ewallet['amount'];
         $btc_amount=$this->ewallet['total_btc_amount'];

        
        return $this->subject('Add Fund To E-Wallet')
                    ->markdown('emails.ewallet.addfund') ->with([
                    'name' => $name,
                    'fromname'=>$fromname,
                    'amount'=>$amount,
                    'btc_amount'=>$btc_amount,
                ]);
    }
}
