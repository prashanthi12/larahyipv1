<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Fundtransfer;
use App\Useraccount;
use App\Events\FundTransferMail;

class FundTransferReceiver extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The transaction instance.
     *
     * @var transaction
     */
    protected $result;
  
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(FundTransferMail $result)
    {
        $this->result = $result;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        // $fromuser = Useraccount::where('id', $this->result->from_account_id)->first();
        // $senderID = $fromuser->user_id;
        // $sender = User::where('id', $senderID)->first();
       // $touser = Useraccount::where('id', $this->result->to_account_id)->first();
        // $receiverID = $touser->user_id;
        // $receiver = User::where('id', $receiverID)->first();
        return $this->markdown('emails.fundtransfer.fundtransferreceiver')
                    ->with([
                        'amount' => $this->result->result->amount,
                        'receiver' => $this->result->user->name,
                        'sender' => $this->result->sender->name,
                        'signature' => trans('mail.signature'),
                        'actionText' => trans('mail.click_to_login'),  
                        'account_no' => $this->result->result->to_account_id,                 
                        'actionUrl' => url('/myaccount/fundtransfer/type/received')
        ]);
    }
}
