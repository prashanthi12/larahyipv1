<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketCategories extends Model
{
    protected $fillable = [
        'name', 'color'
    ];

    public function category()
    {
        return $this->hasMany('App\Ticket', 'category_id', 'id');
    }

    public function categorylist()
    {
        return $this->hasMany('App\TicketCategoriesUsers', 'category_id', 'id');
    }

    public function agentcategory()
    {
        return $this->belongsToMany('App\User');
    }
}
