<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CustomCollection;
use Illuminate\Database\Eloquent\SoftDeletes;
class Payment extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'credit_transaction_id', 'debit_transaction_id','amount','type', 'comments',
    ];
    protected $dates = ['deleted_at'];

    public function credit() {
    	return $this->hasOne('App\Transaction', 'credit_transaction_id', 'id');
    }

    public function debit() {
    	return $this->hasOne('App\Transaction', 'debit_transaction_id', 'id');
    }

     /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new CustomCollection($models);
    }
}
