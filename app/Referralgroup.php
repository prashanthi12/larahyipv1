<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referralgroup extends Model
{
    protected $fillable = [
        'name', 'referral_commission', 'level_commission', 'min_amount', 'active', 'is_default'
    ];

    protected $casts = [
        'level_commission' => 'array',
    ];

    public function users() {
    	return $this->hasManyThrough(  
    		'App\User',
            'App\Userprofile',
            'referral_group_id', 
            'id', 
            'id', 
            'id' 
        );
    }
}
