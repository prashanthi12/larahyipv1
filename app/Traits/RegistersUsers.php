<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use App\Traits\RedirectsUsers;
use Illuminate\Support\Facades\Redirect;
// use App\Classes\BlockIo;
// use App\Paymentgateway;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerification;
use App\Mail\RegisterNewUser;
use App\Mail\AdminNotifyNewUser;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();    

        event(new Registered($user = $this->create($request->all())));

        if (\Session::get('mailchimperror') != '' || \Session::get('sponsererror') != '')
        {        
            session()->flash('error', \Session::get('mailchimperror'));
            session()->flash('faildefaultsponser', \Session::get('sponsererror'));
            \Session::forget('mailchimperror');    
            \Session::forget('sponsererror');      
            return back();   
        }  

        $this->guard()->login($user);
        return $this->registered($request, $user)
                ?: redirect($this->redirectPath());        
       
    }   

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    } 

    public function registerMail($data)
    {
        // dd($data);
        Mail::to($data->user->email)->queue(new EmailVerification($data->userprofile));         
        Mail::to($data->user->email)->queue(new RegisterNewUser($data->user));
        Mail::to($data->admin->email)->queue(new AdminNotifyNewUser($data->user)); 
    }

}
