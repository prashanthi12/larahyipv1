<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

use App\User;
use App\Deposit;
use App\Plan;
use App\Interest;
use App\Transaction;
use App\Useraccount;
use App\Accountingcode;
use App\Helpers\HyipHelper;

use App\Traits\LogActivity;

use Carbon\Carbon;

trait CalculatesInterests
{
    use LogActivity;

    private $_plan_type_id, $command_name, $sysuser;

    private $deposit, $plan, $interestamount, $account, $next_interest_date;

    public function setPlanTypeID($id)
    {
        $this->_plan_type_id = $id;
        // $this->admin = User::find(USER_ROLE_SUPERADMIN);
        //$this->sysuser = User::ByUserType(USER_ROLE_SYSTEM)->first();
        // dd($this->sysuser);
        // echo (new Carbon);exit;
    }

    public function get_deposits()
    {
        // dd("This plan type is: ", $this->plan_type);
        // dd("Plan ID: ", $plan_id);

        return Deposit::active()->planid($this->plan_id)->get();
    }

    public function get_plans()
    {
        // dd("Plans: ", $plan);
        // $comments = App\Post::find(1)->comments()->where('title', 'foo')->first();
        return Plan::active()->where('plantype_id', $this->_plan_type_id)->with('deposits')->get();
    }

    public function calcuateInterests($activedeposits, $plan)
    {
        // dd($activedeposits);
        $this->plan = $plan;
        $this->deposit=$activedeposits;
      //  if (!$activedeposits->isEmpty())
        if (count($activedeposits)>0)
        {
          //  foreach ($activedeposits as $this->deposit)
          //  {
                
                // echo "\n\nDeposit details:\n", dd($this->deposit);

                // $deposit_id = $deposit->id;
                if ($this->deposit->paymentgateway_id == 1 || $this->deposit->paymentgateway_id == 20)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-bankwire']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code_id);
                } 
                elseif ($this->deposit->paymentgateway_id == 2)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-paypal']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                elseif ($this->deposit->paymentgateway_id == 3)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-stpay']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                elseif ($this->deposit->paymentgateway_id == 4)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-payeer']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                elseif ($this->deposit->paymentgateway_id == 5)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-advcash']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                elseif ($this->deposit->paymentgateway_id == 6)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-blockio']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                elseif ($this->deposit->paymentgateway_id == 7)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-coinpayment']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                elseif ($this->deposit->paymentgateway_id == 8)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-bitpay']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                }
                elseif ($this->deposit->paymentgateway_id == 9)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-bitcoin-direct']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                }  
                elseif ($this->deposit->paymentgateway_id == 11)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-skrill']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                elseif ($this->deposit->paymentgateway_id == 12)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-okpay']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                elseif ($this->deposit->paymentgateway_id == 13)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-reinvest']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                }           
                elseif ($this->deposit->paymentgateway_id == 14)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-register-deposit']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                elseif ($this->deposit->paymentgateway_id == 15)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-perfectmoney']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                elseif ($this->deposit->paymentgateway_id == 16)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-neteller']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                elseif ($this->deposit->paymentgateway_id == 17)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-cheque']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                elseif ($this->deposit->paymentgateway_id == 21)
                {
                    $accounting_code = Accountingcode::where([
                    ['active', '=', "1"],
                    ['accounting_code', '=', 'interest-via-mobile']
                    ])->get(['id'])->toArray();
                    $this->accounting_code_id = $accounting_code[0]['id'];
                    //dd($accounting_code);
                } 
                $this->account = Useraccount::where([['entity_reference_id', '=', $this->deposit->id],['entity_type', '=', 'Interest'] ])->first();
                //echo "\n\nAccount details:\n", dump($this->account);

                $this->interestamount = $this->_calculateInterestAmount();
                //dd($this->interestamount);

                while ($this->next_interest_date = $this->getNextInterestDate())
                {
                    // echo 'test';
                    if ($this->_plan_type_id == PLANTYPE_DAILY_BUSINESS)
                    {
                        // echo "\n\n"
                    }

                    DB::transaction(function () {
                        // echo "\nNext_interest_date got is ", $next_interest_date; // exit;
                        $interest = self::createNewInterest($this->deposit, $this->interestamount, $this->next_interest_date);
                        // dump($interest);

                        $transaction = self::createTransactionForInterest($interest, $this->account, $this->accounting_code_id);
                        // dd($transaction);

                        $tranaction_updated = self::updateTransactionIDforInterest($interest, $transaction);
                        // dd($tranaction_updated);

                        // $this->doActivityLog(
                        //     $interest,
                        //     $this->sysuser,
                        //     [
                        //         'plan name' => $this->plan->name,
                        //         'deposit_amount' => $this->deposit->amount,
                        //         'interest_id' => $interest->id,
                        //         'interest_amount' => $this->interestamount
                        //     ],
                        //     'Interest',
                        //     $interest->comment
                        // );
                    });
                }
            //}
        }
    }

    private function _calculateInterestAmount()
    {
        // dd($deposit);
        $interestamount = (($this->deposit->amount) * ($this->deposit->plan->interest_rate) ) / 100;

        return $interestamount;
    }

    public function createNewInterest($deposit, $interestamount, $effectivedate)
    {

        // echo "\n\nInterestCount:", Interest::getExistingRecordCount($this->deposit->id);
        // echo "\n\nInterestCount:",
        $prevCount = Interest::DepositId($this->deposit->id)->count();

        // $label = HyipHelper::plan_label($this->_plan_type_id);

        $interest = new Interest;
        $interest->amount = $this->interestamount;
        $interest->deposit_id = $this->deposit->id;
        $interest->comment = 'Interest credited for '. $this->deposit->id .' for '. ucfirst($this->plan->name) .' #'. ++$prevCount;
        $interest->effective_date = $this->next_interest_date;
        // $interest->created_at = Carbon::now();
        $interest->created_at = new Carbon;
        $interest->save();

        // dd($interest);
        return $interest;
    }

    public function createTransactionForInterest($interest, $account, $accounting_code_id)
    {
        //dd($accounting_code_id);
        $transaction = new Transaction;
        $transaction->account_id = $account->id;
        $transaction->amount = $interest->amount;
        $transaction->type = "credit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code_id;
        $transaction->request = "{object:undefined}";
        $transaction->response = "{object:undefined}";
        $transaction->save();

        return $transaction;
    }

    public function updateTransactionIDforInterest(Interest $interest, Transaction $transaction)
    {
        $interest->transaction_id = $transaction->id;
        $interest->save();
    }

    public function getNextInterestDate()
    {
        // dd($this->deposit);
        // echo "\nLast_interest_date: ",
        $last_interest_date = DB::table('interests')->where('deposit_id', $this->deposit->id)->max('effective_date');

        if ($last_interest_date == null)
        {
            // dd($deposit);
            // Carbon::createFromTimestamp($deposit->created_at);
            // $datetime = (new Carbon($deposit->approved_on))->endOfDay()->addSecond(1)->toDateTimeString();
            /*$datetime = $this->_calculateNextInterestDate($this->deposit->approved_on);
            // echo "null, so use ", $datetime; // exit;

            if ($datetime->gt(Carbon::now()))
            {
                // dd(Carbon::now(), 'end of time');
                return false;
            }

            return $datetime->toDateTimeString();*/

            $last_interest_date = $this->deposit->approved_on;
        }

        // $next_interest_date = (new Carbon($last_interest_date))->endOfDay()->addSecond(1);
        $next_interest_date = $this->_calculateNextInterestDate($last_interest_date);
        // echo ", so use ", $next_interest_date; // exit;

        if ($next_interest_date->gt(Carbon::now()))
        {
            // dd(Carbon::now(), 'end of time');
            return false;
        }

        // dd($next_interest_date);
        return $next_interest_date->toDateTimeString();
    }

    private function _calculateNextInterestDate($date)
    {
        $return_date = new Carbon($date);
        // $check_date = new Carbon($date);
        $check_date1 = new Carbon($date);

        // echo "\n\n", $return_date, ' ', $check_date1->firstOfQuarter()->addMonth(3);
                // ($check_date1->endOfDay()->addSecond(1)->isSaturday() == true ? "sat" : "no");

        switch ($this->_plan_type_id)
        {
            case PLANTYPE_DAILY:
                return $return_date->endOfDay()->addSecond(1);

            case PLANTYPE_DAILY_BUSINESS:
                return ($return_date->endOfDay()->addSecond(1)->isSaturday() == true) ?
                        $return_date->addDays(2) :
                        $return_date;

            case PLANTYPE_HOURLY:
                // $date = '2017-03-24 23:45:56.000000';
                // dd((new Carbon($date)), (new Carbon($date))->minute(0)->second(0)->addHour(1));
                return $return_date->minute(0)->second(0)->addHour(1);

            case PLANTYPE_WEEKLY:
                return $return_date->next(Carbon::MONDAY);

            case PLANTYPE_MONTHLY:
                return $return_date->addMonth(1)->startOfMonth();

            case PLANTYPE_QUATERLY:
                return $return_date->firstOfQuarter()->addMonth(3);

            case PLANTYPE_YEARLY:
                return $return_date->addYear(1)->startOfYear();
        }

        return false;
    }

}
