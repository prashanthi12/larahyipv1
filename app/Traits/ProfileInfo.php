<?php 

namespace App\Traits;

use App\User;
use App\Userprofile;
use App\Accountingcode;
use App\Transaction;

trait ProfileInfo {

    public function getRefUrl($user) {
        return url('/ref').'/'.$user->name;
    }

    public function hasProfile(user $user) {
            return Userprofile::where('user_id', $user->id)->exists();
       }

    public function checkProfileForCompletion (user $user) {
        $userprofile = Userprofile::where('user_id', $user->id)->first();
        if ( is_null($userprofile->firstname) || is_null($userprofile->lastname) || is_null($userprofile->mobile) 
                || is_null($userprofile->country)  || is_null($userprofile->ssn) || is_null($userprofile->kyc_doc) )
                {
                    return false;
                } else {
                     return true;
                }
        }    

    public function totalReferralCommission(user $user) 
    {
        $useraccounts = $user->useraccounts();
        $myOpertiveAccount = $user->useraccounts->where('accounttype_id', 3 )->first();
        $operativeAccountId = $myOpertiveAccount->id;

        $referralResult  = Accountingcode::where([
        ['active', '=', "1"],
        ['accounting_code', '=', "income-via-referral-commission"],
        ])->get(['id'])->toArray();
        $referral_code = $referralResult[0]['id'];

        $totalReferralCommission = Transaction::where([
                                                ['account_id', '=', $operativeAccountId],
                                                ['type','=','credit'],
                                                ['accounting_code_id', '=', $referral_code]
                                            ])->sum('amount');
        return $totalReferralCommission;
    }

    public function totalLevelCommission(user $user) 
    {
        $useraccounts = $user->useraccounts();
        $myOpertiveAccount = $user->useraccounts->where('accounttype_id', 3 )->first();
        $operativeAccountId = $myOpertiveAccount->id;

        $levelcommResult  = Accountingcode::where([
        ['active', '=', "1"],
        ['accounting_code', '=', "income-via-level-commission"],
        ])->get(['id'])->toArray();
        $level_comm_code = $levelcommResult[0]['id'];


        $totalLevelCommissions = Transaction::where([
                                                ['account_id', '=', $operativeAccountId],
                                                ['type','=','credit'],
                                                ['accounting_code_id', '=', $level_comm_code]
                                            ])->sum('amount');
        return $totalLevelCommissions;
    }

    public function totalBonus(user $user) 
    {
        $useraccounts = $user->useraccounts();
        $myOpertiveAccount = $user->useraccounts->where('accounttype_id', 3 )->first();
        $operativeAccountId = $myOpertiveAccount->id;

        $bonusResult  = Accountingcode::where([
        ['active', '=', "1"],
        ['accounting_code', '=', "income-as-bonus"],
        ])->get(['id'])->toArray();
        $bonus_code = $bonusResult[0]['id'];

        $totalbonus = Transaction::where([
                                                ['account_id', '=', $operativeAccountId],
                                                ['type','=','credit'],
                                                ['accounting_code_id', '=', $bonus_code]
                                            ])->sum('amount');
        return $totalbonus;
    }

    public function totalLifeTimeEarnings(user $user) 
    {
        $totalLifeTimeEarnings = session('totalinterest') + $this->totalReferralCommission($user) + $this->totalLevelCommission($user) + $this->totalBonus($user);

        return $totalLifeTimeEarnings;

    }

 }