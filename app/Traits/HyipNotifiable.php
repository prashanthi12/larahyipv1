<?php

namespace App\Traits;

use Illuminate\Notifications;

trait HyipNotifiable
{
    use Notifications\Notifiable;

    // Specify Slack Webhook URL to route notifications to
    public function routeNotificationForSlack() {
        // return $this->slack_webhook_url;
        // dd(SLACK_WEBHOOK_URL);
        return config('settings.slack_webhook');
    }
}