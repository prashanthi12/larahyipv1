<?php 

namespace App\Traits;

use App\User;
use App\Userprofile;
use App\Useraccount;
use App\Transaction;
use Carbon\Carbon;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\CreateNewUser;
use Illuminate\Bus\Queueable;
use App\Plan;
use App\Registrationbonus;
use App\Traits\LogActivity;
use App\Traits\DepositProcess;
use Config;
use App\Referralgroup;
use App\Events\AdminsCreateNewUser;
use Event;

trait NewUser {

    use DepositProcess, LogActivity;

	public function createnewuser($request) 
	{
       // dd($request);
        // $ip = \Request::ip();   //'157.50.21.39';  India IP
        // $signupCountry = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        // $user->ip_address = $ip;
        // $user->signup_country = $signupCountry['country'];
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();        

        if ($user->save())
        {          
            if ($request->usergroup == 4)
            {
                $this->doActivityLog(
                $user,
                $user,
                ['ip' => request()->ip()],
                'register',
                'User Registered'
                );  

                $referralgroup = Referralgroup::where([
                            ['active', '1'],
                            ['is_default', '1'],
                        ])->first();
              
                $sponsorUser = User::where('email', Config::get('settings.default_sponser'))->first();
                $sponsor = $sponsorUser->id;           

                $userprofile = new Userprofile;
                $userprofile->user_id = $user->id;
                $userprofile->usergroup_id = $request->usergroup;
                $userprofile->sponsor_id = $sponsor;
                $userprofile->active = 1;
                $userprofile->referral_group_id = $referralgroup->id;
                $userprofile->email_verified = 1;
                $userprofile->save();

                $account_no = "U-".(100000 + $user->id )."-"."1";
                $account = new Useraccount;
                $account->account_no = $account_no;
                $account->description = "User Operative Account";
                $account->user_id = $user->id;
                $account->accounttype_id = "3";
                $account->entity_reference_id = $userprofile->id;
                $account->entity_type = "Profile";
                $account->save();

                $transaction = new Transaction;
                $transaction->account_id = $account->id;
                $transaction->amount = "0.0";
                $transaction->type = "credit";
                $transaction->status ="1";
                $transaction->accounting_code_id = "1";
                $transaction->request = "{object:undefined}";
                $transaction->response = "{object:undefined}";
                $transaction->save();

                $password = $request->password;
                //firing an event
                Event::fire(new AdminsCreateNewUser($user, $password));

                //Mail::to($user->email)->queue(new CreateNewUser($user, $request->password)); 
                 //Register Bonus 
                $registerbonus = Registrationbonus::where('status', "1")->first();
                if ($registerbonus)
                {

                     $plan = Plan::where('id', '=', $registerbonus->plan)->first();

                    $array = [
                            'amount' => $registerbonus->value,
                            'plan' => $plan->id,
                            'paymentgateway' => 14,
                            'transaction_id' => uniqid(),
                            'user_id' => $user->id
                        ];
                        $request = (object) $array;

                    $deposit = $this->makeDeposit($request);
                    $depositapprove = $this->approve($deposit->id);
                }     

                if ($userprofile->save() && $account->save() && $transaction->save())
                {                   
                    session()->flash('successmessage', 'User Created Successfully');
                    return TRUE;
                }
                else
                {
                    session()->flash('errormessage','User Created  Failed!!!.');
                    return FALSE;
                }
            }
            else
            {
                $activity_desc = $request->usergroup == 2 ? 'Admin Registered' : 'Staff Registered';
                $this->doActivityLog(
                    $user,
                    $user,
                    ['ip' => request()->ip()],
                    'register',
                    $activity_desc
                );      
                $userprofile = new Userprofile;
                $userprofile->user_id = $user->id;
                $userprofile->usergroup_id = $request->usergroup;
                $userprofile->active = 1;
                $userprofile->save();

                if($request->usergroup == 2)
                {
                    $account_no = "U-100000-2";      // "U-".(100000 + $user->id )."-"."1";      
                   // dd($account_no);
                    $account = new Useraccount;
                    $account->account_no = $account_no;
                    $account->description = "User Operative Account";
                    $account->user_id = $user->id;
                    $account->accounttype_id = "3";
                    $account->entity_reference_id = $userprofile->id;
                    $account->entity_type = "Profile";
                    $account->save();
                }

                if ($userprofile->save())
                {
                    $password = $request->password;
                    //firing an event
                    Event::fire(new AdminsCreateNewUser($user, $password));
                
                   // Mail::to($user->email)->queue(new CreateNewUser($user, $request->password));
                    $request->session()->flash('successmessage', 'User Created Successfully');
                    return TRUE;
                }
                else
                {
                    $request->session()->flash('errormessage','User Created  Failed!!!.');
                    return FALSE;
                }                
            }
        }
        else
        {
            $request->session()->flash('errormessage','User Created  Failed!!!.');
            return FALSE;
        }        
	}
	
 }