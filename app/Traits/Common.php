<?php 

namespace App\Traits;
use App\Settings;
use App\User;
use App\Transaction;
use App\Accountingcode;
use App\MailMessage;
use App\Userprofile;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailToUser;
use Illuminate\Support\Facades\Auth;

trait Common 
{
    public function getSettingValue($key) 
    {
        $value = Settings::where('key',$key)->get();
        $value = $value[0]['value'];
        return $value;
    }

    public function getBalance(user $user)
    {   
        $allUserAccount = $user->useraccounts->whereIn('accounttype_id', [3, 5])->pluck('id')->toarray();

        $creditTransactions  = Transaction::whereIn(
                        'account_id' ,$allUserAccount)
                        ->where([['type', '=', 'credit']])->whereNotIn('accounting_code_id', ['68','70','71','73'])->sum('amount');  
            
        $debitTransactions  = Transaction::whereIn(
                        'account_id' ,$allUserAccount)
                        ->where([['type', '=', 'debit']])->whereNotIn('accounting_code_id', ['69','72'])->sum('amount');
            
        $balance = $creditTransactions - $debitTransactions;
        return $balance;
    }

    public function getAccountingCode($account_name) 
    {   
        $accounting_code  = Accountingcode::where([
                        ['active', '=', "1"],
                        ['accounting_code', $account_name]])->get(['id'])->toArray();     
        $accounting_code = $accounting_code[0]['id'];
        return $accounting_code;       
    }

    public function adminSendMassMail($request)
    {
        $request = (object)$request;
        if($request->to == 'active')
        {
            $massmail = Userprofile::where([['active','1'],['usergroup_id','4']])->with('user')->get();
        }

        if($request->to == 'suspend')
        {
            $massmail = Userprofile::where([['active','0'],['usergroup_id','4']])->with('user')->get();
        }

        if($request->to == 'all')
        {
            $massmail = Userprofile::where('usergroup_id','4')->get();
        }
        
        if(count($massmail)>0)
        {
            $batchCount = MailMessage::max('batch');
            if(is_null($batchCount))
            {
                $batchCount = 0;
            }
            $batch = $batchCount + 1;
            foreach ($massmail as $key => $value) 
            {
                $admin = User::find(2);
                $create = [
                    'from_user_id' => $admin->id,
                    'to_user_id' => $value->user->id,
                    'subject' => $request->subject,
                    'message' => $request->message,  
                    'type' => $request->to,
                    'batch' => $batch,
                ];
                $massmail = MailMessage::create($create); 
                $message = (new SendMailToUser($massmail))
                ->onQueue('massmail');

                Mail::to($value->user->email)->queue($message);
            } 
        }
    }

}