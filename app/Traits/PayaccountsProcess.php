<?php 

namespace App\Traits;

use App\User;
use App\Userpayaccounts;
use App\Paymentgateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

trait PayaccountsProcess {

    public function bankwire($request, $current) 
    {
        $user_pay = new Userpayaccounts;
        $user_pay->user_id = Auth::id();
        $user_pay->paymentgateways_id = $request->paymentid;
        $user_pay->active = 1;
        $user_pay->current = $current;
        $user_pay->param1 = $request->bank_name;
        $user_pay->param2 = $request->swift_code;
        $user_pay->param3 = $request->account_no;
        $user_pay->param4 = $request->account_name;
        $user_pay->param5 = $request->account_address;
        $user_pay->save();
    } 

    public function paypal($request, $current) 
    {        
        $user_pay = new Userpayaccounts;
        $user_pay->user_id = Auth::id();
        $user_pay->paymentgateways_id = $request->paymentid;
        $user_pay->active = 1;
        $user_pay->current = $current;
        $user_pay->param1 = $request->paypal_id;
        $user_pay->save();
    } 

    public function stpay($request, $current) 
    {        
         $user_pay = new Userpayaccounts;
        $user_pay->user_id = Auth::id();
        $user_pay->paymentgateways_id = $request->paymentid;
        $user_pay->active = 1;
        $user_pay->current = $current;
        $user_pay->param1 = $request->stpayname;
        $user_pay->save();
    }

    public function payeer($request, $current) 
    {        
        $user_pay = new Userpayaccounts;
        $user_pay->user_id = Auth::id();
        $user_pay->paymentgateways_id = $request->paymentid;
        $user_pay->active = 1;
        $user_pay->current = $current;
        $user_pay->param1 = $request->payeer_id;
        $user_pay->param2 = $request->payeer_email;
        $user_pay->save();
    } 

    public function advcash($request, $current) 
    {        
       $user_pay = new Userpayaccounts;
        $user_pay->user_id = Auth::id();
        $user_pay->paymentgateways_id = $request->paymentid;
        $user_pay->active = 1;
        $user_pay->current = $current;
        $user_pay->param1 = $request->acc_email_id;
        $user_pay->save();
    }

    public function bitcoin($request, $current) 
    {        
        $user_pay = new Userpayaccounts;
        $user_pay->user_id = Auth::id();
        $user_pay->paymentgateways_id = $request->paymentid;
        $user_pay->active = 1;
        $user_pay->current = $current;
        $user_pay->param1 = $request->coinname;
        $user_pay->param2 = $request->coincode;
        $user_pay->save();
    } 

    public function skrill($request, $current) 
    {        
         $user_pay = new Userpayaccounts;
        $user_pay->user_id = Auth::id();
        $user_pay->paymentgateways_id = $request->paymentid;
        $user_pay->active = 1;
        $user_pay->current = $current;
        $user_pay->param1 = $request->email_id;
        $user_pay->save();
    }   

    public function okpay($request, $current) 
    {        
        $user_pay = new Userpayaccounts;
        $user_pay->user_id = Auth::id();
        $user_pay->paymentgateways_id = $request->paymentid;
        $user_pay->active = 1;
        $user_pay->current = $current;
        $user_pay->param1 = $request->payid;
        $user_pay->param2 = $request->paynumber;
        $user_pay->save();
    } 

    public function perfectmoney($request, $current) 
    {        
        $user_pay = new Userpayaccounts;
        $user_pay->user_id = Auth::id();
        $user_pay->paymentgateways_id = $request->paymentid;
        $user_pay->active = 1;
        $user_pay->current = $current;
        $user_pay->param1 = $request->payeeaccount;
        $user_pay->save();
    } 

    public function neteller($request, $current) 
    {        
        $user_pay = new Userpayaccounts;
        $user_pay->user_id = Auth::id();
        $user_pay->paymentgateways_id = $request->paymentid;
        $user_pay->active = 1;
        $user_pay->current = $current;
        $user_pay->param1 = $request->merchant_id;
        $user_pay->param2 = $request->merchant_key;
        $user_pay->save();
    }

    public function bitcoinWallet($request,$user_id,$current,$status) 
    {        
        $user_pay = new Userpayaccounts;
        $user_pay->user_id = $user_id;
        $user_pay->paymentgateways_id = $request->paymentid;
        $user_pay->active = $status;
        $user_pay->current = $current;
        $user_pay->btc_label = $request->label;
        $user_pay->btc_address = $request->btc_address;
        $user_pay->save();
    } 
     public function mobile($request,$user_id,$current,$status) 
    {        
        $user_pay = new Userpayaccounts;
        $user_pay->user_id = $user_id;
        $user_pay->paymentgateways_id = $request->paymentid;
        $user_pay->active = $status;
        $user_pay->current = $current;
        $user_pay->param9 = $request->mobile_no;
        $user_pay->country_id = $request->country;
        $user_pay->save();
    }                                  

 }