<?php

namespace App\Traits;

use App\User;
use App\Plan;
use App\Paymentgateway;
use App\Useraccount;
use App\Transaction;
use App\Deposit;
use App\Accountingcode;
use App\Bonus;
use App\Settings;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Notifications\UserMadeDeposit;
use App\Traits\HyipNotifiable;
use App\Traits\PlacementProcessor;
use App\Traits\CommissionProcessor;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewDepositSuccessfull;
use App\Mail\AdminNotifyNewDeposit;
use App\Mail\ActiveDepositSuccessfull;
use App\Mail\AdminNotifyActiveDeposit;
// use App\Mail\RegisterBonusDeposit;
use Illuminate\Bus\Queueable;
use App\Helpers\HyipHelper;
use App\Registrationbonus;
use App\Epin;
use Event;
use App\Events\UserNewDeposit;
use App\Events\UserActiveDeposit;
use App\Events\UserRegisterBonusDeposit;

trait DepositProcess {

    use HyipNotifiable, PlacementProcessor, CommissionProcessor;

    public function makeDeposit($request)
    {       
        $result = array();
        $admin = User::find(1);
        $user = Auth::user();
        if(is_null(Auth::user()))
        {
            $user = User::where('id', $request->user_id)->first();
        }
        if ($request->user_id)
        {
            $user = User::where('id', $request->user_id)->first();
        }
        //dd($user);
        // if (is_null($request->user_id))
        // {
        //     $request->merge(['user_id' => Auth::user()->id]);
        // }
        $deposit = $this->createNewDeposit($user, $request);
       // dd($deposit);
        $account = $this->createUserDepositAccount($user, $deposit);
        $interestAccount = $this->createUserDepositInterestAccount($user, $deposit);
        $transaction = $this->createNewDepositTransaction($account, $deposit, $request);
        $deposit = $this->updateTransactionIDinDeposit($deposit, $transaction);

        if ($request->paymentgateway == 9)
        {
            $depositupdate = $this->updateDepositBitcoinHashkey($deposit, $request->txnhashkey);
        }

        if ($request->paymentgateway == 7)
        {
            $depositupdate = $this->updateDepositCoinpaymentTxid($deposit, $request->txn_id);
        }

        if ($request->paymentgateway == 13)
        {
            $reinvestdeduction = $this->reinvestTransaction($request);
        }

        if ($request->paymentgateway == 19)
        {
            $reinvestdeduction = $this->ewalletTransaction($request);
        }
        
        if ($deposit && $account && $interestAccount && $transaction)
        {
            
            if ($request->paymentgateway == 1 || $request->paymentgateway == 9 || $request->paymentgateway == 7 || $request->paymentgateway == 17 || $request->paymentgateway == 20)
            {
                //firing an event
                Event::fire(new UserNewDeposit($deposit, $user, $admin));

                // Mail::to($user->email)->queue(new NewDepositSuccessfull($deposit));
                // Mail::to($admin->email)->queue(new AdminNotifyNewDeposit($deposit)); 
            }
            elseif ($request->paymentgateway == 14)            
            {
                //firing an event
                Event::fire(new UserRegisterBonusDeposit($deposit, $user));

              //  Mail::to($user->email)->queue(new RegisterBonusDeposit($deposit));
            }
            else      
            {
                //firing an event
                Event::fire(new UserActiveDeposit($deposit, $user, $admin));

                // Mail::to($user->email)->queue(new ActiveDepositSuccessfull($deposit));
                // Mail::to($admin->email)->queue(new AdminNotifyActiveDeposit($deposit)); 
            }
        }
        return $deposit;
    }

    public function createNewDeposit(User $user, $request)
    {
        //dd($DepositRequest); exit;
        // $user_id = $user->id;
        $bonus = Bonus::where([
                    ['plan', $request->plan],
                    ['triggertype', 1], // Deposit
                    ['status', '1']
                    ])->first();

        $depositamount = $this->checkBonus($user, $bonus, $request->amount);       

        $deposit = new Deposit;
        $deposit->status = "new";
        $deposit->amount = $depositamount;
        $deposit->plan_id = $request->plan;
        $deposit->user_id = $user->id;
        $deposit->paymentgateway_id = $request->paymentgateway;

        if ($deposit->save())
        {
            // Notification::send(Admin::first(), new UserMadeDeposit($deposit));
            // $super_admin = User::find(USER_ROLE_SUPERADMIN);
            // $admin->notify(new UserMadeDeposit($deposit));

            $plan = Plan::where('id', '=', $request->plan)->first();

            $pg = Paymentgateway::where('id', '=', $deposit->paymentgateway_id)->first();
            // dd($pg);

            $this->doActivityLog(
                $deposit,
                $user,
                [
                    'ip' => request()->ip(),
                    'plan name' => $plan->name,
                    'amount' => $deposit->amount,
                    'payment gateway' => $pg->displayname
                ],
                'deposit',
                $user->name .' deposited '. $deposit->amount . ' ' .\Config::get('settings.currency') .' on '.$plan->name
            );//*/

            // dd($user, $plan);

            $this->notify(new UserMadeDeposit($deposit, $user->name, $plan->name, $pg->displayname));

            return $deposit;
        }

        return FALSE;
    }

    public function checkBonus($user, $bonus, $amount) 
    {
        if (!is_null($bonus))
        {
            if ($bonus->triggertype == 1) // Deposit
            {
                // 1 - Flat and 2 - %
                $bonusamount = $bonus->type == 1 ? $bonus->value : ($amount * $bonus->value)/100;
                if($bonus->bonus_cretided_to == 1) // Credited to Account Balance
                {                   
                   // $depositamount = $amount + $bonusamount; 
                    $depositamount = $amount;
                    return $depositamount;   
                }
                else // Credited to Deposit
                {
                    $depositamount = $amount + $bonusamount;
                    return $depositamount;
                }
            }
        }
        else
        {
            $depositamount = $amount;
            return $depositamount;
        }    
    }

    public function createUserDepositAccount($user, $deposit) {

        $user_id = $user->id;
        $no_of_deposits_useraccounts = Useraccount::where([
                                ['user_id', '=', $user_id ],
                                ['accounttype_id', '=' ,'2']
                                ])->count();

        $account_no = "D-". (100000 + $user->id). "-" .($no_of_deposits_useraccounts+1);

        $account = new Useraccount;
        $account->account_no = $account_no;
        $account->description = "User Deposit Account";
        $account->user_id = $user->id;
        $account->accounttype_id = "2";
        $account->entity_reference_id = $deposit->id;
        $account->entity_type = "Deposit";
        if ($account->save())
        {
            return $account;
        }

        return FALSE;
    }

    public function createUserDepositInterestAccount($user, $deposit) {

        $user_id = $user->id;
        $no_of_deposits_useraccounts = Useraccount::where([
                                 ['user_id', '=', $user_id ],
                                 ['accounttype_id', '=' ,'5']
                                ])->count();

        $account_no = "I-". (100000 + $user->id). "-" .($no_of_deposits_useraccounts+1);

        $account = new Useraccount;
        $account->account_no = $account_no;
        $account->description = "User Deposit Interest Account";
        $account->user_id = $user->id;
        $account->accounttype_id = "5";
        $account->entity_reference_id = $deposit->id;
        $account->entity_type = "Interest";
        if ($account->save())
        {
            return $account;
        }

        return FALSE;
    }

    public function createNewDepositTransaction(Useraccount $account, Deposit $deposit, $request) 
    {
       // dd($request); 
        $request_json = array('payment_id' => $deposit->paymentgateway_id, 'plan_id' => $deposit->plan_id);
        
         //BankwireP
        if( $deposit->paymentgateway_id == '1')
        {      
            $accounting_code = $this->getAccountingCode('deposit-via-banktransfer');
            $response_json = array('transaction_number' => $request->transaction_id);
        } 
        //Paypal
        if( $deposit->paymentgateway_id == '2')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-paypal');
            $response_json = array(
                                'transaction_number' => Input::get('PayerID'),
                                'paymentId' => Input::get('paymentId'),
                                'token' => Input::get('token')
                                );
        }
        //Solid trust pay
        if( $deposit->paymentgateway_id == '3')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-solid-trust-pay');
            $response_json = array(
                                'transaction_number' => $request->transaction_id          
                                );
        }
        //Advcash
        if( $deposit->paymentgateway_id == '5')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-advcash');
            $response_json = array(
                                'transaction_number' => $request->transaction_id
                                );
        } 
        //Blockio
        if( $deposit->paymentgateway_id == '6')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-bitcoin');
            $response_json = array(
                                'actual_btc_amount' => $request->btcamount,
                                'transactionaddress' => $request->transactionaddress,
                                'actual_amount' => $request->amount,
            );

            $request_json = array(                                
                                'transaction_number' => $request->transaction_id,
                                'transactionaddress' => $request->transactionaddress,
            );
        }   
        //Coinpayment
        if( $deposit->paymentgateway_id == '7')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-bitcoin');
            $request_json = array(
                                'uuid' => $request->uuid,                    
            );
           
            $response_json = array(                                
                                'transaction_number' => $request->txn_id,
                                'address' => $request->address,
                                'coinamount' => $request->coinamount,
            );
        }  
        //Bitpay
        if( $deposit->paymentgateway_id == '8')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-bitcoin');
            $response_json = array(
                                'transaction_number' => $request->transaction_id,
                                'guid' => $request->guid,
                                'token' => $request->token
            );
        }
        //Bitcoin Direct Transfer
        if( $deposit->paymentgateway_id == '9')
        {
            //$url = 'https://testnet.blockexplorer.com/api/tx/'.$request->txnhashkey;
            $accounting_code = $this->getAccountingCode('deposit-via-bitcoin');
            $response_json = HyipHelper::getBitcoinWalletDetails($request->txnhashkey);
            
            $response_json = json_decode($response_json, true);
             //dd($response_json['vin'][0]['addr']);

            $received_amount = '';
            $received_address = '';
            foreach ($response_json['vout'] as $vout)
            {
                if ($vout['scriptPubKey']['addresses'][0] == $request->admin_address)
                {
                    $received_amount .= $vout['value'];
                    $received_address .= $vout['scriptPubKey']['addresses'][0];
                }                               
            }

            $request_json = array(
                            'btcamount' => $request->btcamount,
                            'admin_address' => $request->admin_address,
                            'received_amount' => $received_amount
            );

            $response_json = array(
                            'hashid' => $response_json['txid'],
                            'confirmations' => $response_json['confirmations'],
                            'received_amount' => $received_amount,
                            'received_address' => $received_address,
                            'from_address' => $response_json['vin'][0]['addr'],
                            'time' => $response_json['time'],
                            'transaction_number' => $request->transaction_id
            );
            //dd($response_json);           
        } 
        // Skrill
        if( $deposit->paymentgateway_id == '11')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-skrill');
            $response_json = array(
                                'transaction_number' => $request->transaction_id,
                                'msid' => $request->msid,
            );
        } 
        //Okpay
        if( $deposit->paymentgateway_id == '12')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-okpay');
            $response_json = array(
                                'transaction_number' => $request->transaction_id
            );
        }  
         //Reinvest
        if( $deposit->paymentgateway_id == '13')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-reinvest');
            $response_json = array('transaction_number' => $request->transaction_id);
        } 

        //Register Bonus Deposit
        if( $deposit->paymentgateway_id == '14')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-registerbonus');
            $response_json = array('transaction_number' => $request->transaction_id);
        }
        //PerfectMoney
        if( $deposit->paymentgateway_id == '15')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-perfectmoney');
            $response_json = array(
                                'transaction_number' => $request->transaction_id
            );
        }   

        //Neteller
        if( $deposit->paymentgateway_id == '16')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-neteller');
            $response_json = array(
                                'transaction_number' => $request->transaction_id,
                                'responseapproval' => $request->responseapproval
            );
        }       

        // Cheque
        if( $deposit->paymentgateway_id == '17')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-cheque');
            $request_json = array('payment_id' => $deposit->paymentgateway_id, 'plan_id' => $deposit->plan_id, 'cheque_no' => $request->cheque_no, 'payeer_name' => $request->payeer_name);
            $response_json = array('transaction_number' => $request->transaction_id);
        } 

        // Cheque
        if( $deposit->paymentgateway_id == '18')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-epin');
            $request_json = array('payment_id' => $deposit->paymentgateway_id, 'plan_id' => $deposit->plan_id);
            $response_json = "{object:undefined}";
        } 

        // For E-Wallet
        if( $deposit->paymentgateway_id == '19')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-ewallet');     
            $response_json = array('transaction_number' => $request->transaction_id);
        }   

        // For Manual
        if( $deposit->paymentgateway_id == '20' || $deposit->paymentgateway_id == '21')
        {
            $accounting_code = $this->getAccountingCode('deposit-via-manual');     
            $response_json = array('transaction_number' => $request->transaction_id);
        }   

        $transaction = new Transaction;
        $transaction->account_id = $account->id;
        $transaction->amount = $deposit->amount;
        $transaction->type = "credit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->request = json_encode($request_json);
        $transaction->response = json_encode($response_json);
        $transaction->save();       
        return $transaction;      

    }

    public function updateTransactionIDinDeposit(Deposit $deposit, Transaction $transaction) 
    {
        $deposit->transaction_id = $transaction->id;
        $deposit->save();
        return $deposit;
    }

    public function updateDepositBitcoinHashkey(Deposit $deposit, $txnhashkey) 
    {
        $deposit->param = $txnhashkey;
        $deposit->save();
        return $deposit;
    }

    public function updateDepositCoinpaymentTxid(Deposit $deposit, $txnid) 
    {
        $deposit->param1 = $txnid;
        $deposit->save();
        return $deposit;
    }
    
    public function checkForActiveDeposit (Deposit $deposit) 
    {
        return  Deposit::where([
                                ['user_id', '=', $deposit->user_id],
                                ['status', '=', 'active']
                                ])->exists();
        }

    public function approve($id) 
    {
        $deposit = Deposit::find($id);

        if ( $deposit->status !== "new" ) 
        {
            return abort(422);
        } else {

        $hasActiveDeposit = $this->checkForActiveDeposit($deposit);

        if( !$hasActiveDeposit ) {
            $placement = $this->processPlacement($deposit);
        }

        $referralCommissionStatus= (bool) $this->getSettingValue('referral_commission_status');

        if ($referralCommissionStatus)
        {
            $referralCommission = $this->remitReferralCommission($deposit);
        }

        $levelCommissionStatus= (bool) $this->getSettingValue('level_commission_status');

        if ($levelCommissionStatus)
        {
            $levelCommission = $this->processLevelCommission($deposit );
        }
              
        $comments_on_approval = 'approved by admin';
        if( $deposit->paymentgateway_id == 2 ) {
            $comments_on_approval = 'auto approved by paypal';
        }
        elseif( $deposit->paymentgateway_id == 11 ) {
            $comments_on_approval = 'auto approved by skrill';
        }
        elseif( $deposit->paymentgateway_id == 15 ) {
            $comments_on_approval = 'auto approved by perfectMoney';
        }
        elseif( $deposit->paymentgateway_id == 9 || $deposit->paymentgateway_id == 6 || $deposit->paymentgateway_id == 7) {
            $comments_on_approval = 'Bitcoin auto approved by admin';
        }

        $deposit->status = "active";
        $deposit->comments_on_approval = $comments_on_approval;
        $deposit->approved_on = Carbon::now();
        if ($deposit->plan->duration != '-1')
        {
            $maturityAfterDays = $this->calculateMaturityDays($deposit); 
            $this->createBonusTransaction($deposit);
            if ($deposit->plan->duration_key == 'hours')
            {
                $deposit->matured_on = Carbon::now()->addHours($maturityAfterDays);
            }
            else
            {
                $deposit->matured_on = Carbon::now()->addDays($maturityAfterDays);
            }
        }
        $deposit->save();
        return $deposit;
        }        
    }

    public function reinvestTransaction($request)
    {
        $request_json = array('reinvest_deposit_transaction_id' => $request->transaction_id, 'reinvest_amount' => $request->amount);

        $account_id = Useraccount::where([
                ['user_id', '=', Auth::id()],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
        $account_id = $account_id[0]['id'];

        $accounting_code = $this->getAccountingCode('reinvest-from-balance');

        $transaction = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->amount = $request->amount;
        $transaction->type = "debit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->request = json_encode($request_json);
        $transaction->save();       
        return $transaction;
    }

    public function maturedDepositTransaction(Deposit $deposit)
    {
        //dd($deposit);
        $response_json = array('comment' => 'Relesed deposit on '.Carbon::now(), 'amount' => $deposit->amount);

        $account_id = Useraccount::where([
                ['user_id', $deposit->user_id],
                ['entity_type', '=', 'Profile']
            ])->get(['id'])->toArray();
        //dd($account_id);
        $account_id = $account_id[0]['id'];
 
        $accounting_code = $this->getAccountingCode('matured-deposit');

        $transaction = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->amount = $deposit->amount;
        $transaction->type = "credit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->response = json_encode($response_json);
        $transaction->save();       
        return $transaction;

    }

    public function partialwithdrawprocess($request)
    {
       // dd($request);
        $amount = $this->sendAdminPartialWithdrawCommission($request);
        $transaction = $this->createPartialWithdrawTransaction($request, $amount);

        $user = User::where('id', Auth::id())->first();
        // dd($user);

        if ($transaction)
        {
            $this->doActivityLog(
                $transaction,
                $user,
                [
                    'ip' => request()->ip(),
                    'amount' => $request->amount,
                    'depositid' => $request->depositid
                ],
                'Partial Withdraw',
                $user->name .' partial withdraw on '. $request->amount .' '. \Config::get('settings.currency') .' in deposit id #'.$request->depositid
            );//*/

            $newdepositAmount = $request->depositamount - $request->amount;
            $deposit  = Deposit::where('id', $request->depositid)->first();
            $deposit->amount = $newdepositAmount;
            $deposit->save();
            return $deposit;
        }     

    }

    public function createPartialWithdrawTransaction($request, $amount)
    {
        $request_json = array('depositid' => $request->depositid);

        $account_id = Useraccount::where([
                ['user_id', '=', Auth::id()],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
        $account_id = $account_id[0]['id'];

        $accounting_code = $this->getAccountingCode('income-via-deposit-partial-withdraw');

        $transaction = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->amount = $amount;
        $transaction->type = "credit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->request = json_encode($request_json);
        $transaction->save();       
        return $transaction;

    }

    public function sendAdminPartialWithdrawCommission($request)
    {     
        $request_json = array('userid' => Auth::id(), 'amount' => $request->amount);

        $commissionvalue  = Deposit::where('id', $request->depositid)->with('plan')->first();

        $admincommission = $commissionvalue->plan->paritial_withdraw_fee;
        $admin_com_amount = ($request->amount * $admincommission) / 100 ;
        $withdrawamount = $request->amount - $admin_com_amount;
        //dd($withdraw_amount);

        if ($admin_com_amount > 0)
        {
            $account_id = Useraccount::where([
                ['user_id', '=', 2],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
            $account_id = $account_id[0]['id'];

            $accounting_code = $this->getAccountingCode('deposit-partial-withdraw-commission');

            $transaction = new Transaction;
            $transaction->account_id = $account_id;
            $transaction->amount = $admin_com_amount;
            $transaction->type = "credit";
            $transaction->status ="1";
            $transaction->accounting_code_id = $accounting_code;
            $transaction->request = json_encode($request_json);
            $transaction->save();       
        }  
        return $withdrawamount;
    }

    public function calculateMaturityDays(Deposit $deposit) {

        $duration = $deposit->plan->duration;
        $key = $deposit->plan->duration_key;

        switch($key) {
        case 'hours':
            return $days = $duration;
            break;
        case 'days':
            return $days = $duration;
            break;
        case 'weeks':
            return $days = $duration * 7;
            break;
        case 'months':
            return $days = $duration * 30;
            break;
        case 'years':
            return $days = $duration * 365;
            break;
        }
    } 

    public function getinvoicetransaction($transaction_id)
    {
        $transaction = Transaction::where('id', '=', $transaction_id)->first();
        $transactionparams = json_decode($transaction->response, true); 

        return $transactionparams['transaction_number'];             
    } 

    public function fundaddedtouser($deposit, $request)
    {
        $transaction = $this->fundAddedTransaction($deposit, $request);

        if ($transaction)
        {
                $deposit->amount = $request->receivedamount;
                $deposit->status = 'problem';
                $deposit->comments_on_problem = $request->problemcomment;
                $deposit->problem_on = Carbon::now();
                $deposit->save();
        }
    }

    public function fundAddedTransaction($deposit)
    {
        $request_json = array('depositid' => $deposit->id);

        $account_id = Useraccount::where([
                ['user_id', '=', $deposit->user_id],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
        $account_id = $account_id[0]['id'];

        $accounting_code = $this->getAccountingCode('fund-added');

        $transaction = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->amount = $deposit->amount;
        $transaction->type = "credit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->request = json_encode($request_json);
        $transaction->save();       
        return $transaction;
    }

    public function getinvoicetransactionrequest($transaction_id)
    {
        $transaction = Transaction::where('id', '=', $transaction_id)->first();
        $transactionparams = json_decode($transaction->request, true); 
        return $transactionparams;             
    } 

    public function updatecouponcodestatus($request)
    {
        $amount = $request->amount;
        $couponcode = $request->couponcode;
        $checkcode = Epin::where([['coupon_value', $amount],['coupon_code', $couponcode],['status', 'new']])->first();
       // dd($checkcode);
        if(!is_null($checkcode))
        {
            $checkcode->status = "used";
            $checkcode->used_by = Auth::id();
            $checkcode->update();
        }
        else
        {
            $checkcode = 0;
        }     
        return $checkcode;
    }

    public function ewalletTransaction($request)
    {
        $amount = \Session::get('amount');
        $request_json = array('ewallet_deposit_transaction_id' => $request->transaction_id, 'amount' => $amount);

        $account_id = Useraccount::where([
                ['user_id', '=', Auth::id()],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
        $account_id = $account_id[0]['id'];

        $accountingcode = 'ewallet-debitfund';
        $accounting_code = $this->getAccountingCode($accountingcode);
 
        $transaction = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->amount = $amount;
        $transaction->type = "debit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->request = json_encode($request_json);
        $transaction->save();       
        return $transaction;
    }

    public function createBonusTransaction($deposit)
    {
        $user = User::find($deposit->user_id);

        $amount = $deposit->amount;
        $bonus = Bonus::where([
                    ['plan', $deposit->plan_id],
                    ['triggertype', 1], // Deposit
                    ['status', '1']
                    ])->first();

        if (!is_null($bonus))
        {
            if ($bonus->triggertype == 1) // Deposit
            {
                // 1 - Flat and 2 - %
                $bonusamount = $bonus->type == 1 ? $bonus->value : ($amount * $bonus->value)/100;
                if($bonus->bonus_cretided_to == 1) // Credited to Account Balance
                {
                    $plan = Plan::where('id', '=', $bonus->plan)->first();

                    $request_json = array('bonusid' => $bonus->id, 'type' => 'bonus');
                    $account_id = Useraccount::where([
                            ['user_id', '=', $user->id],
                            ['entity_type', '=', 'profile']
                        ])->get(['id'])->toArray();
                    $account_id = $account_id[0]['id'];

                    $accounting_code = $this->getAccountingCode('income-as-bonus');

                    $transaction = new Transaction;
                    $transaction->account_id = $account_id;
                    $transaction->amount = $bonusamount;
                    $transaction->type = "credit";
                    $transaction->status ="1";
                    $transaction->accounting_code_id = $accounting_code;
                    $transaction->request = json_encode($request_json);
                    $transaction->comment = 'Bonus credited for the '. $plan->name . 'Plan.';
                    $transaction->save(); 
                    return $transaction;
                }
            }
        }
        return true;     
    }

}