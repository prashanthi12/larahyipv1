<?php

namespace App\Traits;

use App\User;
use App\Deposit;
use App\Placement;
use App\Payment;
use App\Useraccount;
use App\Transaction;
use Carbon\Carbon;
use App\Accountingcode;
use App\Referralgroup;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Collection;
use App\Settings;

trait CommissionProcessor {

	public function processLevelCommission(Deposit $deposit) {

			$levelcommission = $deposit->levelcommission;

            foreach ($levelcommission as $commissionItem) {

			$commission = $commissionItem->commission;

			$upline_account = Useraccount::where([
	                                ['user_id', '=', $commissionItem->beneficiary->id ],
	                                ['entity_type', '=', 'Profile']
	                                ])->first();

	        $upline_account_id = $upline_account ->id;
			
			$comment = "Level Commission for deposit id: <strong>". $deposit->id . 
						" </strong><br> deposit amount: <strong>". $deposit->amount . 
						" </strong><br> deposit by: <strong>" .$deposit->user->name .
						" </strong><br> For level : <strong>" .$commissionItem->level .
						" </strong><br> Commission Percentage : <strong>" .$commissionItem->percentage;

			$commissionType = 'level_commission';

			$level_commission_debit  = Accountingcode::where('active', "1")->where('accounting_code', 'level-commission')->get(['id'])->toArray();
         	
         	$level_commission_debit_id = $level_commission_debit[0]['id']; 
         	
         	if($commission > 0) {
	        $lcDebitTransactionId = $this->createTransaction('1', $commission, 'debit', $level_commission_debit_id, $comment); 
	        }       

	        $level_commission_credit  = Accountingcode::where('active', "1")->where('accounting_code', 'income-via-level-commission')->get(['id'])->toArray();
         	$level_commission_credit_id = $level_commission_credit[0]['id']; 

	        if($commission > 0) {
	        $lcCreditTransactionId = $this->createTransaction($upline_account_id,  $commission, 'credit', $level_commission_credit_id, $comment);
	        }

	        if($commission > 0) {
	        $paymentId = $this->createPayment($lcCreditTransactionId, $lcDebitTransactionId, $commission, $comment, $commissionType);
	        }
		}
	}

	private function getUplines(User $user, int $totalLevelsForCommission ) {

	     	$placement = Placement::where('user_id', $user->id)->first();

			$alphaKey = $placement->alphakey;

			$uplines = [];
            if($alphaKey!='A')
            {

				$uplineKey=explode('-',$alphaKey);
				$uplineKeyLength=count($uplineKey);

				if ($uplineKeyLength >= $totalLevelsForCommission) {
					$uplineCount = $totalLevelsForCommission;
				}else {
					$uplineCount = $uplineKeyLength;
				}
				

				$root_user_id=Placement::where([['alphakey', 'A'],['root_id',0],['spillover_id',0]])->pluck('user_id'); 

				$userid=explode('-B',$alphaKey);
				$userid = array_diff($userid,[$user->id]);//For Deposit User-Remove
				$userid = array_diff($userid, ["A"]);//For Root User Key-Remove
				array_unshift($userid ,$root_user_id[0]);//For Root User -Add

				$uplines = Placement::whereIn('user_id', $userid )->orderby('user_id','DESC')->pluck('user_id')->take($uplineCount)->toarray(); 
				 
				//$uplines = User::whereIn('id', $userid )->orderby('id','DESC')->take($uplineCount)->get();
			} 			
			return $uplines;
	
	}

	public function remitReferralCommission (Deposit $deposit) {

	        $user = $deposit->user;
	       // $referralgroup_id =  $user->userprofile->referral_group_id;
	        $sponsor = $user->sponsor;
	        if(count($sponsor)>0)
	        {
	        //dd($user->referralgroup);
	        $accounting_code = 'income-via-referral-commission';

	        $sponsor_account_id = $user->SponsorOperatingAccount;

			$commission = ($deposit->amount * $sponsor->referralgroup->referral_commission) / 100;
		    
		    $commissionType = 'referral_commission';
		    
		    $comment = "Referral Commission for deposit id: <strong>". $deposit->id 
		        			. "</strong> <br/> Deposit amount: <strong>". $deposit->amount 
		        			. "</strong> <br/> Deposit by: <strong>" . $user->name 
		        			. "</strong> <br/> Referral Commission Group : <strong>". ucfirst($sponsor->referralgroup->name)
		        			. "</strong> <br/> Commission % : <strong>". $sponsor->referralgroup->referral_commission;

		    //@to-do
		    $referral_commission = Accountingcode::where([
	                ['active', '=', "1"],
	                ['accounting_code', '=', 'income-via-referral-commission']
	            ])->get(['id'])->toArray();
	        
	        $referral_commission_code = $referral_commission[0]['id'];

	        //$referral_commission_code = \Common::getAccountingCode(String 'income-via-referral-commission');             

		    $rcDebitTransactionId = $this->createTransaction('1', $commission, 'debit', $referral_commission_code, $comment);  

		    $income_referral_commission = Accountingcode::where([
	                ['active', '=', "1"],
	                ['accounting_code', '=', $accounting_code]
	            ])->get(['id'])->toArray();
	        
	        $income_referral_commission_code = $income_referral_commission[0]['id'];

		    $rcCreditTransactionId = $this->createTransaction($sponsor_account_id, $commission, 'credit', $income_referral_commission_code, $comment);

		    $paymentId = $this->createPayment( $rcCreditTransactionId, $rcDebitTransactionId, $commission, $comment, $commissionType); 
		}

	}

	private  function createTransaction($account, $amount, $type, $accounting_code, $comment ) {

	        $transaction = new Transaction;
	        $transaction->account_id = $account;
	        $transaction->amount = $amount;
	        $transaction->type = $type;
	        $transaction->status ="1";
	        $transaction->accounting_code_id = $accounting_code;
	        $transaction->comment = $comment;
	        $transaction->request = "{object:undefined}";
	        $transaction->response = "{object:undefined}";
	        $transaction->save();

	        return $transaction->id;
	        
	}

	private function createPayment($id1, $id2, $amount, $comment, $type) {

	            $payment = new Payment;
	            $payment->credit_transaction_id = $id1;
	            $payment->debit_transaction_id = $id2;
	            $payment->amount = $amount;
	            $payment->type = $type;
	            $payment->comments = $comment;
	            $payment->save();

	}

}
