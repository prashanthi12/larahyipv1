<?php

namespace App\Traits;

use App\Paymentgateway;
use App\Userpayaccounts;
use App\Withdraw;
use App\Accountingcode;
use App\Transaction;
use App\User;
use App\Useraccount;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;


use App\Traits\LogActivity;


trait WithdrawProcess {

    use LogActivity;

    public function withdrawotp($request)
    {
        $withdraw = $this->createNewWithdraw($request); 
        return $withdraw;
    }

    public function withdrawrequest($request)
    {
        $withdraw = $this->updateWithdraw($request);        
        $transaction = $this->createNewWithdrawTransaction( $request, $withdraw);
        $updatewithdraw = $this->updateWithdrawTransaction( $transaction, $withdraw);
        return $withdraw;
	}  

    public function createNewWithdraw($request)
    {
        //dd($request);
        $user_id = Auth::id();

        if (is_null(Auth::id()))
        {
            $user_id = $request->user_id;
        }

        $withdraw = new Withdraw;
        $withdraw->payaccount_id = $request->userpayaccountid;       
        $withdraw->amount = $request->amount;
        $withdraw->user_id = $user_id;
        $withdraw->status = 'request';
        if ($request->mobileotp != '')
        {
            $withdraw->param1 = $request->mobileotp;
        }        
        $withdraw->save();
        return $withdraw;
    }

    public function updateWithdraw($request)
    {
        if ($request->otpcode != '')
        {
            $withdraw = Withdraw::where([
                        ['param1', '=', $request->otpcode],
                        ['id', '=', $request->withdrawid],
                    ])->first();
        }
        else
        {
            $withdraw = Withdraw::where('id', '=', $request->withdrawid)->first();
        }
        
      
        $withdraw->status = 'pending';
        $withdraw->save();
        return $withdraw;
    }

    public function addAdminCommission($request)
    {
        // dd($request);
        $request_json = array('userid' => $request->userid, 'amount' => $request->amount);
        // dd($request_json);
        $commissionvalue  = Paymentgateway::where('id', $request->paymentgateway)->get(['withdraw_commission'])->toArray();
        $admincommission = $commissionvalue[0]['withdraw_commission'];
        $admin_com_amount = ($request->amount * $admincommission) / 100 ;
        $withdrawamount = $request->amount - $admin_com_amount;
        // dd($withdrawamount);

        if ($admin_com_amount > 0)
        {
             $account_id = Useraccount::where([
                ['user_id', '=', 2],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
            $account_id = $account_id[0]['id'];

            $accountcodeResult  = Accountingcode::where([
                ['active', '=', "1"],
                ['accounting_code', '=', "withdraw-commission"],
                ])->get(['id'])->toArray();
            $accounting_code = $accountcodeResult[0]['id'];

            $transaction = new Transaction;
            $transaction->account_id = $account_id;
            $transaction->amount = $admin_com_amount;
            $transaction->type = "credit";
            $transaction->status ="1";
            $transaction->accounting_code_id = $accounting_code;
            $transaction->request = json_encode($request_json);
            $transaction->save();       
        }
       // dd($withdrawamount);
        return $withdrawamount;
    }

    public function createNewWithdrawTransaction( $request, $withdraw)
    {
        $request_json = array('userpayaccountid' => $request->userpayaccountid);

         $accountcodeResult  = Accountingcode::where('active', "1"); 
         //dd($accountcodeResult);
         if ($request->paymentgateway == 1 || $request->paymentgateway == 20)
         {
            $accounting_code  = $accountcodeResult->where('accounting_code', 'withdraw-via-bankwire')->get(['id'])->toArray();
         }
         if ($request->paymentgateway == 2)
         {
            $accounting_code  = $accountcodeResult->where('accounting_code', 'withdraw-via-paypal')->get(['id'])->toArray();
         }
         if ($request->paymentgateway == 3)
         {
            $accounting_code  = $accountcodeResult->where('accounting_code', 'withdraw-via-stpay')->get(['id'])->toArray();
         }
         if ($request->paymentgateway == 4)
         {
            $accounting_code  = $accountcodeResult->where('accounting_code', 'withdraw-via-payeer')->get(['id'])->toArray();
         }
         if ($request->paymentgateway == 5)
         {
            $accounting_code  = $accountcodeResult->where('accounting_code', 'withdraw-via-advcash')->get(['id'])->toArray();
         }
         if (($request->paymentgateway == 9)||($request->paymentgateway == 7))
         {
            $accounting_code  = $accountcodeResult->where('accounting_code', 'withdraw-via-bitcoin')->get(['id'])->toArray();
         }
         if ($request->paymentgateway == 11)
         {
            $accounting_code  = $accountcodeResult->where('accounting_code', 'withdraw-via-skrill')->get(['id'])->toArray();
         }
         if ($request->paymentgateway == 12)
         {
            $accounting_code  = $accountcodeResult->where('accounting_code', 'withdraw-via-okpay')->get(['id'])->toArray();
         }
         if ($request->paymentgateway == 15)
         {
            $accounting_code  = $accountcodeResult->where('accounting_code', 'withdraw-via-perfectmoney')->get(['id'])->toArray();
         }
         if ($request->paymentgateway == 16)
         {
            $accounting_code  = $accountcodeResult->where('accounting_code', 'withdraw-via-neteller')->get(['id'])->toArray();
         }
         if ($request->paymentgateway == 21)
         {
            $accounting_code  = $accountcodeResult->where('accounting_code', 'withdraw-via-mobile')->get(['id'])->toArray();
         }
        //dd($accounting_code[0]['id']);
     
        $accounting_code = $accounting_code[0]['id'];

        $user_id = Auth::id();
        if (is_null(Auth::id()))
        {
            $user_id = $request->user_id;
        }

        $account_id = Useraccount::where([
                ['user_id', '=', $user_id],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
            $account_id = $account_id[0]['id'];
 
        $transaction = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->amount = $request->amount;
        $transaction->type = "debit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->request = json_encode($request_json);
        $transaction->save();       
        return $transaction;
    }

    public function updateWithdrawTransaction($transaction, $withdraw)
    {
        $updatewithdraw = Withdraw::where('id', '=', $withdraw->id)->first();
        $updatewithdraw->transaction_id = $transaction->id;      
        $updatewithdraw->save();
        return $updatewithdraw;
    }

    public function updateCompleteStatus($request, $id)
    {       
       $completestatus = $this->approveCompleteStatus($request, $id);
       $transactionnumber = $this->updateTransactionNumber($request);        
       return $completestatus;
   }

    public function approveCompleteStatus($request, $id)
    {
        $withdrawamount = $this->addAdminCommission($request);
        // dd($request);
        $withdraw = Withdraw::where('id', '=', $id)->first(); 
        $withdraw->amount = $withdrawamount;  
        $withdraw->status = 'completed';
        if ($request->paymentgateway == 9)
        {
            $withdraw->param = $request->hashkey;
        }
        $withdraw->completed_on = Carbon::now();
        $withdraw->comments_on_complete = $request->comment;
        $withdraw->save();
        return $withdraw;
    }

    public function updateTransactionNumber($request)
    {
        $response_json = array('transaction_number' => $request->transactionnumber);

        $transaction = Transaction::where('id', '=', $request->transactionid)->first();       
        $transaction->response = json_encode($response_json);    
        $transaction->save();
        return $transaction;
    }


    public function updateRejectStatus($request, $id)
    {
        $withdraw = Withdraw::where('id', $id)->first();
        if ($withdraw->status == 'pending')
        {
            $transactionnumber = $this->cancellationTransaction($request);
        }
        $rejectstatus = $this->changeRejectStatus($request, $id);       
        
        return $rejectstatus;
   }

   public function changeRejectStatus($request, $id)
    {
        $withdraw = Withdraw::where('id', '=', $id)->first();   
        $withdraw->status = 'rejected';
        $withdraw->rejected_on = Carbon::now();
        $withdraw->comments_on_rejected = $request->comment;
        $withdraw->save();
        return $withdraw;
    }
   
    public function cancellationTransaction( $request)
    {
        $request_json = array('amount' => $request->amount);

         $accountcodeResult  = Accountingcode::where([
            ['active', '=', "1"],
            ['accounting_code', '=', "withdraw-cancellation"]
            ])->get(['id'])->toArray(); 
        //dd($accountcodeResult);         
     
        $accounting_code = $accountcodeResult[0]['id'];

        $account_id = Useraccount::where([
                ['user_id', '=', $request->userid],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
            $account_id = $account_id[0]['id'];
 
        $transaction = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->amount = $request->amount;
        $transaction->type = "credit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->request = json_encode($request_json);
        $transaction->save();       
        return $transaction;
    }  

 }