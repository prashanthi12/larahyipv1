<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'fullname', 'email', 'contactno', 'skype_gtalk', 'queries'
    ];
}
