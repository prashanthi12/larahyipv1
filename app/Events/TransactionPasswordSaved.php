<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TransactionPasswordSaved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

   // public $userprofile;
    public $user, $newpassword;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    // public function __construct($userprofile)
    // {
    //     $this->userprofile = $userprofile;
    // }

    public function __construct($user, $newpassword)
    {
        $this->newpassword = $newpassword;
        $this->user = $user;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
