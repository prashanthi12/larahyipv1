<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InviteContacts extends Model
{
    protected $tables = "invite_contacts";

    protected $fillable = ['firstname', 'lastname', 'email'];
}
