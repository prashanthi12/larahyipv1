<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],

        'Illuminate\Auth\Events\Registered' => [
            'App\Listeners\LogRegisteredUser',
        ],

        'Illuminate\Auth\Events\Attempting' => [
            'App\Listeners\LogAuthenticationAttempt',
        ],

        'Illuminate\Auth\Events\Authenticated' => [
            'App\Listeners\LogAuthenticated',
        ],

        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\LogSuccessfulLogin',
        ],

        'Illuminate\Auth\Events\Failed' => [
            'App\Listeners\LogFailedLogin',
        ],

        'Illuminate\Auth\Events\Logout' => [
            'App\Listeners\LogSuccessfulLogout',
        ],

        'Illuminate\Auth\Events\Lockout' => [
            'App\Listeners\LogLockout',
        ],

        'App\Events\ContactusAdded' => [
            'App\Listeners\UserContactusMail',
          //  'App\Listeners\NewContactusAdded',
        ],

        'App\Events\InviteNewFriend' => [
            'App\Listeners\InviteNewFriendRequest',
        ],

        'App\Events\AdminMailMessage' => [
            'App\Listeners\AdminMailMessageSend',
        ],

        'App\Events\FundTransferMail' => [
            'App\Listeners\FundTransferSendMail',
          //  'App\Listeners\FundTransferSenderMail',
        ],

        // 'App\Events\FundTransferSend' => [
        //     'App\Listeners\FundTransferSenderMail',
        // ],

        'App\Events\UserTicketSend' => [
            'App\Listeners\UserTicketSendMail',
            // 'App\Listeners\AdminNotifyTicketMail',
            // 'App\Listeners\StaffNotifyTicketMail',
        ],

        'App\Events\AdminWithdrawApprove' => [
            'App\Listeners\WithdrawApproveMail',
        ],

        'App\Events\UserWithdrawOTP' => [
            'App\Listeners\WithdrawOTPMail',
        ],

        'App\Events\AdminWithdrawReject' => [
            'App\Listeners\WithdrawRejectMail',
        ],

        'App\Events\UserWithdrawSend' => [
            'App\Listeners\WithdrawSendMail',
        ],

        // StaffUpdateTicketStatus::class => [
        //     UserNotifyTicketMail::class,
        //     AdminNotifyTicketMail::class,
        // ],

        'App\Events\StaffUpdateTicketStatus' => [
            'App\Listeners\UserNotifyTicketMail',
           // 'App\Listeners\AdminNotifyTicketMail',
        ],

        'App\Events\AdminUpdateTicketStatus' => [
           // 'App\Listeners\UserNotifyTicketMail',
            'App\Listeners\StaffNotifyTicketStatusMail',
        ],

        'App\Events\UserChangePassword' => [
            'App\Listeners\ChangePasswordMail',
        ],

        'App\Events\TransactionPasswordSaved' => [
            'App\Listeners\ResetTransactionPasswordMail',
          //  'App\Listeners\TransactionPasswordMail',
        ],

        'App\Events\NewUserRegister' => [
            'App\Listeners\RegisterUserMail',
            // 'App\Listeners\UserEmailVerification',
            // 'App\Listeners\AdminNotifyNewUserMail',
        ],

        'App\Events\AdminUserResetPassword' => [
            'App\Listeners\AdminUserResetPasswordMail',
        ],

        'App\Events\AdminKYCApprove' => [
            'App\Listeners\AdminKYCApproveMail',
        ],

        'App\Events\AdminKYCReject' => [
            'App\Listeners\AdminKYCRejectMail',
        ],

        'App\Events\AdminNotifyKycDocVerify' => [
            'App\Listeners\AdminNotifyKycDocVerifyMail',
        ],

        'App\Events\AdminsCreateNewUser' => [
            'App\Listeners\CreateNewUserMail',
        ],

        'App\Events\AdminSendBonus' => [
            'App\Listeners\AdminSendBonusUserMail',
        ],

        'App\Events\AdminSendPenalty' => [
            'App\Listeners\AdminSendPenaltyUserMail',
        ],

        'App\Events\UserRegisterBonusDeposit' => [
            'App\Listeners\UserRegisterBonusDepositMail',
        ],

        'App\Events\UserNewDeposit' => [
            // 'App\Listeners\UserNewDepositMail',
           'App\Listeners\AdminNotifyNewDepositMail',
        ],

        'App\Events\UserActiveDeposit' => [
            'App\Listeners\UserActiveDepositMail',
          //  'App\Listeners\AdminNotifyActiveDepositMail',
        ],

        'App\Events\MassMail' => [
            'App\Listeners\AdminSendMassMail',
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
