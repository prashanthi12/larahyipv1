<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Withdraw extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'transaction_id', 'payaccount_id', 'status','amount','param','param1','user_id','completed_on','comments_on_complete' ,'rejected_on','comments_on_rejected', 'cancelled_on', 'autowithdraw'
    ];

    protected $dates = ['completed_on', 'rejected_on', 'deleted_at'];
    protected $with = ['userpayaccounts','user'];
    
   // protected $appends = ['pendingWithdraw'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function transaction() {
        return $this->belongsTo('App\Transaction');
    }

    public function userpayaccounts() {
        return $this->belongsTo('App\Userpayaccounts', 'payaccount_id', 'id');
    }

    public function getPaymentGatewayNameAttribute()
    {
        return $this->userpayaccounts->payment->displayname;
    }

    public function manualdeposit() 
    {
        return $this->hasOne('App\ManualDeposit');
    }

     public function assignmanualdeposit() 
    {
        return $this->hasOne('App\ManualDeposit')->whereNull('type')->where('is_report',0);
    }
     public function latestwithdrawdeposit() 
    {
        return $this->hasOne('App\ManualDeposit')->whereNull('type')->orderBy('id','DESC');
    }
}
