<?php
namespace App\Classes;
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('max_execution_time', 0);
require app_path().'\Classes\advCash\MerchantWebService.php';
$merchantWebService = new MerchantWebService();

$arg0 = new authDTO();
$arg0->apiName = $api_name;
$arg0->accountEmail = $acc_email_id;
$arg0->authenticationToken = $merchantWebService->getAuthenticationToken($api_pwd);

$arg1 = new sendMoneyRequest();
$arg1->amount = $amount;
$arg1->currency = "USD";
$arg1->email =  $advcash_accemail;
//$arg1->walletId = "U000000000000";
$arg1->note = "note";
$arg1->savePaymentTemplate = false;

$validationSendMoney = new validationSendMoney();
$validationSendMoney->arg0 = $arg0;
$validationSendMoney->arg1 = $arg1;

$sendMoney = new sendMoney();
$sendMoney->arg0 = $arg0;
$sendMoney->arg1 = $arg1;

try {
    $merchantWebService->validationSendMoney($validationSendMoney);
    $sendMoneyResponse = $merchantWebService->sendMoney($sendMoney);

    echo print_r($sendMoneyResponse, true)."<br/><br/>";
    echo $sendMoneyResponse->return."<br/><br/>";

} catch (Exception $e) {
    echo "ERROR MESSAGE => " . $e->getMessage() . "<br/>";
    echo $e->getTraceAsString();
}
?>