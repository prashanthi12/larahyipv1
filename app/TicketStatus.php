<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketStatus extends Model
{
	protected $fillable = [
        'name', 'color'
    ];
    
    public function status()
    {
        return $this->hasMany('App\Ticket', 'status_id', 'id');
    }
}
