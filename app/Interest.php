<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interest extends Model
{
     use SoftDeletes;

    protected $fillable = [
        'transaction_id', 'amount', 'deposit_id', 'comment', 'effective_date'
    ];

    protected $dates = ['deleted_at'];

    public function transaction()
    {
        return $this->hasOne('App\Transaction');
    }

    public function deposit()
    {
        return $this->belongsTo('App\Deposit');
    }

    public function scopeDepositId($query, $deposit_id)
    {
        return $query->where('deposit_id', $deposit_id);
    }

    public function calculateInterestAmount ($deposit)
    {
        $interestamount =  (($deposit->amount) * ($deposit->plan->interest_rate) ) / 100;

        return $interestamount;
    }

    public function createNewInterest($deposit, $interestamount, $effectivedate)
    {
        $interest = new Interest;
        $interest->amount = $interestamount;
        $interest->deposit_id = $deposit->id;
        $interest->comment = "Interested credited for ". $deposit->id;
        $interest->effective_date = $effectivedate;
        $interest->created_at = Carbon::now();
        $interest->save();

        return $interest;
    }
   
}
