<?php

use Illuminate\Database\Seeder;

class FaqsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faqs')->insert(
        [
            'title' => "Support Simple & Compounding Interest?.",
            'description' => "LaraHYIP supports simple interest and compound interest models. Also has a Interest Calculator to see ROI on fly.",
            'order'        => 1,
            'language' => 'en',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),     
        ]);
        DB::table('faqs')->insert(
        [
            'title' => "It has Forced Matrix Settings?.",
            'description' => "LaraHYIP supports forced matrix ( m x n ) to encourage the investors to refer and earn Multi Level Commission.",
            'order'        => 2,
            'language' => 'en',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('faqs')->insert(
        [
            'title' => "Is there any bonus?.",
            'description' => "First Deposit Bonus & Performance Bonus.",
            'order'        => 3,
            'language' => 'en',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
    }
}
