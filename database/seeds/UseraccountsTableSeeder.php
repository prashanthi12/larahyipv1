<?php

use Illuminate\Database\Seeder;

class UseraccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('useraccounts')->insert(
        [
            'user_id' => "1",
            'account_no' => "M000001",
            'description' => "Master Spending Account",
            'accounttype_id' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('useraccounts')->insert(
        [
            'user_id' => "1",
            'account_no' => "M000002",
            'description' => "Master Earning Account",
            'accounttype_id' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('useraccounts')->insert(
        [
            'user_id' => "1",
            'account_no' => "U-100000-1",
            'description' => "User Operative Account",
            'accounttype_id' => "3",
            'entity_reference_id' => "1",
            'entity_type' => "Profile",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('useraccounts')->insert(
        [
            'user_id' => "2",
            'account_no' => "U-100000-2",
            'description' => "User Operative Account",
            'accounttype_id' => "3",
            'entity_reference_id' => "1",
            'entity_type' => "Profile",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
    }
}