<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert(
        [
            'name' => "Hourly Basic Plan",
            'plantype_id' => "3",
            'min_amount' => "100",
            'max_amount' => "1000",
            'interest_rate' => "0.25",
            'duration' => "60",
            'duration_key' => "days",
            'publish' => "1",
            'active' => "1",
            'principle_return' => "1",
            'compounding' => "0",
            'orderby' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);

        DB::table('plans')->insert(
        [
            'name' => "Hourly Speed Plan",
            'plantype_id' => "3",
            'min_amount' => "1000",
            'max_amount' => "10000",
            'interest_rate' => "1",
            'duration' => "30",
            'duration_key' => "days",
            'publish' => "1",
            'active' => "1",
            'principle_return' => "0",
            'compounding' => "0",
            'orderby' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);

        DB::table('plans')->insert(
        [
            'name' => "Daily Basic Plan",
            'plantype_id' => "1",
            'min_amount' => "100",
            'max_amount' => "10000",
            'interest_rate' => "1.25",
            'duration' => "90",
            'duration_key' => "days",
            'publish' => "1",
            'active' => "1",
            'principle_return' => "1",
            'compounding' => "0",
            'orderby' => "2",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plans')->insert(
        [
            'name' => "Daily Speed Plan",
            'plantype_id' => "1",
            'min_amount' => "100",
            'max_amount' => "1000",
            'interest_rate' => "2.5",
            'duration' => "90",
            'duration_key' => "days",
            'publish' => "1",
            'active' => "1",
            'principle_return' => "1",
            'partial_withdraw_status' => "0",
            'max_partial_withdraw_limit' => "10",
            'minimum_locking_period' => "10",
            'paritial_withdraw_fee' => "10",
            'compounding' => "0",
            'orderby' => "3",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plans')->insert(
        [
            'name' => "Daily Business Plan",
            'plantype_id' => "2",
            'min_amount' => "100",
            'max_amount' => "10000",
            'interest_rate' => "1.75",
            'duration' => "90",
            'duration_key' => "days",
            'publish' => "1",
            'active' => "1",
            'principle_return' => "1",
            'partial_withdraw_status' => "0",
            'max_partial_withdraw_limit' => "10",
            'minimum_locking_period' => "10",
            'paritial_withdraw_fee' => "10",
            'compounding' => "0",
            'orderby' => "4",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plans')->insert(
        [
            'name' => "Weekly Plan",
            'plantype_id' => "4",
            'min_amount' => "100",
            'max_amount' => "10000",
            'interest_rate' => "3",
            'duration' => "26",
            'duration_key' => "weeks",
            'publish' => "1",
            'active' => "1",
            'principle_return' => "1",
            'compounding' => "0",
            'orderby' => "5",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
         DB::table('plans')->insert(
        [
            'name' => "Monthly Basic Plan",
            'plantype_id' => "5",
            'min_amount' => "100",
            'max_amount' => "10000",
            'interest_rate' => "8.5",
            'duration' => "12",
            'duration_key' => "months",
            'publish' => "1",
            'active' => "1",
            'principle_return' => "1",
            'compounding' => "0",
            'orderby' => "6",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plans')->insert(
        [
            'name' => "Monthly Speed Plan",
            'plantype_id' => "5",
            'min_amount' => "100",
            'max_amount' => "10000",
            'interest_rate' => "8.5",
            'duration' => "12",
            'duration_key' => "months",
            'publish' => "1",
            'active' => "1",
            'principle_return' => "0",
            'compounding' => "0",
            'orderby' => "7",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plans')->insert(
        [
            'name' => "Quarterly Plan",
            'plantype_id' => "6",
            'min_amount' => "100",
            'max_amount' => "10000",
            'interest_rate' => "33",
            'duration' => "12",
            'duration_key' => "months",
            'publish' => "1",
            'active' => "1",
            'principle_return' => "0",
            'compounding' => "0",
            'orderby' => "8",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plans')->insert(
        [
            'name' => "Yearly Plan",
            'plantype_id' => "6",
            'min_amount' => "100",
            'max_amount' => "10000",
            'interest_rate' => "33",
            'duration' => "12",
            'duration_key' => "months",
            'publish' => "1",
            'active' => "1",
            'principle_return' => "0",
            'compounding' => "0",
            'orderby' => "9",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        /*DB::table('plans')->insert(
        [
            'name' => "Unlimited Daily",
            'plantype_id' => "2",
            'min_amount' => "100",
            'max_amount' => "10000",
            'interest_rate' => "2.5",
            'duration' => "-1",
            'duration_key' => "days",
            'publish' => "1",
            'active' => "0",
            'principle_return' => "1",
            'compounding' => "0",
            'orderby' => "10",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plans')->insert(
        [
            'name' => "Compounding Daily",
            'plantype_id' => "2",
            'min_amount' => "100",
            'max_amount' => "10000",
            'interest_rate' => "2.5",
            'duration' => "-1",
            'duration_key' => "days",
            'publish' => "1",
            'active' => "0",
            'principle_return' => "1",
            'compounding' => "1",
            'orderby' => "11",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);*/
    }
}
