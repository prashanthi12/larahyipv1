<?php

use Illuminate\Database\Seeder;

class PaymentgatewaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*DB::table('paymentgateways')->insert([
        	'gatewayname' => 'bank',
        	'displayname' => 'Bank Transfer',
        	'active' => '1',
        	'deposit' => '0',
        	'withdraw' => '1',
            'e_wallet' => '1',
            'status' => 'offline',
        	'exchange' => '0',
        	'params' => '{"bank_name":"Bank of India", "bank_address":"Madurai", "swift_code":"435435435", "account_name":"Tester", "account_no":"0987654321"}',
        	'instructions' => 'some instruction text'
        ]);*/
        DB::table('paymentgateways')->insert([
            'id' => '2',
        	'gatewayname' => 'paypal',
        	'displayname' => 'Paypal',
        	'active' => '1',
        	'deposit' => '1',
        	'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'online',
        	'exchange' => '0',
        	// 'params' => '{"client_id":"ARAI3esay9E-C6jzo3ycOun8gMEf2-PRz96PaXG-w3XlpcZCaCvTJ2rNOI9T2-8LMwYG6uIf4x5tC95a", "secret":"EE6u3yXh4iKUvcGISZ-o0ILMyEafBttvI5hzULTy9qTbY3GLmBIide1z96z1nh0Sgtb0Ss1uqzu2DdJA", "mode":"sandbox"}',
            'params' => '{"merchant_email":"sundari1@gegosoft.com", "mode":"sandbox"}',
        	'instructions' => 'some instruction text'
        ]);
        /*DB::table('paymentgateways')->insert([
        	'gatewayname' => 'stp',
        	'displayname' => 'Solid Trust Pay',
        	'active' => '1',
        	'deposit' => '0',
        	'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'online',
        	'exchange' => '0',
        	'params' => '{"account_login":"stpname"}',
        	'instructions' => 'some instruction text'
        ]);
        DB::table('paymentgateways')->insert([
        	'gatewayname' => 'payeer',
        	'displayname' => 'Payeer',
        	'active' => '1',
        	'deposit' => '0',
        	'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'online',
        	'exchange' => '0',
        	'params' => '{"payeer_id":"payeerid", "m_key":"W22123434"}',
        	'instructions' => 'some instruction text'
        ]);
        DB::table('paymentgateways')->insert([
        	'gatewayname' => 'advcash',
        	'displayname' => 'Advcash',
        	'active' => '1',
        	'deposit' => '0',
        	'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'online',
        	'exchange' => '0',
        	// 'params' => '{"acc_email":"admin@advcash.com","api_name":"hyip", "api_password":"larahyip"}',
            'params' => '{"ac_account_email":"karthickkumar.aj@gmail.com","ac_sci_name":"moneywise","ac_secret":"Gegosoft@2017"}',
        	'instructions' => 'some instruction text'
        ]);
        DB::table('paymentgateways')->insert([
        	'gatewayname' => 'bitcoin1',
        	'displayname' => 'Bitcoin via Block.io',
        	'active' => '1',
        	'deposit' => '0',
        	'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'online',
        	'exchange' => '0',
            'params' => '{"api_key":"bcbf-f1da-18fe-ca75", "pin":"GEG0S0FT123","version":"2", "btc_address":"2MsxpuiHjAAyNZewxKFfrguJmoYF34J4UCz","mode":"BTCTEST"}',
        	// 'params' => '{"api_key":"bitcoinapi", "pin":"blockiopin", "mode":"BTCTEST"}',
        	'instructions' => 'some instruction text'
        ]);
        DB::table('paymentgateways')->insert([
        	'gatewayname' => 'bitcoin2',
        	'displayname' => 'Bitcoin via CoinPayments',
        	'active' => '1',
        	'deposit' => '0',
        	'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'online',
        	'exchange' => '0',
        	'params' => '{"public_key":"publickey", "private_key":"privatekey"}',
        	'instructions' => 'some instruction text'
        ]);
        DB::table('paymentgateways')->insert([
        	'gatewayname' => 'bitcoin3',
        	'displayname' => 'Bitcoin via BitPay',
        	'active' => '1',
        	'deposit' => '0',
        	'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'online',
        	'exchange' => '0',
        	'params' => '{"apikey":"FZFE6qL44Tw55ijJVmVcNShRnqWOpaokvgJ6vHHk", "testmode":"true"}',
        	'instructions' => 'some instruction text'
        ]);
        DB::table('paymentgateways')->insert([
        	'gatewayname' => 'bitcoin4',
        	'displayname' => 'Bitcoin via Direct Wallet Transfer',
        	'active' => '1',
        	'deposit' => '0',
        	'withdraw' => '1',
            'e_wallet' => '1',
            'status' => 'online',
        	'exchange' => '0',
        	'params' => '{"address":"19356KxTs9Bw5AAdxens5hoxDSp5bsUKse"}',
        	'instructions' => 'some instruction text'
        ]);
        DB::table('paymentgateways')->insert([
        	'gatewayname' => 'cash',
        	'displayname' => 'Cash on Hand',
        	'active' => '1',
        	'deposit' => '0',
        	'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'offline',
        	'exchange' => '0',
        	'params' => '{"param1":"Bank of India", "param2":"some account no"}',
        	'instructions' => 'some instruction text'
        ]);
        DB::table('paymentgateways')->insert([
            'gatewayname' => 'skrill',
            'displayname' => 'Skrill',
            'active' => '1',
            'deposit' => '0',
            'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'online',
            'exchange' => '0',
            'params' => '{"pay_to_email":"admin@hyip.com"}',
            'instructions' => 'some instruction text'
        ]);
        DB::table('paymentgateways')->insert([
            'gatewayname' => 'okpay',
            'displayname' => 'Ok Pay',
            'active' => '1',
            'deposit' => '0',
            'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'online',
            'exchange' => '0',
            'params' => '{"ok_receiver":"OK312323"}',
            'instructions' => 'some instruction text about Ok pay'
        ]); */
        DB::table('paymentgateways')->insert([
            'id' => '13',
            'gatewayname' => 'reinvest',
            'displayname' => 'Reinvest',
            'active' => '1',
            'deposit' => '0',
            'withdraw' => '0',
            'e_wallet' => '0',
            'status' => 'reinvest',
            'exchange' => '0',
            'params' => '',
            'instructions' => 'some instruction text about Reinvestment'
        ]);
        /*DB::table('paymentgateways')->insert([
            'gatewayname' => 'registerbonus',
            'displayname' => 'Register Bonus',
            'active' => '1',
            'deposit' => '0',
            'withdraw' => '0',
            'e_wallet' => '0',
            'status' => 'bonus',
            'exchange' => '0',
            'params' => '',
            'instructions' => 'some instruction text about Register Bonus'
        ]);

        DB::table('paymentgateways')->insert([
            'gatewayname' => 'perfectmoney',
            'displayname' => 'PerfectMoney',
            'active' => '1',
            'deposit' => '0',
            'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'online',
            'exchange' => '0',
            'params' => '{"payee_account":"U9007123","payee_name":"larahyip","alternate_passhprase":"test"}',
            'instructions' => 'some instruction text about PerfectMoney'
        ]);

        DB::table('paymentgateways')->insert([
            'gatewayname' => 'neteller',
            'displayname' => 'Neteller',
            'active' => '1',
            'deposit' => '0',
            'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'online',
            'exchange' => '0',
            'params' => '{"merchant_id":"432156789012","merch_key":"123456","paymentmode":"test"}',
            'instructions' => 'some instruction text about Neteller'
        ]);

        DB::table('paymentgateways')->insert([
            'gatewayname' => 'cheque',
            'displayname' => 'Cheque',
            'active' => '1',
            'deposit' => '0',
            'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'offline',
            'exchange' => '0',
            'params' => '{"bank_name":"Indian Bank", "payee_name":"Tester", "account_no":"9876543210"}',
            'instructions' => 'some instruction text about Cheque',
        ]);

        DB::table('paymentgateways')->insert([
            'gatewayname' => 'e-pin',
            'displayname' => 'E - pin',
            'active' => '1',
            'deposit' => '0',
            'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'e-pin',
            'exchange' => '0',
            // 'params' => '{"bank_name":"Indian Bank", "payee_name":"Tester", "account_no":"9876543210"}',
            // 'instructions' => 'some instruction text about Cheque',
        ]);

        DB::table('paymentgateways')->insert([
            'gatewayname' => 'ewallet',
            'displayname' => 'Ewallet',
            'active' => '1',
            'deposit' => '0',
            'withdraw' => '0',
            'e_wallet' => '0',
            'status' => 'e-wallet',
            'exchange' => '0',
            'params' => '',
            'instructions' => 'some instruction text about Ewallet'
        ]);*/

        DB::table('paymentgateways')->insert([
            'id' => '20',
            'gatewayname' => 'manual',
            'displayname' => 'Manual Deposit',
            'active' => '1',
            'deposit' => '1',
            'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'offline',
            'exchange' => '0',
            'params' => '',
            'instructions' => 'Some instruction text about Manual',
        ]);

         DB::table('paymentgateways')->insert([
            'id' => '21',
            'gatewayname' => 'manual-mobile',
            'displayname' => 'Manual Mobile',
            'active' => '1',
            'deposit' => '0',
            'withdraw' => '1',
            'e_wallet' => '0',
            'status' => 'offline',
            'exchange' => '0',
            'params' => '',
            'instructions' => 'Some instruction text about Manual',
        ]);
    }
}
