<?php

use Illuminate\Database\Seeder;

class UserprofilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('userprofiles')->insert(
        [
            'user_id' => "1",
            'usergroup_id' => USER_ROLE_SUPERADMIN,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('userprofiles')->insert(
        [
            'user_id' => "2",
            'usergroup_id' => USER_ROLE_ADMIN,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('userprofiles')->insert(
        [
            'user_id' => "3",
            'usergroup_id' => USER_ROLE_STAFF,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('userprofiles')->insert(
        [
            'user_id' => "4",
            'usergroup_id' => USER_ROLE_STAFF,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('userprofiles')->insert(
        [
            'user_id' => "5",
            'usergroup_id' => USER_ROLE_STAFF,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('userprofiles')->insert(
        [
            'user_id' => 6,
            'usergroup_id' => USER_ROLE_SYSTEM,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
    }
}
