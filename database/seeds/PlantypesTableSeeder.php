<?php

use Illuminate\Database\Seeder;

class PlantypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plantypes')->insert(
        [
            'plantype' => "daily",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plantypes')->insert(
        [
            'plantype' => "daily-business",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plantypes')->insert(
        [
            'plantype' => "hourly",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plantypes')->insert(
        [
            'plantype' => "weekly",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plantypes')->insert(
        [
            'plantype' => "monthly",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plantypes')->insert(
        [
            'plantype' => "quarterly",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('plantypes')->insert(
        [
            'plantype' => "yearly",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
    }
}
