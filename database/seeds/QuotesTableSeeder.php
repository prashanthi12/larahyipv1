<?php

use Illuminate\Database\Seeder;

class QuotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quotes')->insert(
        [
        	'id' => 1,
            'authorimage' => "/uploads/bg/bill-gates.png",
            'text' => "Bitcoin is better than currency in that you don’t have to be physically in the same place, and in the future, financial transactions will eventually be digital, universal and almost free",
            'authorname' => "Bill Gates",
            'language' => 'en',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),     
        ]);
        DB::table('quotes')->insert(
        [
        	'id' => 2,
            'authorimage' => "/uploads/bg/marc-andersen.png",
            'text' => "Inventor of the first web browser Netscape says Blockchain technology will impact the world more than the Internet has. Andreessen has invested $50 million in Coinbase and has vowed to invest hundreds of millions more",
            'authorname' => "Marc Andreessen",
            'language' => 'en',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('quotes')->insert(
        [
        	'id' => 3,
            'authorimage' => "/uploads/bg/julian-assange.png",
            'text' => "Bitcoin actually has the balance and incentives right, and that is why it is starting to take off",
            'authorname' => "Julian Assange",
            'language' => 'en',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('quotes')->insert(
        [
        	'id' => 4,
            'authorimage' => "/uploads/bg/richard-branson.png",
            'text' => "Billionaire Richard Branson hosted the greatest minds in Bitcoin on his private Island to discuss the future of Blockchain technology. Sir Richard has personally invested over $30 million into Bitcoin start ups so far",
            'authorname' => "Richard Branson",
            'language' => 'en',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
    }
}
