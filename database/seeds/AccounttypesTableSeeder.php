<?php

use Illuminate\Database\Seeder;

class AccounttypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accounttypes')->insert(
        [
            'account_type' => "master",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accounttypes')->insert(
        [
            'account_type' => "deposit",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accounttypes')->insert(
        [
            'account_type' => "operative",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accounttypes')->insert(
        [
            'account_type' => "exchange",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accounttypes')->insert(
        [
            'account_type' => "interest",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
    }
}
