<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
        [
            'name' => "superadmin",
            'email' => "superadmin@harambee-pp.com",
            'password' => bcrypt("superadmin"),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('users')->insert(
        [
            'name' => "admin",
            'email' => "admin@harambee-pp.com",
            'password' => bcrypt("admin"),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('users')->insert(
        [
            'name' => "staff",
            'email' => "staff@harambee-pp.com",
            'password' => bcrypt("staff"),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('users')->insert(
        [
            'name' => "staff1",
            'email' => "staff1@harambee-pp.com",
            'password' => bcrypt("staff1"),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('users')->insert(
        [
            'name' => "staff2",
            'email' => "staff2@harambee-pp.com",
            'password' => bcrypt("staff2"),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert(
        [
            'name' => "system",
            'email' => "system@harambee-pp.com",
            'password' => bcrypt('$System123'),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);
    }
}
