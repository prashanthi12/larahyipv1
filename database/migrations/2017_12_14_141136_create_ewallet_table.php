<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEwalletTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ewallet', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('to_user_id');
            $table->integer('from_user_id')->unsigned();
            $table->foreign('from_user_id')->references('id')->on('users');
            $table->double('amount');
            $table->integer('paymentgateway_id')->unsigned();
           // $table->foreign('paymentgateway_id')->references('id')->on('paymentgateways');
            $table->enum('status',['pending','approve','cancel']);
            $table->string('transaction_id');
            $table->text('request')->nullable();
            $table->text('response')->nullable();
            $table->string('bitcoin_hash_id')->nullable();
            $table->string('bitcoin_address')->nullable();
            $table->timestamps();
            $table->datetime('approve_at')->nullable();
            $table->datetime('cancel_at')->nullable();
            $table->text('comments_approve')->nullable();
            $table->text('comments_cancel')->nullable();
            $table->enum('type',['add','transfer']);
            $table->enum('process_via',['user','admin','auto'])->nullable();           
            $table->text('comments_pending')->nullable();
            $table->double('bonus_amount')->nullable();
            $table->double('received_amount')->default(0);
            $table->double('btc_amount')->nullable();
            $table->double('total_btc_amount')->nullable();                                       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ewallet');
    }
}
