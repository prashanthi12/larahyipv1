<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailmessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailmessages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from_user_id');
            //$table->foreign('from_user_id')->references('id')->on('users');
            $table->string('subject');
            $table->text('message');
            $table->integer('to_user_id');
           // $table->foreign('to_user_id')->references('id')->on('users');
            $table->enum('type',['active','suspend','all']);
            $table->integer('batch');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mailmessages');
    }
}
