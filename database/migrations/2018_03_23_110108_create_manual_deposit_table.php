<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManualDepositTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manual_deposit', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('deposit_type', ['existing','new']);
            $table->text('bank_name')->nullable();
            $table->integer('country_id')->nullable();
            $table->text('swift_code')->nullable();
            $table->text('account_no')->nullable();
            $table->string('account_name')->nullable();
            $table->text('account_address')->nullable();
            $table->enum('details_type', ['bank','mobile'])->nullable();
            $table->string('mobile')->nullable();
            $table->integer('deposit_id')->nullable();
            $table->integer('withdraw_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->text('proof')->nullable();
            $table->boolean('status')->default(1);
            $table->boolean('details')->default(1);
            $table->boolean('is_report')->default(0);
            $table->datetime('report_at')->nullable();
            $table->enum('type',['approve','reject','report'])->nullable(); 
            $table->datetime('approve_at')->nullable();
            $table->datetime('reject_at')->nullable();   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manual_deposit');
    }
}
