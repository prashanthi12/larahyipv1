<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            
            $table->increments('id');
            $table->unsignedBigInteger('credit_transaction_id')->nullable();
            $table->foreign('credit_transaction_id')->references('id')->on('transactions')->onDelete('cascade');
            $table->unsignedBigInteger('debit_transaction_id')->nullable();
            $table->foreign('debit_transaction_id')->references('id')->on('transactions')->onDelete('cascade');
            $table->double('amount',15,8);
            $table->enum('type', ['referral_commission', 'level_commission', 'joinin-bonus', 'performance_bonus']);
            $table->text('comments')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
