<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationbonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrationbonuses', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('name');   
            $table->integer('value'); 
            $table->integer('plan')->unsigned();
            $table->foreign('plan')->references('id')->on('plans')->onDelete('cascade');
            $table->boolean('status');           
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('registrationbonuses');
    }
}
