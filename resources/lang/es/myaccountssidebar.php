<?php 

 return [
    "sponsor" => "sponsor",
    "ctadeposit" => "Make a Deposit",
    "ctabutton" => "Complete Your Profile",
    "ctarefer" => "Refer a Friend",
    "menudashboard" => "Dashboard",
    "menuprofile" => "Profile",
    "menudeposits" => "Deposits",
    "menuwithdraw" => "Withdraw",
    "menuearnings" => "Earnings",
    "menureferral" => "Referrals",
    "menusupport" => "Support Ticket"
];
