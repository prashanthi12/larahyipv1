<?php 

 return [
    "welcomeMessageTitle"  => "Welcome to Larahyip",
    "welcomeMessageContent" => "LaraHYIP is making it easy for you to launch your own hyip investment site. The script is built with most commonly used HYIP Manager functionality and ready to install.",
   "dashboardMessageTitle"  => "Welcome to Larahyip Dashboard",
    "dashboardMessageContent" => "LaraHYIP is focusing on delivering high quality solutions to cater the demanding needs of FinTech Industry. The FinTech Industry is growing at rapid pace and the industry demands secure & user friendly applications. By applying modern web technology and proven framework, we focus on building web and mobiles apps that serve the purpose and been developed in few weeks. Contact our team for development of custom solutions.",
];