<?php 

 return [
    "create_profile" => "Create Your Profile",
    "incomplete_profile_alert" => "Your profile hasn't created yet. Complete the profile creation process by clicking the button below.",
    "profile_note" => "Your profile & KYC Info are the legal documents stored with us. If you want to make any updates please initate a support request",
    "username" => "Username",
    "email" => "Email",
    "firstname" => "First Name",
    "lastname" => "Last Name",
    "country" => "Country",
    "mobile" => "Mobile",
    "ssn" => "SSN / Passport No",
    "kyc" => "Attach Proof Document",
    "ctabutton" => "Complete Your Profile First :-)",
    "verified" => "Verified",
    "unverified" => "Unverified",
];
