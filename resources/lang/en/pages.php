<?php 

 return [
    "amount" => "Amount",
    "faqsubtitle" => "Answers to most common questions",
    "contacttitle" => "Contact",
    "datetime" => "Date & Time",
    "duration" => "Duration",
    "duration_key" => "Duration Key",
    "earn" => "Earn",
    "earntitle" => "Earn by Referring",
    "exchangetitle" => "Exchange",
    "faqtitle" => "FAQ",
    "faq" => "Frequently Asked Questions",
    "grid_view" => "Grid View",
    "interest_rate" => "Interest Rate",
    "invest" => "Invest",
    "investment_amount" => "Investment Amount",
    "investtitle" => "Investment Plans",
    "investorsdescription" => "Investors Description",
    "lastinvestorsdesc" => "Investors Description",
    "lastinvestorstitle" => "Latest Investors",
    "newssubtitle" => "Latest updates from our networks",
    "exchangesubtitle" => "Latest updates from our networks",
    "contactsubtitle" => "Let's start a conversation",
    "max_deposit" => "Max. Invest",
    "maximum_partial_withdraw_limit" => "Maximum Partial Withdraw Limit",
    "min_deposit" => "Min. Invest",
    "minimum_locking_period" => "Minimum Locking Period (Days)",
    "news" => "News",
    "newstitle" => "News",
    "noearnsfound" => "No Earns Found",
    "noexchangefound" => "No Exchange Found",
    "nofaqfound" => "No FAQ Found",
    "nonewsfound" => "No News Found",
    "noquotesfound" => "No Quotes Found",
    "no_records_found" => "No Records Found",
    "noreviewsfound" => "No Reviews Found",
    "partial_withdraw_fee" => "Partial Withdraw Fee (%)",
    "partial_withdraw_status" => "Partial Withdraw Status",
    "paymentmode" => "Payment Mode",
    "payoutsdesc" => "Payouts Description",
    "payoutstitle" => "Payouts Title",
    "plan" => "Plan",
    "plan_name" => "Plan Name",
    "plan_type" => "Plan Type",
    "investsubtitle" => "Plans for Everyone",
    "principle_amount_return" => "Principle Amount Return",
    "quotesdesc" => "Quotes Description",
    "quotesdescription" => "Quotes Description",
    "quotestitle" => "Quotes Title",
    "referrals" => "Referrals",
    "referralsdesc" => "Referrals Description",
    "reviewstitle" => "Reviews",
    "statsdescription" => "Statistics Description",
    "statsdesc" => "Statistics Description",
    "statstitle" => "Statistics Title",
    "table_view" => "Table View",
    "investorstitle" => "Top Investors",
    "referralstitle" => "Top Referrals",
    "earnsubtitle" => "Upto 15% on Investments",
    "username" => "Username",
    "reviewssubtitle" => "What our investors to say...",
    "plan_details" => "plan details"
];