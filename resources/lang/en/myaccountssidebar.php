<?php 

 return [
    "sponsor" => "sponsor",
    "ctadeposit" => "Make a Invest",
    "ctabutton" => "Complete Your Profile",
    "ctarefer" => "Refer a Friend",
    "menudashboard" => "Dashboard",
    "menuprofile" => "Profile",
    "menudeposits" => "Investments",
    "menuwithdraw" => "Withdraw",
    "menuearnings" => "Earnings",
    "menureferral" => "Referrals",
    "menusupport" => "Support Ticket"
];
