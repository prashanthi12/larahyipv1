<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'checklogin' => 'You are suspended .Please Contact Admin',
    'termsandcondn_req'=>'Terms and Condition is required',
     'iagree' => 'I agree to',
    'termsandcondn'=>'Terms and Conditions',

];
