<?php 

 return [
    "newrequest" => "New Request",
    "pending" => "Pending",
    "completed" => "Completed",
    "rejected" => "Rejected",
    "withdraws" => "Withdraws",
    "withdraws" => "Withdraws",
    "deposits" => "Deposits",
    "mypayaccounts" => "My Pay Accounts",
    "profile" => "Profile",
    "dashboard" => "Dashboard",
    "earnings" => "Earnings",
    "referrals" => "Referrals",
    "networks" => "Networks",
    "supporttickets" => "Support Tickets",
    "addtestimonial" => "Add Testimonial",
    "fund_transfer" => "Fund Transfer",
    "penalties" => "Penalties",
    "bonus" => "Bonus",
    "message" => "Message",
    "myinbox" => "My Inbox",
    "myepin" => "My E - Pin",
    "ewallet" => "E-Wallet",

];