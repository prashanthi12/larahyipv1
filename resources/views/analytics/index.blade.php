@extends('layouts.main') 
@section('banner')
    @include('partials.homebanner')
@endsection
@section('content')
<div class="row bg-color-1">
    <div class="container">
    	{{ $analyticsData }}
    </div>
</div>
@endsection