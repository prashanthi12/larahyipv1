<div class="grid">
@php 
       $count_down_date='';
       
      
    @endphp
@if (count($withdrawlists) > 0)
    @foreach($withdrawlists as $data)
    @php 
       $count_down_date='';
        if(count($data->assignmanualdeposit) > 0 )
           $count_down_date=$data->assignmanualdeposit->created_at->hour(24);     
    @endphp
    <div class="grid grid-4 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }})</small><br/>
        {{ $data->amount }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('myaccount.payment') }} : </small><br/>       
          @include('withdraw.popoveruserpayaccounts')<br>
      </p>
    </div>   
    <div class="">
        <p style="text-align: center;">
            <small>{{ trans('myaccount.request_date') }} :</small><br/>
                {{ $data->created_at->diffForHumans() }}
        </p>
    </div>
    <div class="">
        <p style="text-align: center;">
            <small>{{ trans('admin.actions') }} :</small><br/>

            @if(count($data->assignmanualdeposit) > 0 && ($data->assignmanualdeposit->proof == ''))
               {{trans('myaccount.withdraw_pending_assign_message')}}
           
            @elseif(count($data->assignmanualdeposit) > 0 && ($data->assignmanualdeposit->proof != ''))
              <a class='btn btn-success btn-xs flex-button' href="{{ $data->assignmanualdeposit->proof }}" download>{{ trans('admin.viewproof') }}</a>
              <a href="#" class="btn btn-success btn-xs flex-button confirm" rel="{{ url('myaccount/deposit/confirm/'.$data->assignmanualdeposit->deposit_id.'/'. $data->id) }}">{{ trans('admin.confirm') }}</a>

            @else
              {{trans('myaccount.withdraw_pending_message')}}
            @endif          
        </p>
      @if(count($data->assignmanualdeposit)>0)
          <div class="" id="clock">{{ $count_down_date->diffForHumans() }}</div> 
      @endif
</div>
<div>
          @if(count($data->assignmanualdeposit)>0)
                 @if(count($data->assignmanualdeposit->deposit)>0)
                 <p><strong>{{trans('myaccount.reportdetails')}}</strong></p>
                
                  <div><p>{{$data->assignmanualdeposit->user->name}}<br>
                  {{$data->assignmanualdeposit->user->email}}<br>
                  {{$data->assignmanualdeposit->user->userprofile->mobile}}</p>
                  </div>
                  
                      
                @if($data->assignmanualdeposit->details_type=='bank')
                  <p><strong>{{trans('myaccount.accountdetails')}}</strong></p>
                  <div>{{$data->assignmanualdeposit->bank_name}}</div>
                  <div>{{$data->assignmanualdeposit->swift_code}}</div>
                  <div>{{$data->assignmanualdeposit->account_no}}</div>
                  <div>{{$data->assignmanualdeposit->account_name}}</div>
                  <div>{{$data->assignmanualdeposit->account_address}}</div>
              

                  @endif
                    @if($data->assignmanualdeposit->details_type=='mobile')
                  <p><strong>{{trans('myaccount.mobile_accountdetails')}}</strong></p>
                  <div>{{$data->assignmanualdeposit->mobile}}</div>
                  <div>{{optional($data->assignmanualdeposit->country)->name}}</div>
                  
            @endif
             @endif
                  @endif
      </div>
    </div>
    </div>
  @endforeach 
@else
    <div class="" style="border:1px solid #ddd; padding: 10px;">{{ trans('forms.nowithdraws') }}</div>
@endif
</div>
{{ $withdrawlists->links() }}

@push('bottomscripts')
<script src="{{url('js/jquery.countdown.js')}}"></script>

<script>
    // $('.clock').countdown('{{$count_down_date}}', function(event) {
      $('.clock').each(function(){
        $(this).countdown('{{ $count_down_date }}', function(event) {
            var $this = $(this).html(event.strftime(''
            + '<div >%D</div><div>:</div> '
            + '<div >%H</div><div>:</div> '
            + '<div >%M</div><div>:</div> '
            + '<div >%S</div>')); 
      })
    });
</script>
@endpush

@push('bottomscripts')
<link rel="stylesheet" href="/css/sweetalert2.min.css">
<script src="/js/sweetalert2.min.js"></script>

<script>
  $(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });

    $('.confirm').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to approve this deposit ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 
});
</script>
@endpush







