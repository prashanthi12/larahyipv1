<div class="grid">
@if (count($withdrawlists) > 0)
    @foreach($withdrawlists as $data)
    <div class="grid grid-4 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
        <p>
            <small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }})</small><br/>
            {{ $data->amount }}
        </p>
    </div>
    <div class="">
        <p>
            <small>{{ trans('myaccount.payment') }} : </small><br/>       
            @include('withdraw.popoveruserpayaccounts')<br>
        </p>
    </div>   
    <div class="">
        <p>
            <small>{{ trans('myaccount.comments') }} :</small><br/>
                {{ trans('myaccount.withdrawrequestcomments') }}<br>
            <small>{{ trans('myaccount.request_date') }} :</small><br/>
                {{ $data->created_at->diffForHumans() }}
        </p>
    </div>
    <div class="">
        <p>
            <small>{{ trans('myaccount.action') }} : </small><br/>       
            <form class="activate" action="{{ url('myaccount/withdraw/otpsendagain') }}" method="post">
                {{ csrf_field() }} 
                <input type="hidden" name="amount" value="{{ $data->amount }}">
                <input type="hidden" name="paymentgateway" value="{{ $data->userpayaccounts->paymentgateways_id }}">
                <input type="hidden" name="userpayaccountid" value="{{ $data->payaccount_id }}">
                <input type="hidden" name="withdrawid" value="{{ $data->id }}">
                <input value="Send" class="btn btn-success btn-sm flex-button" type="submit" onclick="this.disabled=true;this.form.submit();">
            </form>
        </p>
    </div> 
    </div>
    @endforeach 
@else
    <div class="" style="border:1px solid #ddd; padding: 10px;">{{ trans('forms.nowithdraws') }}</div>
@endif
</div>
{{ $withdrawlists->links() }}

@push('bottomscripts')
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });
});
</script>
@endpush







