<div class="grid">
@if (count($withdrawlists) > 0)
    @foreach($withdrawlists as $data)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }})</small><br/>
        {{ $data->amount }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('myaccount.payment') }} : </small><br/>       
            @include('withdraw.popoveruserpayaccounts')<br>
        <small>{{ trans('myaccount.request_date') }} : </small><br/>       
            {{ $data->created_at->diffForHumans() }}
      </p>
    </div>   
    <div class="">
        <p>
            <small>{{ trans('myaccount.comments') }} ({{ trans('myaccount.hover_content') }}) :</small><br/>
                <span data-html="true" data-toggle="comment-popover"  data-content="{{ $data->comments_on_rejected }}">{{ substr($data->comments_on_rejected, 0, 15) }}...</span><br>
            <small>{{ trans('myaccount.rejected_date') }} :</small><br/>
                {{ $data->rejected_on->diffForHumans() }}
        </p>
    </div>
    </div>
  @endforeach 
@else
    <div class="" style="border:1px solid #ddd; padding: 10px;">{{ trans('forms.nowithdraws') }}</div>
@endif
</div>
{{ $withdrawlists->links() }}

@push('bottomscripts')
<script>
  $(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });

    $('[data-toggle="comment-popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });
});
</script>
@endpush







