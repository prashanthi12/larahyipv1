@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.withdrawrequest') }}
    <a href="{{ url('myaccount/withdraw/pending') }}" class="pull-right">{{ trans('myaccount.back_to_list') }}</a>
    </div>
    <div class="panel-body">
         <div class="row">
            <div class="col-md-12">
                 @if (session('mobilecodeerror'))
                    <div class="alert alert-danger">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ session('mobilecodeerror') }}
                    </div>
                    @endif
                @include('layouts.message')
            </div>
        </div>

        <div class="row">

            <div class="col-md-12">
                 <div class="col-md-3">
                <p>{{ trans('forms.minamount') }}{{ Config::get('settings.withdraw_min_amount') }} {{ config::get('settings.currency') }}</p>
                <p>{{ trans('forms.maxamount') }}{{ Config::get('settings.withdraw_max_amount') }} {{ config::get('settings.currency') }}</p>
                <p>{{ trans('forms.balance') }} : {{ $userbalance }} {{ config::get('settings.currency') }}</p>
                 </div> 
                 <div class="col-md-3 col-md-offset-1">
                 <p>{{ trans('forms.total_monthly_withdraw_limit') }} : <span class="label label-warning">{{ \Config::get('settings.monthly_withdraw_limit') }}</span></p>
                <p>{{ trans('forms.user_withdraw_taken_count') }} : <span class="label label-success">{{ $user_withdraw_count }}</span></p>
                <p>{{ trans('forms.remaining_monthly_withdraw_limit') }} : <span class="label label-danger">{{ $monthly_remaining_withdraw_limit }}</span></p>
                 </div> 
                 <div class="col-md-3 col-md-offset-1">
                <p>{{ trans('forms.total_daily_withdraw_limit') }} : <span class="label label-warning">{{ \Config::get('settings.daily_withdraw_limit') }}</span></p>

                <p>{{ trans('forms.daily_withdraw_taken_count') }} : <span class="label label-success">{{ $daily_withdraw_taken_count }}</span></p>

                <p>{{ trans('forms.daily_remaining_withdraw_limit') }} : <span class="label label-danger">{{ $daily_remaining_withdraw_limit }}</span></p>

                 </div>
                 </div>
            
        </div>

        <div class="row">

            @if ($force_withdraw_down == 0)       

            <div class="col-md-10 col-md-offset-1">

                @if ($force_email_verification_for_withdraw == 0 && $force_kyc_verification_for_withdraw == 0) 

                    @include('withdraw.withdrawform')                  

                    
                @else
                    
                         @if ($force_email_verification_for_withdraw == 1 || $force_kyc_verification_for_withdraw == 1) 


                                @if ($isEmailVerified == 0 && $force_email_verification_for_withdraw == 1)
                                      <p>{{ trans('forms.email_verification_incomplete_withdraw_alert') }}</p>

                                @else
                                    @if (($isKycApproved  == 0 || $isKycApproved ==2) && $force_kyc_verification_for_withdraw == 1)
                                         <p>{{ trans('forms.kyc_incomplete_withdraw_alert') }}</p>
                                    <a href="{{ url('/myaccount/profile') }}" class="btn btn-primary btn-lg">{{ trans('myaccount.ctabutton') }}</a>

                                    @else
                                        @include('withdraw.withdrawform')
                                    @endif
                                    
                                @endif

                        @endif
                @endif

            @else
                <div class="col-md-10 col-md-offset-1">
                    <p>{{ trans('forms.force_withdraw_down_info_message') }}</p>
                </div>

            </div>
            @endif
        </div>
    </div>
</div>
@endsection
