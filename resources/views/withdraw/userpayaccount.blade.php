<div class="col-md-10 col-md-offset-1">

<div class="form-group">
        <label>{{ trans('forms.admin_commission') }} :</label>
        {{ $admincommission }} %      
    </div> 

@foreach($payaccount_result as $payaccount)
@php
$current = '';
if ($payaccount['current'] == 1)
{
    $current = 'checked';
}
@endphp


@if ($payaccount['paymentgateways_id'] == 1 || $payaccount['paymentgateways_id'] == 20)    

    <div class="form-group">
        <input type="radio" name="userpayaccountid" value="{{ $payaccount['id'] }}" {{ $current }}>&nbsp;<label>{{ trans('forms.bank_name_lbl') }} :</label>
        {{ $payaccount['param1'] }}       
    </div> 

    <div class="form-group">
        <label>{{ trans('forms.swift_code_lbl') }} :</label>
        {{ $payaccount['param2'] }}       
    </div> 

    <div class="form-group">
        <label>{{ trans('forms.account_no_lbl') }} :</label>
        {{ $payaccount['param3'] }}       
    </div> 

    <div class="form-group">
        <label>{{ trans('forms.account_name_lbl') }} :</label>
        {{ $payaccount['param4'] }}       
    </div> 

    <div class="form-group">
        <label>{{ trans('forms.account_address_lbl') }} :</label>
        {{ $payaccount['param5'] }}       
    </div>     
@endif

@if ($payaccount['paymentgateways_id'] == 2)

	<div class="form-group">
	    <input type="radio" name="userpayaccountid" value="{{ $payaccount['id'] }}" {{ $current }}>&nbsp;<label>{{ trans('forms.paypal_id_lbl') }} :</label>
	    {{ $payaccount['param1'] }}	   
	</div>   
@endif


@if ($payaccount['paymentgateways_id'] == 3)

    <div class="form-group">
        <input type="radio" name="userpayaccountid" value="{{ $payaccount['id'] }}" {{ $current }}>&nbsp;<label>{{ trans('forms.user_name_lbl') }} :</label>
        {{ $payaccount['param1'] }}       
    </div>   

@endif

@if ($payaccount['paymentgateways_id'] == 4)

    <div class="form-group">
        <input type="radio" name="userpayaccountid" value="{{ $payaccount['id'] }}" {{ $current }}>&nbsp;<label>{{ trans('forms.payeer_id_lbl') }} :</label>
        {{ $payaccount['param1'] }}        
    </div>   

     <div class="form-group">
        <label>{{ trans('forms.payeer_email_lbl') }} :</label>
        {{ $payaccount['param2'] }}       
    </div> 
    
@endif

@if ($payaccount['paymentgateways_id'] == 5)

    <div class="form-group">
        <input type="radio" name="userpayaccountid" value="{{ $payaccount['id'] }}" {{ $current }}>&nbsp;<label>{{ trans('forms.account_emailid_lbl') }} :</label>
        {{ $payaccount['param1'] }}       
    </div>     
    
@endif


@if ($payaccount['paymentgateways_id'] == 9)
    
    <div class="form-group">
        <input type="radio" name="userpayaccountid" value="{{ $payaccount['id'] }}" {{ $current }}>&nbsp;<label>{{ trans('forms.bitcoin_coinname_lbl') }} :</label>
        {{ $payaccount['param1'] }}       
    </div>     

    <div class="form-group">
        <label>{{ trans('forms.bitcoin_btccode_lbl') }} :</label>
        {{ $payaccount['param2'] }}      
    </div>     
    
@endif

@if ($payaccount['paymentgateways_id'] == 11)
    
    <div class="form-group">
        <input type="radio" name="userpayaccountid" value="{{ $payaccount['id'] }}" {{ $current }}>&nbsp;<label>{{ trans('forms.skrill_email_lbl') }} :</label>
        {{ $payaccount['param1'] }}        
    </div>    
@endif


@if ($payaccount['paymentgateways_id'] == 12)

    <div class="form-group">
        <input type="radio" name="userpayaccountid" value="{{ $payaccount['id'] }}" {{ $current }}>&nbsp;<label>{{ trans('forms.okpay_payid_lbl') }} :</label>
        {{ $payaccount['param1'] }}       
    </div> 

    <div class="form-group">
        <label>{{ trans('forms.okpay_paynumber_lbl') }} :</label>
        {{ $payaccount['param2'] }}       
    </div>       
@endif


@if ($payaccount['paymentgateways_id'] == 15)

    <div class="form-group">
        <input type="radio" name="userpayaccountid" value="{{ $payaccount['id'] }}" {{ $current }}>&nbsp;<label>{{ trans('forms.payee_account_lbl') }} :</label>
        {{ $payaccount['param1'] }}    
    </div>   
@endif

@if ($payaccount['paymentgateways_id'] == 16)

   
    <div class="form-group">
        <input type="radio" name="userpayaccountid" value="{{ $payaccount['id'] }}" {{ $current }}>&nbsp;<label>{{ trans('forms.merchant_id_lbl') }} :</label>
        {{ $payaccount['param1'] }}    
    </div>  

    <div class="form-group">
        <label>{{ trans('forms.merchant_key_lbl') }} :</label>
        {{ $payaccount['param1'] }}    
    </div>    
@endif
@if ($payaccount['paymentgateways_id'] == 21)

    <div class="form-group">
        <input type="radio" name="userpayaccountid" value="{{ $payaccount['id'] }}" {{ $current }}>&nbsp;<label>{{ trans('forms.mobile_no_lbl') }} :</label>
        {{ $payaccount['param9'] }}    
    </div>   
    <div class="form-group">
        <label>{{ trans('forms.country_lbl') }} :</label>
        {{ optional($payaccount->country)->name }}       
    </div> 
@endif

@endforeach

</div>





