<div class="col-md-12 ">
<form method="post" action="{{ url('superadmin/registrationbonus')}}" class="form-horizontal" id="contact">
{{ csrf_field() }}    
    

   
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label>Name</label>
        <input name="name" class="form-control" value="{{ old('name') }}" type="text">
        <small class="text-danger">{{ $errors->first('name') }}</small>
    </div> 

    <div class="form-group{{ $errors->has('bonusvalue') ? ' has-error' : '' }}">
    <label>Bonus Value (Flat)</label>
        <input name="bonusvalue" class="form-control" value="{{ old('bonusvalue') }}" type="text" >
        <small class="text-danger">{{ $errors->first('bonusvalue') }}</small>
    </div>     

     <div class="form-group" id="plans" >
        <label>Invest Plans</label>
        <select class="form-control" id="plan" name="plan" >
            @foreach ($plans as $plan)
                <option value="{{ $plan->id }}" {{ (Form::old("plan") == $plan->id ? "selected":"") }}>{{ $plan->name }}</option>
            @endforeach
        </select>
        <small class="text-danger">{{ $errors->first('plan') }}</small>
    </div> 

   

    
    
    <div class="form-group">
        {!! Form::submit("Submit", ['class' => 'btn btn-primary']) !!}
        <a href=" " class='btn btn-default'>{{ trans('forms.reset') }}</a>
    </div>
</form>
</div>

