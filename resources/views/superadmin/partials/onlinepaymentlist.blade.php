<table class="table table-bordered">
	<thead>
		<tr>
			<th>#</th>
			<th>Gateway Name</th>
			<th>Invest</th>
			<th>Withdraw</th>
			<th>E-Wallet</th>
			<th>Active</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@if (count($paymentgatewaylist) > 0)
		@foreach ($paymentgatewaylist as $paymentgateway)
		<tr>
			<td>{{ $loop->iteration }}</td>
			<td>{{ $paymentgateway->displayname }}</td>
			@if($paymentgateway->deposit == '1')
				<td>True</td>
			@else
				<td>False</td>
			@endif
			@if($paymentgateway->withdraw == '1')
				<td>True</td>
			@else
				<td>False</td>
			@endif
			@if($paymentgateway->e_wallet == '1')
				<td>True</td>
			@else
				<td>False</td>
			@endif
			@if($paymentgateway->active == '1')
				<td>True</td>
			@else
				<td>False</td>
			@endif
			<td><a href="{{url('/superadmin/paymentgateway/online/edit/'.$paymentgateway->id)}}">Edit</a></td>
		@endforeach
		@endif
	</tbody>
</table>


