@extends('backpack::layout')
@section('content')
<section class="section">

    <div class="row">

        <div class="container">
        
            <h3>Add Bonus Form</h3>
<p class="pull-right"><a href="{{ url('superadmin/bonus') }}">Back to Bonus List</a></p>
                 <div class="panel panel-default">
   
    <div class="panel-body">
       
        <div class="row">
            <div class="col-md-12">
                         
                         @include('superadmin._bonusform')  
                       
            </div>
        </div>
    </div>
 </div>
            </div>
        </div>


</section>
@endsection
