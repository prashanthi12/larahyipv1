@extends('backpack::layout')
@section('content')
<section class="section">

    <div class="row">

        <div class="container">
        
            <h3>Add User Form</h3>
<p class="pull-right"><a href="{{ url('superadmin/members') }}">Back to User List</a></p>
                 <div class="panel panel-default">
   
    <div class="panel-body">
       
        <div class="row">
            <div class="col-md-12">
                         
                            @include('superadmin._userform')
                       
            </div>
        </div>
    </div>
 </div>
            </div>
        </div>


</section>
@endsection
