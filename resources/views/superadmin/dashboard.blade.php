@extends('backpack::layout')
@section('content')
<section class="section">
    <div class="row">
        <div class="container">    
            <h3>Details Overview</h3>
			<p class="pull-right"><a href="{{ url('superadmin/members') }}">Back to User List</a></p>
        	<div class="panel panel-default"> 
    			<div class="panel-body">
       				<div class="row">
            			<div class="col-md-12">
                        	<div class="card-content col-md-3">
		                        <div class="content">
		                            <p>{{ trans('admin.totalusers') }} : {{ $totalmembers }}</p>
		                            <p>{{ trans('admin.activeusers') }} : {{ $activemembers }}</p>
		                            <p>{{ trans('admin.unverifiedusers') }} : {{ $unverifiedmembers }}</p>
		                        </div>
		                    </div>  

		                    <div class="card-content col-md-3">
		                        <div class="content">
		                            <p>{{ trans('admin.activedeposit') }} : <a href="{{ url('admin/deposit/active') }}">{{ $activedeposit }}</a></p>
		                            <p>{{ trans('admin.unapproveddeposit') }} : <a href="{{ url('admin/deposit/new') }}">{{ $unapprovedeposit }}</a></p>
		                            <p>{{ trans('admin.matureddeposit') }} : <a href="{{ url('admin/deposit/matured') }}">{{ $maturedeposit }}</a></p>
		                        </div>
		                    </div> 

		                    <div class="card-content col-md-3">
		                        <div class="content">
		                            <p>{{ trans('admin.pendingpayouts') }} : <a href="{{ url('admin/withdraw/pending') }}">{{ $pendingwithdraw }}</a></p>
		                            <p>{{ trans('admin.completedpayouts') }} : <a href="{{ url('admin/withdraw/completed') }}">{{ $completedwithdraw }}</a></p>
		                            <p>{{ trans('admin.rejectedpayouts') }} : <a href="{{ url('admin/withdraw/rejected') }}">{{ $rejectedwithdraw }}</a></p>
		                        </div>
		                    </div> 

		                    <div class="card-content col-md-3">
		                        <div class="content">
		                            <p>{{ trans('admin.asinterest') }} : {{ $sumofinterest }} {{ config::get('settings.currency') }}</p>
		                            <p>{{ trans('admin.asrefcommission') }} : {{ $sumofreferralcommission }} {{ config::get('settings.currency') }}</p>
		                            <p>{{ trans('admin.aslevelcommission') }} : {{ $sumoflevelcommission }} {{ config::get('settings.currency') }}</p>
		                            <!-- <p>As Bonus Commission :</p> -->
		                        </div>
		                    </div>        
            			</div>
            			<div class="row">
		                  	<div class="col-md-12">           
		                    	<div class=" col-md-6 ui-widget-content">	                     		
		                    		<daily-deposit :keys="{{$date_array_coll->values()}}" :values1="{{$chart_deposits->values()}}" :values2="{{$chart_withdraws->values()}}" label="Daily Data for Last 7 days" type="line"></daily-deposit>
		                     	</div>
		                     	<div class=" col-md-6 ui-widget-content">	                     		
		                    		<plan-deposit :keys="{{$chart_plan_deposits->keys()}}" :values="{{$chart_plan_deposits->values()}}" label="Investments based on Plan" type="pie"></plan-deposit>
		                     	</div>
		                    </div>
		                </div>
        			</div>
    			</div>
			</div>
        </div>
    </div>
</section>
@endsection

@push('bottomscripts')   
<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script> 
    <script src="{{ asset('js/app.js') }}"></script>                        
@endpush
