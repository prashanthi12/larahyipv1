@extends('backpack::layout')
@section('content')
<section class="section">
    <div class="row">
        <div class="container" style="width: 1122px;">      
            <h3>Edit Payment Gateway Details</h3>
            <div class="panel panel-default col-md-8">  
                <div class="panel-body">
                	<p class="pull-right"><a href="{{ url('superadmin/paymentgateway/online') }}">Back to Paymentgateways</a></p>
                	<div class="col-md-8">
						<form method="post" action="{{ url('superadmin/paymentgateway/edit/'.$paymentgateway->id)}}" class="form-horizontal" id="paymentgateway">
						{{ csrf_field() }}
                            <input type="hidden" name="paymentgateway" value="{{ $paymentgateway->id }}">
							<div class="form-group{{ $errors->has('gatewayname') ? ' has-error' : '' }} col-md-12">
    							<label>Name</label>
    							<input type="text" name="gatewayname" value="{{ old('gatewayname',  isset($paymentgateway->gatewayname) ? $paymentgateway->gatewayname : null) }}" class="form-control">
                                <small class="text-danger">{{ $errors->first('gatewayname') }}</small>
            				</div>     
    						<div class="form-group{{ $errors->has('displayname') ? ' has-error' : '' }} col-md-12">
    							<label>Display Name</label>
    							<input type="text" name="displayname" value="{{  old('displayname',  isset($paymentgateway->displayname) ? $paymentgateway->displayname : null) }}" class="form-control">
                                <small class="text-danger">{{ $errors->first('displayname') }}</small>
            				</div>
            				<div>
            				<div class="form-group col-md-6">
								<div>
        							<label>Status</label>
            					</div>
					            <div class="radio">
					                <label for="inactive"><input type="radio" id="inactive" name="status" value="0" @if($paymentgateway->active==0) checked @endif> Inactive</label>					           
					            </div>
					            <div class="radio">
					                <label for="active"><input type="radio" id="active" name="status" value="1" @if($paymentgateway->active==1) checked @endif> Active</label>
					            </div>   
							</div>
							<div class="form-group col-md-6">
                                <div>
                                    <label>Invest</label>
                                </div>
                                <div class="radio">
                                    <label for="disable"><input type="radio" id="disable" name="deposit" value="0" @if($paymentgateway->deposit==0) checked @endif> Disable</label>
                                </div>
                                <div class="radio">
                                    <label for="enable"><input type="radio" id="enable" name="deposit" value="1" @if($paymentgateway->deposit==1) checked @endif> Enable</label>
                                </div>   
                            </div>
							</div>
                            @if($status != 'reinvest' && $status != 'e-pin' && $status != 'e-wallet')
							<div>
                            <div class="form-group col-md-6">
                                <div>
                                    <label>E-Wallet</label>
                                </div>
                                <div class="radio">
                                    <label for="disable"><input type="radio" id="disable" name="ewallet" value="0" @if($paymentgateway->e_wallet==0) checked @endif> Disable</label>
                                </div>
                                <div class="radio">
                                    <label for="enable"><input type="radio" id="enable" name="ewallet" value="1" @if($paymentgateway->e_wallet==1) checked @endif> Enable</label>
                                </div>   
                            </div>
    						<div class="form-group col-md-6">
								<div>
        							<label>Withdraw</label>
            					</div>
					            <div class="radio">
					                <label for="disable"><input type="radio" id="disable" name="withdraw" value="0" @if($paymentgateway->withdraw==0) checked @endif> Disable</label>
					            </div>
					            <div class="radio">
					                <label for="disable"><input type="radio" id="disable" name="withdraw" value="1" @if($paymentgateway->withdraw==1) checked @endif> Enable</label>
					            </div>   
							</div>
							</div>
                            @endif
							<div>
							<div class="form-group col-md-6">
								<div>
        							<label>Status for Invest Fee</label>
            					</div>
					            <div class="radio">
					                <label for="disable"><input type="radio" id="disable" name="depositfee" value="0" @if($paymentgateway->deposit_fee_status==0) checked @endif> Disable</label>
					            </div>
					            <div class="radio">
					                <label for="enable"><input type="radio" id="enable" name="depositfee" value="1" @if($paymentgateway->deposit_fee_status==1) checked @endif> Enable</label>
					            </div>   
							</div>
    						<div class="form-group col-md-6">
								<div>
        							<label>Invest Fee Type</label>
            					</div>
					            <div class="radio">
					                <label for="flat"><input type="radio" id="flat" name="depositfeetype" value="1" @if($paymentgateway->deposit_fee_type==1) checked @endif> Flat</label>
					            </div>
					            <div class="radio">
					                <label for="percentage"><input type="radio" id="percentage" name="depositfeetype" value="2" @if($paymentgateway->deposit_fee_type==2) checked @endif> Percentage</label>
					            </div>   
							</div>
							</div>
							
						    <div class="form-group col-md-12">
    							<label>Fee for Invest</label>
    							<input type="text" name="depositfeevalue" value="{{ $paymentgateway->deposit_fee_value }}" class="form-control">
            				</div> 
                            @if($status != 'reinvest' && $status != 'e-pin' && $status != 'e-wallet') 
            				<div class="form-group{{ $errors->has('withdrawcommission') ? ' has-error' : '' }} col-md-12">
    							<label>Withdraw Commission (%)</label>
    							<input type="text" name="withdrawcommission" value="{{ old('withdrawcommission',  isset($paymentgateway->withdrawcommission) ? $paymentgateway->withdrawcommission : null) }}" class="form-control">
                                <small class="text-danger">{{ $errors->first('withdrawcommission') }}</small>
            				</div>  
            				<div class="form-group col-md-12">
    							<label>Ewallet Fund Minimum Amount</label>
    							<input type="text" name="ewalletminamount" value="" class="form-control">
            				</div>  
            				<div class="form-group col-md-12">
    							<label>Ewallet Fund Maximum Amount</label>
    							<input type="text" name="ewalletmaxamount" value="" class="form-control">
            				</div>  
            				<div class="form-group col-md-12">
    							<label>Ewallet Fund Bonus Amount</label>
    							<input type="text" name="ewalletbonusamount" value="" class="form-control">
            				</div> 
                            @endif 
            				<div class="form-group{{ $errors->has('params') ? ' has-error' : '' }} col-md-12">
    							<label>Details (Please fill the valid details)</label>
    							<!-- <p class="pull-right"><a href="" data-toggle="modal" data-target="#banktransfer">Edit</a></p> -->
    							<textarea class="form-control" name="params">{{ old('params',  isset($paymentgateway->params) ? $paymentgateway->params : null) }}</textarea>
                                <small class="text-danger">{{ $errors->first('params') }}</small>
            				</div>  
            				<div class="form-group{{ $errors->has('instructions') ? ' has-error' : '' }} col-md-12">
    							<label>Instructions</label>
    							<textarea class="form-control" name="instructions">{{ old('instructions',  isset($paymentgateway->instructions) ? $paymentgateway->instructions : null) }}</textarea>
                                <small class="text-danger">{{ $errors->first('instructions') }}</small>
            				</div>    
						    <div class="form-group col-md-12">
						        {!! Form::submit("Update", ['class' => 'btn btn-primary']) !!}
						        <a href="/superadmin/paymentgateway/online" class='btn btn-default'>Cancel</a>
						    </div>
						</form>
					</div>
                </div>
            </div>
        </div>
    </div>

				<!-- Need to be used later -->
<!-- Modal -->
<!-- <div class="modal fade" id="banktransfer" role="dialog" style="margin: 50px;"> 
    <div class="modal-dialog"> -->
    
      <!-- Modal content-->
      	<!-- <div class="modal-content">
	        <div class="modal-header">
	          	<button type="button" class="close" data-dismiss="modal">&times;</button>
	          	<h4 class="modal-title">{{ trans('forms.bank_details') }}</h4>
	        </div>
        	<div class="modal-body">
             	<form method="post" id="bankwire_modal" action="{{ url('superadmin/offline/update/banktransfer')}}">
            	{{ csrf_field() }}
              	<div class="form-group">
	                <label class="control-label">{{ trans('forms.bank_name_lbl') }}:</label>
	                <input type="text" class="form-control" id="bank_name" name="bank_name" required>
                </div> 
              	<div class="form-group">
                	<label class="control-label">{{ trans('forms.swift_code_lbl') }}:</label>
                	<input type="text" class="form-control" id="swift_code" name="swift_code" required>
              	</div> 
	            <div class="form-group">
	                <label class="control-label">{{ trans('forms.account_no_lbl') }}:</label>
	                <input type="text" class="form-control" id="account_no" name="account_no" required>
	            </div> 
              	<div class="form-group">
                	<label class="control-label">{{ trans('forms.account_name_lbl') }}:</label>
                	<input type="text" class="form-control" id="account_name" name="account_name" required>
              	</div> 
              	<div class="form-group">
                	<label class="control-label">{{ trans('forms.account_address_lbl') }}:</label>
                	<input type="text" class="form-control" id="account_address" name="account_address" required>
              	</div> 
              	<div class="form-group">
                    <input type="hidden" name="paymentid" value="1">
                    <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success" id="payment" type="submit" onclick="this.disabled=true;this.form.submit();">
                </div>           
            	</form>
        	</div>
        	<div class="modal-footer">
          		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        	</div>
     	</div>  
  	</div>
</div>  -->
</section>
@endsection


