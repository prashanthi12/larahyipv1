@extends('layouts.myaccount')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('forms.2fa_secret_key') }}</div>
        <div class="panel-body">
            {{ trans('forms.2fa_qr_code_text') }}
            <br />
            <img alt="Image of QR barcode" src="{{ $image }}" />
            <br />
            {{ trans('forms.2fa_qr_code_content') }}<code>{{ $secret }}</code>
            <br /><br />
            <a href="{{ url('/myaccount/2fa/validate') }}">{{ trans('forms.next_btn') }}</a>
        </div>
    </div>
@endsection