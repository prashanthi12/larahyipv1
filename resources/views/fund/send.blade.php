@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.send_fund_transfer_form') }}   
        <a href="{{ url('myaccount/fundtransfer/type/send') }}" class="pull-right">{{ trans('myaccount.back_to_list') }}</a>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                @include('layouts.message')
            </div>
        </div>
        <div class="row">     
            @if ($force_email_verification_for_fund_transfer == 0 && $force_kyc_verification_for_fund_transfer == 0)
                @if (is_null($transaction_password))
                    <center>
                        <p>{{ trans('forms.transaction_password_fund_transfer_alert') }}</p>
                            <a href="{{ url('/myaccount/transaction_password') }}" class="btn btn-primary btn-lg">{{ trans('forms.transaction_password_button') }}</a>
                    </center>
                @else         
                   @include('fund.fundtransferform') 
                @endif                          
            @else          
                @if ($force_email_verification_for_fund_transfer == 1 || $force_kyc_verification_for_fund_transfer == 1) 
                    @if ($user->isEmailVerified == 0 && $force_email_verification_for_fund_transfer == 1)             
                        <p>{{ trans('forms.email_verification_incomplete_fund_transfer_alert') }}</p>
                    @else
                        @if (($user->isKycApproved  == 0 || $user->isKycApproved ==2) && $force_kyc_verification_for_fund_transfer == 1)
                            <p>{{ trans('forms.kyc_incomplete_fund_transfer_alert') }}</p>
                                <a href="{{ url('/myaccount/profile') }}" class="btn btn-primary btn-lg">{{ trans('myaccount.ctabutton') }}</a>
                        @else
                            @include('fund.fundtransferform') 
                        @endif
                    @endif
                @endif                                              
            @endif
        </div>
    </div>
</div>
@endsection
