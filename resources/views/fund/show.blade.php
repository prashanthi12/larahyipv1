@extends('layouts.myaccount')
 @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('myaccount.myfundtransfers') }}
    <a href="{{ url('myaccount/fundtransfer/send') }}" class="pull-right">{{ trans('myaccount.sendfundtransfer') }}</a>
    </div>
    <div class="panel-body">
       
         <div class="row">
            <div class="col-md-12">
                     @include('layouts.message')
                      <div id="tab" class="btn-group" >
                          <a href="{{ url('myaccount/fundtransfer/type/send') }}" class="btn {{ $type == 'send' ? 'active' : '' }}" >{{ trans('myaccount.send') }}</a>
                          <a href="{{ url('myaccount/fundtransfer/type/received') }}" class="btn {{ $type == 'received' ? 'active' : '' }}" >{{ trans('myaccount.received') }}</a>                        
                    </div>
                    @include('fund.list')
            </div>
        </div>
       
    </div>
</div>
@endsection
