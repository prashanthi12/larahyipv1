@extends('layouts.myaccount')
 @section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('forms.two_factor_authentication') }}</div>
        <div class="panel-body">
            @if (Auth::user()->google2fa_secret)
                <a href="{{ url('/myaccount/2fa/disable') }}" class="btn btn-warning">{{ trans('forms.disable_2fa') }}</a>
            @else
                <a href="{{ url('/myaccount/2fa/enable') }}" class="btn btn-primary">{{ trans('forms.enable_2fa') }}</a>
            @endif
        </div>
    </div>
@endsection
