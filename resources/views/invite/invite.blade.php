@extends('layouts.myaccount') 

@section('content')
    <div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.invite_friend') }}</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                @include('layouts.message')
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                           @if( $hasProfile &&  $completeProfile)
                                    @include('invite.inviteform')
                             @else
                                    <center>
                                    <p>{{ trans('forms.invite_friend_alert') }}</p>
                                        <a href="{{ url('/myaccount/profile') }}" class="btn btn-primary btn-lg">{{ trans('myaccount.ctabutton') }}</a>
                                    </center>
                            @endif
            </div>
        </div>
    </div>
 </div>
@endsection
