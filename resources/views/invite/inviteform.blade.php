<form method="post" action="{{ url('myaccount/invitefriend/send_request')}}" class="form-horizontal" id="payment">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label>{{ trans('forms.email_id_lbl') }}</label>
                        <input type="text" name="email" id="email" class='form-control' value="{{ old('email') }}">
                        <small class="text-danger">{{ $errors->first('email') }}</small>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('forms.invite_url_lbl') }}</label>
                        <textarea class='form-control' name="invite_url" readonly="readonly">{{ $inviteUrl }}</textarea>
                    </div>
                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                        <label>{{ trans('forms.message_lbl') }}</label>
                        <textarea class='form-control' name="message">{{ old('message') }}</textarea>
                        <small class="text-danger">{{ $errors->first('message') }}</small>
                    </div>
                    <div class="form-group">
                        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success" type="submit" onclick="this.disabled=true;this.form.submit();">
                        <a href="{{ url('myaccount/deposit') }}" class="btn btn-default">{{ trans('forms.reset') }}</a>
                    </div>
                </form>