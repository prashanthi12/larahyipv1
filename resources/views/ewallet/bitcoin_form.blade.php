 @extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
  <div class="panel-heading">{{ trans('forms.ewallet_addfund') }}<a href="{{ url('myaccount/ewallet') }}" class="pull-right">{{ trans('forms.back') }}</a></div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">            
          <p> {{ trans('ewallet.instructions') }}</p>
            <div class="well">
              <p class="text-center">
                <strong class="bitcoin-address" data-bc-amount="{{ $btc_amount }}" data-bc-label="bitcoinaddress.js project" data-bc-message="{{ $btc_amount }} BTC For Add Fund" data-bc-address="{{ $btc_address }}">{{ $btc_address }}</strong>
              </p>
            </div>
            <div id="bitcoin-address-template" class="bitcoin-address-container" hidden>
              <div>
                <b><span class="bitcoin-address"></span></b>
              </div>
                <a href="#" class="bitcoin-address-action bitcoin-address-action-qr">
                  <i class="fa fa-qrcode"></i>
                  {{ trans('forms.qr_code') }}
                </a>
              <div class="bitcoin-action-hint bitcoin-action-hint-qr">
                <p>
                    {{ trans('forms.bitcoin_direct_wallet_notes') }}
                </p>       
                <p>
                  <b>{{ trans('forms.total_send_amount') }} : {{ $btc_amount }} BTC </b>
                </p>              
                <div class="bitcoin-address-qr-container">
                      <!-- Filled in by JS on action click -->
                </div>
              </div>
              <br/>
              <form method="POST" action="{{ url('myaccount/ewallet/addfund') }}">  
                {{ csrf_field() }}                              
                <div class="form-group{{ $errors->has('hash_id') ? ' has-error' : '' }}">
                  <input name="hash_id" class="form-control" value="" type="text"  placeholder="Enter Hash Key Here">
                  <small class="text-danger">{{ $errors->first('hash_id') }}</small>
                </div>      
                <div class="form-group mt-20">    
                  <input value="{{ trans('forms.submit_complete_btn') }}" class="btn btn-success" type="submit" onclick="this.disabled=true;this.form.submit();">   
                  <a href="{{ url('myaccount/ewallet/show') }}" class="btn btn-default">{{ trans('forms.back') }}</a>
                </div>
              </form>
            </div>
          </div>
        </div>
  </div>
</div>
@endsection

@push('headscripts')
  <script src="{{ asset('js/bitcoinaddress.js') }}"></script>
@endpush