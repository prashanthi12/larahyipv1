<table class="table table-bordered table-striped dataTable" id="ewalletdatatable">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ trans('myaccount.ewallet_amount') }} ({{ config::get('settings.currency') }})</th>
			<th>{{ trans('myaccount.ewallet_datetime') }}</th>
			<th>{{ trans('myaccount.type') }}</th>			
			<th>{{ trans('myaccount.ewallet_comment') }}</th>	
		</tr>
	</thead>
	<tbody>
	@if (count($ewallet ) > 0)
		@foreach($ewallet  as $ewallet)
			<tr>
				<td>{{ $loop->iteration }}</td>		
				<td>{{ $ewallet->amount }} </td>
				<td>{{ $ewallet->created_at->diffForHumans() }}</td>	
				@if($ewallet->type=='credit')
				<td><span class="label label-success">{{ ucfirst($ewallet->type) }}</span></td>
				@elseif($ewallet->type=='debit')
				<td><span class="label label-warning">{{ ucfirst($ewallet->type) }}</span></td>	
				@endif
				<td>{{ $ewallet->comment }}</td>
			</tr>
		@endforeach
	@endif
	</tbody>
</table>

@push('scripts')
<script>
$(document).ready(function(){
    $('#ewalletdatatable').DataTable();           
});
</script>
@endpush