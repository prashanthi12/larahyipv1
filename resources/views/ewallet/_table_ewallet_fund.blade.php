<table class="table table-bordered">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ trans('myaccount.ewallet_amount') }} ({{ config::get('settings.currency') }})</th>
			<th>{{ trans('myaccount.payment_info') }}</th>
			<th>{{ trans('myaccount.ewallet_datetime') }}</th>
			<th>{{ trans('myaccount.status') }}</th>
			<th>{{ trans('myaccount.ewallet_approveat') }}</th>
			<th>{{ trans('myaccount.ewallet_cancelat') }}</th>
			<th>{{ trans('myaccount.ewallet_comment') }}</th>	
		</tr>
	</thead>
	<tbody>
	@if (count($fund ) > 0)
		@foreach($fund  as $fund)
			@php
				$param = json_decode($fund->request, true);
	       		$bankname = $param['bank_name'];
	       		$accountno = $param['account_no'];
	       		$address = $param['bank_address'];
	       		$swiftcode = $param['swift_code'];
	       		$accountname = $param['account_name'];
	       	@endphp
			<tr>
				<td>{{ $loop->iteration }}</td>		
				<td>{{ $fund->amount }} </td>
				<td>
					<p><small>{{ trans('myaccount.payment_method') }} :<br/>{{ $fund->paymentgateway->displayname }}</small><br/>
					@if($fund->paymentgateway_id == '9')
					<small>{{ trans('myaccount.ewallet_amount_btc') }} :</small><br/>{{ sprintf("%.8f", $fund->btc_amount) }} <br/>
					<small>{{ trans('myaccount.btcaddress') }} :</small><br/>{{ $fund->bitcoin_address }} <br/>
					<!-- <small>{{ trans('myaccount.ewallet_txnid') }} :</small><br/>{{ $fund->bitcoin_hash_id }} <br/> -->
	            	<span data-html="true" data-toggle="ewallet-transaction-popover" data-content="{{ $fund->bitcoin_hash_id }}"><small>{{ trans('myaccount.ewallet_txnid') }} :</small></span><br/>{{ substr($fund->bitcoin_hash_id, 0, 10) }}... <br/>
	            	@endif

	            	@if($fund->paymentgateway_id == '1')
					<small>{{ trans('myaccount.ewallet_txnid') }} :</small><br/>{{ $fund->transaction_id }} <br/>
					<small>{{ trans('forms.bank_name_lbl') }} :<br/>{{ $bankname }} </small><br/>
					<small>{{ trans('forms.account_address_lbl') }} :<br/>{{ $address }} </small><br/>
					<small>{{ trans('forms.swift_code_lbl') }} :<br/>{{ $swiftcode }} </small><br/>
					<small>{{ trans('forms.account_name_lbl') }} :<br/>{{ $accountname }} </small><br/>
					<small>{{ trans('forms.account_no_lbl') }} :</small><br/>{{ $accountno }} <br/>
	            	@endif
					</p>
				</td>
				<td>{{ $fund->created_at->diffForHumans() }}</td>	
				@if($fund->status=='approve')
				<td><span class="label label-success">{{ ucfirst($fund->status) }}</span></td>
				@elseif($fund->status=='pending')
				<td><span class="label label-warning">{{ ucfirst($fund->status) }}</span></td>
				@else
				<td><span class="label label-danger">{{ ucfirst($fund->status) }}</span></td>
				@endif
			    <!-- <td>{{ $fund->bitcoin_address }} </td>
	            <td>{{ $fund->bitcoin_hash_id }} </td> -->
				 @if($fund->status=='approve')
                <td>{{ $fund->approve_at->diffForHumans() }}</td>
                @else
                <td> - </td>
                @endif
                @if($fund->status=='cancel')
                <td>{{ $fund->cancel_at->diffForHumans() }}</td>
                @else
                <td> - </td>
             	@endif					
				<td>{{ $fund->comment }}</td>
			</tr>
		@endforeach
	@endif
	</tbody>
</table>

@push('bottomscripts')
<script>
$(document).ready(function()
{
	// alert('fgjdfgjdfg');
   
    $('[data-toggle="ewallet-transaction-popover"]').popover({
        placement : 'right',
        trigger : 'hover'
    });          
});
</script>
@endpush