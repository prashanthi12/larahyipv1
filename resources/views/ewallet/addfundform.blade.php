<div class="col-md-10 col-md-offset-1">
    <div id="depositfrm">
        <form method="post" action="{{ url('myaccount/ewallet')}}" class="form-horizontal"> 
        {{ csrf_field() }}  
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                    {{ \Session::forget('success') }}
                </div>
            @endif  
            <div class="form-group">
                <div class="row">
                    <p>{{ trans('ewallet.add_fund_instruction') }}</p>
                </div>
            </div>
            <div class="form-group">
                <div class="row">         
                    <label>{{ trans('forms.ewallet_min_amount_lbl') }}</label>
                    <p>{{ $min_amount }}</p>
                </div>
            </div>
            <div class="form-group">
                <div class="row"> 
                    <label>{{ trans('forms.ewallet_max_amount_lbl') }}</label>
                    <p>{{ $max_amount }}</p>
                </div>
            </div>
            <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                <div class="row">    
                    <label>{{ trans('forms.ewallet_amount_lbl') }} ( {{ config::get('settings.currency') }} )</label>
                    <input type="text" name="amount" value="{{ old('amount') }}" class='form-control'>
                    <small class="text-danger">{{ $errors->first('amount') }}</small>               
                </div>      
            </div>
            <div class="form-group{{ $errors->has('paymentgateway') ? ' has-error' : '' }}">
                <label>{{ trans('forms.deposit_payment_lbl') }}</label>
                <select class="form-control" id="pgs" name="paymentgateway">            
                    <option value="" >{{ trans('forms.deposit_payment_lbl') }}</option>
                    @foreach ($pgs as $pg)
                        <option value="{{ $pg->id }}" {{ (Form::old("paymentgateway") == $pg->id ? "selected":"") }}>{{ $pg->displayname }}</option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('paymentgateway') }}</small>
            </div>  
            <div class="form-group">  
                <div class="row">  
                	<input value="{{ trans('forms.submit_btn') }}" class="btn btn-success btn-deposit-success" type="submit" onclick="this.disabled=true;this.form.submit();"> 	
                	<a href="{{ url('myaccount/ewallet') }}" class="btn btn-default btn-deposit-res">{{ trans('forms.reset') }}</a>
                </div>
            </div>
        </form> 
    </div>
</div>

