<table class="table table-striped">
@if (count($deposits) > 0)
    @foreach ($deposits as $deposit)
    <tr>
        <td>
            @if ($deposit->user->name == '')
            {{ 'system' }}
            @else
            {{ $deposit->user->name }}
            @endif
        </td>
        <td>{{ $deposit->paymentgateway->displayname }}</td>
        <td>{{ $deposit->amount }} {{ config::get('settings.currency') }}</td>
    </tr>
  @endforeach
@else
    <tr>
        <td>{{ trans('welcome.nodepositfound') }}</td>
    </tr>
@endif  
</table>