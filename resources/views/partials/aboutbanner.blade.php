<div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgabout')) }})">
      <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-white">{{ trans('aboutus.title') }}</h1>        
            <h3 class="title-white">{{ trans('aboutus.description') }}</h3>
        </hgroup>
      </div>
</div> 
