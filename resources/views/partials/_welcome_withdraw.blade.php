<table class="table table-striped">
@if (count($withdraws) > 0)
    @foreach ($withdraws as $withdraw)
    <tr>
        <td>{{ $withdraw->user->name }}</td>
        <td>{{ $withdraw->userpayaccounts->payment->displayname }}</td>
        <td>{{ $withdraw->amount }} {{ config::get('settings.currency') }}</td>
    </tr>
  @endforeach
@else
    <tr>
        <td>{{ trans('welcome.nowithdrawfound') }}</td>
    </tr>
@endif  
</table>