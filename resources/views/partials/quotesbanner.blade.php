<div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgquotes')) }})">
      <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-white">{{ trans('pages.quotestitle') }}</h1>        
            <h3 class="title-white">{{ trans('pages.quotesdesc') }}</h3>
        </hgroup>
      </div>
</div> 
