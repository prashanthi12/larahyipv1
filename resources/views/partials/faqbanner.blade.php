<div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgfaq')) }})">
      <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-white">{{ trans('pages.faqtitle') }}</h1>        
            <h3 class="title-white">{{ trans('pages.faqsubtitle') }}</h3>
        </hgroup>
      </div>
</div> 
