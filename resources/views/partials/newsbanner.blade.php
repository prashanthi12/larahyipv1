<div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgnews')) }})">
      <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-white">{{ trans('pages.newstitle') }}</h1>        
            <h3 class="title-white">{{ trans('pages.newssubtitle') }}</h3>
        </hgroup>
      </div>
</div> 
