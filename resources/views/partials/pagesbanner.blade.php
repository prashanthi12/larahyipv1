<div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgpages')) }})">
      <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-white">{{ $pagedetails->title }}</h1>        
            <h3 class="title-white">{{ $pagedetails->description }}</h3>
        </hgroup>
      </div>
</div> 