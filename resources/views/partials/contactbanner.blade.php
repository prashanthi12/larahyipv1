<div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgcontact')) }})">
      <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-white">{{ trans('pages.contacttitle') }}</h1>        
            <h3 class="title-white">{{ trans('pages.contactsubtitle') }}</h3>
        </hgroup>
      </div>
</div> 
