<div class="bg-pages" style="background-image: url({{ url(Config::get('settings.investorsbanner')) }})">
      <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-white">{{ trans('pages.referralstitle') }}</h1>        
            <h3 class="title-white">{{ trans('pages.referralsdesc') }}</h3>
        </hgroup>
      </div>
</div> 