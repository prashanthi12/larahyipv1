<div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgearn')) }})">
      <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-white">{{ trans('pages.earntitle') }}</h1>        
            <h3 class="title-white">{{ trans('pages.earnsubtitle') }}</h3>
        </hgroup>
      </div>
</div> 
