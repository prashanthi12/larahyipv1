<div class="bg-pages" style="background-image: url({{ url(Config::get('settings.investorsbanner')) }})">
      <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-white">{{ trans('pages.lastinvestorstitle') }}</h1>        
            <h3 class="title-white">{{ trans('pages.lastinvestorsdesc') }}</h3>
        </hgroup>
      </div>
</div> 