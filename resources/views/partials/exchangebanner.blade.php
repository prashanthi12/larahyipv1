<div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgexchange')) }})">
      <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-white">{{ trans('pages.exchangetitle') }}</h1>        
            <h3 class="title-white">{{ trans('pages.exchangesubtitle') }}</h3>
        </hgroup>
      </div>
</div> 
