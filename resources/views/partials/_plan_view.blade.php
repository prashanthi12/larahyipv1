<center><h3>{{ $plan->name }} {{ trans('pages.plan_details') }}</h3></center>
<table class="table table-striped">
    <tbody>
        <tr>
            <td> &nbsp; </td>
            <td>
                    @if (is_null($plan->image))
                    <img src="http://placehold.it/250x200">
                    @else
                    <img src="{{  url($plan->image)  }}" class="plan-image plan-image-detail">
                    @endif
            </td>
        </tr> 
        <tr>
            <td> {{ trans('pages.plan_name') }} </td>
            <td>{{ $plan->name }}</td>
        </tr>   
        <tr>
            <td> {{ trans('pages.plan_type') }} </td>
            <td>{{ $plan->plantype->plantype }}</td>
        </tr>
        <tr>
            <td> {{ trans('pages.duration') }} </td>
            <td>{{ $plan->duration }} {{ $plan->duration_key }}</td>
        </tr>       
       
        <tr>
            <td> {{ trans('pages.min_deposit') }} </td>
            <td>{{ $plan->min_amount }} {{ \Config::get('settings.currency') }}</td>
        </tr>       
        <tr>
            <td> {{ trans('pages.max_deposit') }} </td>
            <td>{{ $plan->max_amount }} {{ \Config::get('settings.currency') }}</td>
        </tr>       
        <tr>
            <td> {{ trans('pages.interest_rate') }}</td>
            <td>{{ $plan->interest_rate }}%</td>
        </tr> 
        
        <tr>
            <td> {{ trans('pages.principle_amount_return') }}</td>
            <td>{{ $plan->principle_return == '1' ? 'Yes' : 'No' }}</td>
        </tr> 

         @if ($plan->partial_withdraw_status == '1')  
        <tr>
            <td> {{ trans('pages.partial_withdraw_status') }}</td>
            <td>{{ $plan->partial_withdraw_status == '1' ? 'Active' : 'Inactive' }}</td>
        </tr> 
        @endif

        @if ($plan->partial_withdraw_status == '1')           
            <tr>
                <td> {{ trans('pages.maximum_partial_withdraw_limit') }}</td>
                <td>{{ $plan->max_partial_withdraw_limit }}%</td>
            </tr>    
            <tr>
                <td> {{ trans('pages.minimum_locking_period') }}</td>
                <td>{{ $plan->minimum_locking_period }} (days)</td>
            </tr> 
            <tr>
                <td> {{ trans('pages.partial_withdraw_fee') }}</td>
                <td>{{ $plan->paritial_withdraw_fee }}%</td>
            </tr>
        @endif

                            
    </tbody>
</table>

