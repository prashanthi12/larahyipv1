<div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bginvest')) }})">
      <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-white">{{ trans('pages.investtitle') }}</h1>        
            <h3 class="title-white">{{ trans('pages.investsubtitle') }}</h3>
        </hgroup>
      </div>
</div> 
