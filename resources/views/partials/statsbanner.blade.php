<div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgstats')) }})">
      <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-white">{{ trans('pages.statstitle') }}</h1>        
            <h3 class="title-white">{{ trans('pages.statsdesc') }}</h3>
        </hgroup>
      </div>
</div> 
