<div id='bitcoin' class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="margin-top: 50px;">
  <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ trans('myaccount.bitcoin_details') }}</h4>
        </div>
        <div class="modal-body">

        <p>{{ trans('myaccount.hashkey') }} : <a href="https://blockexplorer.com/tx/{{ $txnhashkey }}" target="_blank">{{ $txnhashkey }}</a></p>
   
        </div>

      </div>
  </div>
</div>
