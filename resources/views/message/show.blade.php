@extends('layouts.myaccount') 
@section('content')
    <div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.my_message') }}
    <a class="pull-right" href="{{ url('/myaccount/message/send') }}">{{ trans('forms.send') }}</a>
    </div>
    <div class="panel-body">
        @include('layouts.message')
        <div class="row">
            <div class="col-md-12">                       
                @include('message.lists')                        
            </div>
        </div>
    </div>
 </div>
@endsection
