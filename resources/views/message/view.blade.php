@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.my_message_view') }}
        <a href="{{ url('myaccount/message/list') }}" class="pull-right">{{ trans('myaccount.back_to_list') }}</a>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                @include('layouts.message')    
                @include('message.details')
            </div>
        </div>
    </div>
 </div>
@endsection
