<div class="col-md-10 col-md-offset-1">

{!! Form::open(['method' => 'POST', 'route' => 'profile', 'class' => 'form-horizontal']) !!}

    <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
        {!! Form::label('firstname', 'First Name') !!}
        {!! Form::text('firstname', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('firstname') }}</small>
    </div>

    <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
        {!! Form::label('lastname', 'Last Name') !!}
        {!! Form::text('lastname', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('lastname') }}</small>
    </div>

    <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
        {!! Form::label('mobile', 'Mobile Number') !!}
        {!! Form::text('mobile', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('mobile') }}</small>
    </div>

    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
        {!! Form::label('country', 'Country') !!}
        {!! Form::select('country', $country, null, ['id' => 'country', 'class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('country') }}</small>
    </div>

    <div class="form-group{{ $errors->has('ssn') ? ' has-error' : '' }}">
        {!! Form::label('ssn', 'Social Security Number / Passport Number') !!}
        {!! Form::text('ssn', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('ssn') }}</small>
    </div>

    <div class="form-group{{ $errors->has('kyc_doc') ? ' has-error' : '' }}">
        {!! Form::label('kyc_doc', 'Upload ID Proof - Know Your Customer') !!}
        {!! Form::file('kyc_doc', ['required' => 'required']) !!}
        <p class="help-block">You need submit KYC for each profile.</p>
        <small class="text-danger">{{ $errors->first('kyc_doc') }}</small>
    </div>

    <div class="form-group">
        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-primary" type="submit" onclick="this.disabled=true;this.form.submit();">
        {!! Form::reset("Reset", ['class' => 'btn btn-default']) !!}
    </div>

{!! Form::close() !!}
</div>