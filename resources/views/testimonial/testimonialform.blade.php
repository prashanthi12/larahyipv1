<form method="post" action="{{ url('myaccount/testimonial')}}" class="form-horizontal" id="payment">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label>{{ trans('forms.title') }}</label>
                        <input type="text" name="title" id="title" class='form-control' value="{{ old('title') }}">
                        <small class="text-danger">{{ $errors->first('title') }}</small>
                    </div>
                    
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label>{{ trans('forms.description') }}</label>
                        <textarea class='form-control' name="description">{{ old('description') }}</textarea>
                        <small class="text-danger">{{ $errors->first('description') }}</small>
                    </div>

                    <div class="form-group">
                        <label>{{ trans('forms.ratings') }}</label>
                        <select class="form-control" id="rating" name="rating">       
                            @foreach ($ratings as $rating)
                                <option value="{{ $rating }}" {{ (Form::old("rating") == $rating ? "selected":"") }}>{{ $rating }}</option>
                            @endforeach
                        </select>
                        
                    </div>

                    <div class="form-group">
                        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success" type="submit" onclick="this.disabled=true;this.form.submit();">
                        <a href="{{ url('myaccount/testimonial') }}" class="btn btn-default">{{ trans('forms.reset') }}</a>
                    </div>
                </form>