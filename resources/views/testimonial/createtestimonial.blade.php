@extends('layouts.myaccount') 

@section('content')
    <div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.testimonial') }}</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                                    @include('testimonial.testimonialform')
                            
            </div>
        </div>
    </div>
 </div>
@endsection
