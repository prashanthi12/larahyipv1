<div class="container">
<div class="row">
@if (count($earn))
@foreach($earn as $data)
<?php
$imagepixel = explode('x', $data->imagesize);
?>
    <div class="col-md-12">
        <div id="postlist">
            <div class="panel">
                <div class="panel-heading">
                    <div class="text-center">
                        <div class="row">
                            <div class="col-sm-9">
                                <h3 class="pull-left">{{ $data->imagesize }}</h3>
                            </div>
                           
                        </div>
                    </div>
                </div>
                
            <div class="panel-body">
                <img src="{{ $data->image }}">
            </div> 
             <div class="panel-body">
                <p>Code</p>
                <p><textarea cols="55" rows="5" style="width:650px;height:80px;"><a href="{{ $earn_url }}" target="_blank"><img src="{{ url('/') }}/{{$data->image }}" width="{{ $imagepixel[0] }}" height="{{ $imagepixel[1] }}" border="0" ></a> </textarea></p>
            </div>          
        </div>
    </div>       
</div>
@endforeach
@else
    <div class="col-md-12">
        {{ trans('pages.noearnsfound') }}
    </div>
@endif
</div>
</div>
{{ $earn->links() }}