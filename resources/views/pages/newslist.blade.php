<div class="container">
    <div class="row">
        <div class="container mt-20 mb-20">
        @if (count($news))
            @foreach($news as $data)
                @php
                    $lang = \Session::get('locale');
                @endphp
                <div class="col-md-12">
                    <div id="postlist">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <h3 class="pull-left">{{ $data->title }}</h3>
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="pull-right">
                                            <small><em>{{ $data->created_at->diffForHumans() }}</em></small>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>             
                            <div class="panel-body">
                                {!! $data->story !!}
                            </div>        
                        </div>
                    </div>       
                </div>
            @endforeach
        @else
            <div class="col-md-12">
                <center>{{ trans('pages.nonewsfound') }}</center>
            </div>
        @endif
        </div>
    </div>
</div>
{{ $news->links() }}