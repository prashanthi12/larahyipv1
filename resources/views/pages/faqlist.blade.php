<div class="container">
    <div class="row">
        <div class="container mt-20 mb-20">
        @if (count($faq))
            <div class="panel-group" id="faqAccordion">
            @foreach($faq as $data)
                @php
                    $lang = \Session::get('locale');
                @endphp
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question{{ $data->id }}">
                        <h4 class="panel-title">
                            <a href="#" class="ing">{{ $data->title }}</a>
                        </h4>
                    </div>
                    <div id="question{{ $data->id }}" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <p>{!! $data->description !!}</p>
                        </div>
                    </div>
                </div>   
            @endforeach
            </div>
        @else
            <div class="col-md-12">
                <center>{{ trans('pages.nofaqfound') }}</center>
            </div>
        @endif
        {{ $faq->links() }}
        </div>
    </div>
</div>
