@extends('layouts.main') 
@section('banner')
    @include('partials.investbanner')
@endsection
@section('content')
<div class="page-content">
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">            
                        @include('partials._plan_view')                    
                    </div>
                </div>
            </div>     
        </div>
    </div>
</div>
@endsection