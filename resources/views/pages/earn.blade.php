@extends('layouts.main') 
@section('banner')
	@include('partials.earnbanner')
@endsection
@section('content')
<div class="page-content">
	<div class="row">
		<div class="container">
		
		<div class="row">
			<div class="col-md-12">
				@include('pages.earnlist')
			</div>
		</div>
		</div>
	</div>
</div>
@endsection