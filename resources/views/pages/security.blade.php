@extends('layouts.masterpage') 
@section('banner')
    @include('partials.securitybanner')
@endsection
@section('content')
<div class="page-content">
<div class="row">
    <div class="container">
        <div class="col-md-3">
            <nav class="nav-sidebar">
                <ul class="nav">
                    @foreach ($pages as $data)
                        @php
                            $lang = \Session::get('locale');
                        @endphp
                        <li><a href="{{ url('/page/'.$lang.'/'.$data['slug']) }}">{{ $data['navlabel'] }}</a></li>
                    @endforeach             
                </ul>
            </nav>
        </div>
        <div class="col-md-9">
            {!! trans('securitytips.content') !!}
        </div>
    </div>
</div>
</div>
@endsection