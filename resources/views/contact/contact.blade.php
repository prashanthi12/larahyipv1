@extends('layouts.main') 
@section('banner')
    @include('partials.contactbanner')
@endsection
@section('content')
<div class="page--content">
<div class="row">
    <div class="container">
    <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default mt-50">
                    <div class="panel-body">
                        <h3 >{{ trans('forms.contactus_head') }}</h3>
                 @include('layouts.message')
               @include('contact.contactform')
               </div>
               </div>
               </div>
                <div class="col-md-4">
                        <div class="panel panel-default mt-50">                       
                        <div class="panel-body">
                        <h4>{{ trans('forms.contact_address_head') }}</h4>
                                {!! $contactaddress !!}
                        </div>                               
                    </div>
                </div>
        </div>
</div>
</div>
</div>
@endsection

