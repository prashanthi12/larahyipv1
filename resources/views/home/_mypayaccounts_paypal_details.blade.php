  <div class="row"> 
  <div class="col-md-12">   
    <div class="table-responsive">          
        <table class="table table-bordered table-hover">
          <thead>
            <tr>        
              <th>{{ trans('forms.paypal_id_lbl') }}</th>
              <th>{{ trans('forms.autowithdraw') }}</th>
              <th>{{ trans('forms.actions') }}</th>
            </tr>
          </thead>
          <tbody>
          @foreach($paypal_result as $paypal)
            <tr>
              <td>{{ $paypal->param1 }}</td>
              <td>
              @if($paypal->current == '1')
                <a class="paypalauto" href="{{ url('myaccount/currentaccounts/'.$paypal->id.'/'.$paypal->paymentgateways_id.'/'.$paypal->current)}}">{{ trans('forms.yes') }}</a>
              @else
                <a class="paypalauto" href="{{ url('myaccount/currentaccounts/'.$paypal->id.'/'.$paypal->paymentgateways_id.'/'.$paypal->current)}}">{{ trans('forms.no') }}</a>
              @endif
              </td>
              <td>
                <form method="post" class="paypalremovefrm" action="{{ url('myaccount/removeaccount/'.$paypal->id.'') }}">
                {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
                </form>
              </td>       
            </tr>
            @endforeach    
          </tbody>
        </table>
    </div>
  </div>
  </div>

@push('bottomscripts')
<script>
    $(".paypalremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
    $(".paypalauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush