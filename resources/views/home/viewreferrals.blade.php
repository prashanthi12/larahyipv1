@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('myaccount.myreferrals') }}</div>
    <div class="panel-body">
       <div class="row">
            <div class="col-md-12">
                @include('home._stats_referral')
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-12">
                @include('home._table_referrals')
            </div>
        </div>
    </div>
</div>
@endsection
