<div class="grid">
@if (count($myreferrals) > 0)
	@foreach($myreferrals as $myreferral)
      <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>     
          {{ $myreferral[0] }}<br>
          {{ $myreferral[1]->diffForHumans() }}
      </p>     
    </div>
    <div class="">
      <p class="form-group flex-button-group flex-vertical">
          <small>{{ trans('myaccount.noofdeposit') }} :</small>
          <strong>{{ $myreferral[2] }}</strong>
      </p>
    </div>
    <div class="">
      <p class="form-group flex-button-group flex-vertical">
          <small>{{ trans('myaccount.totaldepositamount') }} ({{ config::get('settings.currency') }}) :</small>
          <strong>{{ $myreferral[3] }}</strong>
      </p>
    </div>
    </div>
  @endforeach 
@else
        <div class="panel-heading">{{ trans('myaccount.noreferralsfound') }}</div>
@endif
</div>

