  <div class="row"> 
    <div class="col-md-12">   
    <div class="table-responsive">          
        <table class="table table-bordered table-hover">
          <thead>
            <tr>        
              <th>{{ trans('forms.user_name_lbl') }}</th>
              <th>{{ trans('forms.autowithdraw') }}</th>
              <th>{{ trans('forms.actions') }}</th>       
            </tr>
          </thead>
          <tbody>
          @foreach($stpay_result as $stpay)
            <tr>
              <td>{{ $stpay->param1 }}</td>
              <td>
              @if($stpay->current == '1')
                <a class="stpayauto" href="{{ url('myaccount/currentaccounts/'.$stpay->id.'/'.$stpay->paymentgateways_id.'/'.$stpay->current)}}">{{ trans('forms.yes') }}</a>
              @else
                <a class="stpayauto" href="{{ url('myaccount/currentaccounts/'.$stpay->id.'/'.$stpay->paymentgateways_id.'/'.$stpay->current)}}">{{ trans('forms.no') }}</a>
              @endif
              </td>
              <td>
                <form method="post" class="stpayremovefrm" action="{{ url('myaccount/removeaccount/'.$stpay->id.'') }}">
                {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
                </form>
              </td>               
            </tr>
            @endforeach    
          </tbody>
        </table>
    </div>
</div>
  </div>
            @push('bottomscripts')
<script>
    $(".stpayremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
     $(".stpayauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush