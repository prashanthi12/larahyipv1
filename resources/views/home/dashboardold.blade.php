@extends('layouts.myaccount')

@section('content')
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('myaccount.tittlemyaccount') }}</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                        @if (session('status'))
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ session('status') }}
                                </div>
                            @endif
                            @if (session('paypalsuccess'))
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ session('paypalsuccess') }}
                                </div>
                            @endif
                             @if (session('error'))
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ session('error') }}
                                </div>
                            @endif
                        @include('layouts.message')
                        </div>
                    </div>
                    <div class="well">
                        <p>{{ trans('myaccount.accountno') }} : <strong> {{ session('account_no') }} </strong>
                                <span class="pull-right">{{ trans('myaccount.emailverified') }} : 
                                     @if ($emailverified == 0)
                                    <strong class="label label-danger">{{ trans('myaccount.unverified') }}</strong>
                                    @elseif ($emailverified == 1)
                                    <strong class="label label-success">{{ trans('myaccount.verified') }}</strong>
                                    @endif
                                </span>
                        </p>
                        <p>{{ trans('myaccount.balance') }} : <strong> {{ session('balance') }} </strong> {{ config::get('settings.currency') }}
                            @if(session('balance') > 0)
                                <a href="{{ url('myaccount/deposit?reinvest=1') }}" class="btn btn-primary">{{ trans('myaccount.reinvest') }}</a>
                            @endif
                               <span class="pull-right">{{ trans('myaccount.kyc_approved') }} :
                                 @if ($kycverified == 0)
                                <strong class="label label-danger">{{ trans('myaccount.unverified') }}</strong>
                                @elseif ($kycverified == 1)
                                <strong class="label label-success">{{ trans('myaccount.verified') }}</strong>
                                @elseif ($kycverified == 2)
                                <strong class="label label-danger">{{ trans('myaccount.rejected') }}</strong>
                                @endif
                               </span>                           
                        </p>
                        <div class="row">
                            @if (Config::get('settings.nexmo_status') == '1')
                                {{ trans('myaccount.mobile_status') }} : 
                                @if ($mobilestatus == 0)
                                <strong class="label label-danger">{{ trans('myaccount.unverified') }}</strong>&nbsp;
                                @if ($mobilestatus == 0 && $mobilecode != '')
                                <a href="{{ url('myaccount/mobileverification/'.$mobilecode) }}" class="btn btn-success btn-xs">{{ trans('myaccount.mobile_unverified_alert') }}</a>
                                @endif
                                @else
                                <strong class="label label-success">{{ trans('myaccount.verified') }}</strong>
                                @endif
                            @endif                             
                        </div>
                    </div>

                    @if (! session('completedProfile') )
                        @include('home._welcome')
                    @endif
                     @include('home._stats_deposit')
                     @include('home._stats_earnings')
                     @include('home._stats_referral')
                    </div>

                    <!--Modal-->

                <div class="modal fade" id="dashboard_modal" role="dialog" style="margin: 50px;">
                    <div class="modal-dialog">
    
                    <!-- Modal content-->
                   <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">{{ trans('popup.dashboardMessageTitle') }}</h4>
                        </div>
                        <div class="modal-body">                       
                            <div class="panel-body">
                                <p>{!! trans('popup.dashboardMessageContent') !!}</p>
                            </div>   
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
@php
if (\Config::get('settings.dashboardpopupstatus') == 1)
{
    if (\Session::get('dashboardmessage') == '')
    {
    @endphp
    @push('bottomscripts')
    <script>
         $(function() {
           $('#dashboard_modal').modal('show');
        });
    </script>
    @endpush
    @php
    }
}
\Session::put('dashboardmessage', '1')
@endphp
