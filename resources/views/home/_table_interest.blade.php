<div class="grid">
@if (count($interests) > 0)
    @foreach($interests as $interest)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }})</small><br/>
        {{ $interest->amount }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('myaccount.comment') }} : </small><br/>       
          {{ ucwords(str_replace("-", " ", $interest->accountingcode->accounting_code)) }}<br>
      </p>
    </div>   
    <div class="">
      <p>
        <small>{{ trans('myaccount.datetime') }} :</small><br/>
            {{ $interest->created_at->diffForHumans() }}
      </p>
    </div>
    </div>
  @endforeach 
@else
    <div class="" style="border:1px solid #ddd; padding: 10px;">{{ trans('myaccount.nointerestfound') }}</div>
@endif
</div>
{{ $interests->links() }}







