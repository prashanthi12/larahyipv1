@extends('layouts.myaccount')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('myaccount.viewprofile') }}</div>
        <div class="panel-body">
        	@if ($myprofile->user->isUserProfileCompleted )
            	<table class="table table-striped"> 
            	 	<tr>
            	 		<td>{{ trans('myprofile.username') }} </td>
            	 		<td> <strong>{{ $myprofile->user->name }}</strong></td>
            	 	</tr>
            	 	<tr>
            	 		<td> {{ trans('myprofile.email') }} </td>
            	 		<td> <em>{{ $myprofile->user->email }}</em>
            	 			@if( $myprofile->email_verified )
            	 				<span class="label label-success">{{ trans('myprofile.verified') }}</span>
            	 			@else
            	 				<span class="label label-danger">{{ trans('myprofile.unverified') }}</span>
            	 			@endif
            	 		</td>
            	 	</tr>
            	 	<tr>
            	 		<td> {{ trans('myprofile.firstname') }} </td>
            	 		<td> {{ $myprofile->firstname }}</td>
            	 	</tr>
            	 	<tr>
            	 		<td>{{ trans('myprofile.lastname') }}</td>
            	 		<td> {{ $myprofile->lastname }}</td>
            	 	</tr>
                @if(Config::get('settings.address1'))
                    <tr>
                        <td>{{ trans('forms.address1_lbl') }} </td>
                        <td>{{ $myprofile->address1}} </td>
                    </tr>
                @endif
                @if(Config::get('settings.address2'))
                    <tr>
                        <td>{{ trans('forms.address2_lbl') }}</td>
                        <td>{{ $myprofile->address2}} </td>
                    </tr>
                @endif
                @if(Config::get('settings.city'))
                    <tr>
                        <td>{{ trans('forms.city_lbl') }} </td>
                        <td>{{ $myprofile->city}} </td>
                    </tr>
                @endif
                @if(Config::get('settings.state'))
                    <tr>
                        <td>{{ trans('forms.state_lbl') }} </td>
                        <td>{{ $myprofile->state}} </td>
                    </tr>
                @endif
            	 	<tr>
            	 		<td>{{ trans('myprofile.country') }}</td>
            	 		<td> {{ $myprofile->Country->name }}</td>
            	 	</tr>
            	 	<tr>
            	 		<td>{{ trans('myprofile.mobile') }}</td>
            	 		<td> {{ $myprofile->mobile }}
            	 			@if( $myprofile->mobile_verified )
            	 				<span class="label label-success">{{ trans('myprofile.verified') }}</span>
            	 			@else
            	 				<span class="label label-danger">{{ trans('myprofile.unverified') }}</span>
            	 			@endif
            	 		</td>
            	 	</tr>
            	 	<tr>
            	 		<td>{{ trans('myprofile.ssn') }}</td>
            	 		<td> {{ $myprofile->ssn }}</td>
            	 	</tr>

            	 	<tr>
            	 		<td>{{ trans('myprofile.kyc') }}</td>
            	 		<td><img src={{ $myprofile->kyc_doc }} style="width:200px; height:auto; cursor:pointer" data-toggle="modal" data-target="#kycModal" ></a>
            	 			@if( $myprofile->kyc_verified == 1)
            	 				<span class="label label-success">{{ trans('myprofile.verified') }}</span>
            	 			@elseif( $myprofile->kyc_verified == 0)
            	 				<span class="label label-danger">{{ trans('myprofile.unverified') }}</span>
                            @elseif( $myprofile->kyc_verified == 2)
                                <span class="label label-danger">{{ trans('myaccount.rejected') }}</span>
            	 			@endif
            	 		</td>
            	 	</tr>

            	</table>
            	<div>
            		<p class="bg-info p-20">{{ trans('myprofile.profile_note') }} </p>
            	</div>
        	@else 
        		<div>
        			<p class="bg-info p-20"> {{ trans('myprofile.incomplete_profile_alert')}} </p>
        		</div>
        		<center>
        			<div class="col-md-6 col-md-offset-3"><a href="{{ url('/myaccount/profile') }}" class="btn btn-primary btn-block">{{ trans('myprofile.ctabutton') }}</a></div>
        		</center>
        	@endif
        </div>   
    </div>
    <div id="kycModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">KYC Document</h4>
          </div>
          <div class="modal-body">
            <p><img src={{ $myprofile->kyc_doc }} style="width:560px; height:auto"></p>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection
@push('scripts')
<script>
    function view() {
        console.log("I am hit");
        $('#kycModal').modal('show')
    }
</script>
@endpush
