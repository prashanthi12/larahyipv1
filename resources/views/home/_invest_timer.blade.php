 @if(count($assigned)>0)
 <div class="boxy mt-20 ">
    <div class="col-md-12">
      <h2>{{ trans('myaccount.pending_withdraw_title') }}</h2>
       <hr>     
        @foreach($assigned as $assign)
          @php
            $count_down_date=$assign->created_at->hour(24);
            if($date>$count_down_date)
            {
                $flag_timer=0;
            }
            $deposit=App\ManualDeposit::where('deposit_id',$assign->deposit_id)->with('user','withdraw','deposit')->first();
          @endphp
      
      <h4>{{$deposit->deposit->amount}} ({{ config::get('settings.currency') }})</h4>
      <div class="col-md-6">
          <p>{{trans('myaccount.timedue')}}</p>
              <!-- <div class="grid grid-timer clock" id="clock"></div>  -->
              <div class="" id="clock">{{ $count_down_date->diffForHumans() }}</div>

                @if(($flag_timer==0)&&(optional($assign)->is_report==0))

                   <a href="{{url('myaccount/reportuser/'.$deposit->withdraw->id.'/'.$deposit->id)}}" class="btn btn-success">{{trans('myaccount.report_user')}}</a>
                @endif
                @if(($flag_timer==0)&&(optional($assign)->is_report==1))

                 <p>  {{trans('myaccount.report_user_message')}}</p>
                @endif
                @php 
                  $status = $deposit->deposit->status;
                @endphp
                @if(($status == 'new')&&($flag_timer==1)&&(optional($assign)->proof!='')&&(optional($assign)->is_report==0))

                 <a class='btn btn-success btn-xs flex-button' href="{{ $deposit->proof }}" download>{{ trans('myaccount.viewproof') }}</a>
                 <a href="#" class="btn btn-success btn-xs flex-button confirm" rel="{{ url('myaccount/deposit/approvebyuser/') }}">{{ trans('myaccount.confirm') }}</a>

                <a href="{{url('myaccount/reportuser')}}" class="btn btn-danger btn-xs">{{trans('myaccount.reject')}}</a>

                @endif
                 @if(($status == 'rejected')&&($flag_timer==1)&&(optional($assign)->proof!='')||(optional($assign)->is_report==1))
                     <p>{{trans('myaccount.rejected_message')}}</p>
                @endif
                 @if(($flag_timer==1)&&(optional($assign)->proof!='')&&(optional($assign)->type=='approve'))
                     <p>{{trans('myaccount.confirm_message')}}</p>
                 @endif
     </div>
    
      <div class="col-md-6">
        @if(count($deposit)>0)
            <h4>{{trans('myaccount.reportdetails')}}</h4>
                
              <div><p>{{$deposit->user->name}}<br>
                  {{$deposit->user->email}}<br>
                  {{$deposit->user->userprofile->mobile}}</p>
              </div>
                  
                @if($deposit->details_type=='bank')
                  <h4>{{trans('myaccount.accountdetails')}}</h4>
                  <div>{{$deposit->bank_name}}</div>
                  <div>{{$deposit->swift_code}}</div>
                  <div>{{$deposit->account_no}}</div>
                  <div>{{$deposit->account_name}}</div>
                  <div>{{$deposit->account_address}}</div>
                @endif
              
            @if($deposit->details_type=='mobile')
                  <h4>{{trans('myaccount.mobile_accountdetails')}}</h4>
                  <div>{{$deposit->mobile}}</div>
                  <div>{{optional($deposit->country)->name}}</div>                
            @endif
        @endif     
      </div>
      @endforeach   
  </div>
</div>
@endif

@push('bottomscripts')
<script src="{{url('js/jquery.countdown.js')}}"></script>

<script>
    // $('.clock').countdown('{{$count_down_date}}', function(event) {
    $('.clock').each(function(){
        $(this).countdown('{{ $count_down_date }}', function(event) {
            var $this = $(this).html(event.strftime(''
            + '<div >%D</div><div>:</div> '
            + '<div >%H</div><div>:</div> '
            + '<div >%M</div><div>:</div> '
            + '<div >%S</div>'));          
        })    
    }); 
</script>
@endpush

@push('bottomscripts')
<link rel="stylesheet" href="/css/sweetalert2.min.css">
<script src="/js/sweetalert2.min.js"></script>

<script>
  $(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });

    $('.confirm').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to approve this deposit ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 
});
</script>
@endpush