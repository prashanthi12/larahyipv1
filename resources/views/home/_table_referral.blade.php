<div class="grid">
@if (count($referralCommissions) > 0)
  	@foreach($referralCommissions as $referralCommission)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }})</small><br/>
        {{ $referralCommission->amount }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('myaccount.comment') }} : </small><br/>       
          {!! $referralCommission->comment !!}<br>
      </p>
    </div>   
    <div class="">
      	<p style="text-align: center;">
        	<small>{{ trans('myaccount.datetime') }} :</small><br/>
            	{{ $referralCommission->created_at->diffForHumans() }}
      	</p>
    </div>
    </div>
  @endforeach 
@else
    <div class="" style="border:1px solid #ddd; padding: 10px;">{{ trans('myaccount.noreferralsfound') }}</div>
@endif
</div>
{{ $referralCommissions->links() }}







