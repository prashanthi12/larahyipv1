<h4>{{ trans('forms.my_neteller') }}</h4>
@if(count($neteller_result) > 0)
   @include('home._mypayaccounts_neteller_details')
@endif
<p>
<a href="" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#neteller_modal">{{ trans('forms.add_neteller') }}</a>                       
</p>
<hr>
 <!-- Modal -->
  <div class="modal fade" id="neteller_modal" role="dialog" style="margin: 70px;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ trans('forms.neteller_details') }}</h4>
        </div>
        <div class="modal-body">
            <form method="post" id="neteller_form" action="{{ url('myaccount/payaccounts')}}">
            {{ csrf_field() }}
              <div class="form-group">
                <label for="recipient-name" class="control-label">{{ trans('forms.merchant_id_lbl') }}:</label>
                <input type="text" class="form-control" id="merchant_id" name="merchant_id" required>
              </div> 

              <div class="form-group">
                <label for="recipient-name" class="control-label">{{ trans('forms.merchant_key_lbl') }}:</label>
                <input type="text" class="form-control" id="merchant_key" name="merchant_key" required>
              </div> 
             
              <div class="form-group">
                <input type="hidden" name="paymentid" value="16">
                <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success" id="payment" type="submit" onclick="this.disabled=true;this.form.submit();">     
              </div>           
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('forms.close') }}</button>        
        </div>
    </div>   
  </div>
</div>