<div class="grid">
@if (count($partialWithdraws) > 0)
    @foreach($partialWithdraws as $partialWithdraw)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }})</small><br/>
        {{ $partialWithdraw->amount }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('myaccount.plan_name') }} : </small><br/>       
          {{ $partialWithdraw->present()->getPlanname($partialWithdraw->id) }}<br>
      </p>
    </div>   
    <div class="">
        <p style="text-align: center;">
            <small>{{ trans('myaccount.datetime') }} :</small><br/>
                {{ $partialWithdraw->created_at->diffForHumans() }}
        </p>
    </div>
    </div>
  @endforeach 
@else
    <div class="" style="border:1px solid #ddd; padding: 10px;">{{ trans('myaccount.nopartialwithdrawfound') }}</div>
@endif
</div>
{{ $partialWithdraws->links() }}







