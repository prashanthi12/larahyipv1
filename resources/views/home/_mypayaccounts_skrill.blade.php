<h4>{{ trans('forms.my_skrill') }}</h4>
@if(count($skrill_result) > 0)
   @include('home._mypayaccounts_skrill_details')
@endif
<p>
<a href="" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#skrill_modal">{{ trans('forms.add_skrill') }}</a>                       
</p>
<hr>
 <!-- Modal -->
  <div class="modal fade" id="skrill_modal" role="dialog" style="margin: 120px;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ trans('forms.skrill_details') }}</h4>
        </div>
        <div class="modal-body">
            <form method="post" id="skrill_form" action="{{ url('myaccount/payaccounts')}}">
            {{ csrf_field() }}
              <div class="form-group">
                <label for="recipient-name" class="control-label">{{ trans('forms.skrill_email_lbl') }}:</label>
                <input type="text" class="form-control" id="email_id" name="email_id" required>
              </div> 
              <div class="form-group">
                <input type="hidden" name="paymentid" value="11">
                  <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success" id="payment" type="submit" onclick="this.disabled=true;this.form.submit();">     
              </div>           
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('forms.close') }}</button>        
        </div>
    </div>    
  </div>
</div>