 <div class="boxy mt-20">
    <div class="col-md-12">
        <div class="col-md-3">
            <p>{{ trans('myaccount.noofreferrals') }} <br/>({{ trans('myaccount.direct') }})</p>
            <h3 class="is-amount">{{ $user->referralCount }}</h3>
        </div>
        <div class="col-md-3">
            <p>{{ trans('myaccount.totaldeposit') }} <br/>{{ trans('myaccount.byreferrals') }}</p>
            <h3 class="is-amount">{{ $user->depositByReferrals }} <small>{{ config::get('settings.currency') }}</small></h3>
        </div>
        <div class="col-md-6">
            <p>{{ trans('myaccount.referrallink') }} <br/> {{ trans('myaccount.sharelink') }}</p>
            <p class=""><a href="{{ url('/ref/') }}/{{ Auth::user()->name }}" target="_blank">{{ url('/ref') }}/{{ Auth::user()->name }}</a></p>
            <p><!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox"></div></p>
        </div>
    </div>
</div>