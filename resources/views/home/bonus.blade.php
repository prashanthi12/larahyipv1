@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('myaccount.my_bonus') }}</div>
    <div class="panel-body">  
        <div class="row mt-20">
            <div class="col-md-12">              
                @include('home._table_sendbonus')              
            </div>
        </div>
    </div>
</div>
@endsection
