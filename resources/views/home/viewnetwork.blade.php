@extends('layouts.myaccount') 
@push('headscripts')
<style>
.google-visualization-orgchart-node {
    border: 0 !important;
}
</style>
@endpush
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('myaccount.mynetwork') }}</div>
    <div class="panel-body">
     @if($noNetwork)
        <p>{{ trans('myaccount.nonetworkmessage') }}</p>
     @else
           <div class="row">
                <div class="col-md-12">
                  @if (count($myNetwork) > 0)
                    <div style="max-height: 300px; overflow: scroll;">
                        <div id="chart_div"></div>
                    </div>
                  @endif
                    <div style="max-height: 300px; overflow: scroll;">
                    @include('home._table_network')
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection

@push('bottomscripts')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
      google.charts.load('current', {packages:["orgchart"]});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Manager');
        data.addColumn('string', 'ToolTip');

        // For each orgchart box, provide the name, manager, and tooltip to show.
        data.addRows(
          {!! $myNetworkArray !!}
        );

        // Create the chart.
        var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
        // Draw the chart, setting the allowHtml option to true for the tooltips.
        chart.draw(data, {allowHtml:true});
      }
  </script>
@endpush
