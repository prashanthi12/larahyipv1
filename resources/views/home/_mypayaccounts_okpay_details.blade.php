  <div class="row">
  <div class="col-md-12">    
    <div class="table-responsive">          
        <table class="table table-bordered table-hover">
          <thead>
            <tr>        
              <th>{{ trans('forms.okpay_payid_lbl') }}</th>
              <th>{{ trans('forms.okpay_paynumber_lbl') }}</th>
              <th>{{ trans('forms.autowithdraw') }}</th>
              <th>{{ trans('forms.actions') }}</th>       
            </tr>
          </thead>
          <tbody>
          @foreach($okpay_result as $okpay)
            <tr>
              <td>{{ $okpay->param1 }}</td>
              <td>{{ $okpay->param2 }}</td>
               <td>
              @if($okpay->current == '1')
                <a class="okpayauto" href="{{ url('myaccount/currentaccounts/'.$okpay->id.'/'.$okpay->paymentgateways_id.'/'.$okpay->current)}}">{{ trans('forms.yes') }}</a>
              @else
                <a class="okpayauto" href="{{ url('myaccount/currentaccounts/'.$okpay->id.'/'.$okpay->paymentgateways_id.'/'.$okpay->current)}}">{{ trans('forms.no') }}</a>
              @endif
              </td>
              <td>
                <form method="post" class="okpayremovefrm" action="{{ url('myaccount/removeaccount/'.$okpay->id.'') }}">
                {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
                </form>
              </td>            
            </tr>
            @endforeach    
          </tbody>
        </table>
    </div>
</div>
</div>

@push('bottomscripts')
<script>
    $(".okpayremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
    $(".okpayauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush