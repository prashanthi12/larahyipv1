  <div class="row">
  <div class="col-md-12">    
    <div class="table-responsive">          
        <table class="table table-bordered table-hover">
          <thead>
            <tr>        
              <th>{{ trans('forms.payee_account_lbl') }}</th>
              <th>{{ trans('forms.autowithdraw') }}</th>
              <th>{{ trans('forms.actions') }}</th>       
            </tr>
          </thead>
          <tbody>
          @foreach($perfectmoney_result as $perfectmoney)
            <tr>
              <td>{{ $perfectmoney->param1 }}</td>
               <td>
              @if($perfectmoney->current == '1')
                <a class="perfectmoneyauto" href="{{ url('myaccount/currentaccounts/'.$perfectmoney->id.'/'.$perfectmoney->paymentgateways_id.'/'.$perfectmoney->current)}}">{{ trans('forms.yes') }}</a>
              @else
                <a class="perfectmoneyauto" href="{{ url('myaccount/currentaccounts/'.$perfectmoney->id.'/'.$perfectmoney->paymentgateways_id.'/'.$perfectmoney->current)}}">{{ trans('forms.no') }}</a>
              @endif
              </td>
              <td>
                <form method="post" class="perfectmoneyremovefrm" action="{{ url('myaccount/removeaccount/'.$perfectmoney->id.'') }}">
                {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
                </form>
              </td>            
            </tr>
            @endforeach    
          </tbody>
        </table>
    </div>
  </div>
</div>

@push('bottomscripts')
<script>
    $(".perfectmoneyremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
    $(".perfectmoneyauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush