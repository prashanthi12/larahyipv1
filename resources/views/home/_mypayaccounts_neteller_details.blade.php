  <div class="row">
  <div class="col-md-12">    
    <div class="table-responsive">          
        <table class="table table-bordered table-hover">
          <thead>
            <tr>        
              <th>{{ trans('forms.merchant_id_lbl') }}</th>
              <th>{{ trans('forms.merchant_key_lbl') }}</th>
              <th>{{ trans('forms.autowithdraw') }}</th>
              <th>{{ trans('forms.actions') }}</th>       
            </tr>
          </thead>
          <tbody>
          @foreach($neteller_result as $neteller)
            <tr>
              <td>{{ $neteller->param1 }}</td>
              <td>{{ $neteller->param2 }}</td>
               <td>
              @if($neteller->current == '1')
                <a class="netellerauto" href="{{ url('myaccount/currentaccounts/'.$neteller->id.'/'.$neteller->paymentgateways_id.'/'.$neteller->current)}}">{{ trans('forms.yes') }}</a>
              @else
                <a class="netellerauto" href="{{ url('myaccount/currentaccounts/'.$neteller->id.'/'.$neteller->paymentgateways_id.'/'.$neteller->current)}}">{{ trans('forms.no') }}</a>
              @endif
              </td>
              <td>
                <form method="post" class="netellerremovefrm" action="{{ url('myaccount/removeaccount/'.$neteller->id.'') }}">
                {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
                </form>
              </td>            
            </tr>
            @endforeach    
          </tbody>
        </table>
    </div>
  </div>
</div>

@push('bottomscripts')
<script>
    $(".netellerremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
    $(".netellerauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush