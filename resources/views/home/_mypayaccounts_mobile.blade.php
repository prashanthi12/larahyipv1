<h4>{{ trans('forms.my_mobile') }}</h4>
@if(count($manualdeposit_mobile_result) > 0)
   @include('home._mypayaccounts_mobile_details')
@endif
<p>
<a href="" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#mobile">{{ trans('forms.add_mobile_account') }}</a>                       
</p>
<hr>
 <!-- Modal -->
  <div class="modal fade" id="mobile" role="dialog" style="margin: 50px;"> 
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ trans('forms.mobile_details') }}</h4>
        </div>
        <div class="modal-body">
             <form method="post" id="" action="{{ url('myaccount/payaccounts')}}">
            {{ csrf_field() }}
             

            <div class="form-group">
              <label>{{ trans('forms.country_lbl') }}</label>
              <select class="form-control" id="country" name="country" onchange="getCountryCode(this.value);" required>
              <option value="">Select</option>
                  @foreach ($country as $country)
                      <option value="{{ $country->id }}"  >{{ $country->name }}</option>
                  @endforeach
              </select>
             
            </div>

              <div class="form-group">
                <label class="control-label">{{ trans('forms.mobile_no_lbl') }}:</label>
                <input type="text" class="form-control" id="mobile_no" name="mobile_no" value="" required>
              </div> 
              
              <div class="form-group">
                    <input type="hidden" name="paymentid" value="21">
                   <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success" id="payment" type="submit" onclick="this.disabled=true;this.form.submit();">        
              </div>           
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('forms.close') }}</button>
        </div>
      </div>  
  </div>
</div>
@push('bottomscripts')
<script>
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
function getCountryCode(countryid)
{ 
    $.post( "/myaccount/getmobilecode", { countryid: countryid })
      .done(function( data ) {
        $('#mobile_no').val(data);
   });
}
</script>
@endpush