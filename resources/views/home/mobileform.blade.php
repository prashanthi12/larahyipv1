<div class="col-md-12 ">
<form method="post" action="{{ url('myaccount/verificationprocess/')}}" class="form-horizontal" id="contact">
{{ csrf_field() }}
<input type="hidden" name="verificationcode" value="{{ $verificationcode }}">
    <div class="form-group{{ $errors->has('mobilecode') ? ' has-error' : '' }}">
        <input name="mobilecode" class="form-control" value="{{ old('mobilecode') }}" type="text" placeholder="{{ trans('forms.codehere') }}">
        <small class="text-danger">{{ $errors->first('mobilecode') }}</small>
    </div>     
    <div class="form-group">
        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-primary" type="submit" onclick="this.disabled=true;this.form.submit();">
        <a href="" class='btn btn-default'>{{ trans('forms.reset') }}</a>
    </div>
</form>
</div>
