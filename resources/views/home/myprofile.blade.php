@extends('layouts.myaccount')

@section('content')
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('myprofile.create_profile') }}</div>

                <div class="panel-body">
                   @if (session('mobilecodeerror'))
                    <div class="alert alert-danger">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ session('mobilecodeerror') }}
                    </div>
                    @endif
                    <div class="row">
                      @include('home.profileform')
                    </div>
            </div>
           </div>
@endsection
