<div class="grid">
@if (count($bonuses) > 0)
    @foreach($bonuses as $bonus)
    <div class="grid grid-2 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }})</small><br/>       
          <strong>{{ $bonus->amount }}</strong>
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('myaccount.comment') }} : </small><br/>       
          {{ $bonus->comments }}<br>
          {{ $bonus->created_at->diffForHumans() }}
      </p>     
    </div>
    </div>
  @endforeach 
@else
        <div class="">{{ trans('myaccount.nobonusfound') }}</div>
@endif
</div>
{{ $bonuses->links() }}


