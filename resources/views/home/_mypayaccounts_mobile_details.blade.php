<div class="row">
    <div class="col-md-12">    
    <div class="table-responsive">          
        <table class="table table-bordered table-hover">
          <thead>
            <tr>        
              <th>{{ trans('forms.mobile_no_lbl') }}</th>
              <th>{{ trans('forms.country_lbl') }}</th>
              
              <th>{{ trans('forms.autowithdraw') }}</th>
              <th>{{ trans('forms.actions') }}</th>       
            </tr>
          </thead>
          <tbody>
          @foreach($manualdeposit_mobile_result as $mobile)
            <tr>
              <td>{{ $mobile->param9 }}</td>
              <td>{{ optional($mobile->country)->name }}</td>
              
              <td>
               @if($mobile->current == '1')
                <a class="mobileauto" href="{{ url('myaccount/currentaccounts/'.$mobile->id.'/'.$mobile->paymentgateways_id.'/'.$mobile->current)}}">{{ trans('forms.yes') }}</a>
              @else
                <a class="mobileauto" href="{{ url('myaccount/currentaccounts/'.$mobile->id.'/'.$mobile->paymentgateways_id.'/'.$mobile->current)}}">{{ trans('forms.no') }}</a>
              @endif
              </td> 
              <td>
              <form method="post" class="mobileremovefrm" action="{{ url('myaccount/removeaccount/'.$mobile->id.'') }}">
                {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
              </form>
              </td>           
            </tr>
            @endforeach    
          </tbody>
        </table>
    </div>
    </div>
  </div>
  
@push('bottomscripts')
<script>
    $(".mobileremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
     $(".mobileauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush