@extends('layouts.myaccount')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('forms.transactionpassword') }}
            @if (!is_null($transactionpassword))   
                <a href="{{ url('/myaccount/rest_transaction_password') }}" class="btn btn-primary btn-xs pull-right" id="transaction_password_click">{{ trans('forms.transaction_password_reset_btn') }}</a>
            @endif
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    @if (session('errormessage'))
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session('errormessage') }}
                        </div>
                    @endif

                    @if (session('successmessage'))
                        <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session('successmessage') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                @include('home.transactionpasswordform')
            </div>                     
        </div>
    </div>
@endsection

@push('bottomscripts')
<script>  
     $("#transaction_password_click").on("click", function(){
        return confirm("{{ trans('forms.transaction_password_confirm_alert') }}");
    });
</script>
@endpush
