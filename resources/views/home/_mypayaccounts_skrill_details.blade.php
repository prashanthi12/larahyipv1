  <div class="row">
  <div class="col-md-12">    
    <div class="table-responsive">          
        <table class="table table-bordered table-hover">
          <thead>
            <tr>        
              <th>{{ trans('forms.skrill_email_lbl') }}</th>
              <th>{{ trans('forms.autowithdraw') }}</th>
              <th>{{ trans('forms.actions') }}</th>       
            </tr>
          </thead>
          <tbody>
          @foreach($skrill_result as $skrill)
            <tr>
              <td>{{ $skrill->param1 }}</td>
               <td>
              @if($skrill->current == '1')
                <a class="skrillauto" href="{{ url('myaccount/currentaccounts/'.$skrill->id.'/'.$skrill->paymentgateways_id.'/'.$skrill->current)}}">{{ trans('forms.yes') }}</a>
              @else
                <a class="skrillauto" href="{{ url('myaccount/currentaccounts/'.$skrill->id.'/'.$skrill->paymentgateways_id.'/'.$skrill->current)}}">{{ trans('forms.no') }}</a>
              @endif
              </td>
              <td>
                <form method="post" class="skrillremovefrm" action="{{ url('myaccount/removeaccount/'.$skrill->id.'') }}">
                {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
                </form>
              </td>            
            </tr>
            @endforeach    
          </tbody>
        </table>
    </div>
  </div>
</div>

@push('bottomscripts')
<script>
    $(".skrillremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
    $(".skrillauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush