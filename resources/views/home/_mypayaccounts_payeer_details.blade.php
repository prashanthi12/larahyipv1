  <div class="row"> 
    <div class="col-md-12">   
    <div class="table-responsive">          
        <table class="table table-bordered table-hover">
          <thead>
            <tr>        
              <th>{{ trans('forms.payeer_id_lbl') }}</th>
              <th>{{ trans('forms.payeer_email_lbl') }}</th>
              <th>{{ trans('forms.autowithdraw') }}</th>
              <th>{{ trans('forms.actions') }}</th>       
            </tr>
          </thead>
          <tbody>
          @foreach($payeer_result as $payeer)
            <tr>
              <td>{{ $payeer->param1 }}</td>
              <td>{{ $payeer->param2 }}</td>
              <td>
              @if($payeer->current == '1')
                <a class="payeerauto" href="{{ url('myaccount/currentaccounts/'.$payeer->id.'/'.$payeer->paymentgateways_id.'/'.$payeer->current)}}">{{ trans('forms.yes') }}</a>
              @else
                <a class="payeerauto" href="{{ url('myaccount/currentaccounts/'.$payeer->id.'/'.$payeer->paymentgateways_id.'/'.$payeer->current)}}">{{ trans('forms.no') }}</a>
              @endif
              </td>
              <td>
                <form method="post" class="payeerremovefrm" action="{{ url('myaccount/removeaccount/'.$payeer->id.'') }}">
                {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
                </form>
              </td>            
            </tr>
            @endforeach    
          </tbody>
        </table>
    </div>
    </div>
  </div>
        @push('bottomscripts')
<script>
    $(".payeerremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
     $(".payeerauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush