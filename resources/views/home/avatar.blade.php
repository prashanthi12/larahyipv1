@extends('layouts.myaccount')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('forms.changeavatar') }}</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session('status') }}
                    </div>
                @endif                            
                </div>
            </div>
            <div class="row">
                @include('home.avatarform')
            </div>                     
        </div>
    </div>
@endsection
