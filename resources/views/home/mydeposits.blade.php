<section>
<table class="table table-bordered">
<thead>
	<tr>
		<td>#</td>
		<td>{{ trans('myaccount.plan_name') }}</td>
		<td>{{ trans('myaccount.amount') }}</td>
		<td>{{ trans('myaccount.duration') }}</td>
		<td>{{ trans('myaccount.maturity_date') }}</td>
	</tr>
</thead>
<tbody>
	@foreach($mydeposits as $mydeposit)
	<tr>
		<td>{{ $loop->iteration }}</td>
		<td>{{ $mydeposit->plan->name }}</td>
		<td>{{ $mydeposit->amount }}</td>
		<td>{{ $mydeposit->plan->duration }}</td>
		<td>{{ $mydeposit->matured_on }}</td>
	</tr>
	@endforeach
</tbody>
</table>
</section>