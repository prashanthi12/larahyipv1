@extends('layouts.adminpanel')

@section('content')
<section class="section">
  <div class="row">
      <div class="container">
        <h3>Send Mail List</h3>       
        <div class="boxy boxy-white">
            <div class="row">
                <div class="col-sm-12">
                  @include('layouts.message') 
                </div>
            </div>
            @include('admin.sendmail.send_list')
        </div>
      </div>
    </div>
</section>
@endsection



        

