<table class="table table-bordered" id="">
        <thead>
            <tr>                
                <th>Name</th>               
                <th>Subject</th>               
                <th>Message</th> 
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
            @foreach($list as $send)
                <tr>
                    <td><a href="{{ url('admin/massmail/') }}/{{ $send->batch }}">
                    <strong>{{ ucfirst($send->type) }} Users</strong></a></td>
                    <td>{{ $send->subject }}</td>
                    <td><a class='viewmessage' href="#" data-toggle='modal' data-target1='{{ $send->id }}'>View Message</a></td>
                    <td>{{ $send->created_at->format('d/m/Y H:i:s') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>    
    
<div class="modal fade" id="message-modals" role="dialog"></div>
{{$list->links()}}

@push('scripts')
    <script>
        $('.viewmessage').on('click', function () {
            var $this = $(this).data('target1');
            $('#message-modals').load("{{url('admin/viewmessage')}}/" + $this, function (response, status, xhr) {
                if (status == "success") {
                    $(response).modal('show');
                }
            });
        });
    </script>
@endpush