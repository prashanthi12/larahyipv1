@extends('layouts.adminpanel')
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <h3>{{ trans('admin.newslettersettings') }}</h3>
                <div class="boxy boxy-white">
                <div class="p-20">
                <h4>{{ trans('admin.title1') }}</h4>
                <h5>{{ trans('admin.title2') }}</h5>
                    <div class="well">
                        <p>{{ trans('admin.status') }} :  {{ Config::get('settings.mailchimp_status') == 0 ? 'Inactive' : 'Active'  }}</label></p>
                        <p>{{ trans('admin.apikey') }} :  {{ env('MAILCHIMP_APIKEY') }}</label></p>
                        <p>{{ trans('admin.listid') }} :  {{ env('MAILCHIMP_LIST_ID') }}</label></p>
                        <p>{{ trans('admin.fromemail') }} :  {{ env('MAILCHIMP_LIST_FROM_EMAIL') }}</label></p>
                    </div> 
                </div> 
            </div>  
        </div>
    </div>
</section>
@endsection