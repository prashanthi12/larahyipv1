@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <h3>{{ trans('admin.staffs') }}</h3>   
			<div class="boxy boxy-white">
                @include('layouts.message')
                @include('adminpartials._stafflist')
            </div>    
        </div>
	</div>
</section>
@endsection