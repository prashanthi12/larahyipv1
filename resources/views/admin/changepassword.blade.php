@extends('layouts.adminpanel')

@section('content')
        <section class="section">

    <div class="row">

        <div class="container">
        
            <h3>Change Password</h3>
                 <div class="panel panel-default">
   
    <div class="panel-body">
       
        <div class="row">
            <div class="col-md-12">
                        @if (session('successmessage'))
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ session('successmessage') }}
                                </div>
                            @endif
                            @if (session('errormessage'))
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ session('errormessage') }}
                                </div>
                            @endif
                       
                    
                      @include('admin.changepasswordform')
                    </div>
        </div>
    </div>
 </div>
            </div>
        </div>


</section>
@endsection
