@extends('layouts.adminpanel')

@section('content')
<section class="section">
    <div class="container">
    <div class="content">
        <h3>{{ trans('admin.users') }} <small>( {{ $usersData->count() }} )</small></h3>        
    </div>
    </div>
    <div class="container mt-20 mb-20">
        @include('layouts.message')
        <div class="boxy boxy-white">
        <table class="table table-bordered table-striped dataTable"  id="datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{ trans('admin.name') }}</th>
                    <th>{{ trans('admin.dateofjoin') }}</th>
                    <th>{{ trans('admin.sponsor') }}</th>
                    <th>{{ trans('admin.referrals') }}</th>
                    <th>{{ trans('admin.balance') }} ({{ config::get('settings.currency') }})</th>
                    <th>{{ trans('admin.activedeposit') }} ({{ config::get('settings.currency') }})</th>
                    <th>{{ trans('admin.lifetimedeposit') }} ({{ config::get('settings.currency') }})</th>
                    <th>{{ trans('admin.lifetimeearnings') }} ({{ config::get('settings.currency') }})</th>
                </tr>
            </thead>
            <tbody>
            @foreach($usersData as $userData)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td width="220px">
                        <p class="trim">
                            <a href="{{ url('admin/users') }}/{{ $userData['name'] }} ">
                            <strong>{{ $userData['name'] }}</strong>
                            </a>
                        </p>
                        <p class="trim">
                        @if( $userData['emailVerified'] )
                            <a href="mailto:{{ $userData['email'] }}" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-message" title="{{ $userData['email'] }}"></span>Email</a>
                        @else
                            <a href="mailto:{{ $userData['email'] }}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-message" title="{{ $userData['email'] }}"></span>Email</a>
                            @endif
                        @if( $userData['active'] )
                            <span class="label label-success"><i class="glyphicon glyphicon-lock" title="verified"></i></span>
                        @else
                            <span class="label label-danger"><i class="glyphicon glyphicon-lock" title="verified"></i></span>
                        @endif
                        @if( $userData['isUserProfileCompleted'] )
                            <span class="label label-success">Profile</span>
                        @else
                            <span class="label label-danger">Profile</span>
                        @endif
                        @if( $userData['kycApproved'] == 1)
                            <span class="label label-success">KYC</span>
                        @elseif( $userData['kycApproved'] == 0 || $userData['kycApproved'] == 2)
                            <span class="label label-danger">KYC</span>
                        @endif
                        </p>
                    </td>
                    <td>
                        <span title="{{ $userData['doj'] }}">{{ $userData['doj']->format('d-m-y') }}</span>
                    </td>
                    <td>{{ $userData['sponsor'] }}</td>
                    <td class="text-center">{{ $userData['refCount'] }}</td>
                    <td class="is-money text-right">{{ $userData['balance'] }} </td>
                    <td class="is-money text-right">{{ $userData['activeDeposit'] }} </td>
                    <td class="is-money text-right">{{ $userData['lifeTimeDeposit'] }} </td>
                    <td class="is-money text-right">{{ $userData['lifeTimeEarnings'] }} </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endpush

