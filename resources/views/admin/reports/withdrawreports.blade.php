@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
                <h3>Withdraws Reports</h3>   

                        <div class="boxy boxy-white">
                            <table class="table table-bordered" id="withdrawreportdatatable">
                            	<thead>
                            	    <tr>
                                        <th>#</th>
                            	        <th>Name</th> 
                            	        <th>Option</th> 
                            	    </tr>
                                </thead>
                                <tbody>        
                                    <tr>
                                        <td>1</td>
                                        <td>Completed</td>
                                        <td><a id="export" href="{{ url('admin/reports/withdraws/export/completed') }}" class="btn btn-success btn-xs">Export</a></td>      
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Pending</td>
                                        <td><a id="export" href="{{ url('admin/reports/withdraws/export/pending') }}" class="btn btn-success btn-xs">Export</a></td>            
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Rejected</td>
                                        <td><a id="export" href="{{ url('admin/reports/withdraws/export/rejected') }}" class="btn btn-success btn-xs">Export</a></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Request</td>
                                        <td><a id="export" href="{{ url('admin/reports/withdraws/export/request') }}" class="btn btn-success btn-xs">Export</a></td>
                                    </tr>
	                           </tbody>
                            </table>
                        </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#withdrawreportdatatable').DataTable();
    });
</script>
@endpush