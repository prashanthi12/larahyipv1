@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
                <h3>Sponsors Reports</h3>   

                        <div class="boxy boxy-white">
                            <table class="table table-bordered" id="sponsorsreportdatatable">
                            	<thead>
                            	    <tr>
                                        <th>#</th>
                            	        <th>Name</th> 
                            	        <th>Option</th> 
                            	    </tr>
                                </thead>
                                <tbody>        
                                    <tr>
                                        <td>1</td>
                                        <td>Top 10 Sponsors - By Numbers</td>
                                        <td><a id="export" href="{{ url('admin/reports/sponsors/export/1') }}" class="btn btn-success btn-xs">Export</a></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Top 100 Sponsors - By Numbers</td>
                                        <td><a id="export" href="{{ url('admin/reports/sponsors/export/2') }}" class="btn btn-success btn-xs">Export</a></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Top 10 Sponsors - By Investment volume</td>
                                        <td><a id="export" href="{{ url('admin/reports/sponsors/export/3') }}" class="btn btn-success btn-xs">Export</a></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Top 100 Sponsors - By Investment volume</td>
                                        <td><a id="export" href="{{ url('admin/reports/sponsors/export/4') }}" class="btn btn-success btn-xs">Export</a></td>
                                    </tr>
                            	</tbody>
                            </table>

            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#sponsorsreportdatatable').DataTable();
    });
</script>
@endpush