@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
                <h3>Investment Payment Reports</h3>   
                <div class="boxy boxy-white">
                            <table class="table table-bordered" id="depositpgsreportdatatable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Payment Gateway Name</th> 
                                        <th>Option</th> 
                                    </tr>
                                </thead>
                                <tbody> 
                                @foreach($payments as $payment)     
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $payment->displayname }}</td>
                                        <td><a id="export" href="{{ url('admin/reports/deposits/payments/export') }}/{{ $payment['id'] }}" class="btn btn-success btn-xs">Export</a></td>            
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#depositpgsreportdatatable').DataTable();
    });
</script>
@endpush