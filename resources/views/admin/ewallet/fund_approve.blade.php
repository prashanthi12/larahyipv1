@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <h3>{{ trans('admin.fund_approve_form') }}</h3>       
            <div class="panel panel-default"> 
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            @include('admin.ewallet.fund_approveform')                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
