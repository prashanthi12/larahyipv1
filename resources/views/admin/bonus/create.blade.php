@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">      
            <h3>Bonus Form</h3>
            <div class="panel panel-default">  
                <div class="panel-body">     
                    <div class="row">
                        <div class="col-md-12">                      
                            @include('admin.bonus.bonusform')                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
