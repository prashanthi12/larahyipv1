@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <h3>{{ trans('admin.settingsvalues') }}</h3>   
            <div class="boxy boxy-white" style="padding: 40px 100px;">
                <table class="table table-striped table-bordered">
                    <thead>
                        <th>{{ trans('admin.settings') }}</th>
                        <th>{{ trans('admin.value') }}</th>
                    </thead>
                    <tbody>
                    @foreach($settings as $data)
                        <tr>
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->value }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>                
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
    // $(document).ready(function(){
    //     $('#depositreportdatatable').DataTable();
    // });
</script>
@endpush