<table class="table table-bordered" id="ticketdatatable">
    <thead>
        <tr>
            <th>{{ trans('admin.owner') }}</th> 
            <th>{{ trans('admin.subject') }}</th>
            <th>{{ trans('admin.status') }}</th> 
            <th>{{ trans('admin.agent') }}</th>
            <th>{{ trans('admin.createdon') }}</th>   
            <th>{{ trans('admin.lastupdatedon') }}</th>            
        </tr>
    </thead>
    @if (count($result))
        @foreach($result as $data)
        <tbody>
            <tr>
                <td>            
                    @if (is_null($data->user))
                        {{ 'system' }}
                    @else
                        <a href="{{ url('admin/users/'.$data->user->name) }}">{{ $data->user->name }}</a>
                    @endif
                </td>           
                <td>               
                    <a href="{{ url('admin/ticket/'.$data['id']) }}">               
                        {{ $data['subject'] }}
                    </a>
                </td>           
                <td>{{ $data->status->name }}</td>
                @if ($userprofile->usergroup_id != 3) 
                <td>{{ $data->agent->name }}</td>
                @endif
                <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>
                <td>{{ $data->updated_at->format('d/m/Y H:i:s') }}</td>
            </tr>
        </tbody>
        @endforeach
    @else
    <tbody>
        <td colspan="6">
            {{ trans('forms.noticketsfound') }}
        </td>
    </tbody>
    @endif
 </table>

 @push('scripts')
<script>
    $(document).ready(function(){
        $('#ticketdatatable').DataTable();
    });
</script>
@endpush