@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <h3>{{ trans('admin.testimonialslist') }}</h3>   
             <div class="boxy boxy-white">
                <div class="p-20">
                    @include('layouts.message')   
                    @include('adminpartials._testimoniallist')
                </div>
            </div>
        </div>
    </div>
</section>
@endsection