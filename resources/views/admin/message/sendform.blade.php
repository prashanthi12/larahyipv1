<div class="col-md-12 ">
  <form method="post" action="{{ url('admin/message/save')}}" class="form-horizontal" id="contact">
{{ csrf_field() }}
    @if(Request::segment(3) == 'send')
      <div class="form-group{{ $errors->has('users') ? ' has-error' : '' }}">
          <input type="text" name="users" id="users" class='' value="{{ old('users') }}">
          <small class="text-danger">{{ $errors->first('users') }}</small>
      </div> 
    @else
      <div class="form-group{{ $errors->has('users') ? ' has-error' : '' }}">
          <input type="text" name="users" id="users" class='' value="{{ old('users') }}">
          <small class="text-danger">{{ $errors->first('users') }}</small>
      </div>  
    @endif
    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
        <textarea  rows="5" class="form-control"  placeholder="Enter your message here" name="message" required="required">{{ old('message') }}</textarea>
        <small class="text-danger">{{ $errors->first('message') }}</small>
    </div>  
    
    <div class="form-group">
        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-primary" type="submit" onclick="this.disabled=true;this.form.submit();"> 
        <a href="" class='btn btn-default'>{{ trans('forms.reset') }}</a>
    </div>
</form>
</div>

@push('styles')
<style>
.progControlSelect2 {
  width: 350px;
}
form {
  margin-top: 2px;
}
</style>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css">
@endpush

@push('scripts')
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>

<script>
$(document).ready(function(){
  var $progControl = $(".progControlSelect2").select2({
        placeholder: "Select Users"//placeholder
    });
  $(".iOSSelect2").on("click", function () { $progControl.val(["sw", "oc"]).trigger("change"); });
  $(".clearSelect2").on("click", function () { $progControl.val(null).trigger("change"); });
})
</script>
@endpush

@push('scripts')
<script src="https://cdn.jsdelivr.net/typeahead.js/0.9.3/typeahead.min.js"></script>

<script>
$(document).ready(function() {
    $("#users").typeahead({
        name : 'sear',
        remote: {
            url : '/admin/searchuser?query=%QUERY'
        }      
    });
});
</script>
@endpush
