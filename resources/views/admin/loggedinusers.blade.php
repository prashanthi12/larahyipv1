@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <h3>{{ trans('admin.loggedin_userslist') }}</h3>   
            </div>
            <div class="boxy boxy-white">
                <div class="mt-20 mb-20">
                    <table class="table table-bordered table-striped dataTable" id="logdatatable">
                        <thead>
                            <tr> 
                                <th>#</th>
                                <th>{{ trans('admin.username') }}</th>
                                <th>{{ trans('admin.ipaddress') }}</th>
                                <th>{{ trans('admin.date') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($loggedinusers as $data)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $data['user']['name'] }}</td>
                                    <td>{{ $data['ip_address'] }}</td>
                                    <td>{{ date("d-m-Y", $data['last_activity']) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
@push('scripts')
<script>
    $(document).ready(function(){
       // $('#logdatatable').DataTable();
    });
</script>
@endpush
            </div>    
        </div>
    </div>
</section>
@endsection
