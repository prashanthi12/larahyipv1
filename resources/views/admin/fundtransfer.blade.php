@extends('layouts.adminpanel') @section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <h3>{{ trans('admin.fundtransfers') }}</h3>   
            </div>
            <div class="boxy boxy-white">
                @include('adminpartials._fundtransferlist')
            </div>    
        </div>
    </div>
</section>
@endsection