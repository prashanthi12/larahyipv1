<table class="table table-bordered" id="requestdatatable">
    <thead>
        <tr>
            <th>{{ trans('admin.username') }}</th>
            <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th> 
            <th>{{ trans('admin.payment') }}</th>
            <th>{{ trans('admin.requestdate') }}</th>
            <th>{{ trans('admin.action') }}</th>            
        </tr>
    </thead>
    <tbody>
        @foreach($withdrawlists as $data)           
        <tr>
            <td><a href="{{ url('admin/users/'.$data->user->name) }}">{{ $data->user->name }}</a></td>
            <td>{{ $data->amount }}</td>
            <td>@include('adminpartials._popoveruserpayaccounts')</td>
            <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>
            <td>                 
                <a id="rejected" href="#" rel="{{ url('admin/withdraw/reject/'.$data['id'].'') }}" class="btn btn-danger btn-xs reject" >{{ trans('admin.reject') }}</a>
            </td>            
        </tr>
        @endforeach
    </tbody>
 </table>

 @push('scripts')
<script>
$(document).ready(function(){
    $('#requestdatatable').DataTable();

    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });

    $('.reject').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to rejected this withdraw request ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 
});
</script>
@endpush