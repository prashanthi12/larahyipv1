<table class="table table-bordered" id="pendingdatatable">
    <thead>
      <tr>
          <th>{{ trans('admin.username') }}</th> 
          <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th> 
          <th>{{ trans('admin.payment') }}</th>
          <th>{{ trans('admin.requestdate') }}</th>
          <th>{{ trans('admin.status') }}</th>
          <th>{{ trans('admin.actions') }}</th>            
      </tr>
    </thead>
    <tbody>
        @foreach($withdrawlists as $data)           
        <tr>
            <td><a href="{{ url('admin/users/'.$data->user->name) }}">{{ $data->user->name }}</a></td>
            <td>{{ $data->amount }}</td>
            <td>@include('adminpartials._popoveruserpayaccounts')</td>
            <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>
            <td>  
            @if(count($data->latestwithdrawdeposit) > 0 )
            <p>{{trans('admin.depositassigned')}}

                   {{$data->latestwithdrawdeposit->deposit->user->name}}<br>
                 
                   {{$data->latestwithdrawdeposit->deposit->created_at->format('d-m-Y H:i:s')}}</p>

            @else
                  <p>{{trans('admin.adminwithdraw_unassigned')}}</p>

            @endif



            </td>
            <td> 
              @if(count($data->latestwithdrawdeposit) > 0) 
                <a class='btn btn-success btn-xs flex-button' href="{{ $data->manualdeposit->proof }}" download>{{ trans('admin.viewproof') }}</a> 
                @endif
             {{-- @else        --}}
                <a id="completed" href="#" rel="{{ url('admin/withdraw/complete/'.$data['id'].'') }}" class="btn btn-success btn-xs complete" >{{ trans('admin.approve') }}</a>
                 
                <a id="rejected" href="#" rel="{{ url('admin/withdraw/reject/'.$data['id'].'') }}" class="btn btn-danger btn-xs reject" >{{ trans('admin.reject') }}</a>
             {{-- @endif --}}
            </td>            
        </tr>
         @endforeach
    </tbody>
 </table>

 @push('scripts')
<script>
$(document).ready(function(){
    $('#pendingdatatable').DataTable();

    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });

    $('.complete').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to approve this withdraw request ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 

    $('.reject').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to rejected this withdraw request ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 
});   
</script>
@endpush