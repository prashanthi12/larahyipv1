<table class="table table-bordered" id="completeddatatable">
    <thead>
        <tr>
            <th>{{ trans('admin.username') }}</th>
            <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th> 
            <th>{{ trans('admin.payment') }}</th>
            <th>{{ trans('admin.comments') }}</th> 
            <th>{{ trans('admin.completeddate') }}</th>                    
        </tr>
    </thead>
    <tbody>
        @foreach($withdrawlists as $data)       
        <tr>
            <td><a href="{{ url('admin/users/'.$data->user->name) }}">{{ $data->user->name }}</a></td>
            <td>{{ $data->amount }} </td>
            <td>@include('adminpartials._popoveruserpayaccounts')</td>
            <td>{{ $data->comments_on_complete }}</td>
            <td>{{ $data->completed_on->format('d/m/Y H:i:s') }}</td>            
        </tr>
        @endforeach
    </tbody>
</table>

@push('scripts')
<script>
    $(document).ready(function(){
       // $('#completeddatatable').DataTable();

        $('[data-toggle="popover"]').popover({
            placement : 'top',
            trigger : 'hover'
        });
    });
</script>
@endpush