@extends('layouts.adminpanel') @section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <h3> Report Investment List</h3>
            <div class="boxy boxy-white">
                <div class="row">
                    <div class="col-md-12">
                        @include('layouts.message')
                    </div>
                </div>
                   
                        @include('adminpartials._reportdepositlist')
                    
            </div>
        </div>
    </div>
</section>
@endsection
