@extends('layouts.adminpanel') @section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <h3>{{ ucfirst($status) }} Investment List</h3>
            <div class="boxy boxy-white">
                <div class="row">
                    <div class="col-md-12">
                        @include('layouts.message')
                    </div>
                </div>
                    @if ($status == 'new')    
                        @include('adminpartials._newdepositlist')
                    @elseif ($status == 'active')
                        @include('admin.deposit.activedeposit_list')
                    @elseif ($status == 'matured')
                        @include('admin.deposit.matureddeposit_list')
                    @elseif ($status == 'released')    
                        @include('admin.deposit.releaseddeposit_list')
                    @elseif ($status == 'rejected')
                        @include('admin.deposit.rejecteddeposit_list')
                     @elseif ($status == 'problem')
                        @include('admin.deposit.problemdeposit_list')
                    @elseif ($status == 'archived')
                        @include('admin.deposit.archiveddeposit_list')
                    @endif
            </div>
        </div>
    </div>
</section>
@endsection
