@extends('layouts.adminpanel') 
@section('content')
<section class="section">
  <div class="row">
      <div class="container">
        <h3>{{ trans('admin.add_details') }}</h3>
        <div class="panel panel-default">
          <div class="panel-body">
              <div class="row">
                  <div class="col-md-6">
                      @include('layouts.message')

                    <form method="post" id="add_details_form" action="">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>{{ trans('admin.amount') }} :</label>
                        {{ $deposit->amount }} {{ config::get('settings.currency') }}     
                    </div> 
                    <div class="form-group">
                        <label>{{ trans('admin.plan') }} :</label>
                        {{ $deposit->planName }}       
                    </div> 
                    <div class="form-group">
                        <label>{{ trans('admin.username') }} :</label>
                        {{ $deposit->user->name }}       
                    </div> 
                    <div class="form-group{{ $errors->has('deposit_details_type') ? ' has-error' : '' }}">
                        <label>{{ trans('forms.select_deposit_type_lbl') }}</label><br>
                        <label for="deposit_details_type">
                            <input type="radio" id="deposit_details_type" name="deposit_details_type" value="1" {{ Form::old("deposit_details_type") == 1 ? 'checked' : '' }} onclick="depositType(this.value)"> Select Existing Withdraw 
                        </label>
                        <label for="deposit_details_type">
                            <input type="radio" id="deposit_details_type" name="deposit_details_type" value="2" {{ Form::old("deposit_details_type") == 2 ? 'checked' : '' }} onclick="depositType(this.value)"> Add New Details
                        </label>
                        <small class="text-danger">{{ $errors->first('deposit_details_type') }}</small>
                    </div>    
                    @php
                        $withdrawusersfield = 'none';
                        if( Form::old("deposit_details_type") == 1)
                        {
                          $withdrawusersfield = 'block';
                        }
                    @endphp
                    <div id="withdrawusersfield" style="display:{{ $withdrawusersfield }}"> 
                      @if(count($withdraw) > 0)
                        <div class="form-group{{ $errors->has('users') ? ' has-error' : '' }}">
                            <label>Withdraw Users</label>
                            <select class="form-control" id="users" name="users" onchange="userpayaccount(this.value);">
                            <option value="">Select</option>
                                @foreach ($withdraw as $users)
                                    <option value="{{ $users->id }}">{{ $users->user->name }}</option>
                                @endforeach
                            </select>
                            <small class="text-danger">{{ $errors->first('users') }}</small>
                        </div>
                      @else
                        <div><small class="text-danger">No Existing Withdraw Found.</small></div>
                      @endif
                    </div>
                      
                    <div id="userpayaccount"></div>

                    @php
                        $addnewdetailfield = 'none';
                        if( Form::old("deposit_details_type") == 2)
                        {
                          $addnewdetailfield = 'block';
                        }
                    @endphp
                    <div id="addnewdetailfield" style="display:{{ $addnewdetailfield }}">
                      <div class="form-group{{ $errors->has('deposit_type') ? ' has-error' : '' }}">
                          <label>{{ trans('forms.deposit_type_lbl') }}</label>
                          <select class="form-control" id="deposit_type" name="deposit_type">
                            <option value="">Select</option>
                            <option value="1">Bank Details</option>
                            <option value="2">Mobile Details</option> 
                          </select>
                          <small class="text-danger">{{ $errors->first('deposit_type') }}</small>
                      </div>                   
                    </div>
                      @php
                        $bankdetailsfield = 'none';
                        if( Form::old("deposit_type") == 1)
                        {
                          $bankdetailsfield = 'block';
                        }
                      @endphp
                      <div id="bankdetailsfield" style="display:{{ $bankdetailsfield }}">
                        <div class="form-group{{ $errors->has('account_name') ? ' has-error' : '' }}">
                          <label class="control-label">{{ trans('forms.account_name_lbl') }}:</label>
                          <input type="text" class="form-control" id="account_name" name="account_name">
                          <small class="text-danger">{{ $errors->first('account_name') }}</small>
                        </div> 
                        <div class="form-group{{ $errors->has('account_no') ? ' has-error' : '' }}">
                          <label class="control-label">{{ trans('forms.account_no_lbl') }}:</label>
                          <input type="text" class="form-control" id="account_no" name="account_no">
                          <small class="text-danger">{{ $errors->first('account_no') }}</small>
                        </div>                         
                        <div class="form-group{{ $errors->has('bank_name') ? ' has-error' : '' }}">
                          <label class="control-label">{{ trans('forms.bank_name_lbl') }}:</label>
                          <input type="text" class="form-control" id="bank_name" name="bank_name">
                          <small class="text-danger">{{ $errors->first('bank_name') }}</small>
                        </div> 
                        <div class="form-group{{ $errors->has('account_address') ? ' has-error' : '' }}">
                          <label class="control-label">{{ trans('forms.bank_address_lbl') }}:</label>
                          <input type="text" class="form-control" id="account_address" name="account_address">
                          <small class="text-danger">{{ $errors->first('account_address') }}</small>
                        </div> 
                        <div class="form-group{{ $errors->has('swift_code') ? ' has-error' : '' }}">
                          <label class="control-label">{{ trans('forms.swift_code_lbl') }}:</label>
                          <input type="text" class="form-control" id="swift_code" name="swift_code">
                          <small class="text-danger">{{ $errors->first('swift_code') }}</small>
                        </div>  
                        <div class="form-group{{ $errors->has('bank_country') ? ' has-error' : '' }}">
                            <label>{{ trans('forms.country_lbl') }}</label>
                            <select class="form-control" id="bank_country" name="bank_country">
                            <option value="">Select</option>
                                @foreach ($country as $countries)
                                    <option value="{{ $countries->id }}">{{ $countries->name }}</option>
                                @endforeach
                            </select>
                            <small class="text-danger">{{ $errors->first('bank_country') }}</small>
                        </div>
                      </div>
                      
                      <!-- For Mobile -->
                      @php
                        $mobiledetailsfield = 'none';
                        if( Form::old("deposit_type") == 2)
                        {
                          $mobiledetailsfield = 'block';
                        }
                      @endphp
                      <div id="mobiledetailsfield" style="display:{{ $mobiledetailsfield }}">
                        <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                          <label>{{ trans('forms.country_lbl') }}</label>
                          <select class="form-control" id="country" name="country" onchange="getCountryCode(this.value);">
                            <option value="">Select</option>
                              @foreach ($country as $country)
                                  <option value="{{ $country->id }}">{{ $country->name }}</option>
                              @endforeach  
                          </select>
                          <small class="text-danger">{{ $errors->first('country') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                          <label>{{ trans('forms.mobile_no_lbl') }} ({{ trans('forms.country_code_alert') }})</label> 
                          <input name="mobile" id="mobile" class="form-control bfh-phone" value="{{ old('mobile') }}" type="text">
                          <small class="text-danger">{{ $errors->first('mobile') }}</small>
                        </div>
                      </div>

                      <div class="form-group">
                        <input type="hidden" name="depositid" value="{{ $id }}">
                        <input type="hidden" name="userid" value="{{ $userid }}">
                        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success" id="payment" type="submit" onclick="this.disabled=true;this.form.submit();"> 
                        <a href="" class='btn btn-primary'>{{ trans('forms.reset') }}</a>      
                      </div>           
                    </form>              
                </div>
              </div>  
            </div>
          </div>
      </div>
    </div>
</section>
@endsection

@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
  $("#deposit_type").on("change", function(){
      var details_id = $('#deposit_type').val();

      if (details_id == 1)
      {
        $('#bankdetailsfield').show();
      }
      else
      {
        $('#bankdetailsfield').hide();
      }

      if (details_id == 2)
      {
          $('#mobiledetailsfield').show();
      }
      else
      {
          $('#mobiledetailsfield').hide();
      }
  });

$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
function getCountryCode(countryid)
{ 
    $.post( "/admin/getmobilecode", { countryid: countryid })
      .done(function( data ) {
        $('#mobile').val(data);
   });
}

function userpayaccount(withdrawid)
{ 
     $.get( "/admin/withdraw/userpayaccount/"+ withdrawid)
      .done(function( data ) {
        $('#userpayaccount').html(data);
   });
}

function depositType(typeid)
{
     if (typeid == 1)
      {
        $('#withdrawusersfield').show();
        $('#bankdetailsfield').hide();
        $('#mobiledetailsfield').hide();
      }
      else
      {
        $('#withdrawusersfield').hide();
        $('.payaccounts').hide();
      }

      if (typeid == 2)
      {
          $('#addnewdetailfield').show();
          $('#withdrawusersfield').hide();
          $('.payaccounts').hide();
      }
      else
      {
          $('#addnewdetailfield').hide();
      }
}
</script>
@endpush