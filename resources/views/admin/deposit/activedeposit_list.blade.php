<table class="table table-bordered table-striped dataTable" id="activedepositdatatable">
    <thead>
         <tr>
            <th>{{ trans('admin.id') }}</th>
            <th>{{ trans('admin.username') }}</th>
            <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
            <th>{{ trans('admin.depositedon') }}</th>
            <th>{{ trans('admin.approvedon') }}</th>
            <th>{{ trans('admin.maturedon') }}</th>
            <th>{{ trans('admin.plan') }}</th>
            <th>{{ trans('admin.paymentmethod') }}</th>
            <th>{{ trans('admin.paymentdetails') }}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($depositlists as $activedeposit)
        <tr>
            <td>#{{ $loop->iteration  }}</td>
            <td>
                <a href="{{ url('admin/users/'.$activedeposit->user->name) }}">{{ $activedeposit->user->name }}</a>
            </td>
            <td>{{ $activedeposit->amount }} </td>
            <td>{{ $activedeposit->created_at->format('d/m/Y H:i:s') }}</td>
            <td>{{ $activedeposit->approved_on->format('d/m/Y H:i:s') }}</td>
            <td>
                @if (!is_null($activedeposit->matured_on))
                    {{ $activedeposit->matured_on->format('d/m/Y H:i:s') }}
                @else
                --
                @endif
            </td>
            <td>{{ $activedeposit->plan->name }}</td>
            <td>{{ $activedeposit->paymentgateway->displayname }}</td>
            @if ($activedeposit->paymentgateway->id == 9)
                <td>
                    <a class='bitcoin' href="#" data-toggle='modal' data-target1='{{ $activedeposit->transaction_id }}'>{{ trans('admin.viewdetails') }}</a>
                </td>
            @else
                <td>{{ $activedeposit->present()->getTransactionNumber($activedeposit->transaction_id) }}</td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
{{ $depositlists->links() }}

<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

@push('scripts')
<script>
$('.bitcoin').on('click', function () {
    var $this = $(this).data('target1');
    $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {
        if (status == "success") {
            $(response).modal('show');
        }
    });
});

$(document).ready(function(){
   // $('#activedepositdatatable').DataTable();  
});
</script>
@endpush