<table class="table table-bordered table-striped dataTable"  id="archivedepositdatatable">
    <thead>
         <tr>
            <th>{{ trans('admin.id') }}</th>
            <th>{{ trans('admin.username') }}</th>
            <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
            <th>{{ trans('admin.depositedon') }}</th>
            <th>{{ trans('admin.archivedon') }}</th>
            <th>{{ trans('admin.comments') }}</th>
            <th>{{ trans('admin.plan') }}</th>
            <th>{{ trans('admin.paymentmethod') }}</th>
            <th>{{ trans('admin.paymentdetails') }}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($depositlists as $archiveddeposit)
        <tr>
            <td>{{ $loop->iteration  }}</td>
            <td><a href="{{ url('admin/users/'.$archiveddeposit->user->name) }}">{{ $archiveddeposit->user->name }}</a></td>
            <td>{{ $archiveddeposit->amount }} </td>
            <td>{{ $archiveddeposit->created_at->format('d/m/Y H:i:s') }}</td>
            <td>{{ $archiveddeposit->archived_on->format('d/m/Y H:i:s') }}</td>
            <td>{{ $archiveddeposit->comments_on_archive }}</td>
            <td>{{ $archiveddeposit->plan->name }}</td>
            <td>{{ $archiveddeposit->paymentgateway->displayname }}</td>
            @if ($archiveddeposit->paymentgateway->id == 9)
                <td><a class='bitcoin' href="#" data-toggle='modal' data-target1='{{ $archiveddeposit->transaction_id }}'>View Details</a></td>
            @else
                <td>{{ $archiveddeposit->present()->getTransactionNumber($archiveddeposit->transaction_id) }}</td>
            @endif   
        </tr>
    @endforeach
    </tbody>
</table>
{{ $depositlists->links() }}

<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

@push('scripts')
<script>
 $('.bitcoin').on('click', function () {
    var $this = $(this).data('target1');
    $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {
        if (status == "success") {
            $(response).modal('show');
        }
    });
}); 
$(document).ready(function(){
       // $('#archivedepositdatatable').DataTable();
});
</script>
@endpush