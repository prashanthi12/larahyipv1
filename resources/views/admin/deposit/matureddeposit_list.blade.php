<table class="table table-bordered table-striped dataTable"  id="matureddepositdatatable">
    <thead>
        <tr>
            <th>{{ trans('admin.id') }}</th>
            <th>{{ trans('admin.username') }}</th>
            <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
            <th>{{ trans('admin.interest') }} ({{ config::get('settings.currency') }})</th>
            <th>{{ trans('admin.depositedon') }}</th>
            <th>{{ trans('admin.maturedon') }}</th>
            <th>{{ trans('admin.comments') }}</th>
            <th>{{ trans('admin.plan') }}</th>
            <th>{{ trans('admin.paymentmethod') }}</th>
            <th>{{ trans('admin.paymentdetails') }}</th>
            <th>{{ trans('admin.action') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($depositlists as $matureddeposit)
        <tr>
            <td>{{ $loop->iteration  }}</td>
            <td><a href="{{ url('admin/users/'.$matureddeposit->user->name) }}">{{ $matureddeposit->user->name }}</a></td>
            <td>{{ $matureddeposit->amount }}</td>
            <td>{{ $matureddeposit->interest->pluck('amount')->sum() }} </td>
            <td>{{ $matureddeposit->created_at->format('d/m/Y H:i:s') }}</td>
            <td>{{ $matureddeposit->matured_on->format('d/m/Y H:i:s') }}</td>
            <td>{{ $matureddeposit->comments_on_maturity }}</td>
            <td>{{ $matureddeposit->plan->name }}</td>
            <td>{{ $matureddeposit->paymentgateway->displayname }}</td>
            @if ($matureddeposit->paymentgateway->id == 9)
                <td><a class='bitcoin' href="#" data-toggle='modal' data-target1='{{ $matureddeposit->transaction_id }}'>{{ trans('admin.viewdetails') }}</a></td>
            @else
                <td>{{ $matureddeposit->present()->getTransactionNumber($matureddeposit->transaction_id) }}</td>
            @endif
            <td>
            @if ($matureddeposit->plan->principle_return == 1)
                <a href="#" rel="{{ url('admin/releasedeposit/'.$matureddeposit->id.'') }}" class="btn btn-success btn-sm flex-button release">{{ trans('admin.released') }}</a>
            @else
                --
            @endif
            </td>  
        </tr>
        @endforeach
    </tbody>
</table>
{{ $depositlists->links() }}

<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

@push('scripts')
<script>
$('.bitcoin').on('click', function () {
    var $this = $(this).data('target1');
    $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {
        if (status == "success") {
            $(response).modal('show');
        }
    });
}); 
$(document).ready(function(){
   // $('#matureddepositdatatable').DataTable();

    $('.release').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to release this deposit ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    });   
});
</script>
@endpush