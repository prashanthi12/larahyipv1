<div class="col-md-10 col-md-offset-1 payaccounts">

@if($payaccount->paymentgateways_id==20)
	<div class="form-group">
        <label>{{ trans('forms.account_name_lbl') }} :</label>
        {{ $payaccount['param4'] }}       
    </div> 
    <div class="form-group">
        <label>{{ trans('forms.account_no_lbl') }} :</label>
        {{ $payaccount['param3'] }}       
    </div> 
	<div class="form-group">
        <label>{{ trans('forms.bank_name_lbl') }} :</label>
        {{ $payaccount['param1'] }}       
    </div> 
    <div class="form-group">
        <label>{{ trans('forms.bank_address_lbl') }} :</label>
        {{ $payaccount['param5'] }}       
    </div> 
    <div class="form-group">
        <label>{{ trans('forms.swift_code_lbl') }} :</label>
        {{ $payaccount['param2'] }}       
    </div> 
@endif
@if($payaccount->paymentgateways_id==21)
    <div class="form-group">
        <label>{{ trans('forms.mobile_no_lbl') }} :</label>
        {{ $payaccount['param9'] }}       
    </div> 
    <div class="form-group">
        <label>{{ trans('forms.country_lbl') }} :</label>
        {{ optional($payaccount->country)->name }}       
    </div>
    @endif

</div>