<table class="table table-bordered table-striped dataTable"  id="problemdepositdatatable">
    <thead>
        <tr>
            <th>{{ trans('admin.id') }}</th>
            <th>{{ trans('admin.username') }}</th>
            <th>{{ trans('admin.depositamount') }}</th>
            <th>{{ trans('admin.depositedon') }}</th>
            <th>{{ trans('admin.comments') }}</th>
            <th>{{ trans('admin.plan') }}</th>
            <th>{{ trans('admin.paymentmethod') }}</th>
            <th>{{ trans('admin.paymentdetails') }}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($depositlists as $problemdeposit)
        <tr>
            <td>#{{ $loop->iteration  }}</td>
            <td><a href="{{ url('admin/users/'.$problemdeposit->user->name) }}">{{ $problemdeposit->user->name }}</a></td>
            <td>{{ $problemdeposit->amount }} {{ config::get('settings.currency') }}</td>
            <td>{{ $problemdeposit->created_at->format('d/m/Y H:i:s') }}</td>
            <td>{{ $problemdeposit->comments_on_problem }}</td>
            <td>{{ $problemdeposit->plan->name }}</td>
            <td>{{ $problemdeposit->paymentgateway->displayname }}</td>
            @if ($problemdeposit->paymentgateway->id == 9)
                <td><a class='bitcoin' href="#" data-toggle='modal' data-target1='{{ $problemdeposit->transaction_id }}'>{{ trans('admin.viewdetails') }}</a></td>
            @else
                <td>{{ $problemdeposit->present()->getTransactionNumber($problemdeposit->transaction_id) }}</td>
            @endif         
        </tr>
    @endforeach
    </tbody>
</table>
{{ $depositlists->links() }}

<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

@push('scripts')
<script>
$('.bitcoin').on('click', function () {
    var $this = $(this).data('target1');
    $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {
        if (status == "success") {
            $(response).modal('show');
        }
    });
});
$(document).ready(function(){
      //  $('#problemdepositdatatable').DataTable();  

        $(".activate").on("submit", function(){
        return confirm("Do you want to activate this deposit.?");
    });
});
</script>
@endpush