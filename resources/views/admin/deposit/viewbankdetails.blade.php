<div id='bankdetails' class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="margin-top: 50px;">
  <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ trans('forms.bankdetails') }}</h4>
        </div>
        @if($bankdeposit->details_type == 'bank')
          <div class="modal-body">
            <p>{{ trans('forms.account_name_lbl') }} : {{ $bankdeposit->account_name }} </p>
            <p>{{ trans('forms.account_no_lbl') }} : {{ $bankdeposit->account_no }} </p>
            <p>{{ trans('forms.bank_name_lbl') }} : {{ $bankdeposit->bank_name }}</p>
            <p>{{ trans('forms.bank_address_lbl') }} : {{ $bankdeposit->account_address }}</p>
            <p>{{ trans('forms.swift_code_lbl') }} : {{ $bankdeposit->swift_code }}</p>
          </div>
        @else
          <div class="modal-body">
            <p>{{ trans('forms.country_lbl') }} : {{ $bankdeposit->country->name }}</p>
            <p>{{ trans('forms.mobile_no_lbl') }} : {{ $bankdeposit->mobile }}</p>
          </div>
        @endif
      </div>
  </div>
</div>
