@extends('layouts.adminpanel') 
@section('content')
<section class="section">
	<div class="row">
        <div class="container">
            <h3>{{ trans('admin.earnings') }}</h3>   
                <div class="boxy boxy-white">
                    @include('adminpartials._earninglist')
                </div>    
        </div>
    </div>
</section>
@endsection