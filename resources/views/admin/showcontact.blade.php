@extends('layouts.adminpanel') 
@section('content')
    <div class="container">
        <div class="row">
            <h3>{{ trans('admin.contactform_submissions') }}</h3>   
        </div>
        <div class="row">
            <div class="boxy boxy-white">
                @include('adminpartials._contactlist')
            </div>   
        </div>
    </div>
@endsection
