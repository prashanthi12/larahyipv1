@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
    	<div class="container">
	    	<div class="col-md-12">
				<h3>Invite Contacts</h3>
			</div>
			<div class="boxy boxy-white">
                <div class="row">
                    <div class="col-md-12">
                        @include('layouts.message')
                    </div>
                </div> 
                <form class="invitecontacts" method="post" id="import-form" action="/admin/invitecontacts/contactsmap" enctype="multipart/form-data">
					{{ csrf_field() }}
	                <div class="p-10">
		                <div class="form-group" style="padding-left: 10px;">
		                	<label>Upload CSV File</label>						
							<div>														
								<input type="file" name="invitecontacts_csv" />
							</div>	
							<div style="padding-top: 10px;">
								<input type="submit" value="Next" class="btn btn-primary" onclick="this.disabled=true;this.form.submit();"> 
								<a href="" class="btn btn-default">Reset</a>
							</div>
		                </div>
	                </div>
                </form>
            </div>
        </div>
	</div>
</section>
@endsection