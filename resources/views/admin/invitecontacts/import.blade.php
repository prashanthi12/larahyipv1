@extends('layouts.adminpanel')
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <h3>Invite Contacts</h3>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.message')
                </div>
            </div> 
            <div class="boxy boxy-white">
                <form class="form-horizontal" method="POST" action="/admin/import_parse" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
                        <label for="csv_file" class="col-md-4 control-label">CSV file to import</label>
                        <div class="col-md-6">
                            <input id="csv_file" type="file" class="form-control" name="csv_file" required>
                            @if ($errors->has('csv_file'))
                                <span class="help-block">
                                <strong>{{ $errors->first('csv_file') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="header" checked> File contains header row?
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Parse CSV
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
