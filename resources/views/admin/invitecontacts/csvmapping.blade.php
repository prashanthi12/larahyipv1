@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
    	<div class="container">
	    	<div class="content">
	    		<h3>Contacts Mapping </h3>
	    	</div>
	    	<form class="contactsmap" method="post" id="import-form" action="/admin/invitecontacts/contactsmap/store">
				{{ csrf_field() }}
			<div class="boxy boxy-white">
                <div class="col-md-12">
                	<div class="col-md-6">
                    	<table class="table table-bordered table-striped">
    						<thead>
    							<tr>
    								<th>Imported File Headers</th>
    								<th>Contacts Attributes</th>
    							</tr>
    						</thead>
    						<tbody>
    						@foreach($header as $title)
    						<tr>	
    							<td>{{ $title }}</td>	
    							<td>
    								<select class="form-control show-menu-arrow selectpicker" name="contacts[]" required="required">
    									<option selected="selected">Please select</option> 
							    		<option value="1">firstname</option>
						    			<option value="2">lastname</option>
						    			<option value="3">email</option>	
									</select>
    							</td>	    							
    						</tr>
    						@endforeach
    						</tbody>
    					</table>
    					<div style="padding-top: 10px;">
							<input type="submit" value="Import" class="btn btn-primary" onclick="this.disabled=true;this.form.submit();"> 
							<a href="/admin/invitecontacts" class="btn btn-default">Cancel</a>
						</div>
					</div>
                </div>
            </div>
            </form>
		</div>
	</div>
</section>
@endsection