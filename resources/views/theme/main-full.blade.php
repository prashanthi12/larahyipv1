@include('theme.main-head')
<body class="bg-wave-full">
    <div id="app">
    <div class="site">
    <div class="site-content">
    <div class="row">
        @include('layouts.sitelinks')
            <div class="push-me-top">
            <div class="row-fluid">
                @yield('banner')
            </div>
        </div>
        </div>
        <div class="row">
                @yield('content')
        </div>
        </div>
        <div class="row">
        @include('theme.footer-mini')
        </div>
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    {!! Config::get('settings.google_analytics_code') !!}
    @stack('customscripts')
     <script src='https://www.google.com/recaptcha/api.js'></script>
     @stack('bottomscripts')
</body>
</html>
