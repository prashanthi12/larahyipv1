@include('theme.main-head')
<body>
    <div id="app">
    <div class="row">
      <div class="bg-wave">
        @include('layouts.sitelinks')
            <div class="push-me-top">
            <div class="row-fluid">
                @yield('banner')
            </div>
            </div>
        </div>
        </div>
        <div class="row">
                @yield('content')
        </div>
        <div class="row">
        @include('theme.footer')
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    {!! Config::get('settings.google_analytics_code') !!}
    @stack('customscripts')
     <script src='https://www.google.com/recaptcha/api.js'></script>
     @stack('bottomscripts')
</body>
</html>
