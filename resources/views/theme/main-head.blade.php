<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> InvestRange {{ Config::get('settings.sitetitle') }} </title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ Config::get('settings.favicon') }}" type="image/x-icon">
    <!-- Scripts -->
<script>
    window.Laravel = {!!json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
</script>
</head>