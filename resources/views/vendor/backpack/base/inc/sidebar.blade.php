@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>


          <!-- ======================================= -->
          <li><a href="{{ url('superadmin/members') }}"><i class="fa fa-group"></i> <span>Users</span></a></li>
          <!--   <li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Users, Roles, Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
      <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
      <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
    </ul>
  </li> -->
<li class="treeview">
<a href="#"><i class="fa fa-telegram"></i> <span>Manage</span> <i class="fa fa-angle-left pull-right"></i></a>
<ul class="treeview-menu">
<li><a href="{{ url('superadmin/plan') }}"><i class="fa fa-tag"></i> <span>Invest Plans</span></a></li>
<li><a href="{{ url('superadmin/referralgroups') }}"><i class="fa fa-tag"></i> <span>Referral Groups</span></a></li>
<li><a href="{{ url('superadmin/bonus') }}"><i class="fa fa-tag"></i> <span>Invest Bonus</span></a></li>
<li><a href="{{ url('superadmin/registrationbonus') }}"><i class="fa fa-tag"></i> <span>Registration Bonus</span></a></li>
<li><a href="{{ url('superadmin/paymentgateway/online') }}"><i class="fa fa-tag"></i> <span>Payment Gateways</span></a></li>
<li><a href="{{ url('superadmin/accountingcode') }}"><i class="fa fa-tag"></i> <span>Accounting Codes</span></a></li>
<li><a href="{{ url('superadmin/pages') }}"><i class="fa fa-tag"></i> <span>Pages</span></a></li>
<li><a href="{{ url('superadmin/news') }}"><i class="fa fa-tag"></i> <span>News</span></a></li>
<li><a href="{{ url('superadmin/exchanges') }}"><i class="fa fa-tag"></i> <span>Exchanges</span></a></li>
<li><a href="{{ url('superadmin/banners') }}"><i class="fa fa-tag"></i> <span>Banners</span></a></li>
<li><a href="{{ url('superadmin/sliders') }}"><i class="fa fa-tag"></i> <span>Sliders</span></a></li>
<li><a href="{{ url('superadmin/faqs') }}"><i class="fa fa-tag"></i> <span>FAQ</span></a></li>
<li><a href="{{ url('superadmin/ticketstatus') }}"><i class="fa fa-tag"></i> <span>Ticket Status</span></a></li>
<li><a href="{{ url('superadmin/ticketpriority') }}"><i class="fa fa-tag"></i> <span>Ticket Priority</span></a></li>
<li><a href="{{ url('superadmin/ticketcategory') }}"><i class="fa fa-tag"></i> <span>Ticket Category</span></a></li>
<li><a href="{{ url('superadmin/ticketcategoryuser') }}"><i class="fa fa-tag"></i> <span>Ticket Category User</span></a></li>
<li><a href="{{ url('superadmin/testimonials') }}"><i class="fa fa-tag"></i> <span>Testimonials</span></a></li>
<li><a href="{{ url('superadmin/quote') }}"><i class="fa fa-tag"></i> <span>Quotes</span></a></li>


</ul>
</li>
<li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/elfinder') }}"><i class="fa fa-files-o"></i> <span>File manager</span></a></li>
<li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/language') }}"><i class="fa fa-flag-o"></i> <span>Languages</span></a></li>
<li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/language/texts') }}"><i class="fa fa-language"></i> <span>Language Files</span></a></li>
{{-- <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/backup') }}"><i class="fa fa-hdd-o"></i> <span>Backups</span></a></li> --}}
<li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/log') }}"><i class="fa fa-terminal"></i> <span>Logs</span></a></li>
<li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/firewall') }}"><i class="fa fa-ban"></i> <span>Firewall</span></a></li>
<li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/setting') }}"><i class="fa fa-cog"></i> <span>Settings</span></a></li>

<li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/enveditor') }}"><i class="glyphicon glyphicon-pencil"></i> <span>ENV Editor</span></a></li>

<li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/helpdocument') }}"><i class="glyphicon glyphicon-pencil"></i> <span>Help Document</span></a></li>

        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
