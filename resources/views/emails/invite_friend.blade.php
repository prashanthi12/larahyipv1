@component('mail::message')

Hi,

<p> {{ $message }} </p>

<p>	@component('mail::button', ['url' => $url])
    {{ trans('mail.invite_friend_action_button_text') }}
	@endcomponent
</p>

{!! trans('mail.user_signature', array('name' => $sender)) !!}
@endcomponent

