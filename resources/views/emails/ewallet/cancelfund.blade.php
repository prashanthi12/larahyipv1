@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

<p>{{ trans('mail.ewallet_cancelfund_content') }}
   
</p>

<p>{{ trans('mail.ewallet_amount',['amount'=>$amount]) }} ( {{ config::get('settings.currency') }} )</p>
<p>{{ trans('mail.ewallet_send_btc_amount',['btc_amount'=>$btc_amount]) }}( {{ config::get('settings.donation_currency') }} )</p>
<p>{{ trans('mail.ewallet_txn_id',['txn_id'=>$txn_id]) }}</p>

{{ trans('mail.thanks_regards_text') }},<br>
{{ trans('mail.regarding_text')}}
  
@endcomponent
