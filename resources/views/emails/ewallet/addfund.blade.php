@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

<p>{{ trans('mail.ewallet_addfund_content') }}
   
</p>
<p>{{ trans('mail.ewalletfrom',['fromname'=>$fromname]) }}</p>
<p>{{ trans('mail.ewallet_amount',['amount'=>$amount]) }} ( {{ config::get('settings.currency') }} )</p>
<p>{{ trans('mail.ewallet_btc_amount',['btc_amount'=>$btc_amount]) }}( {{ config::get('settings.donation_currency') }} )</p>
{{ trans('mail.thanks_regards_text') }},<br>
{{ trans('mail.regarding_text')}}
  
@endcomponent
