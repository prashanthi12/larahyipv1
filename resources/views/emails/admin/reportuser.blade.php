@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

{{ trans('mail.report_user_content',['from'=>$withdrawuser,'user'=>$investuser]) }}.<br>

{!! $signature !!}
@endcomponent