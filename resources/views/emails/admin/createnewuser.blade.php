@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

<p>{{$message }}</p>
<p>{{$login_email_text }}</p>
<p>{{$login_password_text }}</p>
<p>
    @component('mail::button', ['url' => $actionUrl])
    {{ $actionText }}
    @endcomponent
</p>
{!! $signature !!}
  
@endcomponent



