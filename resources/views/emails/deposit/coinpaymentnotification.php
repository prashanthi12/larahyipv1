@component('mail::message')
 
{{ trans('mail.hi_text', ['name' => 'admin']) }},<br>

<p>{{ $coin_payment_content }} 
     @component('mail::button', ['url' => $new_deposit_link])
    {{ $deposit_link_text }}
    @endcomponent
 </p>

 {!! $signature !!}
 @endcomponent
