@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

<p>{{ trans('mail.coupon_code_content', array(
    'amount' => $amount, 'currency' => config::get('settings.currency') ) ) }} </p>

<p>{{ trans('mail.coupon_code') }} : {{ $couponcode }} </p>

{!! $signature !!}
@endcomponent
