@component('mail::message')

{{ trans('mail.hi_text', ['name' => $username]) }},<br>

<p>{{ trans('mail.content', array(
    'deposited_amount' => $deposited_amount, 'currency' => config::get('settings.currency') ) ) }} : </p>
<p>{{ trans('mail.bank_name') }} : {{ $bankname }} </p>
<p>{{ trans('mail.swift_code') }} : {{ $swiftcode }} </p>
<p>{{ trans('mail.account_no') }} : {{ $accountno }} </p>
<p>{{ trans('mail.account_name') }} : {{ $accountname }} </p>
<p>{{ trans('mail.account_address') }} : {{ $accountaddress }} </p>
<p>{{ trans('mail.country') }} : {{ $country }} </p>


{!! $signature !!}
@endcomponent
