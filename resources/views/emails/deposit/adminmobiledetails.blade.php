@component('mail::message')

{{ trans('mail.hi_text', ['name' => $username]) }},<br>

<p>{{ trans('mail.content', array(
    'deposited_amount' => $deposited_amount, 'currency' => config::get('settings.currency') ) ) }} : </p>
<p>{{ trans('mail.country') }} : {{ $country }} </p>
<p>{{ trans('mail.mobile') }} : {{ $mobile }} </p>


{!! $signature !!}
@endcomponent
