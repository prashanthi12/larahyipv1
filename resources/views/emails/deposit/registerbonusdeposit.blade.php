@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

{{ trans('mail.register_bonus_deposit_content', array(
    'amount' => $amount, 'currency' => config::get('settings.currency') ) ) }} <br>

{!! $signature !!}
@endcomponent
