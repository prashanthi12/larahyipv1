@component('mail::message')

{{ trans('mail.hi') }},<br>

<p>{{trans('mail.assign_bank_details') }}</p>
<p>{{ trans('mail.username') }} : {{ $username }} </p>
<p>{{ trans('mail.deposited_amount') }} : {{ $deposited_amount }} {{ \Config::get('settings.currency') }}</p>
<p>{{ trans('mail.plan_name') }} : {{ $plan_name }} </p>


{!! $signature !!}
@endcomponent
