@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

{{ trans('mail.otp_content', ['otpcode' => $otpcode]) }} <br>

{!! $signature !!}

@endcomponent