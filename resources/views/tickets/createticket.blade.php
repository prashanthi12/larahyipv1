@extends('layouts.myaccount') 

@section('content')
    <div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.create_new_ticket') }}</div>
    <div class="panel-body">       
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                           
                        @include('tickets.createticketform')
                            
            </div>
        </div>
    </div>
 </div>
@endsection
