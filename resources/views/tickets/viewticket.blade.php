@extends('layouts.myaccount') 

@section('content')
    <div class="panel panel-default">
    <div class="panel-heading">{{ $ticketdetails->subject.' '.trans('forms.details') 
    }}
    @if ($userprofile->usergroup_id == 3) 
    <p class="pull-right">
      <select class="form-control" id="priority" name="priority" 
      onchange="changeTicketstatus(this.value, {{ $ticketdetails->id }})">  
        @foreach ($ticketstatus as $status)
            <option value="{{ $status->id }}" {{ (Form::old("status") == $status->id ? "selected":"") }}>{{ $status->name }}</option>
        @endforeach
    </select>
    </p>
    @endif
    </div>
    <div class="panel-body">  
            @include('layouts.message')
        <div class="row">
            <div class="col-md-12">
                        <input type="hidden" name="baseurl" id="baseurl" value="{{url('/')}}">   
                        @include('tickets.ticketdetails')
                            
            </div>
        </div>
    </div>
 </div>
@endsection
<script type="text/javascript">
    
function changeTicketstatus(statusid, ticketid )
{ 
   var base_url = $('#baseurl').val();
   window.location.href = base_url + "/myaccount/ticket/update" + "/" + statusid + "/" + ticketid;
        
}
</script>