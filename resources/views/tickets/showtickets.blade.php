@extends('layouts.myaccount') 

@section('content')
    <div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.my_tickets') }}
    @if ($userprofile->usergroup_id == 4) 
    <a class="pull-right" href="{{ url('/myaccount/ticket/create') }}">{{ trans('forms.create') }}</a>
    @endif
    </div>
    <div class="panel-body">
        @include('layouts.message')
        <div class="row">
            <div class="col-md-12">
                           
                        @include('tickets.ticketlists')
                            
            </div>
        </div>
    </div>
 </div>
@endsection
