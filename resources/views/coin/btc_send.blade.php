@extends('layouts.myaccount')
@section('content')
<div class="panel panel-default">
    <div class="row">
        <div class="col-md-12">
       	 	@include('layouts.message')
        </div>
    </div>
    <div class="panel-heading">{{ trans('myaccount.send_btc_to_external_wallet') }}
        <a href="{{ url('myaccount/ewallet/history') }}" class="pull-right">{{ trans('myaccount.back_to_ewallet') }}</a>
    </div>
    <div class="panel-body">
    	<div class="row"> 
    		<div class="col-md-10 col-md-offset-1">
                <div id="coinsendform">                         
            		@include('coin.btc_send_form')
            	</div>
            </div>
        </div>
    </div>
</div>
@endsection

