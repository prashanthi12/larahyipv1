<div class="widget-grid">
@if($assigndepositsCount > 0)
  @foreach($assigndeposits as $assigndeposit)
    <div class="grid-item a">
      <p>
        <small>{{ trans('admin.depositby') }} : </small><br/>       
        @if (is_null($assigndeposit->user))
          {{ 'system' }}
        @else
          <a href="{{ url('admin/users/'.$assigndeposit->user->name) }}">{{ ucfirst($assigndeposit->user->name) }}</a>
                @endif
                <br/><small>{{ $assigndeposit->created_at->diffForHumans() }}</small></p>
            </div>
            <div class="grid-item b">
                <p class="text-right">{{ config::get('settings.currency') }} <strong>{{ $assigndeposit->amount }}</strong> </p>
            </div>
            <div class="grid-item c">
            
            <p><small>{{ trans('admin.depositplan') }} :</small><br/>{{ $assigndeposit->plan->name }}<br/>
            <small>{{ trans('admin.paymentvia') }} :</small><br/>{{ $assigndeposit->paymentgateway->displayname }}<br/>
            @if ($assigndeposit->paymentgateway->id == 9)
            <p>
                <a class='bitcoin' href="#" data-toggle='modal' data-target1='{{ $assigndeposit->transaction_id }}'>
                {{ trans('admin.viewhashdetails') }}
                </a></p>
   
                @if ($assigndeposit->present()->getBitcoinActualAmount($assigndeposit->transaction_id) != $assigndeposit->present()->getBitcoinReceivedAmount($assigndeposit->transaction_id))
                    <p><span class="label label-danger" >{{ trans('admin.problem') }}</span></p>
                @endif

            @else
            <small>{{ trans('admin.uniquetransactionid') }} :</small><br/>{{ $assigndeposit->present()->getTransactionNumber($assigndeposit->transaction_id) }}</p>
            @endif
            </div>
            <div class="grid-item d">
                <p class="form-group flex-button-group flex-vertical">
                    <a href="{{ url('admin/deposit/'.$assigndeposit->id.'/addbankdetails/'.$assigndeposit->user->id) }}" class="btn btn-success btn-xs flex-button">{{ trans('admin.set_details') }}</a>                                  
                </p>
            </div>
        @endforeach
@else
    <div class="">{{ trans('admin.no_depositapproval_found') }}</div>
@endif   
</div>
<div>
    @if($assigndepositsCount > 5)
      {{-- <p class="text-center"> <a href="{{ url('admin/actions/assigndeposit') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_pendingdeposits') }}</a></p> --}}

      <p class="text-center"> <a href="{{ url('admin/deposit/new') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_pendingdeposits') }}</a></p>
    @endif
</div>

