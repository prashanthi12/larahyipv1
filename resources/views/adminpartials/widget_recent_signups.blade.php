<div class="widget-grid">
@if($signedupusersCount > 0)
  @foreach($signedupuserslists as $data)
    @php 
      $properties['ip']='';
      if ( isset($data['loguser'][0]))
      {
        $properties = json_decode($data['loguser'][0]['properties'], true); 
      }
      else if(count($data['loguser']) > 0)
      {
        $properties = json_decode($data['loguser']['properties'], true); 
      }
      
      if ( isset($properties['ip']))
      {
          $ipAddress = $properties['ip'];
      }
    @endphp
    <div class="grid-item a">
      <p>
        <small>{{ trans('admin.username') }} : </small><br/>       
          <a href="{{ url('admin/users/'.$data->name) }}">{{ $data->name }}</a><br>
          {{ $data->created_at->diffForHumans() }}
      </p>
    </div>
    <div class="grid-item b">
      <p>
        <small>{{ trans('admin.email') }} : </small><br/>       
          {{ $data->email }}
      </p>     
    </div>
    <div class="grid-item c">           
      <p>
        <small>{{ trans('admin.ipaddress') }} : </small><br/>       
          {{ $ipAddress }}<br/>
      </p>  
    </div>
    <div class="grid-item d">           
      <p>
        <small>{{ trans('admin.country') }} : </small><br/> 
        {{ $data->present()->getSignupCountry('157.2.1.5') }}     
      </p>  
    </div>
  @endforeach
@endif   
</div>
<div>
    @if($signedupusersCount > 5)
       <p class="text-center"> <a href="{{ url('admin/users') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_recentsignups') }}</a></p>
    @endif
</div>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

