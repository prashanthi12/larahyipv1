<div class="tab-container">
@include('adminpartials._stats_user_earnings')
<h3>{{ trans('admin.earningslist') }}</h3>
<table class="table table-bordered table-striped dataTable"  id="earingsdatatable">
<thead>
    <tr>
        <th>#</th>
        <th>{{ trans('admin.amount') }}</th>
        <th>{{ trans('admin.type') }}</th>
        <th>{{ trans('admin.comments') }}</th>
        <th>{{ trans('admin.createdon') }}</th>
    </tr>
</thead>
<tbody>
    @foreach($earnings as $earning)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td class="is-amount text-right">{{ $earning->amount }} {{ 
            config::get('settings.currency') }}</td>
            <td>{{  ucwords(str_replace("-", " ", $earning->accountingcode->accounting_code)) }} </td>
            <td>{!! $earning->comment !!} </td>
            <td>{{ $earning->created_at->format('d/m/Y H:i:s') }}</td>
        </tr>
    @endforeach
    
</tbody>
</table>
</div>
@push('scripts')
<script>
    $(document).ready(function(){
        $('#earingsdatatable').DataTable();
    });
</script>
@endpush