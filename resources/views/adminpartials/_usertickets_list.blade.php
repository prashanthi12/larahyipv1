<div class="tab-container">
<h3>{{ trans('admin.ticketslist') }}</h3>
<table class="table table-bordered table-striped dataTable"  id="logdatatable">
<thead>
    <tr>
        <th>{{ trans('admin.subject') }}</th> 
        <th>{{ trans('admin.status') }}</th>
        <th>{{ trans('admin.agent') }}</th>               
        <th>{{ trans('admin.lastupdated') }}</th>      
    </tr>
</thead>
<tbody>
@foreach($user->ticketuser as $data)
<?php
$properties = json_decode($data['properties'], true);
?> 
    <tr>
        <td><a href="{{ url('admin/ticket/'.$data['id']) }}">{{ $data['subject'] }}</a></td>   
        <td>{{ $data->status->name }}</td>    
        <td>{{ $data->agent->name }}</td>
        <td>{{ $data->updated_at->diffForHumans() }}</td>         
    </tr>
 @endforeach
</tbody>
</table>
</div>

@push('scripts')
<script>
    $(document).ready(function(){
        $('#logdatatable').DataTable();
    });
</script>
@endpush