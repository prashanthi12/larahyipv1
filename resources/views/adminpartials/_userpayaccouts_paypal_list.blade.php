@foreach($paypal_result as $data)
<dl class="bankaccount-info dl-horizontal">  
    <p>
        {{ $data['param1'] }}
        @if($data['current'] == 1) 
        	<span class="label label-success">{{ trans('admin.primary') }}</span>
        @endif
    </p>
</dl>
@endforeach