<div class="tab-container">
<table class="table table-bordered" id="referralsdatatable">
<thead>
	<tr>
		<th>#</th>
		<th>{{ trans('myaccount.name') }}</th>
		<th>{{ trans('myaccount.dateofjoin') }} </th>
		<th>{{ trans('myaccount.noofdeposit') }}</th>
		<th>{{ trans('myaccount.totaldepositamount') }} ({{ config::get('settings.currency') }})</th>
	</tr>
</thead>
<tbody>
	@if (count($myreferrals) > 0)
	@foreach($myreferrals as $myreferral)
		<tr>
			<td>{{ $loop->iteration }}</td>
			<td>{{ $myreferral[0] }} </td>
			<td>{{ $myreferral[1]->format('d/m/Y H:i:s') }}</td>
			<td>{{ $myreferral[2] }}</td>
			<td>{{ $myreferral[3] }} </td>
		</tr>
	@endforeach
	@else
	<tr><td colspan="5">{{ trans('myaccount.noreferralsfound') }}</td></tr>
	@endif
</tbody>
</table>
</div>
@push('scripts')
<script>
    $(document).ready(function(){
        $('#referralsdatatable').DataTable();
    });
</script>
@endpush