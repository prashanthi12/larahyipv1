<table class="table table-bordered table-striped dataTable" id="reportdepositdatatable">
    <thead>
         <tr>
            <th>{{ trans('admin.id') }}</th>
            <th>{{ trans('admin.username') }}</th>
            <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
            <th>{{ trans('admin.depositedon') }}</th>
            <th>{{ trans('admin.plan') }}</th>
            <th>{{ trans('admin.paymentmethod') }}</th>
            <th>{{ trans('admin.paymentdetails') }}</th>
            <th>{{ trans('admin.status') }}</th>
            <th>{{ trans('admin.reporton') }} / {{ trans('admin.rejecton') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($reportlist as $reportdeposit)
        <tr>
            <td>#{{ $loop->iteration  }}</td>
            <td>       
                @if (is_null($reportdeposit->user))
                {{ 'system' }}
                @else
                 <a href="{{ url('admin/users/'.$reportdeposit->user->name) }}">{{ $reportdeposit->user->name }}</a>
                @endif
            </td>
            <td class="text-right">{{ $reportdeposit->deposit->amount }} </td>
            <td>{{ $reportdeposit->created_at->format('d/m/Y H:i:s') }}</td>
            <td>{{ $reportdeposit->deposit->plan->name }}</td>
            <td>{{ $reportdeposit->deposit->paymentgateway->displayname }} <br>
              
            </td>
                
            <td width="18%">
                <div class="form-group flex-button-group">  
                  

                  
                        <a class='bankdetails btn btn-success btn-xs flex-button' href="#" data-toggle='modal' data-target='{{ $reportdeposit->id }}'>{{ trans('admin.viewdetails') }}</a> 
                     
            

                    
                </div>
            </td>
            <td>{{ucfirst($reportdeposit->type)}}</td>
            <td>
            @if($reportdeposit->type=='report')
            {{ $reportdeposit->report_at->format('d/m/Y H:i:s') }}
            @else
            {{ $reportdeposit->reject_at->format('d/m/Y H:i:s') }}
            @endif

            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $reportlist->links() }}

<div class="modal fade" id="bankdetails-modals" role="dialog"></div>

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {    
   
   $('.bankdetails').on('click', function () {
    var $this = $(this).data('target');
      url = "{{ url('admin/viewbankdetails/') }}";
        $('#bankdetails-modals').load(url + '/' + $this, function (response, status, xhr) {
            if (status == "success") {
                $(response).modal('show');
            }
        });         
  }); 

});
</script>
@endpush
