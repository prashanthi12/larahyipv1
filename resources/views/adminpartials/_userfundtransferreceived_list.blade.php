<div class="tab-container">
    <h3>{{ trans('admin.fundtransfer_receivedlist') }}</h3>
    <table class="table table-bordered" id="fundtransferreceivedatatable">
        <thead>
            <tr>
                <th>{{ trans('admin.sender') }}</th>
                <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th> 
                <th>{{ trans('admin.senddate') }}</th>                
            </tr>
        </thead>
        <tbody>
            @if (count($receivetransferlists) > 0)
                @foreach($receivetransferlists as $data)
                <tr>
                    <td>{{ $data->present()->getUsername($data->fundtransfer_from_id->user_id) }}</td>
                    <td>{{ $data->amount }}</td> 
                    <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>                       
                </tr>
                 @endforeach
            @else
                <tr>               
                    <td colspan="6">{{ trans('forms.no_fundtransfer_found') }}</td>
                </tr>
            @endif
        </tbody>
    </table>
 </div>

@push('scripts')
<script>
    $(document).ready(function(){
        $('#fundtransferreceivedatatable').DataTable();

    });
</script>
@endpush