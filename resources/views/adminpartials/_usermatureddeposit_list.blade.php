<div class="tab-container">
@include('adminpartials._stats_user_deposit_detail')
<h3>{{ trans('admin.matureddepositlist') }}</h3>
<table class="table table-bordered table-striped dataTable"  id="matureddepositdatatable">
<thead>
     <tr>
        <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
        <th>{{ trans('admin.interest') }} ({{ config::get('settings.currency') }})</th>
        <th>{{ trans('admin.depositedon') }}</th>
        <th>{{ trans('admin.maturedon') }}</th>
        <th>{{ trans('admin.comments') }}</th>
        <th>{{ trans('admin.plan') }}</th>
        <th>{{ trans('admin.paymentmethod') }}</th>
        <th>{{ trans('admin.bitcoindetails') }}</th>
        <th>{{ trans('admin.action') }}</th>
    </tr>
</thead>
<tbody>
 @foreach($matureddeposits as $matureddeposit)
    <tr>
        <td>{{ $matureddeposit->amount }}</td>
        <td>{{ $matureddeposit->interest->pluck('amount')->sum() }} </td>
        <td>{{ $matureddeposit->created_at->format('d/m/Y H:i:s') }}</td>
        <td>{{ $matureddeposit->matured_on->format('d/m/Y H:i:s') }}</td>
        <td>{{ $matureddeposit->comments_on_maturity }}</td>
        <td>{{ $matureddeposit->plan->name }}</td>
        <td>{{ $matureddeposit->paymentgateway->displayname }}</td>
         @if ($matureddeposit->paymentgateway->id == 9)
             <td><a class='maturedeposit-bitcoin' href="#" data-toggle='modal' data-target1='{{ $matureddeposit->transaction_id }}'>{{ trans('admin.viewdetails') }}</a></td>
        @else
             <td>-</td>
        @endif
       <td>
            <form method="post" class="release" action="{{ url('admin/releasedeposit') }}">
            {{ csrf_field() }} 
            <input type="hidden" name="depositid" value="{{ $matureddeposit->id }}">
            {!! Form::submit('Released' , ['class' => 'btn btn-success btn-sm flex-button']) !!}
            </form>
        </td>
    </tr>
    @endforeach
</tbody>
</table>
</div>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>
@push('scripts')
<script>
 $('.maturedeposit-bitcoin').on('click', function () {
            var $this = $(this).data('target1');

            $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {

                if (status == "success") {
                    $(response).modal('show');
                }
            });
        });
$(document).ready(function(){
        $('#matureddepositdatatable').DataTable();
});
$(".release").on("submit", function(){
        return confirm("Do you want to release this deposit?");
    });
</script>
@endpush