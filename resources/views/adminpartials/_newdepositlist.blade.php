<table class="table table-bordered table-striped dataTable" id="newdepositdatatable">
    <thead>
         <tr>
            <th>{{ trans('admin.id') }}</th>
            <th>{{ trans('admin.username') }}</th>
            <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
            <th>{{ trans('admin.depositedon') }}</th>
            <th>{{ trans('admin.plan') }}</th>
            <th>{{ trans('admin.paymentmethod') }}</th>
            <th>{{ trans('admin.paymentdetails') }}</th>
            <th>{{ trans('admin.status') }}</th>
            <th>{{ trans('admin.actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($depositlists as $newdeposit)
        <tr>
            <td>#{{ $loop->iteration  }}</td>
            <td>       
                @if (is_null($newdeposit->user))
                {{ 'system' }}
                @else
                 <a href="{{ url('admin/users/'.$newdeposit->user->name) }}">{{ $newdeposit->user->name }}</a>
                @endif
            </td>
            <td class="text-right">{{ $newdeposit->amount }} </td>
            <td>{{ $newdeposit->created_at->format('d/m/Y H:i:s') }}</td>
            <td>{{ $newdeposit->plan->name }}</td>
            <td>{{ $newdeposit->paymentgateway->displayname }} <br>
              @if($newdeposit->paymentgateway->id == 20)

              @endif
            </td>
            @if ($newdeposit->paymentgateway->id == 9)
            <td>
                <a class='bitcoin' href="#" data-toggle='modal' data-target1='{{ $newdeposit->transaction_id }}'>
                {{ trans('admin.viewdetails') }}
                @if ($newdeposit->present()->getBitcoinActualAmount($newdeposit->transaction_id) != $newdeposit->present()->getBitcoinReceivedAmount($newdeposit->transaction_id))
                    - {{ trans('admin.problem') }}
                @endif
                </a>
            </td>
            @else
            <td>{{ $newdeposit->present()->getTransactionNumber($newdeposit->transaction_id) }}</td>
            @endif
            <td>
              
               @if (count($newdeposit->manualdeposit) > 0 )
               
                 @if (count($newdeposit->manualdeposit->withdraw) > 0 )
                    <p>{{trans('admin.depositassigned')}}

                   {{$newdeposit->manualdeposit->withdraw->user->name}}<br>
                 
                   {{$newdeposit->manualdeposit->created_at->format('d-m-Y H:i:s')}}</p>
                   @else
                      <p>{{trans('admin.admindepositassigned')}}</p>
                   @endif
                  @else
                  <p>{{trans('admin.admindeposit_unassigned')}}</p>

               @endif

            </td>
            <td width="18%">
                <div class="form-group flex-button-group">  
                    @if (($newdeposit->paymentgateway_id == 20) && (count($newdeposit->manualdeposit)) <= 0)
                        <a href="{{ url('admin/deposit/'.$newdeposit->id.'/addbankdetails/'.$newdeposit->user->id) }}" class="btn btn-success btn-xs flex-button">{{ trans('admin.set_details') }}</a>                               
                    @elseif (($newdeposit->paymentgateway_id == 1) || ($newdeposit->manualdeposit->proof !=''))
                     {{--   <a href="#" class="btn btn-success btn-xs flex-button confirm" rel="{{ url('admin/deposit/confirm/'.$newdeposit->id) }}">{{ trans('admin.confirm') }}</a>      --}}   
                    @elseif ($newdeposit->paymentgateway_id != 20)
                      {{--  <a href="#" class="btn btn-success btn-xs flex-button onlineconfirm" rel="{{ url('admin/deposit/approve/'.$newdeposit->id) }}">{{ trans('admin.confirm') }}</a> --}}
                        <!-- <form class="activate" action="{{ url('admin/deposit/approve/'.$newdeposit->id) }}" method="post">
                        {{ csrf_field() }} 
                            {!! Form::submit("Confirm", ['class' => 'btn btn-success btn-xs flex-button']) !!}
                        </form> -->
                    @endif

                    @if (count($newdeposit->manualdeposit) > 0)
                        <a class='bankdetails btn btn-success btn-xs flex-button' href="#" data-toggle='modal' data-target='{{ $newdeposit->manualdeposit->id }}'>{{ trans('admin.viewdetails') }}</a> 
                        
                    @endif

                    @if (count($newdeposit->manualdeposit) > 0 && ($newdeposit->manualdeposit->proof !=''))
                        <a class='btn btn-success btn-xs flex-button' href="{{ $newdeposit->manualdeposit->proof }}" download>{{ trans('admin.viewproof') }}</a> 
                    @endif
                    @if(count($newdeposit->manualdeposit) > 0 && ($newdeposit->manualdeposit->deposit_type =='new'))
                      {{--  <a href="#" class="btn btn-danger btn-xs flex-button reject" rel="{{ url('admin/deposit/reject/'.$newdeposit->id.'') }}" >{{ trans('admin.reject') }}</a> --}}
                    @elseif($newdeposit->paymentgateway_id != 20)
                       {{-- <a href="#" class="btn btn-danger btn-xs flex-button reject" rel="{{ url('admin/deposit/reject/'.$newdeposit->id.'') }}" >{{ trans('admin.reject') }}</a> --}}
                    @endif
                    <a href="#" class="btn btn-success btn-xs flex-button confirm" rel="{{ url('admin/deposit/confirm/'.$newdeposit->id) }}">{{ trans('admin.confirm') }}</a> 
                    <a href="#" class="btn btn-danger btn-xs flex-button reject" rel="{{ url('admin/deposit/reject/'.$newdeposit->id.'') }}" >{{ trans('admin.reject') }}</a>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $depositlists->links() }}
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>
<div class="modal fade" id="bankdetails-modals" role="dialog"></div>

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {    
   // $('#newdepositdatatable').DataTable();

    // $(".activate").on("submit", function(){
    //     return confirm("Do you want to activate this deposit.?");
    // });  

    $('.confirm').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to approve this deposit ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 

    $('.onlineconfirm').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to approve this deposit ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 

    $('.reject').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to reject this deposit ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    });     

 $('.bitcoin').on('click', function () {
    var $this = $(this).data('target1');
        $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {
            if (status == "success") {
                $(response).modal('show');
            }
        });
    }); 
 
  // For Manual Deposit Added Bank Details 
  $('.bankdetails').on('click', function () {
    var $this = $(this).data('target');
      url = "{{ url('admin/viewbankdetails/') }}";
        $('#bankdetails-modals').load(url + '/' + $this, function (response, status, xhr) {
            if (status == "success") {
                $(response).modal('show');
            }
        });         
  }); 

});
</script>
@endpush
