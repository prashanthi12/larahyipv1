<div class="mt-20 mb-20">
    <table class="table table-bordered table-striped dataTable"  id="funddatatable">
        <thead>
            <tr>        
                <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
                <th>{{ trans('admin.sender') }}</th>
                <th>{{ trans('admin.receiver') }}</th>
                <th>{{ trans('admin.transferdate') }}</th>      
            </tr>
        </thead>
        <tbody>
            @foreach($fundtransfer as $data)
            <tr>
                <td>{{ $data->amount }} {{ config::get('settings.currency') }}</td>
                <td>{{ $data->present()->getUsername($data->fundtransfer_from_id->user_id) }}</td>
                <td>{{ $data->present()->getUsername($data->fundtransfer_to_id->user_id) }}</td>
                <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>       
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@push('scripts')
<script>
    $(document).ready(function(){
        $('#funddatatable').DataTable();
    });
</script>
@endpush