<div class="tab-container">
<table class="table table-bordered" id="bonusdatatable">
<thead>
    <tr>
        <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
        <th>{{ trans('admin.comments') }}</th>
        <th>{{ trans('admin.date') }}</th>
    </tr>
</thead>
<tbody>
    @foreach($bonuses as $bonus)
        <tr>
            <td>{{ $bonus->amount }} </td>
            <td>
                <span data-html="true" data-toggle="comment-popover"  data-content="{{ $bonus->comments }}">
                {{  substr($bonus->comments, 0, 15) }}...</span>
            </td>
            <td>{{ $bonus->created_at->format('d/m/Y H:i:s') }}</td>
            
        </tr>
    @endforeach
   
</tbody>
</table>
</div>
@push('scripts')
<script>
    $(document).ready(function(){
        $('#bonusdatatable').DataTable();
    });

    $('[data-toggle="comment-popover"]').popover({
        placement : 'left',
        trigger : 'hover'
    });
</script>
@endpush