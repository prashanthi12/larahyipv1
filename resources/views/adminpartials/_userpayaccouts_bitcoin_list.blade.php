@foreach($bitcoin_result as $data)
<dl class="bankaccount-info dl-horizontal">   
    <dt>{{ trans('admin.walletname') }}</dt>
    <dd>
    {{ $data['param1'] }}
     @if($data['current'] == 1) 
    <span class="label label-success">{{ trans('admin.primary') }}</span>
    @endif
    </dd>
    <dt>{{ trans('admin.address') }}</dt>
    <dd>{{ $data['param2'] }}</dd>
</dl>
@endforeach
