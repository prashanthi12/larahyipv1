<div id='bitcoin' class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="margin-top: 50px;">
  <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ trans('admin.bitcoindetails') }}</h4>
        </div>
        <div class="modal-body">

        <p>{{ trans('admin.hashkey') }} : <a href="https://blockexplorer.com/tx/{{ $txnhashkey }}" target="_blank">{{ $txnhashkey }}</a></p>
        <p>{{ trans('admin.confirmations') }} : {{ $confirmations }}</p>
        <p>{{ trans('admin.actual_deposited_amount') }} : {{ $actual_deposited_amount }} BTC</p>
        <p>{{ trans('admin.deposited_amount') }} : {{ $received_amount }} BTC</p>
        <p>{{ trans('admin.bitcoin_transaction_time') }} : {{ $bitcoin_transaction_time }}</p>
        @if ($actual_deposited_amount != $received_amount)
        <p>{{ trans('admin.problemon') }} :<span class="label label-danger"> {{ trans('admin.differenceamounttext') }}</span></p>
        @endif
        </div>

      </div>
  </div>
</div>
