<div class="mt-20 mb-20">
<table class="table table-bordered table-striped table-resposnive dataTable"  id="contactdatatable">
<thead>
    <tr>
        <th>#</th>
        <th>{{ trans('admin.sendername') }}</th>
        <th>{{ trans('admin.email') }}</th>
        <th>{{ trans('admin.contactnumber') }}</th>
        <th>{{ trans('admin.skype/gtalk') }}</th>
        <th>{{ trans('admin.queries') }}</th>
        <th>{{ trans('admin.date') }}</th>
    </tr>
</thead>
<tbody>
@foreach($contact as $data)
    <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $data->fullname }}</td>
        <td>{{ $data->email }}</td>
        <td>{{ $data->contactno }}</td>
        <td>{{ $data->skype_gtalk }}</td>
        <td>
        {{ rawurldecode(substr($data->queries, 0, 100)) }} ...
        <a href="/admin/contactus/moreinfo/{{ $data->id }}">More Info</a>
        </td>       
        <td>{{ $data->created_at->format('d/m/Y') }}</td>
    </tr>
 @endforeach
</tbody>
</table>
</div>

@push('scripts')
<script>
    $(document).ready(function(){
        $('#contactdatatable').DataTable();
    });
</script>
@endpush