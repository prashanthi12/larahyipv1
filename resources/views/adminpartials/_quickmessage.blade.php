<div class="widget-inner paddingMessage">
	@if (session('success'))
        <div class="alert alert-success messageHide" id="messageDisplay">
         	<a href="{{ url('admin/message/conversation/'.$conversations['id']) }}">{{ session('success') }}</a>
        </div>
    @endif
    <form method="post" action="{{ url('admin/quickmessage') }}" class="form-horizontal" id="contact">
	{{ csrf_field() }}
		<div class="form-group">
			<div id="messageDisplay"></div>
		</div> 
	    <!-- <div class="form-group{{ $errors->has('users') ? ' has-error' : '' }}">
	        <select class="progControlSelect2 form-control js-example-basic-single"  name="users" required="required">
		        @foreach ($userlist as $user)
		        <option value="{{ $user->id }}" >{{ $user->name }}</option>
		        @endforeach
	      	</select>
	        <small class="text-danger">{{ $errors->first('users') }}</small>
	    </div>  -->
	    <div class="form-group{{ $errors->has('users') ? ' has-error' : '' }}">
       		<input type="text" name="users" id="users" class='' value="{{ old('users') }}">
        	<small class="text-danger">{{ $errors->first('users') }}</small>
        </div>

	    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
	        <textarea  rows="5" class="form-control"  id="message" placeholder="Enter your message here" name="message" required="required">{{ old('message') }}</textarea>
	        <small class="text-danger">{{ $errors->first('message') }}</small>
	    </div>  

		<div class="form-group">        
	        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-primary" type="submit" onclick="this.disabled=true;this.form.submit();"> 
	        <a href="" class='btn btn-default'>{{ trans('forms.reset') }}</a>
	    </div>
	</form>
</div>

@push('scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {	
	$('.js-example-basic-single').select2();

	$("#messageDisplay").delay(10000).fadeOut("slow");
	$("#messageDisplay").click(function(){
        $("#messageDisplay").delay(1000).fadeOut("slow");
    });			
});
</script>
@endpush

@push('scripts')
<script src="http://cdn.jsdelivr.net/typeahead.js/0.9.3/typeahead.min.js"></script>

<script>
$(document).ready(function() {
    $("#users").typeahead({
        name : 'sear',
        remote: {
            url : '/admin/searchuser?query=%QUERY'
        }
    });
});
</script>
@endpush