<div class="tab-container">
@include('adminpartials._stats_user_deposit_detail')
<h3>{{ trans('admin.rejecteddepositlist') }}</h3>
<table class="table table-bordered table-striped dataTable"  id="rejectdepositdatatable">
<thead>
     <tr>
        <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
        <th>{{ trans('admin.depositedon') }}</th>
        <th>{{ trans('admin.rejectedon') }}</th>
        <th>{{ trans('admin.comments') }}</th>
        <th>{{ trans('admin.plan') }}</th>
        <th>{{ trans('admin.paymentmethod') }}</th>
        <th>{{ trans('admin.bitcoindetails') }}</th>
    </tr>
</thead>
<tbody>
 @foreach($rejecteddeposits as $rejecteddeposit)
    <tr>
        <td>{{ $rejecteddeposit->amount }}</td>
        <td>{{ $rejecteddeposit->created_at->format('d/m/Y H:i:s') }}</td>
        <td>{{ $rejecteddeposit->rejected_on->format('d/m/Y H:i:s') }}</td>
        <td>{{ $rejecteddeposit->comments_on_reject }}</td>
        <td>{{ $rejecteddeposit->plan->name }}</td>
        <td>{{ $rejecteddeposit->paymentgateway->displayname }}</td>
        @if ($rejecteddeposit->paymentgateway->id == 9)
             <td><a class='rejectdeposit-bitcoin' href="#" data-toggle='modal' data-target1='{{ $rejecteddeposit->transaction_id }}'>{{ trans('admin.viewdetails') }}</a></td>
        @else
             <td>-</td>
        @endif
       
    </tr>
    @endforeach
</tbody>
</table>
</div>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>
@push('scripts')
<script>
$('.rejectdeposit-bitcoin').on('click', function () {
            var $this = $(this).data('target1');

            $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {

                if (status == "success") {
                    $(response).modal('show');
                }
            });
        });
$(document).ready(function(){
        $('#rejectdepositdatatable').DataTable();
});
</script>
@endpush