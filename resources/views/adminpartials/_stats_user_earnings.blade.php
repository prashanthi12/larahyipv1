<h4>{{ trans('admin.earningsdetails') }}</h4>
   <div class="col-md-12 well">
    <div class="col-md-3">
        <p>{{ trans('admin.earningsasinterest') }}</p>
        <h3 class="is-amount">{{ $totalInterest }} <small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    <div class="col-md-2">
        <p>{{ trans('admin.referralcommission') }}</p>
        <h3 class="is-amount">{{ $totalReferralCommission }} <small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    <div class="col-md-2">
        <p>{{ trans('admin.levelcommission') }}</p>
        <h3 class="is-amount">{{ $totalLevelCommission }} <small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    <div class="col-md-2">
        <p>{{ trans('admin.bonus') }}</p>
        <h3 class="is-amount">{{ $totalBonus }} <small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    <div class="col-md-3">
        <p>{{ trans('admin.lifetimeearnings') }}</p>
        <h3 class="is-amount">{{ $totalLifeTimeEarnings }} <small>{{ config::get('settings.currency') }}</small></h3>
    </div>
</div>