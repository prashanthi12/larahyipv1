<div class="tab-container">
<table class="table table-bordered" id="penaltydatatable">
<thead>
    <tr>
        <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
        <th>{{ trans('admin.comments') }}</th>
        <th>{{ trans('admin.date') }}</th>
    </tr>
</thead>
<tbody>
    @foreach($penalties as $penalty)
        <tr>
            <td>{{ $penalty->amount }} </td>
            <td>
                <span data-html="true" data-toggle="comment-popover"  data-content="{{ $penalty->comments }}">
                {{  substr($penalty->comments, 0, 15) }}...</span>
            </td>
            <td>{{ $penalty->created_at->format('d/m/Y H:i:s') }}</td>
            
        </tr>
    @endforeach
   
</tbody>
</table>
</div>
@push('scripts')
<script>
    $(document).ready(function(){
        $('#penaltydatatable').DataTable();
    });

    $('[data-toggle="comment-popover"]').popover({
        placement : 'left',
        trigger : 'hover'
    });
</script>
@endpush