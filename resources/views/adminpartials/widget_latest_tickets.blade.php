<div class="widget-grid">
@if(count($ticketlist) > 0)
  @foreach($ticketlist as $data)
    <div class="grid-item a">
      <p>
        <small>{{ trans('admin.from') }} : </small><br/>       
          <a href="{{ url('admin/users/'.$data->user->name) }}">{{ $data->user->name }}</a>
      </p>
    </div>
    <div class="grid-item b">
      <p>
        <small>{{ trans('admin.subject') }} : </small><br/>       
          <a href="{{ url('admin/ticket/'.$data['id']) }}">{{ $data['subject'] }}</a>
      </p>     
    </div>
    <div class="grid-item c">           
      <p>
        <small>{{ trans('admin.agent') }} : </small><br/>  
          <a href="{{ url('admin/staffs') }}">{{ $data->agent->name }}</a>
      </p>  
    </div>
    <div class="grid-item c">           
      <p>
        <small>{{ trans('admin.date') }} : </small><br/>       
          {{ $data->created_at }}
      </p>  
    </div>
  @endforeach
  @else
    <div class="">{{ trans('admin.no_fundtransfer_found') }}</div>
@endif   
</div>
<div>
    @if(count($ticketlist) >= 5)
       <p class="text-center"> <a href="{{ url('admin/ticket') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_tickets') }}</a></p>
    @endif
</div>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

