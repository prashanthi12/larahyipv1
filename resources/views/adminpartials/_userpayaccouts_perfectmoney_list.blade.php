@foreach($pm_result as $data)
<dl class="bankaccount-info dl-horizontal">    
    <dt>{{ trans('admin.payeeaccount') }}</dt>
    <dd>
    {{ $data['param1'] }}
     @if($data['current'] == 1) 
    <span class="label label-success">{{ trans('admin.primary') }}</span>
    @endif
    </dd>
    
</dl>
@endforeach