<div class="container">
    <div class="boxy boxy-white p-20">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1default" data-toggle="tab">{{ trans('admin.userprofile') }}</a></li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown">{{ trans('admin.deposits') }} <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#tab21default" data-toggle="tab">{{ trans('admin.new') }}</a></li>
                    <li><a href="#tab22default" data-toggle="tab">{{ trans('admin.active') }}</a></li>
                    <li><a href="#tab23default" data-toggle="tab">{{ trans('admin.matured') }}</a></li>
                    <li><a href="#tab24default" data-toggle="tab">{{ trans('admin.released') }}</a></li>
                    <li><a href="#tab25default" data-toggle="tab">{{ trans('admin.rejected') }}</a></li>
                    <li><a href="#tab26default" data-toggle="tab">{{ trans('admin.archived') }}</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown">{{ trans('admin.withdraws') }} <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#tab34default" data-toggle="tab">{{ trans('admin.requestcancelled') }}</a></li>
                    <li><a href="#tab31default" data-toggle="tab">{{ trans('admin.pending') }}</a></li>
                    <li><a href="#tab32default" data-toggle="tab">{{ trans('admin.completed') }}</a></li>
                    <li><a href="#tab33default" data-toggle="tab">{{ trans('admin.rejected') }}</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown">{{ trans('admin.fundtransfers') }} <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#tab81default" data-toggle="tab">{{ trans('admin.send') }}</a></li>
                    <li><a href="#tab82default" data-toggle="tab">{{ trans('admin.received') }}</a></li>
                </ul>
            </li>
            <li><a href="#tab11default" data-toggle="tab">{{ trans('admin.bonus') }}</a></li>
            <li><a href="#tab10default" data-toggle="tab">{{ trans('admin.penalty') }}</a></li>
            <li><a href="#tab9default" data-toggle="tab">{{ trans('admin.referrals') }}</a></li>
            <li><a href="#tab4default" data-toggle="tab">{{ trans('admin.earnings') }}</a></li>
            <li><a href="#tab5default" data-toggle="tab">{{ trans('admin.tickets') }}</a></li>
            <li><a href="#tab6default" data-toggle="tab">{{ trans('admin.activitylogs') }}</a></li>
            <li><a href="#tab7default" data-toggle="tab">{{ trans('admin.loginhistory') }}</a></li>
            <li><a href="#tab12default" data-toggle="tab">{{ trans('admin.messages') }}</a></li>
            <li><a href="#tab13default" data-toggle="tab">{{ trans('admin.myoutbox') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="tab1default">
                @include('adminpartials._userdetail_profile_tab')
            </div>
            <div class="tab-pane fade" id="tab2default">
            @include('adminpartials._userdeposit_list')
            </div>
            <div class="tab-pane fade" id="tab3default">{{ trans('admin.withdraws') }}</div>
            <div class="tab-pane fade" id="tab4default">                       
                @include('adminpartials._userearnings_list')
            </div>
            <div class="tab-pane fade" id="tab5default">                       
                @include('adminpartials._usertickets_list')
            </div>
            <div class="tab-pane fade" id="tab7default">                       
                @include('adminpartials._userloginhistory_list')
            </div>
            <div class="tab-pane fade" id="tab6default">                       
                @include('adminpartials._useractivitylog_list')
            </div>
            <div class="tab-pane fade" id="tab31default">                      
                @include('adminpartials._userwithdrawpending_list')
            </div>
            <div class="tab-pane fade" id="tab32default">                      
                @include('adminpartials._userwithdrawcompleted_list')
            </div>
            <div class="tab-pane fade" id="tab33default">                      
                @include('adminpartials._userwithdrawrejected_list')
            </div>
            <div class="tab-pane fade" id="tab34default">                      
                @include('adminpartials._userwithdrawrequest_list')
            </div>

            <!-- Deposits -->
             <div class="tab-pane fade" id="tab21default">                     
                 @include('adminpartials._usernewdeposit_list')
            </div>
            <div class="tab-pane fade" id="tab22default">                      
                @include('adminpartials._useractivedeposit_list')
            </div>
            <div class="tab-pane fade" id="tab23default">                      
                @include('adminpartials._usermatureddeposit_list')
            </div>
             <div class="tab-pane fade" id="tab24default">                     
               @include('adminpartials._userreleaseddeposit_list')
            </div>
            <div class="tab-pane fade" id="tab25default">                      
               @include('adminpartials._userrejecteddeposit_list')
            </div>
            <div class="tab-pane fade" id="tab26default">                      
                @include('adminpartials._userarchiveddeposit_list')
            </div>
            <!-- Fund Transfers -->
            <div class="tab-pane fade" id="tab81default">                      
                @include('adminpartials._userfundtransfersend_list')
            </div>
            <div class="tab-pane fade" id="tab82default">                      
                @include('adminpartials._userfundtransferreceived_list')
            </div>
            <!-- Referrals -->
            <div class="tab-pane fade" id="tab9default">          
                @include('adminpartials._userreferrals_list')
            </div>

            <div class="tab-pane fade" id="tab10default">          
               @include('adminpartials._user_penalty_list')
            </div>

            <div class="tab-pane fade" id="tab11default">          
               @include('adminpartials._user_bonus_list')
            </div>

            <div class="tab-pane fade" id="tab12default">          
               @include('adminpartials._user_messages_list')
            </div>

            <div class="tab-pane fade" id="tab13default">          
               @include('adminpartials._user_mail_messages_list')
            </div>
        </div>
    </div>
</div>