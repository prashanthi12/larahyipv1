<div class="widget-grid">
@if($pendingkycCount > 0)
  @foreach($pendingkyclists as $pendingkyclist)
    <div class="grid-item a">
      <p>
        <small>{{ trans('admin.username') }} : </small><br/>       
          <a href="{{ url('admin/users/'.$pendingkyclist->user->name) }}">{{ $pendingkyclist->user->name }}</a>
      </p>
    </div>
    <div class="grid-item b">
      <p>
        <small>{{ trans('admin.kycdoc') }} : </small><br/>       
          <img id="myImg{{ $pendingkyclist->id }}" class="Image" src="{{ ($pendingkyclist->kyc_doc) }}"  width="50" height="50"> 
      </p>     
    </div>
    <div class="grid-item c">           
      <p><small>{{ trans('admin.date') }} :</small><br/>{{ $pendingkyclist->created_at->diffForHumans() }}<br/></p>
    </div>
    <div class="grid-item d">
      <p class="form-group flex-button-group flex-vertical">
          @if ($pendingkyclist->kyc_doc != '')
            <a href="#" rel="{{ url('admin/users/verifykyc/'.$pendingkyclist->id.'') }}" class="btn btn-success btn-xs flex-button flex-button-vertical verifykyc">{{ trans('admin.verifykyc') }}</a>

            <a href="#" rel="{{ url('admin/users/rejectkyc/'.$pendingkyclist->id.'') }}" class="btn btn-danger btn-xs flex-button flex-button-vertical rejectkyc">{{ trans('admin.rejectkyc') }}</a> 
          @else
            {{ trans('admin.notuploaded') }}
          @endif 
      </p>
    </div>
  @endforeach
@else
    <div class="">{{ trans('admin.no_kycapprovals_found') }}</div>
@endif   
</div>
<div>
    @if($pendingkycCount > 5)
       <p class="text-center"> <a href="{{ url('admin/actions/kyc') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_pendingkyc') }}</a></p>
    @endif
</div>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {    
    $('.verifykyc').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to approve the kyc document ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    });     

    $('.rejectkyc').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to reject the kyc document ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 

});
</script>
@endpush