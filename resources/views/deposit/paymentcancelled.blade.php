@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.make_deposit') }}
		<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a>
    </div>
    <div class="panel-body">            
        <div class="row">
	        <div class="col-md-12">
	            <div class="alert alert-danger">{{ trans('forms.paymentcancelled') }}</div>
	        </div>
        </div>
    </div>
</div>
</div>
@endsection
