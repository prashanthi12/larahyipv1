@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.okpay') }}<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">            
              <p> {{ $instructions }}</p>
              @if ($depositfeestatus == 1)
                  <p>
                            {{ trans('forms.deposit_charge') }} : {{ Session::get('depositfee') }} 

                             @if (Session::get('depositfeetype') == 1) 
                              Flat
                            @elseif (Session::get('depositfeetype') == 2)
                              %
                            @endif
                    </p>

                    <p>
                            {{ trans('forms.total_deposit_amount') }} : {{ $amount  }} {{ \Config::get('settings.currency') }}
                    </p>

              @endif
                <form  method="post" action="https://checkout.okpay.com/">
            {{ csrf_field() }}
                       <input type="hidden" name="ok_receiver" value="{{ $params['ok_receiver'] }}"/>
                       <input type="hidden" name="ok_item_1_name" value="{{ $username.'-'.$plan }}"/>
                       <input type="hidden" name="ok_item_1_price" value="{{  $amount  }}"/>
                       <input type="hidden" name="ok_currency" value="{{ Config::get('settings.currency') }}"/>
                       <input type="hidden" name="ok_return_success" value="{{ url('myaccount/deposit/okpayprocess') }}"/>
                       <input type="hidden" name="ok_return_fail" value="{{ url('myaccount/deposit/paymentcancelled') }}"/>
                       <center>
                       <input type=image src="{{ asset('images/okpay.jpg') }}" >
                       </center>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
