<div class="col-md-10 col-md-offset-1">
<div id="depositfrm">
 <!-- <form method="post" action="{{ url('myaccount/deposit')}}" class="form-horizontal"> -->

 {!! Form::open(['method' => 'POST', 'route' => 'deposit', 'class' => 'form-horizontal', 'id' => 'deposit_form']) !!}
 {{ csrf_field() }}

    @if ($reinvest == 1)
    <div class="form-group">
        <label>{{ trans('forms.reinvestment_amount_lbl') }} : </label>
        {{ $user->balance }} {{ config::get('settings.currency') }}
        <input type="hidden" name="reinvestamount" id="reinvestamount" class='form-control' value="{{ $user->balance }}">
        <input type="hidden" name="paymentgateway" id="paymentgateway" class='form-control' value="13">
        <input type="hidden" name="reinvest" id="reinvest" class='form-control' value="{{ $reinvest }}">
    </div>
    @endif

	<div class="form-group{{ $errors->has('plan') ? ' has-error' : '' }}">
	    <label>{{ trans('forms.deposit_plan_lbl') }}</label>
	    <select class="form-control" id="plan" name="plan" onchange="getamount(this.value);">
	    <option value="">{{ trans('forms.deposit_plan_lbl') }}</option>
	    	@foreach ($plans as $plan)
	    		<option value="{{ $plan->id }}" {{ (Form::old("plan") == $plan->id ? "selected":"") }}>{{ $plan->name }}</option>
	    	@endforeach
	    </select>
	    <small class="text-danger">{{ $errors->first('plan') }}</small>
	</div>

	<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
	    <label>{{ trans('forms.deposit_amount_lbl') }}</label>
	    <input type="text" name="amount" id="amount" class='form-control' value="{{ old('amount') }}">
	    <small class="text-danger">{{ $errors->first('amount') }}</small>
	    <small class="plan_amount"></small>
	</div>

     @if (!isset($reinvest))
	<div class="form-group{{ $errors->has('paymentgateway') ? ' has-error' : '' }}">
	    <label>{{ trans('forms.deposit_payment_lbl') }}</label>
	    <select class="form-control" id="pgs" name="paymentgateway">	    	
	    	<option value="" >{{ trans('forms.deposit_payment_lbl') }}</option>
	    	@foreach ($pgs as $pg)
	    		<option value="{{ $pg->id }}" {{ (Form::old("paymentgateway") == $pg->id ? "selected":"") }}>{{ $pg->displayname }}</option>
	    	@endforeach
	    </select>
	    <small class="text-danger">{{ $errors->first('paymentgateway') }}</small>
	</div>	
    @endif

	@php
	$advdivstyle = 'none';
	if( Form::old("paymentgateway") == 5)
	{
		$advdivstyle = 'block';
	}
    @endphp

	<div id="advcashfield" style="display:{{ $advdivstyle }}">
		 <div class="form-group{{ $errors->has('account_email') ? ' has-error' : '' }}">
          <label>{{ trans('forms.account_email_lbl') }}</label>
          <input type="text" name="account_email" id="account_email" class='form-control' value="{{ old('account_email') }}">
          <small class="text-danger">{{ $errors->first('account_email') }}</small>
        </div>
	</div>

    @php
    $chequedivstyle = 'none';
    if( Form::old("paymentgateway") == 17)
    {
        $chequedivstyle = 'block';
    }
    @endphp

    <div id="chequefield" style="display:{{ $chequedivstyle }}">
        <div class="form-group{{ $errors->has('cheque_no') ? ' has-error' : '' }}">
          <label>{{ trans('forms.cheque_no_lbl') }}</label>
          <input type="text" name="cheque_no" id="cheque_no" class='form-control' value="{{ old('cheque_no') }}">
          <small class="text-danger">{{ $errors->first('cheque_no') }}</small>
        </div>
        <div class="form-group{{ $errors->has('payeer_name') ? ' has-error' : '' }}">
          <label>{{ trans('forms.payeer_name_lbl') }}</label>
          <input type="text" name="payeer_name" id="payeer_name" class='form-control' value="{{ old('payeer_name') }}">
          <small class="text-danger">{{ $errors->first('payeer_name') }}</small>
        </div>
        <!-- <div class="form-group{{ $errors->has('deposit_on') ? ' has-error' : '' }}">
          <label>{{ trans('forms.deposit_on_lbl') }}</label>
          <input type="text" name="deposit_on" id="deposit_on" class='form-control' value="{{ old('deposit_on') }}">
          <small class="text-danger">{{ $errors->first('deposit_on') }}</small>
        </div> -->
    </div>

    <div class="form-group">    
    	<input value="{{ trans('forms.submit_btn') }}" class="btn btn-success btn-deposit-success" type="submit" onclick="this.disabled=true;this.form.submit();"> 	
    	<a href="{{ url('myaccount/deposit') }}" class="btn btn-default btn-deposit-res">{{ trans('forms.reset') }}</a>
    </div>

<!-- </form> -->
{!! Form::close() !!}
</div>
</div>

@push('bottomscripts')
<script>
     $("#pgs").on("change", function(){
     	var paymentid = $('#pgs').val();

     	if (paymentid == 5)
     	{
     		$('#advcashfield').show();
     	}
     	else
     	{
     		$('#advcashfield').hide();
     	}

        if (paymentid == 17)
        {
            $('#chequefield').show();
        }
        else
        {
            $('#chequefield').hide();
        }
    });

$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

function getamount(planid)
{ 
     $.post( "deposit/view_plan_amount", { planid: planid })
      .done(function( data ) {
        $('.plan_amount').html(data);
   });
}
</script>
@endpush