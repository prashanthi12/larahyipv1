<div class="col-md-10 col-md-offset-1">
<div id="partialwithdraw">
 <form method="post" action="{{ url('myaccount/deposit/processpartialwithdraw')}}" class="form-horizontal">

 {{ csrf_field() }}

<p>{{ trans('forms.max_partial_withdraw_amount') }} : {{ $deposit->plan->max_partial_withdraw_limit }} (%)</p>
<p>{{ trans('forms.min_withdraw_amount') }} : {{ Config::get('settings.withdraw_min_amount') }} {{ Config::get('settings.currency') }}</p>
<p>{{ trans('forms.partial_withdraw_fee') }} : {{ $deposit->plan->paritial_withdraw_fee  }} %</p>
<p>{{ trans('forms.deposited_amount') }} : {{ $deposit->amount  }} {{ Config::get('settings.currency') }}</p>

	<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
	    <label>{{ trans('forms.deposit_amount_lbl') }}</label>
	    <input type="text" name="amount" id="amount" class='form-control' value="{{ old('amount') }}">
	    <small class="text-danger">{{ $errors->first('amount') }}</small>
        <input type="hidden" name="maxpartialamount" id="maxpartialamount"  value="{{ $deposit->plan->max_partial_withdraw_limit }}">
        <input type="hidden" name="depositamount" id="depositamount"  value="{{ $deposit->amount }}">
        <input type="hidden" name="depositid" id="depositid"  value="{{ $deposit->id }}">
	</div>

    <div class="form-group">    
    	<input value="{{ trans('forms.submit_btn') }}" class="btn btn-success btn-deposit-success" type="submit" onclick="this.disabled=true;this.form.submit();"> 	
    	<a href="{{ url('myaccount/deposit') }}" class="btn btn-default btn-deposit-res">{{ trans('forms.reset') }}</a>
        <!-- <a href="{{ url('myaccount/deposit') }}" class="btn btn-danger btn-deposit-res">{{ trans('forms.back') }}</a> -->
    </div>
</form>
</div>
</div>
