
@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.paypal') }}<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">            
              <p> {{ $instructions }}</p>
               @if ($depositfeestatus == 1)
                  <p>
                            {{ trans('forms.deposit_charge') }} : {{ Session::get('depositfee') }} 
                             @if (Session::get('depositfeetype') == 1) 
                              Flat
                            @elseif (Session::get('depositfeetype') == 2)
                              %
                            @endif
                    </p>

                    <p>
                            {{ trans('forms.total_deposit_amount') }} : {{ $amount  }} {{ \Config::get('settings.currency') }}
                    </p>

              @endif

                  <form class="form-horizontal" method="POST" id="payment-form" role="form" action="{{ url('myaccount/deposit/processpaypal')}}" >

                        {{ csrf_field() }}

                       <input id="amount" type="hidden" class="form-control" name="amount" value="{{ $amount }}">
                       <input id="plan" type="hidden" class="form-control" name="plan" value="{{ $plan }}">
                       <input id="paymentgateway" type="hidden" class="form-control" name="paymentgateway" value="{{ $paymentgateway }}">
                        
                        <div class="form-group">

                            <div class="col-md-6 col-md-offset-4">

                                <button type="submit" class="btn btn-primary" onclick="this.disabled=true;this.form.submit();">

                                    {{ trans('forms.pay_paypal_btn') }}

                                </button>

                            </div>

                        </div>

                    </form>

              </div>
        </div>
    </div>
</div>
@endsection