@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.make_deposit') }}</div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <p> {{ $instructions }}</p>
            <form method="POST" action="{{ url('myaccount/deposit/ewallet') }}">  
              {{ csrf_field() }}
              <div class="form-group">
                <label class="col-md-4 control-label">{{ trans('forms.amount_to_be_deposited') }}</label>
                <div class="col-md-8">
                  <label for="input01">{{ $amount }}</label>
                  <input name="amount" class="form-control" value="{{ $amount }}" type="hidden">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">{{ trans('forms.transaction_ref_id') }}</label>
                <div class="col-md-8">
                  <label for="input01">{{ $transaction_id }}</label>
                  <input name="transaction_id" class="form-control" value="{{ $transaction_id }}" type="hidden">
                </div>
              </div>
              <div class="form-group">
                <input name="plan" class="form-control" value="{{ $plan }}" type="hidden">
              </div>
              <div class="form-group">
                <input name="paymentgateway" class="form-control" value="{{ $paymentgateway }}" type="hidden">
              </div>                       
              <div class="form-group mt-20">    
                <input value="{{ trans('forms.submit_complete_btn') }}" class="btn btn-success" type="submit" onclick="this.disabled=true;this.form.submit();">   
                <a href="{{ url('myaccount/deposit') }}" class="btn btn-default">{{ trans('forms.back') }}</a>
              </div>
            </form>
        </div>
      </div>
    </div>
</div>
@endsection           
            