@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
	<div class="panel-heading">{{ trans('forms.make_deposit') }}</div>
    <div class="panel-body">
    	<div class="row">
      		Thank you for the Investment. Please check your mail for further details.
      	</div>
    </div>
</div>
@endsection   