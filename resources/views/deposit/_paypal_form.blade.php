@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.make_matrix_deposit') }}</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">               
            @if ($params['merchant_email'] != '')
                  @if($params['mode']=='live')
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                  @elseif($params['mode']=='sandbox')
                    <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
                  @endif
                        <input type="hidden" name="cmd" value="_xclick">
                        <input type="hidden" name="business" value="{{$params['merchant_email']}}">
                        <input id="amount" type="hidden" class="form-control" name="amount" value="{{ $amount }}">
                        <input id="plan" type="hidden" class="form-control" name="plan" value="{{ $plan }}">
                        <input id="paymentgateway" type="hidden" class="form-control" name="paymentgateway" value="{{ $paymentgateway }}">
                        <input type="hidden" name="currency_code" value="{{Config::get('settings.currency')}}">
                        <input type="hidden" name="cancel_return" value="{{url('myaccount/deposit/paymentcancelled')}}">
                        <input type="hidden" name="return" value="{{url('myaccount/deposit/successpaypal')}}">
                        <input type="hidden" value="2" name="rm"> 
                        <input value="{{ trans('forms.submit_complete_btn') }}" class="btn btn-success" type="submit" onclick="this.disabled=true;this.form.submit();"> 
                    </form>
                 @else
                    <h4> {{ trans('forms.error_payment_settings') }} </h4>
                    <p> <a href="{{ url('myaccount/deposit') }}" class="btn btn-default">{{ trans('forms.back') }}</a></p>
            @endif
        </div>
      </div>
    </div>
</div>
@endsection           
            