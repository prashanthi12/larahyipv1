@extends('layouts.myaccount') 

@section('content')
    <div class="panel panel-default">
    	<div class="panel-heading">{{ trans('myaccount.uploadproof') }}</div>
	    <div class="panel-body">       
	        <div class="row">
	            <div class="col-md-10 col-md-offset-1">
	            	<form method="post" enctype="multipart/form-data">
    				{{ csrf_field() }}
    				<div class="form-group{{ $errors->has('proof') ? ' has-error' : '' }}">
    					<label>{{ trans('forms.attachment') }} {{ trans('forms.proofattachment_types') }}</label>
        				<input type="file" name="proof"/>
    					<small class="text-danger">{{ $errors->first('proof') }}</small>
    				</div>
    				<div class="form-group">
				        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success" type="submit" onclick="this.disabled=true;this.form.submit();">
				        <a href="" class="btn btn-default">{{ trans('forms.reset') }}</a>
				    </div>
	            </div>
        	</div>
    	</div>
 	</div>
@endsection
