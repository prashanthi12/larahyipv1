<div class="col-md-12 ">

<form method="post" action="{{ url('admin/deposit/approve/'.$depositid)}}" class="form-horizontal" id="contact">
{{ csrf_field() }}

   <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
        <label>Transaction Reference Number : </label>
        <label>{{ $deposit->present()->getTransactionNumber($deposit->transaction_id) }}</label>
    </div>   

    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
        <label>Invoiced Amount : </label>
       <label>{{ $deposit->amount }} {{ \Config::get('settings.currency') }}</label>
    </div>  


    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
        <label>Amount Received</label>
        <input type="number" name="depositamount" id="depositamount" class="form-control" required="required">
        <small class="text-danger">{{ $errors->first('comment') }}</small>
    </div>  
   
   
 
    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
        <textarea  rows="5" class="form-control"  placeholder="Comments, usually the transaction reference from your account like... " name="comment" id="comment" required="required"></textarea>
        <small class="text-danger">{{ $errors->first('comment') }}</small>
    </div>  


    <div class="form-group">
        {!! Form::submit("Confirm", ['class' => 'btn btn-primary']) !!}
    </div>
    </form> 
    <hr>
    <p>If the received amount is different from the needed amount, then you can add that in the Account Balance (Don't Confirm the deposit). The user later can  use REINVEST option to deposit from the account balance</p>
     <div class="form-group">
        <form class="addbalance" action="{{ url('admin/deposit/fundaddeduser/'.$depositid) }}" method="post">
        <input type="hidden" name="receivedamount" id="receivedamount" value="">
        <input type="hidden" name="problemcomment" id="problemcomment" value="">
                             {{ csrf_field() }} 
                             {!! Form::submit("Don't Confirm...Add to Account Balance", ['class' => 'btn btn-danger flex-button']) !!}
        </form>  

         <a href="{{ url('admin/deposit/new/') }}" class='btn btn-linnk'>Back to List</a>
    </div>

</div>

@push('scripts')

<script type="text/javascript">

$(document).ready(function() {
       
    //  $("#addedfund").on("click", function(){
    //     return confirm("Do you want to added this deposit amount to user account balance? ");

    // });

     $(".addbalance").on("submit", function(){       

        if (confirm('Do you want to added this deposit amount to user account balance?')) 
        {
            var depositamount = $('#depositamount').val();
            $('#receivedamount').val(depositamount);
            var comment = $('#comment').val();
            $('#problemcomment').val(comment);
                if (depositamount == '' || comment == '')
                {
                    alert('All fields are mandatory');
                    return false;
                }
               
                return true;
        } 
        else 
        {
            
            return false;
        }
    });      
});

</script>
@endpush
