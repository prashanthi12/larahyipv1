 @extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.neteller') }}<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">            
              <p> {{ $instructions }}</p>
               @if ($depositfeestatus == 1)
                  <p>
                            {{ trans('forms.deposit_charge') }} : {{ Session::get('depositfee') }} 
                             @if (Session::get('depositfeetype') == 1) 
                              Flat
                            @elseif (Session::get('depositfeetype') == 2)
                              %
                            @endif
                    </p>

                    <p>
                            {{ trans('forms.total_deposit_amount') }} : {{ $amount  }} {{ \Config::get('settings.currency') }}
                    </p>

              @endif
     
              <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                      <form method="post" action="{{ url('myaccount/deposit/processneteller') }}">
                            {{ csrf_field() }}
                      <input type="hidden" name="version" value=" 4.1">
                      <input type="hidden" name="amount" size="10" value="{{ $amount }}" maxlength="10">
                      <input type="hidden" name="currency" value="{{ \Config::get('settings.currency') }}" size="10" maxlength="3">
                      <input type="text" name="net_account" size="20" value="" maxlength="100" class="form-control" placeholder="Your Account Id Here" required="required"><br/>
                      <input type="text" name="secure_id" size="10" value="" maxlength="6" class="form-control" placeholder="Your Secret Id Here" required="required"><br/>
                      <input type="hidden" name="merchant_id" value="{{ $params['merchant_id'] }}">
                      <input type="hidden" name="merch_key" value="{{ $params['merch_key'] }}">
                      <input type="hidden" name="merch_transid" value="{{ $transaction_number }}" maxlength="50">
                      <input type="hidden" name="language_code" value="EN">
                      <input type="hidden" name="merch_name" value="{{ $username }}">
                      <input type="hidden" name="merch_account" value="436346" maxlength="50">
                      <input type="hidden" name="custom_1" value="Deposit for {{ $plandetail->name }} plan" maxlength="50">
                      <input type="hidden" name="custom_2" value="Deposit for {{ $plandetail->name }} plan" maxlength="50">
                      <input type="hidden" name="custom_3" value="Deposit for {{ $plandetail->name }} plan" maxlength="50">
                     <center>
                        <input type=image src="{{ asset('images/neteller.jpg') }}" width="100px">
                      </center>
                      </form>
                  </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection