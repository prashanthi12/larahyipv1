@extends('layouts.myaccount') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <h3>Investment Confirmation</h3>
            <div class="panel panel-default"> 
                <div class="panel-body">       
                    <div class="row">
                        <div class="col-md-12">                        
                            @include('deposit.approve')                     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
