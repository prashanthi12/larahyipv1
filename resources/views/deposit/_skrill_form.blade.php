@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.skrill') }}<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            <p> {{ $instructions }}</p>
             @if ($depositfeestatus == 1)
                  <p>
                            {{ trans('forms.deposit_charge') }} : {{ Session::get('depositfee') }} 

                             @if (Session::get('depositfeetype') == 1) 
                              Flat
                            @elseif (Session::get('depositfeetype') == 2)
                              %
                            @endif
                    </p>

                    <p>
                            {{ trans('forms.total_deposit_amount') }} : {{ $amount  }} {{ \Config::get('settings.currency') }}
                    </p>

              @endif
        <?php
        $trans_id=mt_rand();
        ?>

        @if ($params['pay_to_email'] != '')
        <form action="https://pay.skrill.com" method="post" >
         <input type="hidden" name="pay_to_email" value="{{ $params['pay_to_email'] }}">
         <input type="hidden" name="return_url" value="{{ url('myaccount/deposit/skrillprocess') }}">
        <input type="hidden" name="cancel_url" value="{{ url('myaccount/deposit/paymentcancelled') }}">
        <input type="hidden" name="status_url" value="{{ url('myaccount/deposit/skrillprocess') }}">
        <input type="hidden" name="language" value="EN">
        <input type="hidden" name="amount" value="{{ $amount }}">
        <input type="hidden" name="currency" value="{{ Config::get('settings.currency') }}">
        <input type="hidden" name="detail1_description" value="{{ $username.' deposited on '.$planname.' plan.' }}">
        <input type="hidden" name="detail1_text" value="{{ $username.' deposited on '.$planname.' plan.' }}">
         <center><input type="image" name="pay" style="width:100px;height:50px;" src="{{ asset('images/skrill.jpg') }}"  ></center>
        </form>
        @else
            <h4> {{ trans('forms.error_payment_settings') }} </h4>
            <p><a href="{{ url('myaccount/deposit') }}" >{{ trans('forms.back') }}</a></p>
        @endif 
       </div>
        </div>
    </div>
</div>
@endsection