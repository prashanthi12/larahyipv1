<div class="dark-footer">
<div class="container">
    <div class="row ">
    <div class="mt-50 mb-50">
    <div class="col-md-4">
        <p class="clearfix">
            <a class="navbar-brand-footer" href="{{ url('/') }}" title="{{ Config::get('settings.sitename') }}">
            <img src="{{ url(Config::get('settings.sitelogo')) }}" alt="{{ Config::get('settings.sitename') }}"  class="clear-fix" >       
            </a>
        </p>
        <p class="footer-text clearfix">{!! trans('welcome.footer-about-text') !!}</p>
    </div>
    @if (Config::get('settings.footer_link_status') == '1')
    <div class="col-md-4 visible-xs visible-sm">
         <ul class="footer-menu-mobile">
            <li><a href="{{ url('/news') }}" class="footer-menu-item">{{ trans('mainnavigation.menunews') }}</a></li>
            <li><a href="{{ url('/reviews') }}" class="footer-menu-item">{{ trans('mainnavigation.menureviews') }}</a></li>
            <li><a href="{{ url('/exchange') }}" class="footer-menu-item">{{ trans('mainnavigation.menuexchange') }}</a></li>
            <li><a href="{{ url('/faq') }}" class="footer-menu-item">{{ trans('mainnavigation.menufaq') }}</a></li>
            <li><a href="{{ url('/privacy') }}" class="footer-menu-item">{{ trans('mainnavigation.menuprivacypolicy') }}</a></li>
            <li><a href="{{ url('/terms') }}" class="footer-menu-item">{{ trans('mainnavigation.menutermsservice') }}</a></li>
            <li><a href="{{ url('/quotes') }}" class="footer-menu-item">{{ trans('mainnavigation.quotes') }}</a></li>
            <li><a href="{{ url('/stats') }}" class="footer-menu-item">{{ trans('mainnavigation.statistics') }}</a></li>
            <li><a href="{{ url('/topinvestors') }}" class="footer-menu-item">{{ trans('mainnavigation.topinvestors') }}</a></li>
             <li><a href="{{ url('/earn') }}" class="footer-menu-item">{{ trans('mainnavigation.menuearn') }}</a></li>           
            <li><a href="{{ url('/privacy') }}" class="footer-menu-item">{{ trans('mainnavigation.menuprivacypolicy') }}</a></li>
            <li><a href="{{ url('/terms') }}" class="footer-menu-item">{{ trans('mainnavigation.menutermsservice') }}</a></li>
            <li><a href="{{ url('/security') }}" class="footer-menu-item">{{ trans('mainnavigation.menusecuritytips') }}</a></li>
            <li><a href="{{ url('/payouts') }}" class="footer-menu-item">{{ trans('mainnavigation.payouts') }}</a></li>
            <li><a href="{{ url('/topreferrals') }}" class="footer-menu-item">{{ trans('mainnavigation.topreferrals') }}</a></li>
            <li><a href="{{ url('/lastinvestors') }}" class="footer-menu-item">{{ trans('mainnavigation.lastinvestors') }}</a></li>
        </ul>
    </div>
    @endif
    
    <div class="visible-md visible-lg">
    @if (Config::get('settings.footer_link_status') == '1')
    <div class="col-md-2 ">
         <ul class="footer-menu-vertical">
            <li><a href="{{ url('/news') }}" class="footer-menu-item">{{ trans('mainnavigation.menunews') }}</a></li>
            <li><a href="{{ url('/reviews') }}" class="footer-menu-item">{{ trans('mainnavigation.menureviews') }}</a></li>
            <li><a href="{{ url('/exchange') }}" class="footer-menu-item">{{ trans('mainnavigation.menuexchange') }}</a></li>
            <li><a href="{{ url('/faq') }}" class="footer-menu-item">{{ trans('mainnavigation.menufaq') }}</a></li>
            <li><a href="{{ url('/quotes') }}" class="footer-menu-item">{{ trans('mainnavigation.quotes') }}</a></li>
            <li><a href="{{ url('/stats') }}" class="footer-menu-item">{{ trans('mainnavigation.statistics') }}</a></li>
            <li><a href="{{ url('/topinvestors') }}" class="footer-menu-item">{{ trans('mainnavigation.topinvestors') }}</a></li>
        </ul>
    </div>
    <div class="col-md-2 ">
        <ul class="footer-menu-vertical">
            <li><a href="{{ url('/earn') }}" class="footer-menu-item">{{ trans('mainnavigation.menuearn') }}</a></li>           
            <li><a href="{{ url('/privacy') }}" class="footer-menu-item">{{ trans('mainnavigation.menuprivacypolicy') }}</a></li>
            <li><a href="{{ url('/terms') }}" class="footer-menu-item">{{ trans('mainnavigation.menutermsservice') }}</a></li>
            <li><a href="{{ url('/security') }}" class="footer-menu-item">{{ trans('mainnavigation.menusecuritytips') }}</a></li>
            <li><a href="{{ url('/payouts') }}" class="footer-menu-item">{{ trans('mainnavigation.payouts') }}</a></li>
            <li><a href="{{ url('/topreferrals') }}" class="footer-menu-item">{{ trans('mainnavigation.topreferrals') }}</a></li>
             <li><a href="{{ url('/lastinvestors') }}" class="footer-menu-item">{{ trans('mainnavigation.lastinvestors') }}</a></li>
        </ul>
    </div> @endif
    </div>

    <div class="col-md-4">
        <div class="mt-50">
        <p class="text-center"><img src="{{ url(Config::get('settings.securitybanner') ) }}" class="img-resposnive footer-ssl-logo" max-width="100%"></p>
        </div>
    </div>
    </div>
    </div>
</div>
</div>
<div class="dark-copy-footer">
<nav class="navbar navbar-default navbar-bottom mb-0">
    <div class="container">
       
    </div>
    <div class="container">
        <div class="mt-10">
                <p class="copy-text">&copy;Copyright {{ Carbon\Carbon::now()->year }}. {{Config::get('settings.sitename') }} {{ trans('welcome.footer_text') }}</p>
                        <p class="copy-text"><small>Server Time : {{ Carbon\Carbon::now()->format('d/m/Y H:i:s') }}</small></p>
        </div>
    </div>
</nav>
</div>
</div>