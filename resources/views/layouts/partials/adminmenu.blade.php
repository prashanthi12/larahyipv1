<ul class="nav navbar-nav navbar-flex">
    <li class="admin-menu">
        <a href="{{ url('admin/dashboard') }}"><i class="glyphicon glyphicon-th"></i>
                                     {{ trans('admin.dashboard') }}</a></li>
    <li class="admin-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i>{{ trans('admin.users') }}<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="{{ url('admin/users/create/new') }}">{{ trans('admin.addnew') }}</a></li>
            <li><a href="{{ url('admin/users') }}">
                                     {{ trans('admin.members') }}</a></li>
            <li><a href="{{ url('admin/staffs') }}">{{ trans('admin.staffs') }}</a></li>
        </ul>
    </li>
    <li class="admin-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-ok"></i>{{ trans('admin.actions') }}<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="{{ url('admin/actions/newinvestment') }}">{{ trans('admin.newdeposit') }}</a></li>
            <li><a href="{{ url('admin/actions/maturedinvestment') }}">{{ trans('admin.maturedeposit') }}</a></li>
            <li><a href="{{ url('admin/actions/withdraw') }}">{{ trans('admin.withdraw') }}</a></li>
            <li><a href="{{ url('admin/actions/kyc') }}">{{ trans('admin.kyc') }}</a></li>
        </ul>
    </li>
    <li class="admin-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-briefcase"></i>{{ trans('admin.deposits') }} <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="{{ url('admin/deposit/new') }}">{{ trans('admin.new') }}</a></li>
            <li><a href="{{ url('admin/deposit/active') }}">{{ trans('admin.active') }}</a></li>
            <li><a href="{{ url('admin/deposit/matured') }}">{{ trans('admin.matured') }}</a></li>
            <li><a href="{{ url('admin/deposit/released') }}">{{ trans('admin.released') }}</a></li>
            <li><a href="{{ url('admin/deposit/rejected') }}">{{ trans('admin.rejected') }}</a></li>
            <li><a href="{{ url('admin/deposit/problem') }}">{{ trans('admin.problem') }}</a></li>
            <li><a href="{{ url('admin/deposit/archived') }}">{{ trans('admin.archived') }}</a></li>
            <li><a href="{{ url('admin/reportuser') }}">{{ trans('admin.reportuser') }}</a></li>
        </ul>
    </li>
    <li class="admin-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-chevron-down"></i>{{ trans('admin.withdraws') }} <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="{{ url('admin/withdraw/completed') }}">{{ trans('admin.completed') }}</a></li>
            <li><a href="{{ url('admin/withdraw/pending') }}">{{ trans('admin.pending') }}</a></li>
            <li><a href="{{ url('admin/withdraw/request') }}">{{ trans('admin.request') }}</a></li>
            <li><a href="{{ url('admin/withdraw/rejected') }}">{{ trans('admin.rejected') }}</a></li>
        </ul>
    </li>
    <li class="admin-menu"><a href="{{ url('admin/earnings') }}"><i class="glyphicon glyphicon-plus"></i>
                                     {{ trans('admin.earnings') }}</a></li>
    <li class="admin-menu"><a href="{{ url('admin/ticket') }}"><i class="glyphicon glyphicon-tasks"></i>
                                     {{ trans('admin.tickets') }}</a></li>
    <li class="admin-menu"><a href="{{ url('admin/fundtransfer') }}"><i class="glyphicon glyphicon-random"></i>
                                     {{ trans('admin.transfers') }}</a></li>
    <li class="admin-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-option-horizontal"></i>{{ trans('admin.reports') }}<b class="caret"></b></a>
        <ul class="dropdown-menu pull-right">
            <li><a href="{{ url('admin/reports/deposits') }}">{{ trans('admin.depositsbystatus') }}</a></li>
            <li><a href="{{ url('admin/reports/deposits/payments') }}">{{ trans('admin.depositsbypayment') }}</a></li>
            <li><a href="{{ url('admin/reports/deposits/plans') }}">{{ trans('admin.depositsbyplans') }}</a></li>
            <li><a href="{{ url('admin/reports/withdraws') }}">{{ trans('admin.withdraws') }}</a></li>
            <li><a href="{{ url('admin/reports/fundtransfers') }}">{{ trans('admin.fundtransfers') }}</a></li>
            <li><a href="{{ url('admin/reports/earnings') }}">{{ trans('admin.earnings') }}</a></li>
            <li><a href="{{ url('admin/reports/investors') }}">{{ trans('admin.investors') }}</a></li>
            <li><a href="{{ url('admin/reports/sponsors') }}">{{ trans('admin.sponsors') }}</a></li>
        </ul>
    </li>
    <li class="admin-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-option-vertical"></i>{{ trans('admin.more') }} <b class="caret"></b></a>
        <ul class="dropdown-menu  pull-right">
            <li><a href="{{ url('admin/log') }}">{{ trans('admin.activitylogs') }}</a></li>
            <li><a href="{{ url('admin/testimonials') }}">{{ trans('admin.testimonials') }}</a></li>
            <!-- <li><a href="{{ url('admin/newsletter') }}">{{ trans('admin.newsletter') }}</a></li> -->
            <li><a href="{{ url('admin/contactus') }}">{{ trans('admin.contactus') }}</a></li>
            <li><a href="{{ url('admin/settings') }}">{{ trans('admin.settingsbyvalue') }}</a></li>
            <li><a href="{{ url('admin/bonus') }}">{{ trans('admin.sendbonus') }}</a></li>
            <li><a href="{{ url('admin/bonus/list') }}">{{ trans('admin.bonuslist') }}</a></li>
            <li><a href="{{ url('admin/penalty') }}">{{ trans('admin.sendpenalty') }}</a></li>
            <li><a href="{{ url('admin/penalty/list') }}">{{ trans('admin.penaltylist') }}</a></li>
            <li><a href="{{ url('admin/message/send') }}">{{ trans('admin.sendmessagetouser') }}</a></li>
            <li><a href="{{ url('admin/message/list') }}">{{ trans('admin.messagelist') }}</a></li>
            <li><a href="{{ url('admin/loggedinusers') }}">{{ trans('admin.loggedinusers') }}</a></li>
            <li><a href="{{ url('admin/importcontacts') }}">{{ trans('admin.contacts') }}</a></li>
            <li><a href="{{ url('admin/ewallet/fund/show') }}">{{ trans('admin.fund_ewallet') }}</a></li>
            <li><a href="{{ url('admin/sendmail') }}">{{ trans('admin.send_mail_list') }}</a></li>
            <li><a href="{{ url('admin/massmail') }}">{{ trans('admin.send_mass_mail') }}</a></li>
        </ul>
    </li>
</ul>