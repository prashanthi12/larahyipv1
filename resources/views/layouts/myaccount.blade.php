<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>:: {{ Config::get('settings.sitename') }} :: My Account</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ Config::get('settings.favicon') }}" type="image/x-icon">
    <!-- Scripts -->
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 
  
    <script>
    window.Laravel = {!!json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    @stack('headscripts')
    {!! Config::get('settings.headerscript') !!}
</head>
<body>
    <div id="app">
        @include('layouts.sitelinks')
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    @include('layouts.sidebar')
                </div>
                <div class="col-md-9">
                    @yield('content')
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    {!! Config::get('settings.google_analytics_code') !!}
    {!! Config::get('settings.addthis_code') !!}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
    <!-- Go to www.addthis.com/dashboard to customize your tools --> 

      @stack('bottomscripts')
      {!! Config::get('settings.footerscript') !!}

     @include('layouts.footercss')
     <style>
        .nav-2 {
            background-color: #c7c7c7;
            margin-top: 0;
            margin-bottom: 0;
            display: block;
        }
    </style>

    
</body>
</html>
