@php
    $currentUser = Auth::user(); 
    if (isset($currentUser->unreadNotifications))
    {
        $usernotifications = $currentUser->unreadNotifications;
        session(['usernotifications'=> $usernotifications]);             
    } 
    if(!is_null($currentUser))
    {
        $auto_withdrawal_status = \Config::get('settings.auto_withdrawal_status');
    }     
@endphp
@if (Auth::guest() && Cookie::get('sponsor') )
<div class="row-fluid top-sponsor-bar">
        <div class="container">
            <div style="margin-top: 8px;"><p>{{ trans('welcome.invite')}} : {{ Cookie::get('sponsor') }}</p></div>
        </div>
</div>
@endif
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}" title="{{ Config::get('settings.sitename') }}">
            <img src="{{ url(Config::get('settings.sitelogo')) }}" alt="{{ Config::get('settings.sitename') }}" style="padding-bottom:8px;" >       
            </a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                <li><a href="{{ url('/') }}">{{ trans('mainnavigation.menuhome') }}</a></li>
                <li><a href="{{ url('/about') }}">{{ trans('mainnavigation.menuabout') }}</a></li>
                <li><a href="{{ url('/invest') }}">{{ trans('mainnavigation.menuinvest') }}</a></li>               
                <li><a href="{{ url('/contact') }}">{{ trans('mainnavigation.menucontact') }}</a></li>
                @if (Auth::guest())
                <li><a href="{{ url('/login') }}">{{ trans('mainnavigation.menulogin') }}</a></li>
                <li><a href="{{ url('/register') }}">{{ trans('mainnavigation.menuregister') }}</a></li>
                @else
                <li><a href="{{ url('myaccount/home') }}">{{ trans('mainnavigation.menumyaccount') }}</a></li>
                @if (session('usergroupid') == 4)
                <li class="dropdown">
                    @include('partials.notifications')
                </li>
                @endif
                <li class="dropdown">                   
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        @if (session('usergroupid') == 4)
                            <li>
                                <a href="{{ url('myaccount/profile/validate2fa') }}" >{{ trans('mainnavigation.menueditprofile') }}
                                </a>                            
                            </li>
                             <li>
                                <a href="{{ url('myaccount/change_password') }}" >{{ trans('mainnavigation.menuchangepassword') }}
                                </a>                            
                            </li>
                            <li>
                                <a href="{{ url('myaccount/transaction_password') }}" >{{ trans('mainnavigation.menutransactionpassword') }}
                                </a>
                               
                            </li>
                            <li>
                                <a href="{{ url('myaccount/changeavatar') }}" >{{ trans('mainnavigation.menuechangeavatar') }}
                                </a>
                               
                            </li>
                            <li>
                                <a href="{{ url('myaccount/twofactor') }}">{{ trans('mainnavigation.two_factor') }}</a>
                            </li>
                            @if($auto_withdrawal_status == 1)
                            <li>
                                <a href="{{ url('myaccount/autowithdrawal') }}" >{{ trans('mainnavigation.menuautowithdrawal') }}
                                </a>
                               
                            </li>
                            @endif
                            @if(Auth::user()->isImpersonating())
                             <li>
                                <a href="{{ url('users/stop') }}" >{{ trans('mainnavigation.stop_impersonate') }}
                                </a>                            
                            </li>
                            @endif
                        @else
                        @if (session('usergroupid') == 3)
                             <li>
                                <a href="{{ url('staff/changepassword') }}" >{{ trans('mainnavigation.menuchangepassword') }}
                                </a>                            
                            </li>
                        @endif
                        @endif
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        {{ trans('mainnavigation.menulogout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(Session::get('languageslist')->count() > 1)
                    <li>
                        <select name="language_switch" id="language_switch" style="margin: 6px 0" class="form-control">
                        @foreach (Session::get('languageslist') as $lang)
                        <option value="{{ url('/setlocale/'.$lang->abbr) }}" {{ Session::get('locale') == $lang->abbr ? 'selected' : '' }}>{{ $lang->name }}</option>
                        @endforeach
                    </select>
                    </li>
                    @endif
            </ul>
        </div>
    </div>
</nav>

@push('bottomscripts')
<script>
        jQuery(document).ready(function($) {
            $("#language_switch").change(function() {
                window.location.href = $(this).val();
            })
        });
</script>
@endpush
