<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="keywords" content="{{ $pagedetails->seokeyword }}">

    <meta name="description" content="{{ $pagedetails->seodescription }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $pagedetails->seotitle }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Scripts -->
<script>
    window.Laravel = {!!json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
</script>
</head>
<body>
    <div id="app">
        @include('layouts.sitelinks')
            <div class="push-me-top">
            <div class="row-fluid">
                @yield('pagesbanner')
            </div>
            </div>
        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
        @include('layouts.footer')
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
     {!! Config::get('settings.footerscript') !!}
 @include('layouts.footercss') 
</body>
</html>
