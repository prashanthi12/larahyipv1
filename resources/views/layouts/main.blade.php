<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ Config::get('settings.sitetitle') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ Config::get('settings.favicon') }}" type="image/x-icon">
    <!-- Scripts -->
    <meta name="google-site-verification" content="DaShug4PjaLEphOGLfHEpFk9azcNYVIEiyITIB3R7bc" />
<script>
    window.Laravel = {!!json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
</script>
{!! Config::get('settings.headerscript') !!}
</head>
<body>
    <div id="app">
        @include('layouts.sitelinks')
            <div class="push-me-top">
            <div class="row-fluid">
              @include('layouts.message')
                @yield('banner')
            </div>
            </div>
            <div class="main-layout">

                @yield('content')
            </div>
        @include('layouts.footer')
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    {!! Config::get('settings.google_analytics_code') !!}
    @stack('customscripts')
     <script src='https://www.google.com/recaptcha/api.js'></script>
     @stack('bottomscripts')
     {!! Config::get('settings.footerscript') !!}
 @include('layouts.footercss')    
</body>
</html>
