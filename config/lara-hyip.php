<?php

defined('USER_ROLE_SUPERADMIN') or define('USER_ROLE_SUPERADMIN', 1);
defined('USER_ROLE_ADMIN') or define('USER_ROLE_ADMIN', 2);
defined('USER_ROLE_STAFF') or define('USER_ROLE_STAFF', 3);
defined('USER_ROLE_MEMBER') or define('USER_ROLE_MEMBER', 4);
defined('USER_ROLE_SYSTEM') or define('USER_ROLE_SYSTEM', 5);

defined('HOME_SUPERADMIN') or define('HOME_SUPERADMIN', '/superadmin/dashboard');
defined('HOME_ADMIN') or define('HOME_ADMIN', '/admin/dashboard');
defined('HOME_STAFF') or define('HOME_STAFF', '/staff/dashboard');
defined('HOME_MEMBER') or define('HOME_MEMBER', '/');
// define('USER_ROLE_ACCOUNT', '2');

defined('PLANTYPE_DAILY') or define('PLANTYPE_DAILY', 1);
defined('PLANTYPE_DAILY_BUSINESS') or define('PLANTYPE_DAILY_BUSINESS', 2);
defined('PLANTYPE_HOURLY') or define('PLANTYPE_HOURLY', 3);
defined('PLANTYPE_WEEKLY') or define('PLANTYPE_WEEKLY', 4);
defined('PLANTYPE_MONTHLY') or define('PLANTYPE_MONTHLY', 5);
defined('PLANTYPE_QUATERLY') or define('PLANTYPE_QUATERLY', 6);
defined('PLANTYPE_YEARLY') or define('PLANTYPE_YEARLY', 7);

defined('URL_ADMIN_VIEW_DEPOSIT') or define('URL_ADMIN_VIEW_DEPOSIT', '/admin/deposit/');

defined('VAL_ACTIVE_INT') or define('VAL_ACTIVE_INT', 1);

defined('TBL_USERS') or define('TBL_USERS', 'users');
defined('TBL_USERPROFILES') or define('TBL_USERPROFILES', 'userprofiles');

defined('MAX_LEVELS_FOR_LEVEL_COMMISSION') or define('MAX_LEVELS_FOR_LEVEL_COMMISSION', 12);

// defined('SLACK_WEBHOOK_URL') or define('SLACK_WEBHOOK_URL', 'https://hooks.slack.com/services/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');